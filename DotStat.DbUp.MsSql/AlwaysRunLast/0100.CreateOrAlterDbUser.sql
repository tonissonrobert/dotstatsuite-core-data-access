﻿-- Login (server principal) and user (database principal) creation/alteration has been split into 2 separate scripts in order 
-- to make DbUp tool compatible with Azure SQL Database. On SQL Database it is not supported to switch between databases
-- nor referencing other database than the one defined in the conenction string.
IF NOT EXISTS (SELECT 1 FROM sys.database_principals WHERE TYPE = 'S' AND name = '$loginName$')
BEGIN
    CREATE USER [$loginName$] FOR LOGIN [$loginName$];

    ALTER ROLE [DotStatWriter] ADD MEMBER [$loginName$];
END
