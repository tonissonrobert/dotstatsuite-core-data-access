SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
 
DECLARE 	
	@DSD_ID int,
	@MSD_ID int,
	@SDMX_ID nvarchar(1000),

	@msg nvarchar(1000),

	@table_version char(1),	
  @NewLineChar AS CHAR(2) =  CHAR(10),
	@U_DSD_VERSION_ID varchar(20),
	@Transaction_ID int
	
DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY, MSD_ID int, SDMX_ID nvarchar(1000));

DECLARE 
	@VERSIONS TABLE ([version] char(1));

INSERT into @DSD_IDS 
	SELECT ART_ID, MSD_ID, [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')' 
	FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' AND MSD_ID IS NOT NULL ORDER BY ART_ID
	
--------------------------------------------------------
--Create new transaction for DSD
SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		
INSERT 
	INTO [management].[DSD_TRANSACTION]([TRANSACTION_ID], [ART_ID], [TABLE_VERSION], [EXECUTION_START], [EXECUTION_END], [SUCCESSFUL], [ARTEFACT_FULL_ID], [USER_EMAIL])
	VALUES (@transaction_ID, -1, 'X', GETDATE(), GETDATE(), 0, 'N/A', NULL)
--------------------------------------------------------
	
WHILE (Select Count(*) FROM @DSD_IDS) > 0
BEGIN

  SELECT TOP 1 @DSD_ID = ART_ID, @MSD_ID = MSD_ID, @SDMX_ID=SDMX_ID FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID
	
--------------------------------------------------------
	DELETE @VERSIONS
	INSERT into @VERSIONS SELECT 'A' union all SELECT 'B'
--------------------------------------------------------

	BEGIN TRY
		WHILE (Select Count(*) FROM @VERSIONS) > 0
		BEGIN
			-- select next version
			SELECT top 1 @table_version = [version] from @VERSIONS
			DELETE FROM @VERSIONS where [version]=@table_version

			SET @U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;

			BEGIN TRAN
			
				-- recreate metadata df views
				exec dbo.CREATE_MSD_DF_VIEWS @DSD_ID, @MSD_ID, @table_version; 

			COMMIT TRAN
		END		
	END TRY  
	BEGIN CATCH 
		IF (XACT_STATE()) <> 0 ROLLBACK TRAN
						
		SELECT @msg= 'The following error was found while trying to migrate MSD DF VIEW ['+ @SDMX_ID +'] (' + @U_DSD_VERSION_ID + '):'
			+ @NewLineChar + 'ErrorLine:' + CAST(ERROR_LINE() AS VARCHAR)
			+ @NewLineChar + 'ErrorNumber:' + CAST(ERROR_NUMBER() AS VARCHAR)
			+ @NewLineChar + 'ErrorMessage:' + ERROR_MESSAGE()+@NewLineChar 
		
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2023-04-25-01.sp_CREATE_MSD_DF_VIEW.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
				
	END CATCH

END

--update transaction info
UPDATE  [management].[DSD_TRANSACTION] SET [EXECUTION_END] = GETDATE(), [SUCCESSFUL] = 1
WHERE [TRANSACTION_ID] = @Transaction_ID