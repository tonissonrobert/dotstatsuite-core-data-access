--Create role if it does not exist yet
IF NOT EXISTS(SELECT 1 FROM sys.database_principals WHERE TYPE = 'R' AND name = 'DotStatReader')
BEGIN
   CREATE ROLE [DotStatReader];
END

--Add role to datareader and datawriter roles
ALTER ROLE [db_datareader] ADD MEMBER [DotStatReader];