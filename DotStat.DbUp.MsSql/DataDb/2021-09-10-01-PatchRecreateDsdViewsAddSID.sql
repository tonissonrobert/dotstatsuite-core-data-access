﻿SET NOCOUNT ON;
SET CONCAT_NULL_YIELDS_NULL ON;
GO


--CLEAN TEMP VIEWS
IF OBJECT_ID(N'tempdb..#Temp_DSD_Components') IS NOT NULL
BEGIN
DROP TABLE #Temp_DSD_Components
END
IF OBJECT_ID(N'tempdb..#Temp_DSDs') IS NOT NULL
BEGIN
DROP TABLE #Temp_DSDs
END
IF OBJECT_ID(N'tempdb..#DimGroupAttributes') IS NOT NULL
BEGIN
DROP TABLE #DimGroupAttributes
END


 SELECT ART_ID, ID,[DSD_LIVE_VERSION],[DSD_PIT_VERSION]
 INTO #Temp_DSDs
 FROM [management].[ARTEFACT] 
 WHERE [type] = 'DSD'
  
DECLARE @DSD_ID int,
	@DSD NVARCHAR(MAX),
	@isLiveSet as bit,
	@isPITset as bit,
	@msg NVARCHAR(MAX),
	@Transaction_ID int,
    @sql NVARCHAR(MAX),
    @viewA NVARCHAR(MAX),
    @viewB NVARCHAR(MAX),
	@SelectPart NVARCHAR(MAX),
	@FromPart NVARCHAR(MAX),
	@JoinPart NVARCHAR(MAX),
	@DimGroupAttr int,
	@HasTimeDim as bit,
    @NewLineChar AS CHAR(2) =  CHAR(10);

WHILE (Select Count(*) FROM #Temp_DSDs) > 0
BEGIN
	SELECT @isPITset=0
	SELECT @isLiveSet=0

    SELECT TOP 1 @DSD_ID = ART_ID, 
	@isLiveSet = CASE WHEN [DSD_LIVE_VERSION] IS NULL THEN 0 ELSE 1 END,
	@isPITset = CASE WHEN [DSD_PIT_VERSION] IS NULL THEN 0 ELSE 1 END
	FROM #Temp_DSDs

    DELETE #Temp_DSDs WHERE ART_ID = @DSD_ID

	--- check if fact table exists
	IF (NOT EXISTS (SELECT 1 
				FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'data' 
                 AND  TABLE_NAME = 'FACT_'+CAST(@DSD_ID AS VARCHAR)+'_A'))
	BEGIN
		-- SKIP when no fact table has been created and no [DSD_LIVE_VERSION], nor [DSD_PIT_VERSION] has been set
		IF( @isLiveSet = 0 AND @isPITset = 0)
		BEGIN
			CONTINUE
		END

		--ERROR no existing fact table, but a value for [DSD_LIVE_VERSION] or [DSD_PIT_VERSION] has been set
		SELECT @DSD  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'

		--Create new transaction for DSD
		SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		INSERT INTO [management].[DSD_TRANSACTION] 
		([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[USER_EMAIL])
		VALUES (@Transaction_ID, @DSD_ID, 'X',GETDATE(),GETDATE(),0,NULL)
		
		SELECT @msg= 'The following error was found while trying to recreate the DSD Views for the DSD ['+ @DSD +'] (The creation of the view will be skiped): The table [data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_A] does not exist in the database, but a value has been set for the fields [DSD_LIVE_VERSION] or [DSD_PIT_VERSION] in the [management].[ARTEFACT] for the given DSD.'+ @NewLineChar 
	
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2021-09-10-01-PatchRecreateDsdViewsAddSID.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
		
		--update transaction info
		UPDATE  [management].[DSD_TRANSACTION] 
		SET [EXECUTION_END]= GETDATE(), [SUCCESSFUL] = 1
		WHERE [TRANSACTION_ID]=@Transaction_ID

		CONTINUE
	END
	
	BEGIN TRY 
		BEGIN TRAN
	
			SELECT c.[DSD_ID], c.[ID], c.[CL_ID], c.COMP_ID,c.[TYPE], c.[ATT_ASS_LEVEL], c.[ATT_STATUS]
			INTO #Temp_DSD_Components
			FROM [management].[ARTEFACT] a 
			INNER JOIN [management].[COMPONENT] c 
			ON a.ART_ID = c.DSD_ID
			WHERE a.ART_ID=@DSD_ID AND ISNULL(c.[ATT_ASS_LEVEL], '' )<>'DataSet'

			SELECT @HasTimeDim = 0
			IF EXISTS(SELECT 1 FROM sys.columns 
				  WHERE Name = N'PERIOD_SDMX'
				  AND Object_ID = Object_ID(N'[data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_A]'))
			BEGIN
				SELECT @HasTimeDim=1
			END
	
----------------------------START BUILD SELECT STATEMENT---------------------------
			SELECT @SelectPart= 'SELECT [FI].[SID],[FA].[VALUE] AS [OBS_VALUE]'
			--check if dsd has time dimension
			IF (@HasTimeDim = 1)
			BEGIN
				SELECT @SelectPart+= ',[FA].[PERIOD_SDMX] AS [TIME_PERIOD],[FA].[PERIOD_START],[FA].[PERIOD_END]'
			END
	
			SELECT @SelectPart+=ISNULL(STUFF(( 
				SELECT ','+
				  CASE 
				  --coded dimensions and coded attributes
					WHEN C.[CL_ID] IS NOT NULL 
						THEN '[CL_'+[C].[ID]+'].[ID] AS ['+[C].[ID]+']'+@NewLineChar
				  --Non-coded dimensions
				   WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'Dimension'
						THEN '[FI].[DIM_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+']'
				  --Non-coded attributes at observation level
				   WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'Observation'
						THEN '[FA].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+']'
				  --Non-coded attributes at dimgroup level
				   WHEN C.[TYPE] = 'Attribute' AND (C.[ATT_ASS_LEVEL] = 'DimensionGroup' OR C.[ATT_ASS_LEVEL] = 'Group')
						THEN '[ATTR].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+']'
					END
				FROM #Temp_DSD_Components C  FOR XML PATH('')
			), 1, 0, ''), '')
		
		--------------------------------END BUILD SELECT STATEMENT-------------------------------

	
		--------------------------------START BUILD FROM STATEMENT-------------------------------
			SELECT @FromPart='FROM [data].[FILT_'+CAST(@DSD_ID AS VARCHAR)+'] FI'+@NewLineChar+'INNER JOIN [data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_{V}] FA ON FI.[SID] = FA.[SID]'+@NewLineChar
		--------------------------------END BUILD FROM STATEMENT-------------------------------------
	
		--------------------------------START JOIN FROM STATEMENT------------------------------------
			SELECT @JoinPart=ISNULL(STUFF(( 
				SELECT 
				  CASE 
				  --coded dimensions
					WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Dimension'
						THEN 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON FI.[DIM_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
				  --coded attributes at observation level
					WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'Observation' 
						THEN 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON FA.[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
				  END
				FROM #Temp_DSD_Components C FOR XML PATH('')
			), 1, 0, ''),'') 

			--DIMGROUP lvl attributes	
			SELECT *
			INTO #DimGroupAttributes
			FROM #Temp_DSD_Components 
			WHERE DSD_ID = @DSD_ID AND (ATT_ASS_LEVEL ='DimensionGroup' OR [ATT_ASS_LEVEL] = 'Group')

			IF((Select Count(*) FROM #DimGroupAttributes) > 0) 
			BEGIN
				SELECT @JoinPart+= 'LEFT JOIN [data].[ATTR_'+CAST(@DSD_ID AS VARCHAR)+'_{V}] AS [ATTR] ON FI.[SID] = ATTR.[SID]'+@NewLineChar 
				SELECT @JoinPart+= ISNULL(STUFF(( 
					SELECT 
					  CASE 
					  --coded attributes
					  WHEN C.[CL_ID] IS NOT NULL 
							THEN 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON ATTR.[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
					  END
					FROM #DimGroupAttributes C  FOR XML PATH('')
				), 1, 0, ''), '')
			END
------------------------------------END JOIN FROM STATEMENT---------------------------------------

			--DELETE VIEW A IF EXISTS
			SET @viewA ='[data].[VI_CurrentDataDsd_'+ CAST( @DSD_ID AS VARCHAR)+'_A]'
			IF OBJECT_ID(@viewA, 'V') IS NOT NULL
			BEGIN
				EXEC  ('DROP VIEW '+@viewA)
			END
	
			--DELETE VIEW B IF EXISTS
			SET @viewB ='[data].[VI_CurrentDataDsd_'+ CAST( @DSD_ID AS VARCHAR)+'_B]'
			IF OBJECT_ID(@viewB, 'V') IS NOT NULL
			BEGIN
				EXEC  ('DROP VIEW '+@viewB)
			END
			
			--CREATE THE VIEWS
			SELECT @sql = 'CREATE VIEW '+@viewA+' AS ' + @NewLineChar + REPLACE(@SelectPart + @NewLineChar + @FromPart + @JoinPart,'{V}','A');
			--print @sql
			EXEC sp_executesql @sql

			SELECT @sql = 'CREATE VIEW '+@viewB+' AS ' + @NewLineChar + REPLACE(@SelectPart + @NewLineChar + @FromPart + @JoinPart,'{V}','B');
			--print @sql
			EXEC sp_executesql @sql
			
			DROP TABLE #DimGroupAttributes
			DROP TABLE #Temp_DSD_Components
		COMMIT TRAN
	END TRY  
	BEGIN CATCH  
		
		ROLLBACK TRAN

		SELECT @DSD  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'

		--Create new transaction for DSD
		SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		INSERT INTO [management].[DSD_TRANSACTION] 
		([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[USER_EMAIL])
		VALUES (@Transaction_ID, @DSD_ID, 'X',GETDATE(),GETDATE(),0,NULL)
		
		SELECT @msg= 'The following error was found while trying to recreate the DSD Views for the DSD ['+ @DSD +'] (The creation of the view will be skiped):'+ ' ErrorNumber:'+CAST(ERROR_NUMBER() AS VARCHAR)+' ErrorMessage:'+ERROR_MESSAGE()+@NewLineChar 
	
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2021-09-10-01-PatchRecreateDsdViewsAddSID.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
		
		--update transaction info
		UPDATE  [management].[DSD_TRANSACTION] 
		SET [EXECUTION_END]= GETDATE(), [SUCCESSFUL] = 1
		WHERE [TRANSACTION_ID]=@Transaction_ID

	END CATCH
END

DROP TABLE #Temp_DSDs


