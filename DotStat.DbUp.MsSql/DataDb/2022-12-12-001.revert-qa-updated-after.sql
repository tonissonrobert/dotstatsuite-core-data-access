SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
 
if not exists(select 1 from dbo.SchemaVersions where scriptname like  'DotStat.DbUp.MsSql.DataDb.%.updated-after-feature.sql')
	return;

------------------------------------------------------------------------

DECLARE 	
	@DSD_ID int,
	@SDMX_ID nvarchar(1000),

	@sql_migration NVARCHAR(MAX),	
	@msg nvarchar(1000),

	@table_version char(1),	
    @NewLineChar AS CHAR(2) =  CHAR(10),
	@U_DSD_VERSION_ID varchar(20),
	@Transaction_ID int,
	@HAS_TIME_DIM bit;
	
DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY, SDMX_ID nvarchar(1000));

DECLARE 
	@VERSIONS TABLE ([version] char(1));

INSERT into @DSD_IDS 
	SELECT ART_ID, [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')' 
	FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' ORDER BY ART_ID

--------------------------------------------------------
--Create new transaction for DSD
SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		
INSERT 
	INTO [management].[DSD_TRANSACTION]([TRANSACTION_ID], [ART_ID], [TABLE_VERSION], [EXECUTION_START], [EXECUTION_END], [SUCCESSFUL], [ARTEFACT_FULL_ID], [USER_EMAIL])
	VALUES (@transaction_ID, -1, 'X', GETDATE(), GETDATE(), 0, 'N/A', NULL)
--------------------------------------------------------
	
WHILE (Select Count(*) FROM @DSD_IDS) > 0
BEGIN

    SELECT TOP 1 @DSD_ID=ART_ID, @SDMX_ID=SDMX_ID FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID
	
	print 'Migrating DSD: ' + @SDMX_ID + ' ,ID:' + CAST(@DSD_ID AS VARCHAR)

--------------------------------------------------------
	DELETE @VERSIONS
	INSERT into @VERSIONS SELECT 'A' union all SELECT 'B'
--------------------------------------------------------

	BEGIN TRY
		WHILE (Select Count(*) FROM @VERSIONS) > 0
		BEGIN
			-- select next version
			SELECT top 1 @table_version = [version] from @VERSIONS
			DELETE FROM @VERSIONS where [version]=@table_version

			SET @U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;

			IF NOT EXISTS(SELECT 1 FROM sys.columns WHERE [Name] = N'LAST_STATUS' AND Object_ID = Object_ID(N'data.FACT_' + @U_DSD_VERSION_ID))
			BEGIN
				print @U_DSD_VERSION_ID + ' already migrated, skipped';
				CONTINUE
			END
			
			BEGIN TRAN

			-- CHECK IF TIME DIM EXISTS
			SELECT @HAS_TIME_DIM = count(*) FROM sys.columns WHERE [Name] = N'PERIOD_SDMX' AND Object_ID = Object_ID(N'data.FACT_' + @U_DSD_VERSION_ID)

			-- drop constraints
			set @sql_migration = 'drop index I_CLUSTERED_DSD_{U_DSD_VERSION_ID} ON [data].FACT_{U_DSD_VERSION_ID};'
			set @sql_migration += 'alter table [data].FACT_{U_DSD_VERSION_ID} drop constraint DF_FACT_{U_DSD_VERSION_ID}_LAST_STATUS;'
			set @sql_migration += 'alter table [data].FACT_{U_DSD_VERSION_ID} drop constraint PK_DSD_{U_DSD_VERSION_ID};'
			set @sql_migration = REPLACE(@sql_migration, '{U_DSD_VERSION_ID}', @U_DSD_VERSION_ID)
			exec sp_executesql @sql_migration;
			
			-- recreate indexes
			set @sql_migration = 'alter table [data].FACT_{U_DSD_VERSION_ID} add constraint PK_DSD_{U_DSD_VERSION_ID} PRIMARY KEY CLUSTERED([SID] {TIME_COLUMMNS});'
			set @sql_migration = REPLACE(@sql_migration, '{U_DSD_VERSION_ID}', @U_DSD_VERSION_ID)
			set @sql_migration = REPLACE(@sql_migration, '{TIME_COLUMMNS}', case @HAS_TIME_DIM when 1 then ',[PERIOD_START],[PERIOD_END] DESC' else '' end)
			exec sp_executesql @sql_migration;

			-- drop LAST_STATUS column
			set @sql_migration = 'ALTER table data.FACT_{U_DSD_VERSION_ID} drop column [LAST_STATUS]';
			set @sql_migration = REPLACE(@sql_migration, '{U_DSD_VERSION_ID}', @U_DSD_VERSION_ID)		
			exec sp_executesql @sql_migration;
			
			COMMIT TRAN
		END		
	END TRY  
	BEGIN CATCH 
		IF (XACT_STATE()) <> 0 ROLLBACK TRAN
						
		SELECT @msg= 'The following error was found while trying to migrate DSD ['+ @SDMX_ID +'] [' + @U_DSD_VERSION_ID + ']:'
			+ @NewLineChar + 'ErrorLine:' + CAST(ERROR_LINE() AS VARCHAR)
			+ @NewLineChar + 'ErrorNumber:' + CAST(ERROR_NUMBER() AS VARCHAR)
			+ @NewLineChar + 'ErrorMessage:' + ERROR_MESSAGE()+@NewLineChar 
		
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2022-12-12-001.revert-qa-updated-after.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
				
	END CATCH

END

--update transaction info
UPDATE  [management].[DSD_TRANSACTION] SET [EXECUTION_END] = GETDATE(), [SUCCESSFUL] = 1
WHERE [TRANSACTION_ID] = @Transaction_ID