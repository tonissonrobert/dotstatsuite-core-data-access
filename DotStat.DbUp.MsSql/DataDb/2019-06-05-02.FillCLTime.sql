DECLARE 
    @YearMin int = 1900,
    @YearMax int = 2080,
    @CurrentYear int,
    @CurrentYearItemId int;

SET @CurrentYear = @YearMin

TRUNCATE TABLE [Management].[CL_TIME]

WHILE @CurrentYear <= @YearMax 
BEGIN
    DECLARE @YearStart datetime = CONVERT(DATETIME, DATEFROMPARTS(@CurrentYear, 1, 1));
    DECLARE @YearEnd datetime = DATEADD(ms, -2, CONVERT(DATETIME, DATEFROMPARTS(@CurrentYear + 1, 1, 1)));
    DECLARE @YearId varchar(50) = CONVERT(VARCHAR, @CurrentYear);

    --Insert year
    INSERT INTO [Management].[CL_TIME] ([ITEM_PARENT_ID], [ID], [START_PERIOD], [END_PERIOD])
        VALUES (IDENT_CURRENT('[Management].[CL_TIME]'), @YearId, @YearStart, @YearEnd);

    SET @CurrentYearItemId = SCOPE_IDENTITY();

    DECLARE @CurrentMonth int = 1;
    DECLARE @CurrentSemesterItemId int = -1;
    DECLARE @CurrentQuarterItemId int= -1;
    DECLARE @CurrentMonthItemId int = -1;

    WHILE @CurrentMonth <= 12
    BEGIN
        IF (@CurrentMonth % 6 = 1)
        BEGIN
            DECLARE @Semester int = @CurrentMonth / 6 + 1;
            DECLARE @SemesterStart datetime = CONVERT(DATETIME, DATEFROMPARTS(@CurrentYear, (@Semester - 1) * 6 + 1, 1));
            DECLARE @SemesterEnd datetime = DATEADD(ms, -2, DATEADD(m, 6, @SemesterStart));
            DECLARE @SemesterId varchar(50) = CONVERT(VARCHAR, @CurrentYear) + '-B' + CONVERT(VARCHAR, @Semester);

            --Insert semester
            INSERT INTO [Management].[CL_TIME] ([ITEM_PARENT_ID], [ID], [START_PERIOD], [END_PERIOD])
                VALUES (@CurrentYearItemId, @SemesterId, @SemesterStart, @SemesterEnd);

            SET @CurrentSemesterItemId = SCOPE_IDENTITY();
        END

        IF (@CurrentMonth % 3 = 1)  
        BEGIN
            DECLARE @Quarter int = @CurrentMonth / 3 + 1;
            DECLARE @QuarterStart datetime = CONVERT(DATETIME, DATEFROMPARTS(@CurrentYear, (@Quarter - 1) * 3 + 1, 1));
            DECLARE @QuarterEnd datetime = DATEADD(ms, -2, DATEADD(m, 3, @QuarterStart));
            DECLARE @QuarterId varchar(50) = CONVERT(VARCHAR, @CurrentYear) + '-Q' + CONVERT(VARCHAR, @Quarter);

            --Insert quarter
            INSERT INTO [Management].[CL_TIME] ([ITEM_PARENT_ID], [ID], [START_PERIOD], [END_PERIOD])
                VALUES (@CurrentSemesterItemId, @QuarterId, @QuarterStart, @QuarterEnd);

            SET @CurrentQuarterItemId = SCOPE_IDENTITY();
        END

        DECLARE @MonthStart datetime = CONVERT(DATETIME, DATEFROMPARTS(@CurrentYear, @CurrentMonth, 1));
        DECLARE @MonthEnd datetime = DATEADD(ms, -2, DATEADD(m, 1, @MonthStart));
        DECLARE @MonthId varchar(50) = CONVERT(VARCHAR, @CurrentYear) + '-' + FORMAT(@CurrentMonth, '00');

        --Insert month
        INSERT INTO [Management].[CL_TIME] ([ITEM_PARENT_ID], [ID], [START_PERIOD], [END_PERIOD])
            VALUES (@CurrentQuarterItemId, @MonthId, @MonthStart, @MonthEnd);

        SET @CurrentMonthItemId = SCOPE_IDENTITY();

        --Days ?

        SET @CurrentMonth = @CurrentMonth + 1
    END

    SET @CurrentYear = @CurrentYear + 1;
END