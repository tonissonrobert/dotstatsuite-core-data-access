CREATE OR ALTER PROC CREATE_DF_VIEWS_REPLACE
	@DSD_ID int,
	@table_version char(1)
AS
BEGIN
	DECLARE
		@sdmxId as NVARCHAR(1000),
		@U_DSD_VERSION_ID varchar(20),
		@NewLineChar AS CHAR(2) =  CHAR(10),
		@isLiveSet as bit,
		@isPITset as bit,			
		@HasTimeDim as bit = 0,
		@TimeDimId as int = 0,
		@msg NVARCHAR(MAX),
		@sql NVARCHAR(MAX),
		@dimensionsJoin NVARCHAR(MAX) ='',
		@dimensionsSelect NVARCHAR(MAX) ='',
		@dimensionsSelectNull NVARCHAR(MAX) ='',
		@timeDimSelect NVARCHAR(MAX) ='',
		@timeDimSelectNull NVARCHAR(MAX) ='',
		@measureColumnSelect NVARCHAR(MAX) ='',
		@measureColumnSelectNull NVARCHAR(MAX) ='',
		@obsLvlAttrSelect NVARCHAR(MAX) ='',
		@obsLvlAttrSelectNull NVARCHAR(MAX) ='',
		@obsLvlAttrJoin NVARCHAR(MAX) ='',
		@dimGroupLvlAttrSelect NVARCHAR(MAX) ='',
		@dimGroupLvlAttrSelectNull NVARCHAR(MAX) ='',
		@dimGroupLvlAttrJoin NVARCHAR(MAX) ='',
		@hasdimGroupLevelAttributes as int = 0,
		@dataSetAttrLvlSelect NVARCHAR(MAX) ='',
		@dataSetAttrLvlSelectNull NVARCHAR(MAX) ='',
		@dataSetAttrLvlJoin NVARCHAR(MAX) ='',
		@hasDataSetLevelAttributes as int = 0,
		@ctePart NVARCHAR(MAX) ='',
		@ctePartObs NVARCHAR(MAX) ='',
		@ctePartDimGroup NVARCHAR(MAX) ='',
		@ctePartDataSet NVARCHAR(MAX) ='',
		@subQueryPart NVARCHAR(MAX) ='',
		@subQueryPartObs NVARCHAR(MAX) ='',
		@subQueryPartDimGroup NVARCHAR(MAX) ='',
		@subQueryPartDataSet NVARCHAR(MAX) ='',
		@DF_ID int;

	DECLARE
		@DSD_Components TABLE(
			COMP_ID int not null,
			ID varchar(50) not null, 
			[TYPE] varchar(50) not null, 	
			CL_ID int, 
			ATT_ASS_LEVEL varchar(50),
			ATT_STATUS varchar(11),
			IS_TIME_DIM_REFERENCED bit not null
		)

	DECLARE
		@DF_IDS TABLE(ART_ID int PRIMARY KEY, SDMX_ID nvarchar(1000));

    SELECT
		@U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version,
		@sdmxId = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')',
		@isLiveSet = CASE WHEN [DSD_LIVE_VERSION] IS NULL THEN 0 ELSE 1 END,
		@isPITset = CASE WHEN [DSD_PIT_VERSION] IS NULL THEN 0 ELSE 1 END	
	FROM [management].[ARTEFACT] 		
	WHERE [type] = 'DSD' AND ART_ID = @DSD_ID;

	--- check if fact table exists
	IF (NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'data' AND  TABLE_NAME = 'FACT_' + @U_DSD_VERSION_ID))
	BEGIN
		-- SKIP when no fact table has been created and no [DSD_LIVE_VERSION], nor [DSD_PIT_VERSION] has been set
		IF( @isLiveSet = 0 AND @isPITset = 0)
		BEGIN
			RETURN	
		END

		set @msg = 'The following error was found while trying to recreate the DSD Views for the DSD ['+ @sdmxId +'] (The creation of the view will be skiped): The table [data].[FACT_'+ @U_DSD_VERSION_ID + '] does not exist in the database, but a value has been set for the fields [DSD_LIVE_VERSION] or [DSD_PIT_VERSION] in the [management].[ARTEFACT] for the given DSD.';

		throw 51000, @msg, 1;
	END

	-- retrieve IF DSD has TIME dimension
	SELECT @HasTimeDim=1, @TimeDimId = COMP_ID 
	FROM [management].[COMPONENT] WHERE [TYPE] ='TimeDimension' AND DSD_ID = @DSD_ID 
	
	---------------------------- COMPONENTS ---------------------------
	INSERT INTO @DSD_Components
	SELECT c.COMP_ID,c.ID,c.[TYPE],c.[CL_ID],c.[ATT_ASS_LEVEL], c.[ATT_STATUS], CASE WHEN RT.ATTR_ID IS NULL THEN 0 ELSE 1 END
	FROM [management].[COMPONENT] c 	
		LEFT JOIN (SELECT [ATTR_ID] FROM [management].[ATTR_DIM_SET] WHERE DIM_ID = @TimeDimId) AS RT ON C.COMP_ID = RT.ATTR_ID
	WHERE c.DSD_ID=@DSD_ID and [Type] in ('Dimension','Attribute') 
	
	SET @dimensionsSelect = ISNULL(STUFF(( 
		SELECT 
			CASE 
			--coded dimensions 
			WHEN C.[CL_ID] IS NOT NULL 
				THEN ' [CL_'+[C].[ID]+'].[ID] AS ['+[C].[ID]+'],'
			--Non-coded dimensions
			WHEN C.[CL_ID] IS NULL 
				THEN ' [FI].[DIM_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+'],'
			END
		FROM @DSD_Components C WHERE C.[TYPE] = 'Dimension' FOR XML PATH('')
	), 1, 0, ''),'x') 
	
	SET @dimensionsSelectNull = ISNULL(STUFF(( 
		SELECT ' NULL AS ['+[C].[ID]+'],'
		FROM @DSD_Components C WHERE C.[TYPE] = 'Dimension' FOR XML PATH('')
	), 1, 0, ''),'') 

	SET @dimensionsJoin = ISNULL(STUFF(( 
		SELECT  'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON FI.[DIM_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
		FROM @DSD_Components C WHERE C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Dimension' FOR XML PATH('')
	), 1, 0, ''),'') 

	--Time Dimension
	IF(@HasTimeDim = 1)
	BEGIN
		SET @timeDimSelect =' [FA].[PERIOD_SDMX] AS [TIME_PERIOD], [FA].[PERIOD_START], [FA].[PERIOD_END],' 
		SET @timeDimSelectNull =' NULL AS [TIME_PERIOD], NULL AS [PERIOD_START], NULL AS[PERIOD_END],' 
	END

	--Measure
	SET @measureColumnSelect = ' [FA].[VALUE] AS [OBS_VALUE],'
	SET @measureColumnSelectNull = ' NULL AS [OBS_VALUE],'

	--Attributes
	
	SELECT @obsLvlAttrSelect = ISNULL(STUFF(( 
		SELECT 
			CASE 
			--coded attributes at observation level
			WHEN C.[CL_ID] IS NOT NULL 
				THEN ' [CL_'+[C].[ID]+'].[ID] AS ['+[C].[ID]+'],'
			--Non-coded attributes at observation level
			WHEN C.[CL_ID] IS NULL 
				THEN ' [FA].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+'],'
			END
		FROM @DSD_Components C WHERE C.[TYPE] = 'Attribute' AND (C.[ATT_ASS_LEVEL] = 'Observation' OR C.[IS_TIME_DIM_REFERENCED] = 1) FOR XML PATH('')
	), 1, 0, ''), '')
	
	SELECT @obsLvlAttrSelectNull = ISNULL(STUFF(( 
		SELECT ' NULL AS ['+[C].[ID]+'],'
		FROM @DSD_Components C WHERE C.[TYPE] = 'Attribute' AND (C.[ATT_ASS_LEVEL] = 'Observation' OR C.[IS_TIME_DIM_REFERENCED] = 1) FOR XML PATH('')
	), 1, 0, ''), '')
	
	SELECT @obsLvlAttrJoin = ISNULL(STUFF(( 
		SELECT 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON FA.[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
		FROM @DSD_Components C WHERE C.[TYPE] = 'Attribute' AND (C.[ATT_ASS_LEVEL] = 'Observation' OR C.[IS_TIME_DIM_REFERENCED] = 1) AND C.[CL_ID] IS NOT NULL FOR XML PATH('')
	), 1, 0, ''), '')
	
    --dimGroup level
	SELECT @hasdimGroupLevelAttributes = 1
	WHERE EXISTS( SELECT TOP 1 1
	FROM @DSD_Components C WHERE C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] IN ('DimensionGroup','Group') AND C.[IS_TIME_DIM_REFERENCED] = 0
	)
	
	IF(@hasdimGroupLevelAttributes = 1)
	BEGIN
		SELECT @dimGroupLvlAttrSelect = ISNULL(STUFF(( 
			SELECT 
				CASE 
				--coded attributes at observation level
				WHEN C.[CL_ID] IS NOT NULL 
					THEN ' [CL_'+[C].[ID]+'].[ID] AS ['+[C].[ID]+'],'
				--Non-coded attributes at observation level
				WHEN C.[CL_ID] IS NULL 
					THEN ' [ATTR].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+'],'
				END
			FROM @DSD_Components C WHERE C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] IN ('DimensionGroup','Group') AND C.[IS_TIME_DIM_REFERENCED] = 0 FOR XML PATH('')
		), 1, 0, ''), '')
	
		SELECT @dimGroupLvlAttrSelectNull = ISNULL(STUFF(( 
			SELECT ' NULL AS ['+[C].[ID]+'],'
			FROM @DSD_Components C WHERE C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] IN ('DimensionGroup','Group') AND C.[IS_TIME_DIM_REFERENCED] = 0 FOR XML PATH('')
		), 1, 0, ''), '')
	
		SELECT @dimGroupLvlAttrJoin = ISNULL(STUFF(( 
			SELECT 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON ATTR.[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
			FROM @DSD_Components C WHERE C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] IN ('DimensionGroup','Group') AND C.[IS_TIME_DIM_REFERENCED] = 0 AND C.[CL_ID] IS NOT NULL FOR XML PATH('')
		), 1, 0, ''), '')
	END
	
    --dataSet level
	
	SELECT @hasDataSetLevelAttributes = 1
	WHERE EXISTS( SELECT TOP 1 1
	FROM @DSD_Components C WHERE C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'DataSet' AND C.[IS_TIME_DIM_REFERENCED] = 0
	)

	IF(@hasDataSetLevelAttributes = 1)
	BEGIN
		SELECT @dataSetAttrLvlSelect = ISNULL(STUFF(( 
			SELECT 
				CASE 
				--coded attributes at observation level
				WHEN C.[CL_ID] IS NOT NULL 
					THEN ' [CL_'+[C].[ID]+'].[ID] AS ['+[C].[ID]+'],'
				--Non-coded attributes at observation level
				WHEN C.[CL_ID] IS NULL 
					THEN '[ADF].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+'],'
				END
			FROM @DSD_Components C WHERE C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'DataSet' AND C.[IS_TIME_DIM_REFERENCED] = 0 FOR XML PATH('')
		), 1, 0, ''), '')
	
		SELECT @dataSetAttrLvlSelectNull = ISNULL(STUFF(( 
			SELECT ' NULL AS ['+[C].[ID]+'],'
			FROM @DSD_Components C WHERE C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL]  = 'DataSet' AND C.[IS_TIME_DIM_REFERENCED] = 0 FOR XML PATH('')
		), 1, 0, ''), '')
	
		SELECT @dataSetAttrLvlJoin = ISNULL(STUFF(( 
			SELECT 'LEFT JOIN [management].[CL_'+CAST(CL_ID AS VARCHAR)+'] AS [CL_'+[ID]+'] ON [ADF].[DF_ID]={DF_ID} AND ADF.COMP_'+CAST(COMP_ID AS VARCHAR)+' = [CL_'+[ID]+'].[ITEM_ID]' + @NewLineChar
			FROM @DSD_Components C WHERE C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'DataSet' AND C.[IS_TIME_DIM_REFERENCED] = 0 AND C.[CL_ID] IS NOT NULL FOR XML PATH('')
		), 1, 0, ''), '')
	END

	----------------------------START BUILD  STATEMENT---------------------------
	
	SELECT @ctePartObs= 'WITH OBS_LEVEL AS (' + @NewLineChar 
	+ 'SELECT FI.[SID], '+ @dimensionsSelect + @timeDimSelect + @NewLineChar 
	+ @measureColumnSelect + @NewLineChar 
	+ @obsLvlAttrSelect + @NewLineChar 
	+ @dimGroupLvlAttrSelectNull + @NewLineChar 
	+ @dataSetAttrLvlSelectNull + @NewLineChar 
	+ '[LAST_UPDATED]' + @NewLineChar 
	+ 'FROM [data].[FACT_'+ @U_DSD_VERSION_ID + '] FA' + @NewLineChar 
	+ 'LEFT JOIN [data].[FILT_'+CAST(@DSD_ID AS VARCHAR)+'] FI ON FI.[SID] = FA.[SID]' + @NewLineChar
	+ @dimensionsJoin + @NewLineChar 
	+ @obsLvlAttrJoin + @NewLineChar 
	+ ')' + @NewLineChar
	
	SELECT @subQueryPartObs= 'SELECT * FROM OBS_LEVEL' + @NewLineChar 
	
	IF(@hasdimGroupLevelAttributes = 1)
	BEGIN
		SELECT @ctePartDimGroup = ', SERIES_LEVEL AS (' + @NewLineChar 
		+ 'SELECT FI.[SID], '+ @dimensionsSelect + @timeDimSelectNull + @NewLineChar 
		+ @measureColumnSelectNull + @NewLineChar 
		+ @obsLvlAttrSelectNull + @NewLineChar 
		+ @dimGroupLvlAttrSelect + @NewLineChar 
		+ @dataSetAttrLvlSelectNull + @NewLineChar 
		+ '[LAST_UPDATED]' + @NewLineChar 
		+ 'FROM [data].[ATTR_'+ @U_DSD_VERSION_ID + '] ATTR' + @NewLineChar 
		+ 'LEFT JOIN [data].[FILT_'+CAST(@DSD_ID AS VARCHAR)+'] FI ON FI.[SID] = ATTR.[SID]' + @NewLineChar
		+ @dimensionsJoin + @NewLineChar 
		+ @dimGroupLvlAttrJoin + @NewLineChar 
		+ ')' + @NewLineChar
	
		SELECT @subQueryPartDimGroup = 'UNION ALL' + @NewLineChar + 'SELECT * FROM SERIES_LEVEL' + @NewLineChar 
	END
	
	IF(@hasDataSetLevelAttributes = 1)
	BEGIN
		SELECT @ctePartDataSet = ', DF_LEVEL AS (' + @NewLineChar 
		+ 'SELECT NULL AS [SID], '+ @dimensionsSelectNull + @timeDimSelectNull + @NewLineChar 
		+ @measureColumnSelectNull + @NewLineChar 
		+ @obsLvlAttrSelectNull + @NewLineChar 
		+ @dimGroupLvlAttrSelectNull + @NewLineChar 
		+ @dataSetAttrLvlSelect + @NewLineChar 
		+ '[LAST_UPDATED]' + @NewLineChar 
		+ 'FROM [data].[ATTR_'+ @U_DSD_VERSION_ID + '_DF] ADF' + @NewLineChar 
		+ @dataSetAttrLvlJoin + @NewLineChar 
		+ 'WHERE [ADF].[DF_ID] = {DF_ID}' + @NewLineChar
		+ ')' + @NewLineChar
	
		SELECT @subQueryPartDataSet = 'UNION ALL' + @NewLineChar + 'SELECT * FROM DF_LEVEL' + @NewLineChar 
	END
	
	--------------------------------------------------------------------------------------------
	
	INSERT into @DF_IDS 
	select ART_ID, [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')' 	
	from management.ARTEFACT where [TYPE]='DF' and DF_DSD_ID=@DSD_ID

	WHILE (Select Count(*) FROM @DF_IDS) > 0
	BEGIN
		SELECT TOP 1 @DF_ID = ART_ID, @sdmxId = SDMX_ID FROM @DF_IDS
		DELETE @DF_IDS WHERE ART_ID = @DF_ID
		
		print 'Recreating Data Replace VIEW(' + @table_version + ') for DF: ' + @sdmxId + ', ID:' + CAST(@DF_ID AS VARCHAR);

		SET @sql = REPLACE(
			'CREATE OR ALTER VIEW [data].[VI_CurrentDataReplaceDataFlow_{DF_ID}_' + @table_version + '] AS'  + @NewLineChar 
			+ @ctePartObs + @ctePartDimGroup+ @ctePartDataSet 
			+ @subQueryPartObs + @subQueryPartDimGroup + @subQueryPartDataSet,
			'{DF_ID}',
			CAST(@DF_ID AS VARCHAR)
		);
		
		--print @sql;
		exec sp_executesql @sql;
	END
END