﻿SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
  
DECLARE 	
	@DSD_ID int,	
	@sql_view NVARCHAR(MAX),
	@sql_where NVARCHAR(MAX),
	@table_version char(1),	
    @NewLineChar AS CHAR(2) =  CHAR(10),
	@U_DSD_VERSION_ID varchar(20),
	@Transaction_ID int;

DECLARE 
	@VERSIONS TABLE ([version] char(1));
	
DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY);

INSERT into @DSD_IDS 
	SELECT ART_ID FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' ORDER BY ART_ID

--------------------------------------------------------
--Create new transaction for DSD
SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		
INSERT 
	INTO [management].[DSD_TRANSACTION]([TRANSACTION_ID], [ART_ID], [TABLE_VERSION], [EXECUTION_START], [EXECUTION_END], [SUCCESSFUL], [ARTEFACT_FULL_ID], [USER_EMAIL])
	VALUES (@transaction_ID, -1, 'X', GETDATE(), GETDATE(), 0, 'N/A', NULL)
--------------------------------------------------------
	
WHILE (Select Count(*) FROM @DSD_IDS) > 0
BEGIN

    SELECT TOP 1 @DSD_ID = ART_ID FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID
	
	print 'Migrating DSD: ' + CAST(@DSD_ID AS VARCHAR)		
	
	SELECT @sql_where = 'WHERE [FA].[SID] IS NOT NULL';	
	IF EXISTS(
		SELECT TOP 1 1 
        FROM [management].[ARTEFACT] a 
		INNER JOIN [management].[COMPONENT] c 
		ON a.ART_ID = c.DSD_ID
		WHERE A.ART_ID=@DSD_ID AND C.[TYPE] = 'Attribute' AND (C.[ATT_ASS_LEVEL] = 'DimensionGroup' OR C.[ATT_ASS_LEVEL] = 'Group')
	)
	BEGIN
		
		SELECT @sql_where = @sql_where + ' OR [ATTR].[SID] IS NOT NULL';	
	END

--------------------------------------------------------
	DELETE @VERSIONS
	INSERT into @VERSIONS SELECT 'A' union all SELECT 'B'
--------------------------------------------------------

	BEGIN TRY
		WHILE (Select Count(*) FROM @VERSIONS) > 0
		BEGIN
		
			-- select next version
			SELECT top 1 @table_version = [version] from @VERSIONS
			DELETE FROM @VERSIONS where [version]=@table_version

			SET @U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version;

			-- Handle dsd view
			select @sql_view = v.VIEW_DEFINITION from INFORMATION_SCHEMA.VIEWS v where v.TABLE_NAME = 'VI_CurrentDataDsd_' + @U_DSD_VERSION_ID;
			SELECT @sql_view = REPLACE(@sql_view, 'CREATE VIEW', 'CREATE OR ALTER VIEW');
			SELECT @sql_view = REPLACE(@sql_view, 'FROM [data].FACT_'+@U_DSD_VERSION_ID+' FA', 'FROM [data].FILT_'+CAST(@DSD_ID AS VARCHAR)+' FI');
			SELECT @sql_view = REPLACE(@sql_view, 'INNER JOIN [data].FILT_'+CAST(@DSD_ID AS VARCHAR)+' FI', 'LEFT JOIN [data].FACT_'+@U_DSD_VERSION_ID+' FA');
			
			SELECT @sql_view = @sql_view + @sql_where;
			
			--print @sql_view+ @NewLineChar
			exec sp_executesql @sql_view;		
		END
	END TRY  
	BEGIN CATCH  		
		DECLARE 
			@DSD nvarchar(100),
			@msg nvarchar(1000);
			
		SELECT @DSD  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3]IS NULL THEN '0' ELSE CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'
			
		SELECT @msg= 'The following error was found while trying to update Views for the DSD ['+ @DSD +'] (The update of the view will be skiped):'+ ' ErrorNumber:'+CAST(ERROR_NUMBER() AS VARCHAR)+' ErrorMessage:'+ERROR_MESSAGE()+@NewLineChar 
		
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2022-09-14-00.DsdViewsReturnAttributesWhenNoObsComponents.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
				
	END CATCH
END

--update transaction info
UPDATE  [management].[DSD_TRANSACTION] SET [EXECUTION_END] = GETDATE(), [SUCCESSFUL] = 1
WHERE [TRANSACTION_ID] = @Transaction_ID