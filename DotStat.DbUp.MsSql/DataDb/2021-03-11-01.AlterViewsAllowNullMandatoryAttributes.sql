﻿SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
  
DECLARE 	
	@DSD_ID int,
	@DF_ID int,

	@sql_migration NVARCHAR(MAX),
	
	@sql_view NVARCHAR(MAX),
	@sql_view_name varchar(100),

	@table_version char(1),	
    @NewLineChar AS CHAR(2) =  CHAR(10);

DECLARE @cursor_component_DSDs CURSOR,
		@cursor_component_versions CURSOR,
		@cursor_component_df CURSOR;

SET @cursor_component_DSDs = CURSOR 
FOR SELECT ART_ID FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' ORDER BY ART_ID
	
OPEN @cursor_component_DSDs;
FETCH NEXT FROM @cursor_component_DSDs INTO @DSD_ID
	
WHILE @@FETCH_STATUS = 0
BEGIN
	print 'Migrating DSD: ' + CAST(@DSD_ID AS VARCHAR)

--------------------------------------------------------

	BEGIN TRY
		SET @cursor_component_versions = CURSOR
		FOR SELECT 'A' union all SELECT 'B'
	
		OPEN @cursor_component_versions;
		FETCH NEXT FROM @cursor_component_versions INTO @table_version
	
		WHILE @@FETCH_STATUS = 0
		BEGIN
--------------------------------------------------------

			-- handle dsd view
			SET @sql_view_name = 'VI_CurrentDataDsd_'+ CAST( @DSD_ID AS VARCHAR) + '_' + @table_version
			IF NOT EXISTS(select * FROM sys.views where name = @sql_view_name)
				GOTO SKIP_DSD;

			SELECT @sql_view = v.VIEW_DEFINITION from INFORMATION_SCHEMA.VIEWS v where v.TABLE_NAME = @sql_view_name

			SELECT @sql_view = REPLACE(@sql_view, 'CREATE', 'CREATE OR ALTER');
			SELECT @sql_view = REPLACE(@sql_view, 'INNER JOIN [management]', 'LEFT JOIN [management]');	
		
			exec sp_executesql @sql_view;
-------------------
			-- handle dataflow views
			-- select dataflows belonging to current DSD
			SET @cursor_component_df = CURSOR
			FOR SELECT ART_ID from management.ARTEFACT where [TYPE]='DF' and DF_DSD_ID=@DSD_ID
	
			OPEN @cursor_component_df;
			FETCH NEXT FROM @cursor_component_df INTO @DF_ID
			WHILE @@FETCH_STATUS = 0
			BEGIN
				PRINT 'Migrating DF: ' + CAST(@DF_ID AS VARCHAR) + '_' + @table_version;

				SET @sql_view_name = 'VI_CurrentDataDataFlow_'+ CAST( @DF_ID AS VARCHAR) + '_' + @table_version
				IF NOT EXISTS(select * FROM sys.views where name = @sql_view_name)
					GOTO SKIP_DF;

				SELECT @sql_view = v.VIEW_DEFINITION from INFORMATION_SCHEMA.VIEWS v where v.TABLE_NAME = @sql_view_name
				SELECT @sql_view = REPLACE(@sql_view, 'CREATE', 'CREATE OR ALTER');
				SELECT @sql_view = REPLACE(@sql_view, 'INNER JOIN [management]', 'LEFT JOIN [management]');	
				EXEC sp_executesql @sql_view;
				
				SKIP_DF:
				 FETCH NEXT FROM @cursor_component_df INTO @DF_ID
			END

			CLOSE @cursor_component_df;
			DEALLOCATE @cursor_component_df;

			FETCH NEXT FROM @cursor_component_versions INTO @table_version
		END
	
		CLOSE @cursor_component_versions;
		DEALLOCATE @cursor_component_versions;
	END TRY  
	BEGIN CATCH  
		
		DECLARE 
			@DSD nvarchar(100),
			@msg nvarchar(1000),
			@Transaction_ID int

		SELECT @DSD  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3]IS NULL THEN '0' ELSE CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'
	
	
		--Create new transaction for DSD
		SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		
		
		INSERT INTO [management].[DSD_TRANSACTION] 
		([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[ARTEFACT_FULL_ID],[USER_EMAIL])
		VALUES (@Transaction_ID, @DSD_ID, 'X',GETDATE(),GETDATE(),0, @DSD,null)
		
		SELECT @msg= 'The following error was found while trying to create the Views for the DSD ['+ @DSD +'] (The creation of the view will be skiped):'+ ' ErrorNumber:'+CAST(ERROR_NUMBER() AS VARCHAR)+' ErrorMessage:'+ERROR_MESSAGE()+@NewLineChar 
		
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2021-03-11-01.AlterViewsAllowNullMandatoryAttributes.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
		
		--update transaction info
		UPDATE  [management].[DSD_TRANSACTION] 
		SET [EXECUTION_END]= GETDATE(), [SUCCESSFUL] = 1,TIMED_OUT=0
		WHERE [TRANSACTION_ID]=@Transaction_ID
		
	END CATCH
	SKIP_DSD:
		FETCH NEXT FROM @cursor_component_DSDs INTO @DSD_ID
END;
	
CLOSE @cursor_component_DSDs;
DEALLOCATE @cursor_component_DSDs;
