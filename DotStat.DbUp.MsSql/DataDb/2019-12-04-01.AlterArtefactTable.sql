ALTER TABLE [management].[ARTEFACT] ADD [DSD_MAX_TEXT_ATTR_LENGTH] INT

GO

--This update command gets the currently applied max length on text attribute columns and puts this figure into ARTEFACT table
--In case there is no such attribute, NULL will be applied
UPDATE [management].[ARTEFACT]
   SET [DSD_MAX_TEXT_ATTR_LENGTH] = 
(SELECT MAX(c.max_length) / 2
  FROM sys.tables t
  JOIN sys.columns c on c.object_id = t.object_id
 WHERE (t.name like 'FACT_' + CONVERT(VARCHAR, ART_ID) +'_A' OR t.name like 'ATTR_' + CONVERT(VARCHAR, ART_ID) +'_A%')
   AND t.type = 'U'
   AND c.name like 'COMP_%' 
   AND c.system_type_id = 231 --NVARCHAR
)
WHERE TYPE = 'DSD'
  AND DSD_MAX_TEXT_ATTR_LENGTH IS NULL

GO