CREATE OR ALTER PROC CREATE_MSD_DF_VIEWS
	@DSD_ID int,
	@MSD_ID int,
	@table_version char(1)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE
		@sdmxId as NVARCHAR(1000),
		@U_DSD_VERSION_ID varchar(20),
		@NewLineChar AS CHAR(2) =  CHAR(10),
		@isLiveSet as bit,
		@isPITset as bit,			
		@HasTimeDim as bit = 0,
		@msg NVARCHAR(MAX),
		@sql NVARCHAR(MAX),
		@cte NVARCHAR(MAX),
		@DFLevel NVARCHAR(MAX),
		@DSLevel NVARCHAR(MAX),
		@UnionAllPart NVARCHAR(MAX),
		@DFUnionDSD NVARCHAR(MAX),
		@DF_ID int

	DECLARE
		@DSD_Components TABLE(
			COMP_ID int not null,
			ID varchar(50) not null, 
			[TYPE] varchar(50) not null, 	
			CL_ID int
		);

	DECLARE
		@DF_IDS TABLE(ART_ID int PRIMARY KEY, SDMX_ID nvarchar(1000));

    SELECT
		@U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version,
		@sdmxId = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')',
		@isLiveSet = CASE WHEN [DSD_LIVE_VERSION] IS NULL THEN 0 ELSE 1 END,
		@isPITset = CASE WHEN [DSD_PIT_VERSION] IS NULL THEN 0 ELSE 1 END	
	FROM [management].[ARTEFACT] 		
	WHERE [type] = 'DSD' AND ART_ID = @DSD_ID;

	--- check if meta fact table exists
	IF (NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'data' AND  TABLE_NAME = 'META_DS_' + @U_DSD_VERSION_ID))
	BEGIN
		-- SKIP when no meta fact table has been created and no [DSD_LIVE_VERSION], nor [DSD_PIT_VERSION] has been set
		IF( @isLiveSet = 0 AND @isPITset = 0)
		BEGIN
			RETURN	
		END

		set @msg = 'The following error was found while trying to recreate the META DF View for the DSD ['+ @sdmxId +'] (The creation of the view will be skiped): The table [data].[META_DS_'+ @U_DSD_VERSION_ID + '] does not exist in the database, but a value has been set for the fields [DSD_LIVE_VERSION] or [DSD_PIT_VERSION] in the [management].[ARTEFACT] for the given DSD.';

		throw 51000, @msg, 1;
	END

	-- retrieve IF DSD has TIME dimension
	SELECT @HasTimeDim=1 FROM [management].[COMPONENT] WHERE [TYPE] ='TimeDimension' AND DSD_ID = @DSD_ID 
	
	-- get dimensions
	INSERT INTO @DSD_Components
	SELECT c.COMP_ID,c.ID,c.[TYPE],c.[CL_ID]
	FROM [management].[COMPONENT] c 	
	WHERE c.DSD_ID = @DSD_ID and [Type] = 'Dimension'
	
	-- get metadata attributes
	INSERT INTO @DSD_Components
	SELECT m.[MTD_ATTR_ID],m.[ID], 'MetadataAttribute',m.[CL_ID]
	FROM [management].[METADATA_ATTRIBUTE] m
	WHERE m.MSD_ID = @MSD_ID

	
	----------------------------DF ATTACHEMENT LEVEL STATEMENT---------------------------
	SET @DFLevel = ' SELECT ' + ISNULL(STUFF(( 
		SELECT
			CASE 
			--coded dimensions and coded attributes
			WHEN C.[CL_ID] IS NOT NULL 
				THEN 'CASE WHEN [CL_'+[C].[ID]+'].[ID] IS NULL THEN ''~'' ELSE [CL_'+[C].[ID]+'].[ID] END AS ['+[C].[ID]+'],' 
			--Non-coded dimensions
			WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'Dimension'
				THEN '[ME].[DIM_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+'],'
			--Non-coded referential metadata attributes 
			WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'MetadataAttribute' 
				THEN '[ME].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+'],' 
			END
		FROM @DSD_Components C  FOR XML PATH('')
	), 1, 0, ''), '')
	SET @DFLevel +=  @NewLineChar; 
	SET @DFLevel += 
		CASE @HasTimeDim
			WHEN 1 THEN ' CASE WHEN [PERIOD_SDMX] IS NULL THEN ''~'' ELSE [PERIOD_SDMX] END AS [TIME_PERIOD], [PERIOD_START], [PERIOD_END],' 
			ELSE ''
		END
	
	SET @DFLevel+= ' [ME].[LAST_UPDATED]' + @NewLineChar 

	--------------------------------START BUILD FROM STATEMENT-------------------------------
	SELECT @DFLevel+='FROM [data].[META_DF_{DF_ID}_' + @table_version+ '] ME' + @NewLineChar 

	--------------------------------START JOIN  STATEMENT------------------------------------
	SELECT @DFLevel += ISNULL(STUFF(( 
		SELECT 
			CASE 
			--coded dimensions
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Dimension'
				THEN 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON ME.[DIM_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
			--coded referential metadata attributes 
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'MetadataAttribute' 
				THEN 'LEFT JOIN [management].[CL_'+CAST(C.[CL_ID] AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON ME.[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
			END
		FROM @DSD_Components C FOR XML PATH('')
	), 1, 0, ''),'') 

	----------------------------DATASET ATTACHEMENT LEVEL STATEMENT---------------------------
	
	SET @DSLevel = 'SELECT ' + ISNULL(STUFF(( 
		SELECT 
			CASE 
			-- Coded Dimensions
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Dimension'
				THEN '''~'' AS ['+[C].[ID]+'],' + @NewLineChar
			--Non-coded referential metadata attributes 
			WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'MetadataAttribute' 
				THEN '[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+'],' + @NewLineChar
			END
		FROM @DSD_Components C  FOR XML PATH('')
	), 1, 0, ''), '')
		
	SET @DSLevel += 
		CASE @HasTimeDim
			WHEN 1 THEN '''~'' AS [TIME_PERIOD],''9999-12-31'' AS [PERIOD_START], ''0001-01-01'' AS [PERIOD_END],' 
			ELSE ''
		END

	SET @DSLevel += ' [LAST_UPDATED]'+ @NewLineChar
	

	SELECT @DSLevel+='FROM [data].META_DS_'+ @U_DSD_VERSION_ID + @NewLineChar; 
	SELECT @DSLevel+='WHERE [DF_ID] = {DF_ID}'+ @NewLineChar; 
	
	-------------------------------- UNION ALL DATASET AND DATAFLOW LEVELS STATEMENT-------------------------------
	
	SET @UnionAllPart = 'UNION ALL'+ @NewLineChar
	
	SET @cte = 'WITH [DF_LEVEL] AS (' + @NewLineChar + @DFLevel + @UnionAllPart + @DSLevel + ')' + @NewLineChar
	
	-------------------------------- UNION ALL DATASET AND DATAFLOW LEVELS STATEMENT-------------------------------
	
	SET	@DFUnionDSD = @cte + 'SELECT * FROM DF_LEVEL DF' + @NewLineChar + @UnionAllPart + @NewLineChar;
	SET	@DFUnionDSD += 'SELECT * FROM [data].[VI_MetadataDsd_'+ @U_DSD_VERSION_ID+'] DSD' + @NewLineChar;
	SET	@DFUnionDSD += ' WHERE NOT EXISTS ( 
		SELECT TOP 1 1 FROM DF_LEVEL DF ' + @NewLineChar;
	
	SET @DFUnionDSD += 'WHERE ' + ISNULL(STUFF(( 
		SELECT ' AND ' +
			CASE 
			-- Coded Dimensions
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Dimension'
				THEN 'DF.['+[C].[ID]+'] = DSD.['+[C].[ID]+']' + @NewLineChar
			--Non-coded referential metadata attributes 
			END
		FROM @DSD_Components C  FOR XML PATH('')
	), 1, 4, ' '), '')
		
	SET @DFUnionDSD += 
		CASE @HasTimeDim
			WHEN 1 THEN ' AND DF.[TIME_PERIOD] = DSD.[TIME_PERIOD]' 
			ELSE ''
		END
		
	SET	@DFUnionDSD += @NewLineChar + ')';
	--------------------------------------------------------------------------------------------
	
	INSERT into @DF_IDS 
	select ART_ID, [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')' 	
	from management.ARTEFACT where [TYPE]='DF' and DF_DSD_ID=@DSD_ID

	WHILE (Select Count(*) FROM @DF_IDS) > 0
	BEGIN
		SELECT TOP 1 @DF_ID = ART_ID, @sdmxId = SDMX_ID FROM @DF_IDS
		DELETE @DF_IDS WHERE ART_ID = @DF_ID
		
		print 'Recreating MetadataDataFlow VIEW(' + @table_version + ') for Metadata DF: ' + @sdmxId + ', ID:' + CAST(@DF_ID AS VARCHAR);

		SET @sql = REPLACE(
			'CREATE OR ALTER VIEW [data].[VI_MetadataDataFlow_{DF_ID}_' + @table_version + '] AS'  + @NewLineChar + @DFUnionDSD,
			'{DF_ID}',
			CAST(@DF_ID AS VARCHAR)
		);
		
		--print @sql;
		exec sp_executesql @sql;
	END
END