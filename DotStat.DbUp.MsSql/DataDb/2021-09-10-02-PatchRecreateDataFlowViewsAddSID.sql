﻿SET NOCOUNT ON;
SET CONCAT_NULL_YIELDS_NULL ON;
GO
  
--CLEAN TEMP TABLES
IF OBJECT_ID(N'tempdb..#Temp_DFs') IS NOT NULL
BEGIN
DROP TABLE #Temp_DF_Components
END

IF OBJECT_ID(N'tempdb..#Temp_DSD_Components') IS NOT NULL
BEGIN
	DROP TABLE #Temp_DSD_Components
END


SELECT ART_ID, ID, DF_DSD_ID
INTO #Temp_DFs
FROM [management].[ARTEFACT] 
WHERE [type] = 'DF' 

DECLARE @DF_ID int,
    @DSD_ID int,
	@DF NVARCHAR(MAX),
	@isLiveSet as bit,
	@isPITset as bit,
	@msg NVARCHAR(MAX),
	@Transaction_ID int,
    @sql NVARCHAR(MAX),
    @viewA NVARCHAR(MAX),
    @viewB NVARCHAR(MAX),
	@SelectPart NVARCHAR(MAX),
	@FromPart NVARCHAR(MAX),
	@SubQuerySelectPart NVARCHAR(MAX),
	@SubQueryJoinPart NVARCHAR(MAX),
	@HasTimeDim as bit,
    @NewLineChar AS CHAR(2) =  CHAR(10);


WHILE (Select Count(*) FROM #Temp_DFs) > 0
BEGIN

	SELECT @SubQuerySelectPart= '', @SubQueryJoinPart=''
    SELECT TOP 1 @DF_ID = ART_ID, @DSD_ID = DF_DSD_ID FROM #Temp_DFs
    DELETE #Temp_DFs WHERE ART_ID = @DF_ID

	SELECT @isPITset=0
	SELECT @isLiveSet=0
	
    SELECT TOP 1 
	@isLiveSet = CASE WHEN [DSD_LIVE_VERSION] IS NULL THEN 0 ELSE 1 END,
	@isPITset = CASE WHEN [DSD_PIT_VERSION] IS NULL THEN 0 ELSE 1 END
	FROM [management].[ARTEFACT] WHERE ART_ID= @DSD_ID
	
	--- check if DSD view exists
	SET @viewA ='[data].[VI_CurrentDataDsd_'+ CAST( @DSD_ID AS VARCHAR)+'_A]'
	IF OBJECT_ID(@viewA, 'V') IS NULL
	BEGIN
		-- SKIP when no DSD view has been created and no [DSD_LIVE_VERSION], nor [DSD_PIT_VERSION] has been set
		IF( @isLiveSet = 0 AND @isPITset = 0)
		BEGIN
			CONTINUE
		END

		--ERROR  A value for [DSD_LIVE_VERSION] or [DSD_PIT_VERSION] has been set, but no existing DSD view
		SELECT @DF  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DF_ID AND [TYPE] = 'DF'

		--Create new transaction for DSD
		SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		INSERT INTO [management].[DSD_TRANSACTION] 
		([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[USER_EMAIL])
		VALUES (@Transaction_ID, @DSD_ID, 'X',GETDATE(),GETDATE(),0,NULL)
		
		SELECT @msg= 'The following error was found while trying to recreate the DataFlow Views for the Dataflow ['+ @DF +'] (The creation of the view will be skiped):The view '+ @viewA +' does not exist in the database, but a value has been set for the fields [DSD_LIVE_VERSION] or [DSD_PIT_VERSION] in the [management].[ARTEFACT] for the parent DSD.'+@NewLineChar 
	
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2021-09-10-02-PatchRecreateDataFlowViewsAddSID.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
		
		--update transaction info
		UPDATE  [management].[DSD_TRANSACTION] 
		SET [EXECUTION_END]= GETDATE(), [SUCCESSFUL] = 1
		WHERE [TRANSACTION_ID]=@Transaction_ID

		CONTINUE
	END
	
	BEGIN TRY  
		BEGIN TRAN
			SELECT c.DSD_ID, c.ID, c.CL_ID, c.COMP_ID,c.[TYPE], c.ATT_ASS_LEVEL, c.ATT_STATUS
			INTO #Temp_DSD_Components
			FROM [management].[ARTEFACT] a 
			INNER JOIN [management].[COMPONENT] c 
			ON a.ART_ID = c.DSD_ID
			WHERE A.ART_ID=@DSD_ID

			SELECT @HasTimeDim = 0
			IF EXISTS(SELECT 1 FROM sys.columns 
				WHERE Name = N'PERIOD_SDMX'
				AND Object_ID = Object_ID(N'[data].[FACT_'+CAST(@DSD_ID AS VARCHAR)+'_A]'))
			BEGIN
				SELECT @HasTimeDim=1
			END
	
			--SELECT
			--dimensions			
			SELECT @SelectPart='SELECT [SID],'+ ISNULL(STUFF((  
				SELECT ', ['+[C].[ID]+']'
				FROM #Temp_DSD_Components C 
				WHERE c.[TYPE]='Dimension'
				FOR XML PATH('')
			), 1, 1, ''),'')

			--measure
			SELECT @SelectPart+=',[OBS_VALUE]'
			--check if dsd has time dimension
			IF (@HasTimeDim =1)
			BEGIN
				SELECT @SelectPart+= ',[TIME_PERIOD], [PERIOD_START], [PERIOD_END]'
			END		

			--Has attributes
			IF((Select Count(*) FROM #Temp_DSD_Components WHERE [TYPE]='Attribute') > 0)
			BEGIN
				--attributes
				SELECT @SelectPart+=', '+ ISNULL(STUFF((  
					SELECT ', ['+[C].[ID]+']'
					FROM #Temp_DSD_Components C 
					WHERE c.[TYPE]='Attribute'
					FOR XML PATH('')
				), 1, 1, ''),'') +@NewLineChar
			END
	
			--Has dataset level attributes
			IF((Select Count(*) FROM #Temp_DSD_Components WHERE ATT_ASS_LEVEL ='DataSet') > 0)
			BEGIN
				--SUB QUERY
				SELECT @SubQuerySelectPart='SELECT '+ ISNULL(STUFF((  
					SELECT ', '+
					  CASE 
					  --coded attributes at dimgroup level
						WHEN C.[CL_ID] IS NOT NULL
							THEN '[CL_'+[C].[ID]+'].[ID] AS ['+[C].[ID]+']'
					  --Non-coded attributes at dimgroup level
						ELSE 
							'[ADF].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+']'
					  END
					FROM #Temp_DSD_Components C 
					WHERE ATT_ASS_LEVEL ='DataSet'
				 FOR XML PATH('')
				), 1, 1, ''),'') +@NewLineChar
	
				SELECT @SubQuerySelectPart+='FROM [data].[ATTR_' +CAST(@DSD_ID AS VARCHAR)+'_{V}_DF] ADF'+@NewLineChar
				--PRINT @SubQuerySelectPart

				--JOIN CODELISTS FOR CODED DATASET LVL ATTRIBUTES
				SELECT @SubQueryJoinPart=''
				IF((Select Count(*) FROM #Temp_DSD_Components WHERE ATT_ASS_LEVEL ='DataSet' and CL_ID IS NOT NULL) > 0)
				BEGIN
					SELECT @SubQueryJoinPart+=ISNULL(STUFF(( 
						SELECT 'LEFT JOIN [management].[CL_'+CAST(CL_ID AS VARCHAR)+'] AS [CL_'+[C].[ID]+'] ON ADF.COMP_'+CAST(COMP_ID AS VARCHAR)+' = [CL_'+[C].[ID]+'].[ITEM_ID]'+@NewLineChar
						FROM #Temp_DSD_Components C
						WHERE ATT_ASS_LEVEL ='DataSet' and CL_ID IS NOT NULL FOR XML PATH('')
					), 1, 0, ''),'') 
				END
				SELECT @SubQueryJoinPart+=' WHERE DF_ID = ' + CAST(@DF_ID AS VARCHAR)+@NewLineChar+') AS ATTR'
				--PRINT @SubQueryJoinPart

				--FROM
				SELECT @FromPart='FROM [data].[VI_CurrentDataDsd_'+CAST(@DSD_ID AS VARCHAR)+'_{V}] ' + @NewLineChar + 'OUTER APPLY ('+ @NewLineChar 
				--PRINT @FromPart
		
			END
			ELSE
			BEGIN
				SELECT @FromPart='FROM [data].[VI_CurrentDataDsd_'+CAST(@DSD_ID AS VARCHAR)+'_{V}]'+ @NewLineChar
			END
	
			SELECT @SelectPart+= @FromPart + @SubQuerySelectPart + @SubQueryJoinPart

			--DELETE VIEW A IF EXISTS
			SET @viewA ='[data].[VI_CurrentDataDataFlow_'+ CAST( @DF_ID AS VARCHAR)+'_A]'
			IF OBJECT_ID(@viewA, 'V') IS NOT NULL
			BEGIN
				EXEC  ('DROP VIEW '+@viewA)
			END
	
			--DELETE VIEW B IF EXISTS
			SET @viewB ='[data].[VI_CurrentDataDataFlow_'+ CAST( @DF_ID AS VARCHAR)+'_B]'
			IF OBJECT_ID(@viewB, 'V') IS NOT NULL
			BEGIN
				EXEC  ('DROP VIEW '+@viewB)
			END

			--CREATE THE VIEWS
			SELECT @sql = 'CREATE VIEW '+@viewA+' AS '+@NewLineChar+ REPLACE(@SelectPart,'{V}','A')
			--PRINT @sql
			EXEC sp_executesql @sql

			SELECT @sql = 'CREATE VIEW '+@viewB+' AS '+@NewLineChar+ REPLACE(@SelectPart,'{V}','B')
			--PRINT @sql
			EXEC sp_executesql @sql
			
			DROP TABLE #Temp_DSD_Components
		COMMIT TRAN
	END TRY  
	BEGIN CATCH  
		
		ROLLBACK TRAN
		SELECT @DF  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DF_ID AND [TYPE] = 'DF'

		--Create new transaction for DSD
		SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		INSERT INTO [management].[DSD_TRANSACTION] 
		([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[USER_EMAIL])
		VALUES (@Transaction_ID, @DSD_ID, 'X',GETDATE(),GETDATE(),0,NULL)
		
		SELECT @msg= 'The following error was found while trying to recreate the DataFlow Views for the Dataflow ['+ @DF +'] (The creation of the view will be skiped):'+ ' ErrorNumber:'+CAST(ERROR_NUMBER() AS VARCHAR)+' ErrorMessage:'+ERROR_MESSAGE()+@NewLineChar 
	
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2021-09-10-02-PatchRecreateDataFlowViewsAddSID.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
		
		--update transaction info
		UPDATE  [management].[DSD_TRANSACTION] 
		SET [EXECUTION_END]= GETDATE(), [SUCCESSFUL] = 1
		WHERE [TRANSACTION_ID]=@Transaction_ID
	END CATCH
END

DROP TABLE #Temp_DFs