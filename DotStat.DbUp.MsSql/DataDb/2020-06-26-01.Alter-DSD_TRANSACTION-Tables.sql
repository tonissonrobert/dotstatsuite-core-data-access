ALTER TABLE [management].[DSD_TRANSACTION]
	drop constraint IX_DSD_TRANSACTION

/* Remove unused columns */
ALTER TABLE [management].[DSD_TRANSACTION]
DROP COLUMN 
            [VALIDATION_DATE],
            [RELEASE_DATE];

/* change column names*/
EXEC sp_rename'management.DSD_TRANSACTION.DSD_DATE_CREATED', 'EXECUTION_START', 'COLUMN';
EXEC sp_rename'management.DSD_TRANSACTION.READY_FOR_VALIDATION_DATE', 'EXECUTION_END', 'COLUMN';

ALTER TABLE [management].[DSD_TRANSACTION]
	ADD 
        [SUCCESSFUL] bit null,
		[USER_EMAIL] nvarchar(350) null;
GO

CREATE UNIQUE INDEX IX_DSD_TRANSACTION  
    ON [management].[DSD_TRANSACTION] (ART_ID)  
WHERE [SUCCESSFUL] IS NULL;

/* Insert old logs into altered [DSD_TRANSACTION] table */
insert [management].[DSD_TRANSACTION](
	[TRANSACTION_ID]
      ,[ART_ID]
      ,[DSD_TARGET_VERSION]
      ,[EXECUTION_START]
	  ,[EXECUTION_END]
      ,[SUCCESSFUL]
)
select [TRANSACTION_ID]
      ,[ART_ID]
      ,[DSD_TARGET_VERSION]
      ,[DSD_DATE_CREATED]
      ,[READY_FOR_VALIDATION_DATE]
      ,[SUCCESSFUL]
FROM [management].[DSD_TRANSACTION_LOG]


/* drop old [DSD_TRANSACTION_LOG] table */
drop table [management].[DSD_TRANSACTION_LOG];