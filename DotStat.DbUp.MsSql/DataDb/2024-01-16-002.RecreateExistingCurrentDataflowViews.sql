SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL ON
GO
 
DECLARE 	
	@DSD_ID int,
	@SDMX_ID nvarchar(1000),

	@msg nvarchar(1000),

	@table_version char(1),	
    @NewLineChar AS CHAR(2) =  CHAR(10),
	@U_DSD_VERSION_ID varchar(20),
	@Transaction_ID int;


DECLARE DsdTableId_Cursor CURSOR FORWARD_ONLY 
FOR 
 	SELECT [ART_ID], 
	       TableVersion, 
		   [AGENCY]+':'+a.[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')' AS SdmxId
	  FROM [management].[ARTEFACT] a 
CROSS JOIN (SELECT 'A' TableVersion UNION SELECT 'B' ) t 
	 WHERE [type] = 'DSD' 
	 ORDER BY 1, 2;
--------------------------------------------------------
--Create new transaction for DSD
SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		
INSERT 
	INTO [management].[DSD_TRANSACTION]([TRANSACTION_ID], [ART_ID], [TABLE_VERSION], [EXECUTION_START], [EXECUTION_END], [SUCCESSFUL], [ARTEFACT_FULL_ID], [USER_EMAIL])
	VALUES (@transaction_ID, -1, 'X', GETDATE(), GETDATE(), 0, 'N/A', NULL)
--------------------------------------------------------
	
OPEN DsdTableId_Cursor  
  
FETCH NEXT FROM DsdTableId_Cursor   
INTO @DSD_ID, @table_version, @SDMX_ID
  
WHILE @@FETCH_STATUS = 0  
BEGIN  
	print '';
	print 'Migrating DSD with Internal ID: '+ CAST(@DSD_ID AS VARCHAR);

	BEGIN TRY
		BEGIN TRAN
			-- recreate df views
			exec dbo.CREATE_DF_VIEWS @DSD_ID, @table_version; 
		COMMIT TRAN
	END TRY  
	BEGIN CATCH 
		IF (XACT_STATE()) <> 0 ROLLBACK TRAN
						
		SELECT @msg= 'The following error was found while trying to migrate DSD ['+ @SDMX_ID +'] (' + @U_DSD_VERSION_ID + '):'
			+ @NewLineChar + 'ErrorLine:' + CAST(ERROR_LINE() AS VARCHAR)
			+ @NewLineChar + 'ErrorNumber:' + CAST(ERROR_NUMBER() AS VARCHAR)
			+ @NewLineChar + 'ErrorMessage:' + ERROR_MESSAGE()+@NewLineChar 
		
		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2024-01-08-002.RecreateExistingCurrentDataflowViews.sql',@msg,ERROR_MESSAGE(),'dotstatsuite-dbup')
				
	END CATCH

    -- Get the next row.  
    FETCH NEXT FROM DsdTableId_Cursor   
    INTO @DSD_ID, @table_version, @SDMX_ID 
END   

CLOSE DsdTableId_Cursor;  
DEALLOCATE DsdTableId_Cursor;  

--update transaction info
UPDATE [management].[DSD_TRANSACTION] 
   SET [EXECUTION_END] = GETDATE(), [SUCCESSFUL] = 1
 WHERE [TRANSACTION_ID] = @Transaction_ID


