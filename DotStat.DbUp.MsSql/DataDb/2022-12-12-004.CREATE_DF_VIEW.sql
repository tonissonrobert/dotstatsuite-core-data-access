CREATE OR ALTER PROC CREATE_DF_VIEWS
	@DSD_ID int,
	@table_version char(1)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE
		@sdmxId as NVARCHAR(1000),
		@U_DSD_VERSION_ID varchar(20),
		@NewLineChar AS CHAR(2) =  CHAR(10),
		@isLiveSet as bit,
		@isPITset as bit,			
		@HasTimeDim as bit = 0,
		@msg NVARCHAR(MAX),
		@sql NVARCHAR(MAX),
		@SelectPart NVARCHAR(MAX),
		@FromPart NVARCHAR(MAX),
		@DF_ID int

	DECLARE
		@DSD_Components TABLE(
			COMP_ID int not null,
			ID varchar(50) not null, 
			[TYPE] varchar(50) not null, 	
			CL_ID int, 
			ATT_ASS_LEVEL varchar(50),
			ATT_STATUS varchar(11)
		);

	DECLARE
		@DF_IDS TABLE(ART_ID int PRIMARY KEY, SDMX_ID nvarchar(1000));

    SELECT
		@U_DSD_VERSION_ID = CAST(@DSD_ID AS VARCHAR) + '_' + @table_version,
		@sdmxId = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')',
		@isLiveSet = CASE WHEN [DSD_LIVE_VERSION] IS NULL THEN 0 ELSE 1 END,
		@isPITset = CASE WHEN [DSD_PIT_VERSION] IS NULL THEN 0 ELSE 1 END	
	FROM [management].[ARTEFACT] 		
	WHERE [type] = 'DSD' AND ART_ID = @DSD_ID;

	--- check if fact table exists
	IF (NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'data' AND  TABLE_NAME = 'FACT_' + @U_DSD_VERSION_ID))
	BEGIN
		-- SKIP when no fact table has been created and no [DSD_LIVE_VERSION], nor [DSD_PIT_VERSION] has been set
		IF( @isLiveSet = 0 AND @isPITset = 0)
		BEGIN
			RETURN	
		END

		set @msg = 'The following error was found while trying to recreate the DF View for the DSD ['+ @sdmxId +'] (The creation of the view will be skiped): The table [data].[FACT_'+ @U_DSD_VERSION_ID + '] does not exist in the database, but a value has been set for the fields [DSD_LIVE_VERSION] or [DSD_PIT_VERSION] in the [management].[ARTEFACT] for the given DSD.';

		throw 51000, @msg, 1;
	END

	-- retrieve IF DSD has TIME dimension
	SELECT @HasTimeDim=1 FROM [management].[COMPONENT] WHERE [TYPE] ='TimeDimension' AND DSD_ID = @DSD_ID 

	-- get components
	INSERT INTO @DSD_Components
	SELECT c.COMP_ID,c.ID,c.[TYPE],c.[CL_ID],c.[ATT_ASS_LEVEL], c.[ATT_STATUS]
	FROM [management].[COMPONENT] c
	WHERE c.DSD_ID=@DSD_ID

	----------------------------START BUILD SELECT STATEMENT---------------------------
	
	SET @SelectPart = 'SELECT [SID]' + ISNULL(STUFF(( 
		SELECT ', ' +
			CASE 
			--Non-coded dataset level attributes
			WHEN C.[CL_ID] IS NULL AND C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'DataSet'
				THEN '[ADF].[COMP_'+CAST(C.[COMP_ID] AS VARCHAR)+'] AS ['+[C].[ID]+']'
			--coded dataset level attributes
			WHEN C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'DataSet'
				THEN '[CL_'+[C].[ID]+'].[ID] AS ['+[C].[ID]+']'
			ELSE
				'['+[C].[ID]+']'
			END
		FROM @DSD_Components C  FOR XML PATH('')
	), 1, 0, ''), '')


	SET @SelectPart += CASE @HasTimeDim
			WHEN 1 THEN ', [PERIOD_START], [PERIOD_END]' + @NewLineChar
			ELSE ''
		END

    SET @SelectPart += ', [VI].[LAST_UPDATED]'+ @NewLineChar
	--------------------------------START BUILD FROM STATEMENT-------------------------------

	SELECT @FromPart='FROM [data].VI_CurrentDataDsd_'+ @U_DSD_VERSION_ID + ' AS [VI]' + @NewLineChar; 

	IF exists (SELECT top 1 1 FROM @DSD_Components WHERE [TYPE] = 'Attribute' AND [ATT_ASS_LEVEL] = 'DataSet')
	BEGIN
		SELECT @FromPart += 'FULL JOIN [data].[ATTR_'+@U_DSD_VERSION_ID+'_DF] ADF ON DF_ID = {DF_ID}' + @NewLineChar;

		SELECT @FromPart += ISNULL(STUFF(( 
			SELECT 'LEFT JOIN [management].[CL_'+CAST(CL_ID AS VARCHAR)+'] AS [CL_'+[ID]+'] ON [ADF].[DF_ID]={DF_ID} AND ADF.COMP_'+CAST(COMP_ID AS VARCHAR)+' = [CL_'+[ID]+'].[ITEM_ID]' + @NewLineChar
			FROM @DSD_Components C 
			WHERE C.[CL_ID] IS NOT NULL AND C.[TYPE] = 'Attribute' AND C.[ATT_ASS_LEVEL] = 'DataSet' 	 
			FOR XML PATH('')
		), 1, 0, ''),'') 
	END
	
	--------------------------------------------------------------------------------------------
	
	INSERT into @DF_IDS 
	select ART_ID, [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+ CASE WHEN [VERSION_3] IS NULL THEN '' ELSE '.'+CAST([VERSION_3] AS VARCHAR) END +')' 	
	from management.ARTEFACT where [TYPE]='DF' and DF_DSD_ID=@DSD_ID

	WHILE (Select Count(*) FROM @DF_IDS) > 0
	BEGIN
		SELECT TOP 1 @DF_ID = ART_ID, @sdmxId = SDMX_ID FROM @DF_IDS
		DELETE @DF_IDS WHERE ART_ID = @DF_ID
		
		print 'Recreating VIEW(' + @table_version + ') for DF: ' + @sdmxId + ', ID:' + CAST(@DF_ID AS VARCHAR);

		SET @sql = REPLACE(
			'CREATE OR ALTER VIEW [data].[VI_CurrentDataDataFlow_{DF_ID}_' + @table_version + '] AS'  + @NewLineChar + @SelectPart + @FromPart,
			'{DF_ID}',
			CAST(@DF_ID AS VARCHAR)
		);
		
		exec sp_executesql @sql;
	END
END