﻿SET NOCOUNT ON;
SET CONCAT_NULL_YIELDS_NULL ON;
GO

DECLARE 	
	@DSD_ID int,
	@DSD NVARCHAR(MAX),
	@DFs NVARCHAR(MAX),
	@msg NVARCHAR(MAX),
	@Transaction_ID int,
    @NewLineChar AS CHAR(2) =  CHAR(10);

	
DECLARE
	@DSD_IDS TABLE(ART_ID int PRIMARY KEY);

INSERT into @DSD_IDS 
	SELECT ART_ID FROM [management].[ARTEFACT]
	WHERE [type] = 'DSD' ORDER BY ART_ID
	
WHILE (Select Count(*) FROM @DSD_IDS) > 0
BEGIN

    SELECT TOP 1 @DSD_ID = ART_ID FROM @DSD_IDS
	DELETE @DSD_IDS WHERE ART_ID = @DSD_ID

	-- Delete DSD/DF rows from Artefact table, for which there is not filter and fact tables
	IF OBJECT_ID(N'[data].FILT_' + CAST(@DSD_ID AS VARCHAR)) IS NULL
	OR OBJECT_ID(N'[data].FACT_' + CAST(@DSD_ID AS VARCHAR) + '_A') IS NULL
	OR OBJECT_ID(N'[data].FACT_' + CAST(@DSD_ID AS VARCHAR) + '_B') IS NULL
	BEGIN
		SELECT @DSD  = [AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3]IS NULL THEN '0' ELSE CAST([VERSION_3] AS VARCHAR) END +')'
		FROM [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'

		SELECT @DFs  = STUFF(( 
			SELECT	', '+[AGENCY]+':'+[ID]+'('+CAST([VERSION_1] AS VARCHAR)+'.'+CAST([VERSION_2] AS VARCHAR)+'.'+ CASE WHEN [VERSION_3]IS NULL THEN '0' ELSE CAST([VERSION_3] AS VARCHAR) END +')'
			FROM [management].[ARTEFACT] 
			WHERE [DF_DSD_ID] = @DSD_ID AND [TYPE] = 'DF'
			FOR XML PATH('')
		), 1, 2, '') 
		
		--Create new transaction for DSD
		SELECT @Transaction_ID = NEXT VALUE FOR [management].[TRANSFER_SEQUENCE];
		INSERT INTO [management].[DSD_TRANSACTION] 
		([TRANSACTION_ID],[ART_ID],[DSD_TARGET_VERSION],[EXECUTION_START],[EXECUTION_END],[SUCCESSFUL],[USER_EMAIL])
		VALUES (@Transaction_ID, @DSD_ID, 'X',GETDATE(),NULL,0,NULL)
		
		--Delete DSD 
		SELECT @msg = 'Deleting the DSD ['+ @DSD +'] from [management].[ARTEFACT] table. The corresponding FILTER and FACT_[A/B] tables are missing in the database.'
		DELETE FROM  [management].[ARTEFACT] 
		WHERE [ART_ID] = @DSD_ID AND [TYPE] = 'DSD'

		--LOG
		PRINT @msg
		INSERT INTO [management].[LOGS] 
		([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
		VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2020-08-04-00.CleanOrphanRowsInArtefactTableNoFactTables.sql',@msg,NULL,'dotstatsuite-dbup')

		IF @DFs IS NOT NULL
		BEGIN
			SELECT @msg = 'Deleting all dataflows referencing the DSD ['+ @DSD +'] previously deleted from [management].[ARTEFACT] table. Dataflows: ['+ @DFs +']'+@NewLineChar
			
			--LOG
			PRINT @msg
			INSERT INTO [management].[LOGS] 
			([TRANSACTION_ID],[DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[APPLICATION])
			VALUES (@Transaction_ID,GETDATE(),'ERROR','MSSQL SERVER:'+@@SERVERNAME,'2020-08-04-00.CleanOrphanRowsInArtefactTableNoFactTables.sql',@msg,NULL,'dotstatsuite-dbup')

			--Delete DFs 
			DELETE FROM  [management].[ARTEFACT] 
			WHERE [DF_DSD_ID] = @DSD_ID AND [TYPE] = 'DF'
		END
		
		--update transaction info
		UPDATE  [management].[DSD_TRANSACTION] 
		SET [EXECUTION_END]= GETDATE(), [SUCCESSFUL] = 1
		WHERE [TRANSACTION_ID]=@Transaction_ID

	END
END
