IF (NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'data')) 
BEGIN
    EXEC ('CREATE SCHEMA [data]')
END

IF (NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'management')) 
BEGIN
    EXEC ('CREATE SCHEMA [management]')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[ARTEFACT]') and OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
    CREATE TABLE [management].[ARTEFACT](
        [ART_ID] [int] IDENTITY(1,1) NOT NULL,
        [SQL_ART_ID] [int] NULL,
        [TYPE] [varchar](50) NOT NULL,
        [ID] [varchar](50) NOT NULL,
        [AGENCY] [varchar](50) NOT NULL,
        [VERSION_1] [int] NOT NULL DEFAULT 0,
        [VERSION_2] [int] NOT NULL DEFAULT 0,
        [VERSION_3] [int] NULL,
        [DSD_LIVE_VERSION] [char](1) NULL,
        [DSD_PIT_VERSION] [char](1) NULL,
        [DSD_PIT_RELEASE_DATE] [datetime] NULL,
        [DSD_PIT_RESTORATION_DATE] [datetime] NULL,
        [DSD_DATE_CREATED] [datetime] NULL,
        [DSD_DATE_DELETED] [datetime] NULL,
        [DSD_CUD_MODE] [bit] NULL,
        [DSD_TRANSFER_MODE] [bit] NULL,
        [DSD_VALIDATION] [bit] NULL,
        [DSD_OBS_VALUE_DATA_TYPE] [varchar](50) NULL,
        [DF_DSD_ID] [int] NULL,
        [DF_WHERE_CLAUSE] [varchar](max) NULL,
        [LAST_UPDATED] [datetime] NULL,
     CONSTRAINT [PK_ARTEFACT_backup] PRIMARY KEY CLUSTERED 
    (
        [ART_ID] ASC
    ),
     CONSTRAINT [UK_ARTEFACT_SDMX_AGENCY_ID_VERSION] UNIQUE NONCLUSTERED 
    (
        [TYPE] ASC,
        [ID] ASC,
        [AGENCY] ASC,
        [VERSION_1] ASC,
        [VERSION_2] ASC,
        [VERSION_3] ASC
    )
    )
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[COMPONENT]') and OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
    CREATE TABLE [management].[COMPONENT](
        [COMP_ID] [int] IDENTITY(1,1) NOT NULL,
        [ID] [varchar](50) NOT NULL,
        [TYPE] [varchar](50) NOT NULL,
        [DSD_ID] [int] NOT NULL,
        [CL_ID] [int] NULL,
        [ATT_ASS_LEVEL] [varchar](15) NULL,
        [ATT_STATUS] [varchar](11) NULL,
        [ATT_GROUP_ID] [varchar](50) NULL,
        [ENUM_ID] [int] NULL,
     CONSTRAINT [PK_COMPONENT] PRIMARY KEY CLUSTERED 
    (
        [COMP_ID] ASC
    )
    )
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[ATTR_DIM_SET]') and OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
    CREATE TABLE [management].[ATTR_DIM_SET](
        [ATTR_ID] [int] NOT NULL,
        [DIM_ID] [int] NOT NULL,
    PRIMARY KEY CLUSTERED 
    (
        [ATTR_ID] ASC,
        [DIM_ID] ASC
    )
    )
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[DSD_TRANSACTION]') and OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
    CREATE TABLE [management].[DSD_TRANSACTION](
        [TRANSACTION_ID] [int] NOT NULL,
        [ART_ID] [int] NOT NULL,
        [DSD_TARGET_VERSION] [char](1) NOT NULL,
        [DSD_DATE_CREATED] [datetime] NOT NULL DEFAULT GetDate(),
        [READY_FOR_VALIDATION_DATE] [datetime] NULL,
        [VALIDATION_DATE] [datetime] NULL,
        [RELEASE_DATE] [datetime] NULL,
     CONSTRAINT [PK_DSD_TRANSACTION] PRIMARY KEY CLUSTERED 
    (
        [TRANSACTION_ID] ASC
    ),
     CONSTRAINT [IX_DSD_TRANSACTION] UNIQUE NONCLUSTERED 
    (
        [ART_ID] ASC
    )
    )
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[DSD_TRANSACTION_LOG]') and OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
    CREATE TABLE [management].[DSD_TRANSACTION_LOG](
        [TRANSACTION_ID] [int] NOT NULL,
        [ART_ID] [int] NOT NULL,
        [DSD_TARGET_VERSION] [char](1) NOT NULL,
        [DSD_DATE_CREATED] [datetime] NOT NULL DEFAULT GetDate(),
        [READY_FOR_VALIDATION_DATE] [datetime] NULL,
        [VALIDATION_DATE] [datetime] NULL,
        [RELEASE_DATE] [datetime] NULL,
        [SUCCESSFUL] [bit] NULL,
     CONSTRAINT [PK_DSD_TRANSACTION_LOG_bk] PRIMARY KEY CLUSTERED 
    (
        [TRANSACTION_ID] ASC
    )
    )
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[LOGS]') and OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
    CREATE TABLE [management].[LOGS](
        [TRANSACTION_ID] [int] NULL,
        [DATE] [datetime] NULL,
        [LEVEL] [nvarchar](50) NULL,
        [SERVER] [nvarchar](50) NULL,
        [LOGGER] [nvarchar](255) NULL,
        [MESSAGE] [nvarchar](max) NULL,
        [EXCEPTION] [nvarchar](max) NULL
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[CL_TIME]') and OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
    CREATE TABLE [management].[CL_TIME](
        [ITEM_ID] [int] IDENTITY(1,1) NOT NULL,
        [ITEM_PARENT_ID] [int] NOT NULL,
        [ID] [varchar](50) NOT NULL,
        [START_PERIOD] [datetime] NOT NULL,
        [END_PERIOD] [datetime] NOT NULL,
     CONSTRAINT [PK_CL_TIME] PRIMARY KEY CLUSTERED 
    (
        [ITEM_ID] ASC
    )
    )
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[ENUMERATIONS]') and OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
    CREATE TABLE [management].[ENUMERATIONS](
        [ENUM_ID] [int] NOT NULL,
        [ENUM_NAME] [varchar](30) NULL,
        [ENUM_VALUE] [varchar](50) NULL,
        [ENUM_VALUE_DATATYPE] [varchar](20) NULL,
     CONSTRAINT [PK_ENUMERATIONS] PRIMARY KEY CLUSTERED 
    (
        [ENUM_ID] ASC
    )
    )
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[management].[DF_OUTPUT_TIMESTAMP]') and OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
    CREATE TABLE [management].[DF_OUTPUT_TIMESTAMP](
        [ART_ID] [int] NOT NULL,
        [FILETYPE] [varchar](20) NOT NULL,
        [LANG] [char](2) NOT NULL,
        [FILENAME] [varchar](100) NOT NULL,
        [TIMESTAMP] [datetime] NOT NULL,
     CONSTRAINT [PK_DF_OUTPUT_TIMESTAMP] PRIMARY KEY CLUSTERED 
    (
        [ART_ID] ASC,
        [FileType] ASC,
        [Lang] ASC
    )
    )
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[sysobjects] WHERE id = object_id(N'[dbo].[DB_VERSION]') and OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
    CREATE TABLE [DB_VERSION](
        [SYS_ID] [bigint] IDENTITY(1,1) NOT NULL,
        [VERSION] [varchar](250) NULL,
        CONSTRAINT [PK_DB_VERSION] PRIMARY KEY CLUSTERED ([SYS_ID] ASC)
    )

    INSERT INTO [dbo].[DB_VERSION] ([VERSION]) VALUES ('1.0')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[sysobjects] WHERE id = OBJECT_ID(N'[management].[TRANSFER_SEQUENCE]') AND type = 'SO')
BEGIN
    CREATE SEQUENCE [management].[TRANSFER_SEQUENCE] 
        AS [int]
        START WITH 0
        INCREMENT BY 1
        MINVALUE -2147483648
        MAXVALUE 2147483647
        CACHE 
END
