
-- Set as Canceled all ongoing transactions
UPDATE [management].[DSD_TRANSACTION]
SET [EXECUTION_END] = GETDATE(), [SUCCESSFUL] = 0, [STATUS]='Canceled', [LAST_UPDATED]= GETDATE()
WHERE [SUCCESSFUL] IS NULL

-- Add new column for service identifier
ALTER TABLE [management].[DSD_TRANSACTION]
ADD [SERVICE_ID] [varchar](20) NULL 


GO