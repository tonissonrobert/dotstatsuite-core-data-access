﻿using CommandLine;

namespace DotStat.DbUp.Options
{
    [Verb("tobeexecuted", HelpText = "List scripts which will be executed, e.g.: DotStat.DbUp.exe tobeexecuted --connectionString \"Server=.; Database=MyDataDb; Trusted_connection=true\" --dataDb")]
    public class ToBeExecutedOption : BaseOption
    {
    }
}