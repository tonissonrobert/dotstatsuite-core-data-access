﻿using CommandLine;

namespace DotStat.DbUp.Options
{
    public class BaseOption
    {
        [Option("connectionString", Required = true, HelpText = "The connection string for the database")]
        public string ConnectionString { get; set; }

        [Option("commonDb", Required = true, SetName = "commonDbType", HelpText = "The type of the database, can be either --commonDb or --dataDb or --mappingStoreDb")]
        public bool CommonDb { get; set; }

        [Option("dataDb", Required = true, SetName = "dataDbType", HelpText = "The type of the database, can be either --commonDb or --dataDb or --mappingStoreDb")]
        public bool DataDb { get; set; }

        [Option("mappingStoreDb", Required = true, SetName = "mappingStoreDbType", HelpText = "The type of the database, can be either --commonDb or --dataDb or --mappingStoreDb")]
        public bool MappingStoreDb { get; set; }

        [Option("executionTimeout", Required = false, HelpText = "The execution timeout in minutes, default is 30")]
        public double? ExecutionTimeout { get; set; }
    }
}