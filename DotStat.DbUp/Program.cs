﻿using CommandLine;
using DbUp;
using DotStat.DbUp.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using DbUp.Engine;
using DbUp.Engine.Output;
using DbUp.Engine.Transactions;
using DbUp.Helpers;
using DbUp.SqlServer;
using DbUp.Support;
using log4net;
using System.Reflection;
using System.Diagnostics;

namespace DotStat.DbUp
{
    public class Program
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(Program));
        private static readonly SqlScriptOptions RunAlwaysAtStartOption = new SqlScriptOptions { ScriptType = ScriptType.RunAlways, RunGroupOrder = DbUpDefaults.DefaultRunGroupOrder - 1 };
        private static readonly SqlScriptOptions RunAlwaysAtEndOption = new SqlScriptOptions { ScriptType = ScriptType.RunAlways, RunGroupOrder = DbUpDefaults.DefaultRunGroupOrder + 1 };

        /// <summary>
        /// Entry point
        /// </summary>
        /// <param name="args">The arguments provided at command line.</param>
        /// <returns>
        /// The exit code.
        /// </returns>
        [ExcludeFromCodeCoverage]
        private static int Main(string[] args)
        {
            PrintVersionInfo();

            return
                Parser.Default.ParseArguments<ExecutedOption, ToBeExecutedOption, UpgradeOption>(args)
                    .MapResult(
                        (ExecutedOption option) => GetExecuted(option),
                        (ToBeExecutedOption option) => GetToBeExecuted(option),
                        (UpgradeOption option) => Upgrade(option),
                        error => 1);
        }

        [ExcludeFromCodeCoverage]
        private static int Upgrade(UpgradeOption option)
        {
            try
            {
                DbUpGoogleLogger.ConfigureLog4net(option);

                if (!option.Force)
                {
                    Console.Write("Are you sure you want to continue? [y/N]");
                    var info = Console.ReadKey();

                    if (info.Key != ConsoleKey.Y)
                    {
                        return 1;
                    }

                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("Force option set, continuing without user confirmation.");
                }

                var executionTimeoutInMinutes = option.ExecutionTimeout ?? 30;

                if (option.DropDb)
                {
                    DropDatabase.For.SqlDatabase(option.ConnectionString,
                        Convert.ToInt16(executionTimeoutInMinutes) * 60);
                }

                EnsureDatabase.For.SqlDatabase(option.ConnectionString,
                    Convert.ToInt16(executionTimeoutInMinutes) * 60);

                // Execute scripts against 'master' database (e.g. create/alter login)
                UpgradeEngine upgradeEngine = null;
                DatabaseUpgradeResult result = null;

                if (!option.WithoutDbaScripts)
                {
                    upgradeEngine = DeployChanges.To
                        .SqlDatabase(new DotstatConnectionManager(option.MasterConnectionString))
                        .WithScriptsEmbeddedInAssembly(typeof(MsSql.Resources).Assembly,
                            s => FilterMasterDbScript(s, option))
                        .WithExecutionTimeout(TimeSpan.FromMinutes(executionTimeoutInMinutes))
                        .WithVariables(GetScriptVariables(option))
                        .LogWithLog4net(option)
                        .WithTransactionPerScript()
                        .JournalTo(new NullJournal()) // Do not attempt to journal in master db
                        .Build();

                    result = upgradeEngine.PerformUpgrade();

                    if (!result.Successful)
                    {
                        return LogError(result.Error);
                    }
                }

                // Execute scripts against target database
                upgradeEngine = DeployChanges.To
                    .SqlDatabase(new DotstatConnectionManager(option.ConnectionString))
                    .WithScriptsEmbeddedInAssembly(typeof(MsSql.Resources).Assembly,
                        s => FilterAlwaysRunFirstScript(s, option), RunAlwaysAtStartOption)
                    .WithScriptsEmbeddedInAssembly(typeof(MsSql.Resources).Assembly,
                        s => s.Contains(GetScriptPath(option), StringComparison.OrdinalIgnoreCase))
                    .WithScriptsEmbeddedInAssembly(typeof(MsSql.Resources).Assembly,
                        s => FilterAlwaysRunLastScript(s, option), RunAlwaysAtEndOption)
                    .WithExecutionTimeout(TimeSpan.FromMinutes(option.ExecutionTimeout ?? 30))
                    .JournalTo((connectionManager, logger) => new DotstatJournal(connectionManager, logger))
                    .WithVariables(GetScriptVariables(option))
                    .LogWithLog4net(option)
                    .WithTransactionPerScript()
                    .Build();

                result = upgradeEngine.PerformUpgrade();

                return result.Successful
                    ? LogSuccess()
                    : LogError(result.Error);
            }
            catch (Exception ex)
            {
                return LogError(ex);
            }
            finally
            {
                DbUpGoogleLogger.Flush();
            }
        }

        public static Dictionary<string, string> GetScriptVariables(UpgradeOption option)
        {
            var scriptVariables = new Dictionary<string, string>()
            {
                {"dbName", new SqlConnectionStringBuilder(option.ConnectionString).InitialCatalog},
                {"loginName", option.LoginName},
                {"loginPwd", option.LoginPwd},
                {"alterPassword", option.AlterPassword ? "1" : "0" },
            };

            if (!string.IsNullOrWhiteSpace(option.ROloginName))
            {
                scriptVariables.Add("ROloginName", option.ROloginName);
                scriptVariables.Add("ROloginPwd", option.ROloginPwd);
            }

            if (!option.MappingStoreDb)
            {
                scriptVariables.Add(
                    "dbVersion",
                    (option.CommonDb
                        ? DatabaseVersion.CommonDbVersion
                        : DatabaseVersion.DataDbVersion).ToString());
            }

            return scriptVariables;
        }

        public static bool FilterAlwaysRunFirstScript(string fileName, UpgradeOption option)
        {
            if (option.WithoutDbaScripts)
            {
                return false;
            }

            // Take the SQL scripts from AlwaysRunFirst folder and
            // - exclude the following scripts when database is MappingStore (structure) database
            //   - 0000.SetRestrictedUserMode.sql
            return !string.IsNullOrWhiteSpace(fileName)
                   && fileName.Contains(".AlwaysRunFirst.")
                   && (!option.MappingStoreDb || !fileName.EndsWith("0000.SetRestrictedUserMode.sql"));
        }

        public static bool FilterAlwaysRunLastScript(string fileName, UpgradeOption option)
        {
            if (option.WithoutDbaScripts)
            {
                return !string.IsNullOrWhiteSpace(fileName) && (option.CommonDb || option.DataDb) && fileName.EndsWith("0500.SetVersion.sql");
            }

            // Take the SQL scripts from AlwaysRunLast folder and
            // - exclude the following scripts when database is MappingStore (structure) database
            //   - 0500.SetVersion.sql (db version is managed by maapi.net tool)
            //   - 9900.SetMultiUserMode.sql
            // - exclude the following script if no ROloginName is provided (ie. no read only user should be created/altered)
            //   - 0150.CreateOrAlterReadOnlyDbUser.sql
            return !string.IsNullOrWhiteSpace(fileName)
                   && fileName.Contains(".AlwaysRunLast.")
                   && (!option.MappingStoreDb || (!fileName.EndsWith("0500.SetVersion.sql") &&
                                                  !fileName.EndsWith("9900.SetMultiUserMode.sql")))
                   && (option.IsROloginNameValid() || !fileName.EndsWith("0150.CreateOrAlterReadOnlyDbUser.sql"));
        }

        public static bool FilterMasterDbScript(string fileName, UpgradeOption option)
        {
            // Take the SQL scripts from Master folder and
            // - exclude the following script if no ROloginName is provided (ie. no read only user should be created/altered)
            //   - 0150.CreateOrAlterReadOnlyLogin.sql
            return !string.IsNullOrWhiteSpace(fileName)
                   && fileName.Contains(".MasterDb.")
                   && (option.IsROloginNameValid() || !fileName.EndsWith("0150.CreateOrAlterReadOnlyLogin.sql"));
        }

        private static int GetToBeExecuted(ToBeExecutedOption option)
        {
            EnsureDatabase.For.SqlDatabase(option.ConnectionString);

            var dbUpEngine = DeployChanges.To
                .SqlDatabase(option.ConnectionString)
                .WithScriptsEmbeddedInAssembly(typeof(MsSql.Resources).Assembly, s => s.Contains(GetScriptPath(option), StringComparison.OrdinalIgnoreCase))
                .LogToConsole()
                .Build();

            foreach (var sqlScript in dbUpEngine.GetScriptsToExecute())
            {
                Console.WriteLine(sqlScript.Name);
            }

            return 0;
        }

        private static int GetExecuted(ExecutedOption option)
        {
            var dbUpEngine = DeployChanges.To
                .SqlDatabase(option.ConnectionString)
                .WithScriptsEmbeddedInAssembly(typeof(MsSql.Resources).Assembly, s => s.Contains(GetScriptPath(option), StringComparison.OrdinalIgnoreCase))
                .LogToConsole()
                .Build();

            foreach (var sqlScript in dbUpEngine.GetExecutedScripts())
            {
                Console.WriteLine(sqlScript);
            }

            return 0;
        }

        public static string GetScriptPath(BaseOption option)
        {
            if (option.CommonDb)
            {
                return "CommonDb";
            }

            if (option.DataDb)
            {
                return "DataDb";
            }

            if (option.MappingStoreDb)
            {
                return "MappingStoreDb";
            }

            throw new Exception("Wrong db type.");
        }

        private static int LogError(Exception exception)
        {
            Logger.Error(exception);

            return 1;
        }

        private static int LogSuccess()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Success!");
            Console.ResetColor();

            return 0;
        }

        private static void PrintVersionInfo()
        {
            var dbUpVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;
            Console.WriteLine("SIS-CC .Stat Suite dotstatsuite-dbup tool v" + dbUpVersion);
            
            return ;
        }
    }

    class DotstatJournal : SqlTableJournal
    {
        public DotstatJournal(Func<IConnectionManager> connectionManager, Func<IUpgradeLog> logger) : base(connectionManager, logger, null, "SchemaVersions")
        { }

        public override void StoreExecutedScript(SqlScript script, Func<IDbCommand> dbCommandFactory)
        {
            if (script.SqlScriptOptions.ScriptType == ScriptType.RunOnce)
            {
                base.StoreExecutedScript(script, dbCommandFactory);
            }
        }
    }
}
