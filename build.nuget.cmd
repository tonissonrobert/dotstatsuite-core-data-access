@echo OFF

if "%NUGET_ROOT%"=="" goto WARN

rmdir /S/Q "%userprofile%\.nuget\packages\dotstat.dataaccess.nuget"
rmdir /S/Q "%userprofile%\.nuget\packages\dotstat.test.shared"
rmdir /S/Q "%TEMP%\DotStat.Db"
dotnet pack %~dp0\DotStat.Db -c Debug -o "%TEMP%\DotStat.Db"
dotnet pack %~dp0\DotStat.Test.Shared -c Debug -o "%TEMP%\DotStat.Db"
move "%TEMP%\DotStat.Db\*.nupkg" "%NUGET_ROOT%"

@echo Userprofile = %userprofile%
@echo NUGET_ROOT = %NUGET_ROOT%

dir %NUGET_ROOT%

goto :eof

:WARN
echo -----------------------------------------
echo Please set NUGET_ROOT environment variable with the path to your local nuget packages
echo -----------------------------------------