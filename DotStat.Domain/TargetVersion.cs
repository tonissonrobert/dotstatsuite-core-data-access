namespace DotStat.Domain
{
    public enum TargetVersion
    {
        Live = 0,
        PointInTime = 1
    }
}
