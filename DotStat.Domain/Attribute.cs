﻿using DotStat.Common.Enums;
using DotStat.Common.Localization;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;

namespace DotStat.Domain
{
    public class Attribute : CodeListBasedIdentifiableDotStatObject<IAttributeObject>
    {
        public Constraint Constraint { get; }

        public override Localization.ResourceId ResourceIdOfCodelistCodeRemoved =>
            Localization.ResourceId.ChangeInDsdAttributeCodelistCodeRemoved;
        public override Localization.ResourceId ResourceIdOfCodelistCodeAdded =>
            Localization.ResourceId.ChangeInDsdAttributeCodelistCodeAdded;
        public override Localization.ResourceId ResourceIdOfCodelistChanged =>
            Localization.ResourceId.ChangeInDsdAttributeCodelistChanged;

        public Attribute(IAttributeObject @base, Codelist codelist = null, Constraint constraint = null) : base(@base, codelist)
        {
            DbType = DbTypes.GetDbType(SDMXArtefactType.DataAttribute);
            Constraint = constraint;
        }
    }
}