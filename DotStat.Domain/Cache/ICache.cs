﻿
namespace DotStat.Domain.Cache
{
    public interface ICache
    {
        bool TryGetValue<T>(string key, out T value);

        void Set<T>(string key, object obj);
    }
}
