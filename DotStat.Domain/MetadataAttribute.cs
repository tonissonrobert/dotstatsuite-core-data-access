﻿using DotStat.Common.Enums;
using DotStat.Common.Localization;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;

namespace DotStat.Domain
{
    public class MetadataAttribute : CodeListBasedIdentifiableDotStatObject<IMetadataAttributeObject>
    {
        public override Localization.ResourceId ResourceIdOfCodelistCodeRemoved { get; }
        public override Localization.ResourceId ResourceIdOfCodelistCodeAdded { get; }
        public override Localization.ResourceId ResourceIdOfCodelistChanged { get; }
        public string HierarchicalId { get; set; }
        public MetadataAttribute(IMetadataAttributeObject @base) : base(@base)
        {
            DbType = DbTypes.GetDbType(SDMXArtefactType.MetadataAttribute);
        }
    }
}