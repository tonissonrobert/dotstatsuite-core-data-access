﻿using System;
using System.Runtime.Serialization;

namespace DotStat.Domain
{
    public class Transaction
    {
        public int Id { get; set; }
        public int? ArtefactDbId { get; set; }
        public int? ArtefactChildDbId { get; set; }
        public string ArtefactFullId { get; set; }
        public TargetVersion? RequestedTargetVersion { get; set; }
        public TargetVersion? FinalTargetVersion { get; set; }
        public DbTableVersion? TableVersion { get; set; }
        public DateTime QueuedDateTime { get; set; }
        public DateTime? ExecutionStart { get; set; }
        public DateTime? ExecutionEnd { get; set; }
        public bool? Successful { get; set; }
        public bool Closed => Successful != null;
        public string UserEmail { get; set; }
        public string SourceDataspace { get; set; }
        public string DataSource { get; set; }
        public TransactionStatus Status { get; set; }
        public TransactionType Type { get; set; }
        public bool BlockAllTransactions { get; set; } = false;
        public DateTime LastUpdated { get; set; }
        public string ServiceId { get; set; }

        public const string TransactionDatetimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'";
        public string QueuedDateTimeAsString => QueuedDateTime.ToString(TransactionDatetimeFormat);
        public string ExecutionStartAsString => ExecutionStart?.ToString(TransactionDatetimeFormat);
        public string ExecutionEndAsString => ExecutionEnd?.ToString(TransactionDatetimeFormat);
        public string LastUpdatedAsString => LastUpdated.ToString(TransactionDatetimeFormat);
    }

    

    public enum TransactionStatus
    {
        Queued,
        InProgress,
        Completed,
        TimedOut,
        Canceled,
        Unknown
    }

    public enum TransactionType
    {
        CleanupOrphans,
        CleanupDsd,
        CleanupMappingSets,
        InitDataFlow,
        InitAllMappingSets,
        Import,
        ValidateImport,
        Rollback,
        Restore,
        Transfer,
        ValidateTransfer,
        Unknown
    }
    
    public static class SpecialTransactionArtefactIds
    {
        public static string All = "*";
        public static string None = "N/A";
    }

}
