﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;

namespace DotStat.Test.Moq
{
    public class TestDbDataReader : DbDataReader
    {
        private readonly IEnumerator<object[]> _enumerator;

        public TestDbDataReader(IEnumerable<string[]> data)
        {
            this._enumerator = data.GetEnumerator();
        }

        public TestDbDataReader(IEnumerable<object[]> data)
        {
            this._enumerator = data.GetEnumerator();
        }

        public override string GetName(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override int GetValues(object[] values)
        {
            throw new NotImplementedException();
        }

        public override bool IsDBNull(int ordinal)
        {
            return _enumerator.Current[ordinal] == null || string.IsNullOrEmpty(GetString(ordinal));
        }

        public override int FieldCount { get; }

        public override object this[int ordinal]
        {
            get { return _enumerator.Current[ordinal]; }
        }

        public override object this[string name]
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool HasRows { get; }
        public override bool IsClosed { get; }
        public override int RecordsAffected { get; }

        public override bool NextResult()
        {
            throw new NotImplementedException();
        }

        public override bool Read()
        {
            return this._enumerator.MoveNext();
        }

        public override int Depth { get; }

        public override int GetOrdinal(string name)
        {
            throw new NotImplementedException();
        }

        public override bool GetBoolean(int ordinal)
        {
            var value = _enumerator.Current[ordinal].ToString();
            return Convert.ToBoolean(value);
        }

        public override byte GetByte(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override long GetBytes(int ordinal, long dataOffset, byte[] buffer, int bufferOffset, int length)
        {
            throw new NotImplementedException();
        }

        public override char GetChar(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override long GetChars(int ordinal, long dataOffset, char[] buffer, int bufferOffset, int length)
        {
            throw new NotImplementedException();
        }

        public override Guid GetGuid(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override short GetInt16(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override int GetInt32(int ordinal)
        {
            return int.Parse(GetString(ordinal));
        }

        public override long GetInt64(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override DateTime GetDateTime(int ordinal)
        {
            return DateTime.Parse(_enumerator.Current[ordinal].ToString());
        }

        public override string GetString(int ordinal)
        {
            return _enumerator.Current[ordinal] != null ? 
                _enumerator.Current[ordinal].ToString() : string.Empty;
        }

        public override decimal GetDecimal(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override double GetDouble(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override float GetFloat(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override string GetDataTypeName(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override Type GetFieldType(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override object GetValue(int ordinal)
        {
            throw new NotImplementedException();
        }

        public override IEnumerator GetEnumerator()
        {
            return this._enumerator;
        }
    }
}
