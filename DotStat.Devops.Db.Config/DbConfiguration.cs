﻿using System.Collections.Generic;

namespace DotStat.Devops.Db.Config
{
    public class DbConfiguration
    {
        public Dictionary<string, DbConfig> Databases { get; set; }
    }

    public class DbConfig
    {
        public int DataMaxSize { get; set; }
        public int LogMaxSize { get; set; }
    }
}