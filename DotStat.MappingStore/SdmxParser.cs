﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.MappingStore.Exception;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Objects;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Registry;
using Org.Sdmxsource.Util.Extensions;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.MappingStore
{
    public static class SdmxParser
    {
        public static Dataflow ParseDataFlowStructure(ISdmxObjects sdmxObjects, ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies)
        {
            if (!sdmxObjects.Dataflows.Any())
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowNotFoundBySdmxParser));
            }

            var dataflow = sdmxObjects.Dataflows.First();

            if (dataflow.IsExternalReference?.IsTrue ?? false)
            {
                throw new ExternalDataflowException(
                    string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExternalDataflow),
                        $"{dataflow.AgencyId}:{dataflow.Id}({dataflow.Version})"));
            }

            if (!sdmxObjects.DataStructures.Any())
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DSDNotFoundBySdmxParser));
            }

            var sdmxDsd = sdmxObjects.DataStructures.First();
            var dsd = new Dsd(sdmxDsd);

            var msd = GetCurrentMsd(sdmxDsd, sdmxObjects.MetadataStructures);

            if (msd != null)
            {
                dsd.Msd = new Msd(msd);
            }

            // Return dataflow along with its dsd only.
            if (resolveCrossReferences == ResolveCrossReferences.DoNotResolve)
            {
                dsd.SetDimensions(sdmxDsd.DimensionList.Dimensions.Select((dim, index) => new Dimension(index, dim)));
                dsd.SetAttributes(sdmxDsd.Attributes.Select(attr => new Attribute(attr)));
                return new Dataflow(dataflow, dsd);
            }

            var sdmxCodelists = sdmxObjects.Codelists;
            var constraintResolver = new ConstraintResolver(sdmxObjects.ContentConstraintObjects.FirstOrDefault(c => !c.IsDefiningActualDataPresent));

            var sdmxCodelistsList = sdmxCodelists.ToList();
            dsd.SetDimensions(BuildDimensions(sdmxDsd, sdmxCodelistsList, constraintResolver));
            dsd.SetAttributes(BuildAttributes(sdmxDsd, sdmxCodelistsList, constraintResolver));
            dsd.SetPrimaryMeasure(BuildPrimaryMeasure(sdmxDsd, sdmxCodelistsList));

            return new Dataflow(dataflow, dsd);
        }

        public static Dataflow ParseStructure(IDataflowObject dataflowObject, IDataStructureObject dataStructure, IMetadataStructureDefinitionObject metadataStructureObject, IList<ICodelistObject> sdmxCodelists, IContentConstraintObject contentConstraintObject)
        {
            if (dataflowObject is null)
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowNotFoundBySdmxParser));
            }

            var dataflow = dataflowObject;

            if (dataflow.IsExternalReference?.IsTrue ?? false)
            {
                throw new ExternalDataflowException(
                    string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExternalDataflow),
                        $"{dataflow.AgencyId}:{dataflow.Id}({dataflow.Version})"));
            }

            if (dataStructure is null)
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DSDNotFoundBySdmxParser));
            }

            var dsd = new Dsd(dataStructure);

            if (metadataStructureObject != null)
            {
                dsd.Msd = new Msd(metadataStructureObject);
            }

            var constraintResolver = new ConstraintResolver(contentConstraintObject);

            dsd.SetDimensions(BuildDimensions(dataStructure, sdmxCodelists, constraintResolver));
            dsd.SetAttributes(BuildAttributes(dataStructure, sdmxCodelists, constraintResolver));
            dsd.SetPrimaryMeasure(BuildPrimaryMeasure(dataStructure, sdmxCodelists));

            return new Dataflow(dataflow, dsd);
        }

        public static Dsd ParseDsdStructure(ISdmxObjects sdmxObjects, ResolveCrossReferences resolveCrossReferences = ResolveCrossReferences.ResolveExcludeAgencies)
        {
            if (!sdmxObjects.DataStructures.Any())
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DSDNotFoundBySdmxParser));
            }

            var sdmxDsd = sdmxObjects.DataStructures.First();
            var dsd = new Dsd(sdmxDsd);

            var msd = GetCurrentMsd(sdmxDsd, sdmxObjects.MetadataStructures);

            if (msd != null)
            {
                dsd.Msd = new Msd(msd);
            }

            // Return dataflow along with its dsd only.
            if (resolveCrossReferences == ResolveCrossReferences.DoNotResolve)
            {
                return dsd;
            }

            var sdmxCodelists = sdmxObjects.Codelists;
            var constraintResolver = new ConstraintResolver(sdmxObjects.ContentConstraintObjects.FirstOrDefault(c => !c.IsDefiningActualDataPresent));

            var sdmxCodelistsList = sdmxCodelists.ToList();
            dsd.SetDimensions(BuildDimensions(sdmxDsd, sdmxCodelistsList, constraintResolver));
            dsd.SetAttributes(BuildAttributes(sdmxDsd, sdmxCodelistsList, constraintResolver));
            dsd.SetPrimaryMeasure(BuildPrimaryMeasure(sdmxDsd, sdmxCodelistsList));

            return dsd;
        }

        public static Dsd ParseStructure(IDataStructureObject dataStructure, IMetadataStructureDefinitionObject metadataStructureObject, IList<ICodelistObject> sdmxCodelists)
        {
            if (dataStructure is null)
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DSDNotFoundBySdmxParser));
            }

            if (dataStructure.IsExternalReference?.IsTrue ?? false)
            {
                throw new ExternalDataflowException(
                    string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExternalDataflow),
                $"{dataStructure.AgencyId}:{dataStructure.Id}({dataStructure.Version})"));
            }

            var sdmxDsd = dataStructure;
            var dsd = new Dsd(sdmxDsd);

            if (metadataStructureObject != null)
            {
                dsd.Msd = new Msd(metadataStructureObject);
            }

            var constraintResolver = new ConstraintResolver(null);

            dsd.SetDimensions(BuildDimensions(sdmxDsd, sdmxCodelists, constraintResolver));
            dsd.SetAttributes(BuildAttributes(sdmxDsd, sdmxCodelists, constraintResolver));
            dsd.SetPrimaryMeasure(BuildPrimaryMeasure(sdmxDsd, sdmxCodelists));

            return dsd;
        }

        private static IEnumerable<Dimension> BuildDimensions(
            IDataStructureObject sdmxDsd, 
            IList<ICodelistObject> sdmxCodelists,
            ConstraintResolver constraintResolver
        )
        {
            var index = 0;

            foreach (var dim in sdmxDsd.DimensionList.Dimensions)
            {
                Codelist codelist = null;

                if (!dim.TimeDimension)
                {
                    var agencyId = dim.Representation?.Representation?.AgencyId;
                    var codelistId = dim.Representation?.Representation?.MaintainableId;
                    var version = dim.Representation?.Representation?.Version;

                    codelist = !string.IsNullOrEmpty(codelistId)
                        ? BuildCodelist(sdmxCodelists.First(cl =>
                            cl.Id.Equals(codelistId, StringComparison.InvariantCultureIgnoreCase) &&
                            cl.AgencyId.Equals(agencyId, StringComparison.InvariantCultureIgnoreCase) &&
                            cl.Version.Equals(version, StringComparison.InvariantCultureIgnoreCase)))
                        : null;

                }

                yield return new Dimension(index++, dim, codelist, constraintResolver[dim.Id]);
            }
        }

        private static PrimaryMeasure BuildPrimaryMeasure(
            IDataStructureObject sdmxDsd,
            IList<ICodelistObject> sdmxCodelists
        )
        {
            if (sdmxDsd.PrimaryMeasure == null)
            {
                return null;
            }

            var agencyId = sdmxDsd.PrimaryMeasure.Representation?.Representation?.AgencyId;
            var codelistId = sdmxDsd.PrimaryMeasure.Representation?.Representation?.MaintainableId;
            var version = sdmxDsd.PrimaryMeasure.Representation?.Representation?.Version;

            var codelist = !string.IsNullOrEmpty(codelistId)
                ? BuildCodelist(sdmxCodelists.First(cl =>
                    cl.Id.Equals(codelistId, StringComparison.InvariantCultureIgnoreCase) &&
                    cl.AgencyId.Equals(agencyId, StringComparison.InvariantCultureIgnoreCase) &&
                    cl.Version.Equals(version, StringComparison.InvariantCultureIgnoreCase)))
                : null;

            return new PrimaryMeasure(sdmxDsd.PrimaryMeasure, codelist);
        }

        private static IEnumerable<Attribute> BuildAttributes(
            IDataStructureObject sdmxDsd,
            IList<ICodelistObject> sdmxCodelists, 
            ConstraintResolver constraintResolver
        )
        {
            foreach (var attr in sdmxDsd.Attributes)
            {

                var agencyId = attr.Representation?.Representation?.AgencyId;
                var codelistId = attr.Representation?.Representation?.MaintainableId;
                var version = attr.Representation?.Representation?.Version;

                var codelist = !string.IsNullOrEmpty(codelistId)
                    ? BuildCodelist(sdmxCodelists.First(cl =>
                        cl.Id.Equals(codelistId, StringComparison.InvariantCultureIgnoreCase) &&
                        cl.AgencyId.Equals(agencyId, StringComparison.InvariantCultureIgnoreCase) &&
                        cl.Version.Equals(version, StringComparison.InvariantCultureIgnoreCase)))
                    : null;

                yield return new Attribute(attr, codelist, constraintResolver[attr.Id]);
            }
        }

        private static Codelist BuildCodelist(ICodelistObject sdmxCodelist)
        {
            var codelist = new Codelist(sdmxCodelist);

            var hash = sdmxCodelist.Items.ToDictionary(c => c.Id, c => new Code(codelist, c));

            foreach (var code in hash.Values.Where(c => c.Base.ParentCode != null))
            {
                if (hash.ContainsKey(code.Base.ParentCode))
                {
                    hash[code.Base.ParentCode].AddChildCode(code);
                }
            }

            codelist.SetCodes(hash.Values);

            return codelist;
        }

        private class ConstraintResolver
        {
            private readonly Dictionary<string, Constraint> _dict = new Dictionary<string, Constraint>();

            public ConstraintResolver(IContentConstraintObject c)
            {
                if (c?.IncludedCubeRegion == null)
                    return;

                _dict
                    .AddAll(GetConstraints(c, c.IncludedCubeRegion.KeyValues, true));

                _dict
                    .AddAll(GetConstraints(c, c.IncludedCubeRegion.AttributeValues, true));

                // todo do we need check excluded constraints ?
            }

            private IEnumerable<KeyValuePair<string, Constraint>> GetConstraints(IContentConstraintObject c, IList<IKeyValues> values, bool include)
            {
                return values.Select(kv => new KeyValuePair<string, Constraint>(kv.Id, new Constraint(c, true, kv.Values, kv.TimeRange)));
            }

            public Constraint this[string code] => _dict.ContainsKey(code) ? _dict[code] : null;

        }

        private static IMetadataStructureDefinitionObject GetCurrentMsd(IDataStructureObject dsd, ISet<IMetadataStructureDefinitionObject> msdSet)
        {
            if (!dsd.Annotations.Any() || msdSet == null || !msdSet.Any())
            {
                return null;
            }

            var msdAnnotation = dsd.Annotations.FirstOrDefault(x => x.Type.Equals("Metadata", StringComparison.OrdinalIgnoreCase));

            if (msdAnnotation == null || string.IsNullOrEmpty(msdAnnotation.Title))
            {
                return null;
            }

            return msdSet.FirstOrDefault(x => x.Urn.ToString().Equals(msdAnnotation.Title, StringComparison.OrdinalIgnoreCase));
        }
    }
}