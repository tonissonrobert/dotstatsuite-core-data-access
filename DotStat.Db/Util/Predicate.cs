﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

namespace DotStat.Db.Util
{
    public enum NullTreatment
    {
        IncludeNulls,
        IncludeNullsAndSwitchOff,
        ExcludeNulls
    }

    public abstract class Predicate
    {
        public static string BuildWhereForObservationLevel(
            IDataQuery dataQuery, 
            Dataflow dataFlow,
            ICodeTranslator codeTranslator,
            bool filterLastUpdated = false,
            NullTreatment nullTreatment = NullTreatment.ExcludeNulls,
            bool useExternalValues = true,
            bool includeTimeConstraints = true,
            string tableAlias = ""
        )
        {
            var where = BuildDimQuery(dataQuery, dataFlow, codeTranslator, nullTreatment, true, useExternalValues, includeTimeConstraints, tableAlias);

            //Last updated
            if (filterLastUpdated && dataQuery?.LastUpdatedDate is not null)
                where.Add($"[{DbExtensions.LAST_UPDATED_COLUMN}] >= @LastUpdated");

            return where.Any() 
                ? string.Join(" AND ", where) 
                : null;
        }
       
        private static List<string> BuildDimQuery(
            IDataQuery dataQuery,
            Dataflow dataFlow,
            ICodeTranslator codeTranslator,
            NullTreatment nullTreatment,
            bool applyConstraints,
            bool useExternalValues,
            bool includeTimeConstraints,
            string tableAlias
        )
        {
            var where = new List<string>();
            var appliedConstraints = new HashSet<string>();
            var querySelectionGroup = dataQuery?.SelectionGroups?.FirstOrDefault();
            var tablePrefix = string.IsNullOrEmpty(tableAlias) ? null : $"[{tableAlias}].";

            //Apply query filters if any given by the user
            if (querySelectionGroup != null)
            {
                // non time dimensions
                foreach (var selection in querySelectionGroup.Selections)
                {
                    var filteredSelection = new List<string>(selection.Values.Count);
                    var dim = dataFlow.FindDimension(selection.ComponentId);

                    // filter out non-existing codes & constrained codes from user selection
                    foreach (var mem in selection.Values)
                    {
                        if (dim.Base.HasCodedRepresentation() && !dim.HasCode(mem))
                            continue; // Non existing code

                        if (applyConstraints && !dim.IsAllowed(mem))
                            continue; // Constrained code

                        var value = useExternalValues || dim.Codelist == null
                            ? $"'{mem}'" 
                            : codeTranslator.TranslateCodeToId(dim.Code, mem).ToString();
                        
                        filteredSelection.Add(value);
                    }

                    if (!filteredSelection.Any())
                        throw new SdmxNoResultsException();
                    
                    var values = filteredSelection.ToList();

                    var predicate = new CodelistPredicate(
                        $"{tablePrefix}{dim.SqlColumn(externalColumn: useExternalValues)}",
                        values,
                        nullTreatment,
                        useExternalValues
                    );

                    where.Add(predicate.ToString());

                    // As selection already narrowed down by user selection no need to apply dim constraint again
                    appliedConstraints.Add(dim.Code);
                }
            }

            if (!applyConstraints)
                return where;

            //No dimension constraints if dataFlow ref is null
            if (dataFlow == null)
                return where;

            // Apply dimension constraints ------------------------------
            foreach (var dim in dataFlow.Dimensions.Where(d => !appliedConstraints.Contains(d.Code) && d.Constraint?.IsCodeListBased == true))
            {
                var values = dim.Constraint.Codes
                    .Where(code => dim.Codelist.Codes.Any(cl_code => cl_code.Code.Contains(code)))
                    .Select(code => useExternalValues ? $"'{code}'" : codeTranslator.TranslateCodeToId(dim.Code, code).ToString());

                if (!values.Any())
                    continue;

                var predicate = new CodelistPredicate(
                    $"{tablePrefix}{dim.SqlColumn(externalColumn: useExternalValues)}",
                    values,
                    nullTreatment,
                    useExternalValues
                );

                where.Add(predicate.ToString());
            }

            // Apply time constraint/query range filter
            if(includeTimeConstraints)
                AddTimePredicate(where, dataFlow, querySelectionGroup, tableAlias, nullTreatment);

            return where;
        }

        #region Time predicate

        public static void AddTimePredicate(
            List<string> where,
            Dataflow dataFlow,
            IDataQuerySelectionGroup querySelectionGroup,
            string tableAlias,
            NullTreatment nullTreatment = NullTreatment.ExcludeNulls
        )
        {
            var timeDim = dataFlow.Dsd.TimeDimension;

            if (timeDim == null)
                return;

            var timeRange = timeDim.Constraint?.TimeRange;

            var startDate = Max(querySelectionGroup?.DateFrom?.Date, timeRange?.StartDate?.Date);
            var endDate = Min(querySelectionGroup?.DateTo?.EndDate(), timeRange?.EndDate?.Date);

            // todo refactor predicate logic to use db parameters

            var tablePrefix = string.IsNullOrEmpty(tableAlias) ? null : $"[{tableAlias}].";
            if (startDate != null)
            {
                var timeClause = $"{tablePrefix}{DbExtensions.SqlPeriodEnd()}>='{startDate:yyyy-MM-dd}'";
                var nullClause = "";
                if (nullTreatment is NullTreatment.IncludeNulls)
                {
                    nullClause = $" OR {tablePrefix}{DbExtensions.SqlPeriodEnd()} IS NULL";
                }
                else if (nullTreatment is NullTreatment.IncludeNullsAndSwitchOff)
                {
                    var switchOffValue = dataFlow.Dsd.Base.HasSupportDateTimeAnnotation() ?
                        $"{DateTime.MinValue:yyyy-MM-dd hh:mm:ss}" : $"{DateTime.MinValue.Date:yyyy-MM-dd}";

                    nullClause = $" OR {tablePrefix}{DbExtensions.SqlPeriodEnd()} IS NULL OR {tablePrefix}{DbExtensions.SqlPeriodEnd()} = '{switchOffValue}'";
                }

                where.Add($"({timeClause}{nullClause})");
            }

            if (endDate != null)
            {
                var timeClause = $"{tablePrefix}{DbExtensions.SqlPeriodStart()}<'{endDate:yyyy-MM-dd}'";
                var nullClause = "";
                if (nullTreatment is NullTreatment.IncludeNulls)
                {
                    nullClause = $" OR {tablePrefix}{DbExtensions.SqlPeriodStart()} IS NULL";
                }
                else if (nullTreatment is NullTreatment.IncludeNullsAndSwitchOff)
                {
                    var switchOffValue = dataFlow.Dsd.Base.HasSupportDateTimeAnnotation() ?
                        $"{DateTime.MaxValue:yyyy-MM-dd hh:mm:ss}" : $"{DateTime.MaxValue.Date:yyyy-MM-dd}";

                    nullClause = $" OR {tablePrefix}{DbExtensions.SqlPeriodStart()} IS NULL OR {tablePrefix}{DbExtensions.SqlPeriodStart()} = '{switchOffValue}'";
                }

                where.Add($"({timeClause}{nullClause})");
            }
        }

        private static DateTime? Max(DateTime? dt1, DateTime? dt2) => dt2 == null || dt1 > dt2 ? dt1 : dt2;

        private static DateTime? Min(DateTime? dt1, DateTime? dt2) => dt2 == null || dt1 < dt2 ? dt1 : dt2;

        #endregion
        
        public abstract string ToString(NullTreatment nullTreatment);
    }
}