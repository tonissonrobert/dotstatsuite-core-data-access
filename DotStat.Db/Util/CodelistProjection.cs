﻿using System;
using System.Collections.Generic;

namespace DotStat.Db.Util
{
    public interface ICodelistProjection
    {
        string this[int index] { get; }

        Dictionary<string, int> Map { get; }
    }

    public abstract class CodelistProjectionBase<T> : ICodelistProjection where T : class
    {
        private readonly Lazy<Dictionary<string, int>> _lazy;

        /// <summary>
        /// index of array is sql database ID
        /// </summary>
        protected readonly T[] List;

        protected CodelistProjectionBase(T[] list)
        {
            List = list;
            _lazy = new Lazy<Dictionary<string, int>>(ToDic);
        }

        public string this[int index] => List[index].ToString();

        public Dictionary<string, int> Map => _lazy.Value;

        private Dictionary<string, int> ToDic()
        {
            var dic = new Dictionary<string, int>();

            if (List != null && List.Length > 0)
            {
                for (var i = 1; i < List.Length; i++)
                {
                    // The ids may not be in consecutive order, there might be gaps, e.g. due to failed and rolled-back transaction
                    if (List[i] != null)
                    {
                        dic[List[i].ToString()] = i;
                    }
                }
            }

            return dic;
        }
    }

    public class CodelistProjection : CodelistProjectionBase<string>
    {
        public CodelistProjection(string[] list) : base(list)
        {
        }
    }
}