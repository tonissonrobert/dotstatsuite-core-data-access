﻿using DotStat.Db.Validation;
using DotStat.Domain;
using System.Collections.Generic;

namespace DotStat.DB.Util
{
    public class BulkImportResult
    {
        public IList<DataSetAttributeRow> DataSetLevelAttributeRows;
        public List<IValidationError> Errors = new();
        public int RowsCopied;
        public int NumberOfBatches; 
        public readonly Dictionary<int, BatchAction> BatchActions = new ();

        public BulkImportResult()
        {
        }

        public BulkImportResult(IList<DataSetAttributeRow> dataSetLevelAttributeRows, List<IValidationError> errors, int rowsCopied, Dictionary<int, BatchAction> batchActions)
        {
            DataSetLevelAttributeRows = dataSetLevelAttributeRows;
            Errors = errors;
            RowsCopied = rowsCopied;
            BatchActions = batchActions;
        }
    }
}
