﻿using System.Threading;
using System.Threading.Tasks;
using DotStat.Domain;

namespace DotStat.Db.Util
{
    public interface ICodeTranslator
    {
        Task FillDict(IImportReferenceableStructure referencedStructure, CancellationToken cancellationToken);

        int TranslateCodeToId(string dimensionCode, string itemCode);

        ValueTask<ICodelistProjection> this[IDotStatCodeListBasedIdentifiable artefact, CancellationToken cancellationToken] { get; }

        ICodelistProjection this[string dimCode] { get; }
    }
}