﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Localization;
using DotStat.Db.Exception;
using DotStat.Db.Repository;
using DotStat.Domain;

namespace DotStat.Db.Util
{
    public class CodeTranslator : ICodeTranslator
    {
        protected readonly Dictionary<string, ICodelistProjection> TranslatorCache;

        protected ICodelistRepository CodelistRepository { get; set;}

        public CodeTranslator(ICodelistRepository codelistRepository)
        {
            CodelistRepository = codelistRepository;
            TranslatorCache = new Dictionary<string, ICodelistProjection>();
        }

        public async Task FillDict(IImportReferenceableStructure referencedStructure, CancellationToken cancellationToken)
        {
            if (referencedStructure is Dataflow dataFlow)
                await FillDsdDict(dataFlow.Dsd, cancellationToken);
            else
                await FillDsdDict(referencedStructure as Dsd, cancellationToken);
        }
        
        public int TranslateCodeToId(string dimensionCode, string itemCode)
        {
            if (!TranslatorCache.TryGetValue(dimensionCode, out var codelistProjection))
            {
                throw new DimensionNotFoundByTranslatorException(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DimensionNotFoundByCodeListTranslator), dimensionCode));
            }

            if (!codelistProjection.Map.TryGetValue(itemCode, out var itemId))
            {
                throw new CodeNotFoundByTranslatorException(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CodeNotFoundForDimension), itemCode, dimensionCode));
            }

            return itemId;
        }

        public ValueTask<ICodelistProjection> this[IDotStatCodeListBasedIdentifiable artefact, CancellationToken cancellationToken] =>  GetCodelistProjection(artefact, cancellationToken);

        public ICodelistProjection this[string dimCode] => TranslatorCache[dimCode];

        private async ValueTask<ICodelistProjection> GetCodelistProjection(IDotStatCodeListBasedIdentifiable artefact, CancellationToken cancellationToken)
        {
            if(artefact == null)
                throw new InvalidOperationException("Artefact is null");

            if(artefact.Codelist == null)
                throw new InvalidOperationException("Artefact codelist is null");

            if (!TranslatorCache.TryGetValue(artefact.Code, out var clProjection))
            {
                TranslatorCache[artefact.Code] = clProjection = new CodelistProjection(await CodelistRepository.GetDimensionCodesFromDb(artefact.Codelist.DbId, cancellationToken));
            }

            return clProjection;
        }

        private async Task FillDsdDict(Dsd dsd, CancellationToken cancellationToken)
        {
            foreach (var dim in dsd.Dimensions.Where(dim => dim.Base.HasCodedRepresentation()))
                await GetCodelistProjection(dim, cancellationToken);

            foreach (var attr in dsd.Attributes.Where(x => x.Base.HasCodedRepresentation()))
                await GetCodelistProjection(attr, cancellationToken);
        }
    }
}
