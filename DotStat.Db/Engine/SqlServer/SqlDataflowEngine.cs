﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlDataflowEngine : DataflowEngineBase<SqlDotStatDb>
    {

        public SqlDataflowEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var aretefactDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT [ART_ID] 
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ID] = @Id AND [AGENCY] = @Agency AND [TYPE] = @Type
                     AND [VERSION_1] = @Version1 AND [VERSION_2] = @Version2 
                     AND ([VERSION_3] = @Version3 OR ([VERSION_3] IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) {Value = sdmxId},
                new SqlParameter("Agency", SqlDbType.VarChar) {Value = sdmxAgency},                
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dataflow) },
                new SqlParameter("Version1", SqlDbType.Int) {Value = verMajor},
                new SqlParameter("Version2", SqlDbType.Int) {Value = verMinor},
                new SqlParameter("Version3", SqlDbType.Int) {Value = ((object) verPatch) ?? DBNull.Value}
                );

            return (int) (aretefactDbId ?? -1);
        }

        public override async Task<int> InsertToArtefactTable(Dataflow dataflow, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var artefactId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"INSERT 
                    INTO [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                         ([SQL_ART_ID],[TYPE],[ID],[AGENCY],[VERSION_1],[VERSION_2],[VERSION_3],[LAST_UPDATED],[DF_DSD_ID],[DF_WHERE_CLAUSE]) 
                  OUTPUT INSERTED.ART_ID 
                  VALUES (NULL, @Type, @Id, @Agency, @Version1, @Version2, @Version3, @DT_Now, @DsdId, NULL)",
                cancellationToken,
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dataflow) },
                new SqlParameter("Id", SqlDbType.VarChar) {Value = dataflow.Code},
                new SqlParameter("Agency", SqlDbType.VarChar) {Value = dataflow.AgencyId},
                new SqlParameter("Version1", SqlDbType.Int) {Value = dataflow.Version.Major},
                new SqlParameter("Version2", SqlDbType.Int) {Value = dataflow.Version.Minor},
                new SqlParameter("Version3", SqlDbType.Int) {Value = (object) dataflow.Version.Patch ?? DBNull.Value},
                new SqlParameter("DsdId", SqlDbType.Int) {Value = dataflow.Dsd.DbId},
                new SqlParameter("DT_Now", DateTime.Now)
                );

            return (int) artefactId;
        }

        protected override async Task DeleteFromArtefactTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ART_ID] = @DbId AND [TYPE] = 'DF'",
                cancellationToken,
                new SqlParameter("DbId", SqlDbType.VarChar) { Value = dbId }
                );
        }

        protected override async Task BuildDynamicDataDataFlowView(Dataflow dataFlow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The dependent view does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.ViewExists($"{dataFlow.Dsd.SqlDataDsdViewName(targetVersion)}", cancellationToken))
            {
                return;
            }

            //Dimensions
            var dimensionsColumns = ", " + string.Join(", ", dataFlow.Dsd.Dimensions.Select(d => $"[{d.Code}]"));

            //Measure
            var measureColumn = $", [{dataFlow.Dsd.Base.PrimaryMeasure.Id}]";

            //Attributes
            var attributeColumns = string.Empty;
            var attributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel != AttributeAttachmentLevel.DataSet).ToList();

            if (attributes.Any())
            {
                attributeColumns = ", " + string.Join(", ", attributes.Select(a => $"[{a.Code}]"));
            }

            //Dataset attributes
            var dataSetAttributesColumns = string.Empty;
            var joinDataSetAttributes = string.Empty;
            var joinCodedDataSetAttributes = string.Empty;
            var dataSetAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList();

            if (dataSetAttributes.Any())
            {
                dataSetAttributesColumns = ", " + string.Join(", ", dataSetAttributes.Select(a =>
                    a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[ADF].{a.SqlColumn()} AS [{a.Code}]"));

                joinDataSetAttributes = $@"FULL JOIN  ( SELECT * FROM [{dotStatDb.DataSchema}].[{dataFlow.Dsd.SqlDsdAttrTable(targetVersion)}] WHERE [DF_ID] = {dataFlow.DbId} ) [ADF] ON [ADF].[DF_ID] = {dataFlow.DbId}";

                joinCodedDataSetAttributes = string.Join("\n",
                    dataSetAttributes.Where(a => a.Base.HasCodedRepresentation())
                        .Select(a => 
                            $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON [ADF].[DF_ID] = {dataFlow.DbId} AND [ADF].{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            //Time dimension columns
            var timeDimensionColumns = string.Empty;

            if (dataFlow.Dsd.TimeDimension != null)
            {
                timeDimensionColumns = ", " + string.Join(", ", DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd());
            }

            // LAST_UPDATED
            var updatedAfterColumns = $",[V].{DbExtensions.LAST_UPDATED_COLUMN}";

            var sqlCommand = 
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlDataDataFlowViewName(targetVersion)} 
                   AS
                       SELECT [SID]{dimensionsColumns}
                            {measureColumn}
                            {attributeColumns}
                            {dataSetAttributesColumns}
                            {timeDimensionColumns}   
                            {updatedAfterColumns}
                       FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDataDsdViewName(targetVersion)} AS V 
                       {joinDataSetAttributes}
                       {joinCodedDataSetAttributes}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        protected override async Task BuildDynamicDataReplaceDataFlowView(Dataflow dataFlow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Dimensions
            var dimensions = dataFlow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();
            var dimensionsJoin = string.Join("\n", dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON FI.{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                ));

            var dimensionsSelect = string.Join(", ", dimensions.Select(d =>
                d.Base.HasCodedRepresentation() ? $"[CL_{d.Code}].[ID] AS [{d.Code}]" : $"[FI].{d.SqlColumn()} AS [{d.Code}]")) + ',';

            var dimensionsSelectNull = string.Join(", ", dimensions.Select(d => $"NULL AS [{d.Code}]")) + ',';
            //time dimension
            var timeDim = dataFlow.Dsd.TimeDimension;
            var timeDimSelect = string.Empty;
            var timeDimSelectNull = string.Empty;
            if (timeDim != null)
            {
                timeDimSelect = string.Join(", ",
                        $"[FA].{timeDim.SqlColumn()} AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                        DbExtensions.SqlPeriodStart(),
                        DbExtensions.SqlPeriodEnd()) + ',';
                timeDimSelectNull = string.Join(", ",
                        $"NULL AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                        $"NULL AS {DbExtensions.SqlPeriodStart()}", 
                        $"NULL AS {DbExtensions.SqlPeriodEnd()}") + ',';
            }

            //Measure
            var measureColumnSelect = $" [FA].[VALUE] AS [{dataFlow.Dsd.Base.PrimaryMeasure.Id}],";
            var measureColumnSelectNull = $" NULL AS [{dataFlow.Dsd.Base.PrimaryMeasure.Id}],";

            //Attributes
            //Obs level
            var observationAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                               (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                               attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();
            
            var obsLvlAttrSelect = string.Empty;
            var obsLvlAttrSelectNull = string.Empty;
            var obsLvlAttrJoin = string.Empty;
            if (observationAttributes.Any())
            {
                obsLvlAttrSelect = string.Join(", ", observationAttributes.Select(a => a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[FA].{a.SqlColumn()} AS [{a.Code}]")) + ',';

                obsLvlAttrSelectNull = string.Join(", ", observationAttributes.Select(a => $"NULL AS [{a.Code}]")) + ',';

                obsLvlAttrJoin = string.Join("\n", observationAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                         $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON FA.{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            //dimGroup level
            var dimGroupAttributes = dataFlow.Dsd.Attributes.Where(attr =>
                    (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                    !attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            var dimGroupLvlAttrSelect = string.Empty;
            var dimGroupLvlAttrSelectNull = string.Empty;
            var dimGroupLvlAttrJoin = string.Empty;
            var hasdimGroupLevelAttributes = false;
            if (dimGroupAttributes.Any())
            {
                hasdimGroupLevelAttributes = true;
                dimGroupLvlAttrSelect = string.Join(", ", dimGroupAttributes.Select(a => a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[ATTR].{a.SqlColumn()} AS [{a.Code}]")) + ',';

                dimGroupLvlAttrSelectNull = string.Join(", ", dimGroupAttributes.Select(a => $"NULL AS [{a.Code}]")) + ',';

                dimGroupLvlAttrJoin = string.Join("\n", dimGroupAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                            $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON ATTR.{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            //dataSet level
            var dataSetAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList();

            var dataSetAttrLvlSelect = string.Empty;
            var dataSetAttrLvlSelectNull = string.Empty;
            var dataSetAttrLvlJoin = string.Empty;
            var hasDataSetLevelAttributes = false;
            if (dataSetAttributes.Any())
            {
                hasDataSetLevelAttributes = true;
                dataSetAttrLvlSelect = string.Join(", ", dataSetAttributes.Select(a => a.Base.HasCodedRepresentation()
                    ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                    : $"[ADF].{a.SqlColumn()} AS [{a.Code}]")) + ',';

                dataSetAttrLvlSelectNull = string.Join(", ", dataSetAttributes.Select(a => $"NULL AS [{a.Code}]")) + ',';

                dataSetAttrLvlJoin = string.Join("\n", dataSetAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                         $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON [ADF].[DF_ID] = {dataFlow.DbId} AND [ADF].{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            //Tables
            var filterTable = $"[{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlFilterTable("FI")}";

            var ctePart = 
                $@"WITH OBS_LEVEL AS (
	SELECT FI.[SID], {dimensionsSelect}{timeDimSelect}
        {measureColumnSelect}
        {obsLvlAttrSelect}
	    {dimGroupLvlAttrSelectNull}
	    {dataSetAttrLvlSelectNull}
	    [{DbExtensions.LAST_UPDATED_COLUMN}]
	 FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlFactTable(targetVersion, "FA")}
	 LEFT JOIN {filterTable} ON FI.[SID] = FA.[SID]
	 {dimensionsJoin}
	 {obsLvlAttrJoin}
 )";

            var subQueryPart =  $@"
    SELECT * FROM OBS_LEVEL";

            if (hasdimGroupLevelAttributes)
            {
                ctePart += $@", SERIES_LEVEL AS (
	SELECT FI.[SID], {dimensionsSelect}{timeDimSelectNull}
        {measureColumnSelectNull}
        {obsLvlAttrSelectNull}
	    {dimGroupLvlAttrSelect}
	    {dataSetAttrLvlSelectNull}
	    [{DbExtensions.LAST_UPDATED_COLUMN}]
	 FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDimGroupAttrTable(targetVersion, "ATTR")}
	 LEFT JOIN {filterTable} ON FI.[SID] = ATTR.[SID]
	 {dimensionsJoin}
	 {dimGroupLvlAttrJoin}
 )";

                subQueryPart += $@"
UNION ALL
    SELECT * FROM SERIES_LEVEL";

            }

            if (hasDataSetLevelAttributes)
            {
                ctePart += $@", DF_LEVEL AS (
	SELECT NULL AS [SID], {dimensionsSelectNull}{timeDimSelectNull}
        {measureColumnSelectNull}
        {obsLvlAttrSelectNull}
	    {dimGroupLvlAttrSelectNull}
	    {dataSetAttrLvlSelect}
	    [{DbExtensions.LAST_UPDATED_COLUMN}]
	FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDsdAttrTable(targetVersion, "ADF")}
	{dataSetAttrLvlJoin}
	WHERE [ADF].[DF_ID] = {dataFlow.DbId}
 )";

                subQueryPart += $@"
UNION ALL
    SELECT * FROM DF_LEVEL";

            }

            var viewDefinition = $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlDataReplaceDataFlowViewName(targetVersion)} AS
{ctePart}
{subQueryPart}
";

            await dotStatDb.ExecuteNonQuerySqlAsync(viewDefinition, cancellationToken);
        }
        
        protected override async Task BuildDynamicDataIncludeHistoryDataFlowView(Dataflow dataFlow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var allComponentsSelect = "[SID]";

            //Dimensions
            var dimensions = dataFlow.Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();

            allComponentsSelect += ", " + dimensions.ToColumnList(externalColumn : true); 
            var dimensionsJoin = string.Join("\n", dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON FI.{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                ));

            var dimensionsSelect = string.Join(", ", dimensions.Select(d =>
                d.Base.HasCodedRepresentation() ? $"[CL_{d.Code}].[ID] AS [{d.Code}]" : $"[FI].{d.SqlColumn()} AS [{d.Code}]")) + ',';

            var dimensionsSelectNull = string.Join(", ", dimensions.Select(d => $"NULL AS [{d.Code}]")) + ',';
            //time dimension
            var timeDim = dataFlow.Dsd.TimeDimension;
            var timeDimSelect = string.Empty;
            var timeDimSelectNull = string.Empty;
            if (timeDim != null)
            {
                allComponentsSelect += ", " + string.Join(", ",
                    $"[{dataFlow.Dsd.Base.TimeDimension.Id}]",
                    DbExtensions.SqlPeriodStart(),
                    DbExtensions.SqlPeriodEnd());

               timeDimSelect = string.Join(", ",
                        $"[FA].{timeDim.SqlColumn()} AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                        DbExtensions.SqlPeriodStart(),
                        DbExtensions.SqlPeriodEnd()) + ',';
                timeDimSelectNull = string.Join(", ",
                        $"NULL AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                        $"NULL AS {DbExtensions.SqlPeriodStart()}",
                        $"NULL AS {DbExtensions.SqlPeriodEnd()}") + ',';
            }

            //Measure
            var measureColumnSelect = $" [FA].[VALUE] AS [{dataFlow.Dsd.Base.PrimaryMeasure.Id}],";
            var measureColumnSelectNull = $" NULL AS [{dataFlow.Dsd.Base.PrimaryMeasure.Id}],";

            //Attributes
            //Obs level
            var observationAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                               (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                               attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            var obsLvlAttrSelect = string.Empty;
            var obsLvlAttrSelectNull = string.Empty;
            var obsLvlAttrJoin = string.Empty;
            if (observationAttributes.Any())
            {
                obsLvlAttrSelect = string.Join(", ", observationAttributes.Select(a => a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[FA].{a.SqlColumn()} AS [{a.Code}]")) + ',';

                obsLvlAttrSelectNull = string.Join(", ", observationAttributes.Select(a => $"NULL AS [{a.Code}]")) + ',';

                obsLvlAttrJoin = string.Join("\n", observationAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                         $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON FA.{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            //dimGroup level
            var dimGroupAttributes = dataFlow.Dsd.Attributes.Where(attr =>
                    (attr.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || attr.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                    !attr.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            var dimGroupLvlAttrSelect = string.Empty;
            var dimGroupLvlAttrSelectNull = string.Empty;
            var dimGroupLvlAttrJoin = string.Empty;
            var hasdimGroupLevelAttributes = false;
            if (dimGroupAttributes.Any())
            {
                hasdimGroupLevelAttributes = true;
                dimGroupLvlAttrSelect = string.Join(", ", dimGroupAttributes.Select(a => a.Base.HasCodedRepresentation()
                        ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                        : $"[ATTR].{a.SqlColumn()} AS [{a.Code}]")) + ',';

                dimGroupLvlAttrSelectNull = string.Join(", ", dimGroupAttributes.Select(a => $"NULL AS [{a.Code}]")) + ',';

                dimGroupLvlAttrJoin = string.Join("\n", dimGroupAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                            $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON ATTR.{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            //dataSet level
            var dataSetAttributes = dataFlow.Dsd.Attributes
                .Where(attr => attr.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToList();

            var dataSetAttrLvlSelect = string.Empty;
            var dataSetAttrLvlSelectNull = string.Empty;
            var dataSetAttrLvlJoin = string.Empty;
            var hasDataSetLevelAttributes = false;
            if (dataSetAttributes.Any())
            {
                hasDataSetLevelAttributes = true;
                dataSetAttrLvlSelect = string.Join(", ", dataSetAttributes.Select(a => a.Base.HasCodedRepresentation()
                    ? $"[CL_{a.Code}].[ID] AS [{a.Code}]"
                    : $"[ADF].{a.SqlColumn()} AS [{a.Code}]")) + ',';

                dataSetAttrLvlSelectNull = string.Join(", ", dataSetAttributes.Select(a => $"NULL AS [{a.Code}]")) + ',';

                dataSetAttrLvlJoin = string.Join("\n", dataSetAttributes.Where(a => a.Base.HasCodedRepresentation()).Select(a =>
                         $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{a.Codelist.DbId}] AS [CL_{a.Code}] ON [ADF].[DF_ID] = {dataFlow.DbId} AND [ADF].{a.SqlColumn()} = [CL_{a.Code}].[ITEM_ID]"));
            }

            var allAttributesSelect = dataFlow.Dsd.Attributes.ToColumnList(externalColumn: true);
            if (!string.IsNullOrEmpty(allAttributesSelect))
                allComponentsSelect += "," + allAttributesSelect;
            allComponentsSelect += $",[{dataFlow.Dsd.Base.PrimaryMeasure.Id}]";
            allComponentsSelect += $",[{DbExtensions.LAST_UPDATED_COLUMN}], [ValidTo]";

            //Tables
            var filterTable = $"[{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlFilterTable("FI")}";

            var ctePart =
                $@"WITH OBS_LEVEL AS (
	SELECT FI.[SID], {dimensionsSelect}{timeDimSelect}
        {measureColumnSelect}
        {obsLvlAttrSelect}
	    {dimGroupLvlAttrSelectNull}
	    {dataSetAttrLvlSelectNull}
		[FA].[ValidFrom] AS [{DbExtensions.LAST_UPDATED_COLUMN}],
        [FA].[ValidTo] 
	 FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlFactTable(targetVersion)} FOR SYSTEM_TIME ALL FA
	 LEFT JOIN {filterTable} ON FI.[SID] = FA.[SID]
	 {dimensionsJoin}
	 {obsLvlAttrJoin}
 )";

            var subQueryPart = @$"
    SELECT {allComponentsSelect} FROM OBS_LEVEL";

            if (hasdimGroupLevelAttributes)
            {
                ctePart += $@", SERIES_LEVEL AS (
	SELECT FI.[SID], {dimensionsSelect}{timeDimSelectNull}
        {measureColumnSelectNull}
        {obsLvlAttrSelectNull}
	    {dimGroupLvlAttrSelect}
	    {dataSetAttrLvlSelectNull}
		ATTR.[ValidFrom] AS [{DbExtensions.LAST_UPDATED_COLUMN}],
        ATTR.[ValidTo]
	 FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDimGroupAttrTable(targetVersion)} FOR SYSTEM_TIME ALL ATTR
	 LEFT JOIN {filterTable} ON FI.[SID] = ATTR.[SID]
	 {dimensionsJoin}
	 {dimGroupLvlAttrJoin}
 )";

                subQueryPart += @$"
UNION ALL
    SELECT {allComponentsSelect} FROM SERIES_LEVEL";

            }

            if (hasDataSetLevelAttributes)
            {
                ctePart += $@", DF_LEVEL AS (
	SELECT NULL AS [SID], {dimensionsSelectNull}{timeDimSelectNull}
        {measureColumnSelectNull}
        {obsLvlAttrSelectNull}
	    {dimGroupLvlAttrSelectNull}
	    {dataSetAttrLvlSelect}
		ADF.[ValidFrom] AS [{DbExtensions.LAST_UPDATED_COLUMN}],
        ADF.[ValidTo]
	FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDsdAttrTable(targetVersion)} FOR SYSTEM_TIME ALL ADF
	{dataSetAttrLvlJoin}
	WHERE [ADF].[DF_ID] = {dataFlow.DbId}
 )";

                subQueryPart += @$"
UNION ALL
    SELECT {allComponentsSelect} FROM DF_LEVEL";

            }

            var viewDefinition = $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlDataIncludeHistoryDataFlowViewName(targetVersion)} AS
{ctePart}
{subQueryPart}
";

            await dotStatDb.ExecuteNonQuerySqlAsync(viewDefinition, cancellationToken);
        }

        protected override async Task BuildDeletedView(Dataflow dataFlow, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dataFlow.Dsd.SqlDeletedTable(targetVersion)}", cancellationToken))
            {
                return;
            }

            var dimensions = dataFlow.Dsd.Dimensions
                .Where(dim => !dim.Base.TimeDimension)
                .ToList();

            //Time dimension columns
            var timeDimensionColumns = dataFlow.Dsd.TimeDimension != null
                    ? ", " + string.Join(", ",
                            $"[DEL].[{DbExtensions.TimeDimColumn}] AS [{dataFlow.Dsd.Base.TimeDimension.Id}]",
                            DbExtensions.SqlPeriodStart(), 
                            DbExtensions.SqlPeriodEnd()
                        )
                    : string.Empty;

            //Dimensions
            var dimensionsColumns = dimensions.Any()
                        ? ", " +   string.Join(", ", dimensions.Select(d => d.Base.HasCodedRepresentation() ? $"[CL_{d.Code}].[ID] AS [{d.Code}]" : $"[DEL].{d.SqlColumn()} AS [{d.Code}]")) 
                        : string.Empty;

            //Attributes
            var attributeColumns = dataFlow.Dsd.Attributes.Any()
                ? "," + string.Join(",", dataFlow.Dsd.Attributes.Select(a => $"[DEL].{a.SqlColumn()} as [{a.Code}]"))
                : string.Empty;

            var joinStatements = string.Join(Environment.NewLine, dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [DEL].{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                )
            );

            var sqlCommand =
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dataFlow.SqlDeletedDataViewName(targetVersion)} 
                   AS
                       SELECT [DEL].{DbExtensions.ValueColumn} AS [OBS_VALUE]
                            {timeDimensionColumns}
                            {dimensionsColumns}
                            {attributeColumns}                            
                            ,{DbExtensions.LAST_UPDATED_COLUMN}
                       FROM [{dotStatDb.DataSchema}].{dataFlow.Dsd.SqlDeletedTable(targetVersion)} AS [DEL]
                            {joinStatements}
                       WHERE [DEL].[DF_ID]={dataFlow.DbId}";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }
    }
}