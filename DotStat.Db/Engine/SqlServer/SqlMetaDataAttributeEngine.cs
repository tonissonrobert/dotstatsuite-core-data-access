﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlMetadataAttributeEngine : MetadataAttributeEngineBase<SqlDotStatDb>
    {
        public SqlMetadataAttributeEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<int> GetDbId(string sdmxId, int msdId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var componentDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync($@"SELECT [MTD_ATTR_ID] 
                FROM [{dotStatDb.ManagementSchema}].[{DbExtensions.SqlMetadataAttributeTable()}] 
               WHERE [ID] = @Id AND [MSD_ID] = @MsdId",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) { Value = sdmxId }, 
                new SqlParameter("MsdId", SqlDbType.Int) { Value = msdId });

            return (int)(componentDbId ?? -1);
        }

        public override async Task<int> InsertToComponentTable(MetadataAttribute metadataAttribute, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var metadataAttributeId = (int)await dotStatDb.ExecuteScalarSqlWithParamsAsync($@"INSERT 
                    INTO [{dotStatDb.ManagementSchema}].[{DbExtensions.SqlMetadataAttributeTable()}] 
                         ([ID],[MSD_ID],[CL_ID],[MIN_OCCURS],[MAX_OCCURS],[ENUM_ID])
                  OUTPUT Inserted.MTD_ATTR_ID
                  VALUES (@Id, @MsdId, @ClId, @MinOccurs, @MaxOccurs, @EnumId)",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) { Value = metadataAttribute.HierarchicalId }, 
                new SqlParameter("MsdId", SqlDbType.Int) { Value = metadataAttribute.Dsd.Msd.DbId }, 
                new SqlParameter("ClId", SqlDbType.Int) { Value = DBNull.Value }, 
                new SqlParameter("MinOccurs", SqlDbType.Int) { Value = (object)metadataAttribute.Base.MinOccurs ?? DBNull.Value }, 
                new SqlParameter("MaxOccurs", SqlDbType.Int) { Value = (object)metadataAttribute.Base.MaxOccurs ?? DBNull.Value }, 
                new SqlParameter("EnumId", SqlDbType.BigInt) { Value = (object)(1) });

            return metadataAttributeId;
        }

        protected override async Task DeleteFromComponentTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync($@"DELETE
                    FROM [{dotStatDb.ManagementSchema}].[{DbExtensions.SqlMetadataAttributeTable()}] 
                   WHERE [MTD_ATTR_ID] = @DbId",
                cancellationToken, 
                new SqlParameter("DbId", SqlDbType.VarChar) { Value = dbId });
        }
    }
}
