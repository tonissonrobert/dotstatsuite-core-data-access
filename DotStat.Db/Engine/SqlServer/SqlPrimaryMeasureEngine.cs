﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Db.Util;
using DotStat.Domain;
using PrimaryMeasure = DotStat.Domain.PrimaryMeasure;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlPrimaryMeasureEngine : PrimaryMeasureEngineBase<SqlDotStatDb>
    {
        private readonly DataTypeEnumHelper _dataTypeEnumHelper;

        public SqlPrimaryMeasureEngine(IGeneralConfiguration generalConfiguration, DataTypeEnumHelper dataTypeEnumHelper) : base(generalConfiguration)
        {
            _dataTypeEnumHelper = dataTypeEnumHelper;
        }

        public override async Task<int> GetDbId(string sdmxId, int dsdId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var componentDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync($@"SELECT [COMP_ID] 
                FROM [{dotStatDb.ManagementSchema}].[COMPONENT] 
               WHERE [ID] = @Id AND [DSD_ID] = @DsdId AND [TYPE] = @Type", 
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) { Value = sdmxId }, 
                new SqlParameter("DsdId", SqlDbType.Int) { Value = dsdId }, 
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.PrimaryMeasure) });

            return (int) (componentDbId ?? -1);
        }

        public override async Task<int> InsertToComponentTable(PrimaryMeasure primaryMeasure, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var componentId = (int) await dotStatDb.ExecuteScalarSqlWithParamsAsync($@"INSERT 
                    INTO [{dotStatDb.ManagementSchema}].[COMPONENT] 
                         ([ID], [TYPE] ,[DSD_ID], [CL_ID], [ATT_ASS_LEVEL], [ATT_STATUS], [ENUM_ID], [ATT_GROUP_ID], [MIN_LENGTH], [MAX_LENGTH], [PATTERN], [MIN_VALUE], [MAX_VALUE])
                  OUTPUT Inserted.COMP_ID
                  VALUES (@Id, @Type, @DsdId, @ClId, NULL, NULL, @EnumId, NULL, @MinLength, @MaxLength, @Pattern, @MinValue, @MaxValue)",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) { Value = primaryMeasure.Code }, 
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.PrimaryMeasure) }, 
                new SqlParameter("DsdId", SqlDbType.Int) { Value = primaryMeasure.Dsd.DbId }, 
                new SqlParameter("ClId", SqlDbType.Int) { Value = primaryMeasure.Base.HasCodedRepresentation() ? (object)primaryMeasure.Codelist.DbId : DBNull.Value }, 
                new SqlParameter("EnumId", SqlDbType.BigInt) { Value = primaryMeasure.Base.HasCodedRepresentation() ? DBNull.Value : (object)_dataTypeEnumHelper.GetIdFromTextFormat(primaryMeasure.TextFormat) }, 
                new SqlParameter("MinLength", SqlDbType.Int) { Value = (object)primaryMeasure.TextFormat?.MinLength ?? DBNull.Value }, 
                new SqlParameter("MaxLength", SqlDbType.Int) { Value = (object)primaryMeasure.TextFormat?.MaxLength ?? DBNull.Value }, 
                new SqlParameter("Pattern", SqlDbType.VarChar) { Value = (object)primaryMeasure.TextFormat?.Pattern ?? DBNull.Value }, 
                new SqlParameter("MinValue", SqlDbType.Int) { Value = (object)primaryMeasure.TextFormat?.MinValue ?? DBNull.Value }, 
                new SqlParameter("MaxValue", SqlDbType.Int) { Value = (object)primaryMeasure.TextFormat?.MaxValue ?? DBNull.Value });

            return componentId;
        }

        protected override async Task DeleteFromComponentTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync($@"DELETE
                    FROM [{dotStatDb.ManagementSchema}].[COMPONENT] 
                   WHERE [COMP_ID] = @DbId AND [TYPE] = @Type",
                cancellationToken, 
                new SqlParameter("DbId", SqlDbType.VarChar) { Value = dbId }, 
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.PrimaryMeasure) });
        }
    }
}
