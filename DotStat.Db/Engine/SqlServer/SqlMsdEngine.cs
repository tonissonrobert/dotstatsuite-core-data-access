﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Engine.SqlServer
{
    public class SqlMsdEngine : MsdEngineBase<SqlDotStatDb>
    {
        public SqlMsdEngine(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }
        
        public override async Task<int> GetDbId(string sdmxId, string sdmxAgency, int verMajor, int verMinor, int? verPatch, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var aretefactDbId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT [ART_ID] 
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ID] = @Id AND [AGENCY] = @Agency AND [TYPE] = @Type
                     AND [VERSION_1] = @Version1 AND [VERSION_2] = @Version2 
                     AND ([VERSION_3] = @Version3 OR ([VERSION_3] IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) { Value = sdmxId },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = sdmxAgency },                
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Msd) },
                new SqlParameter("Version1", SqlDbType.Int) { Value = verMajor },
                new SqlParameter("Version2", SqlDbType.Int) { Value = verMinor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = ((object)verPatch) ?? DBNull.Value }

                );

            return (int)(aretefactDbId ?? -1);
        }

        public override async Task<int> InsertToArtefactTable(Msd msd, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var artefactId = await dotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"INSERT 
                    INTO [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                       ([SQL_ART_ID],[TYPE],[ID],[AGENCY],[VERSION_1],[VERSION_2],[VERSION_3],[LAST_UPDATED]) 
                  OUTPUT INSERTED.ART_ID 
                  VALUES (NULL, @Type, @Id, @Agency, @Version1, @Version2, @Version3, @DT_Now)",
                cancellationToken,
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Msd) },
                new SqlParameter("Id", SqlDbType.VarChar) { Value = msd.Code },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = msd.AgencyId },
                new SqlParameter("Version1", SqlDbType.Int) { Value = msd.Version.Major },
                new SqlParameter("Version2", SqlDbType.Int) { Value = msd.Version.Minor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = (object)msd.Version.Patch ?? DBNull.Value },
                new SqlParameter("DT_Now", DateTime.Now)
                );

            return (int)artefactId;
        }

        public override Task<bool> CreateDynamicDbObjects(Msd artefact, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        #region protected
        protected override async Task DeleteFromArtefactTableByDbId(int dbId, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            await dotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"DELETE
                    FROM [{dotStatDb.ManagementSchema}].[ARTEFACT] 
                   WHERE [ART_ID] = @DbId AND [TYPE] = @Type",
                cancellationToken,
                new SqlParameter("DbId", SqlDbType.VarChar) { Value = dbId },
                        new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Msd) }
                );
        }
        protected override async Task BuildDynamicMetaDataTable(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            //Change to allowNull=true when non coded dimensions are introduced. 
            //A value of 0 should be given when the dimension is not being referenced
            var dimensionColumnsWithType = dimensions.ToColumnList(true);
            var dimensionColumns = dimensions.ToColumnList();
            var table = dsd.SqlMetadataDataStructureTable(targetVersion);
            
            var timeDim = dsd.TimeDimension;
            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    // NULL is stored when time is not referenced by the metadata.
                    new[] { timeDim }.ToColumnList(true, true),
                    //The maximum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodStart(true, supportsDateTime, false),
                    //The minimum SQL data/datetime is stored when time is not referenced by the metadata.
                    DbExtensions.SqlPeriodEnd(true, supportsDateTime, false)
                ) + ","
                : null;

            var timeConstraint =
                timeDim != null ? $", [{DbExtensions.SqlPeriodStart()}], [{DbExtensions.SqlPeriodEnd()}] DESC" : null;
            
            string rowId = null;
            var uIndex = dimensionColumns + timeConstraint;

            //Time dimension requires two columns (period_start and period_end)
            if (dsd.Dimensions.Count > (timeDim != null ? 30: 32))
            {
                rowId = $"[ROW_ID] AS ({dsd.SqlBuildRowIdFormula()}) PERSISTED NOT NULL,";
                uIndex = "[ROW_ID]";
            }
            
            var metaAttributeColumns = dsd.Msd.MetadataAttributes
                .ToColumnList(true);

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }

            var constraint = dsd.DataCompression != DataCompressionEnum.NONE ? "" : $", CONSTRAINT [PK_{table}] PRIMARY KEY CLUSTERED ({uIndex})";

            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{table}](
                        {rowId}
	                    {dimensionColumnsWithType},
                        {timeDimensionColumns}
                        {metaAttributeColumns}
                        [{DbExtensions.LAST_UPDATED_COLUMN}] [datetime] NOT NULL
                        {constraint}
                )";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (dsd.DataCompression != DataCompressionEnum.NONE)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{table}
ON [{dotStatDb.DataSchema}].[{table}]
WITH(DATA_COMPRESSION = {dsd.DataCompression})", cancellationToken);
            }

        }

        protected override async Task BuildDeletedTable(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToArray();
            var dimensionColumnsWithType = dimensions.ToColumnList(true, allowNull: true);
            var metaAttributeColumns = string.Join(",", dsd.Msd.MetadataAttributes.Select(x => x.SqlColumn() + " char(1)"));

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }

            var timeDim = dsd.TimeDimension;
            var supportsDateTime = dsd.Base.HasSupportDateTimeAnnotation();

            var timeDimensionColumns = timeDim != null
                ? string.Join(",",
                    new[] { timeDim }.ToColumnList(true, allowNull: true),
                    DbExtensions.SqlPeriodStart(true, supportsDateTime, true),
                    DbExtensions.SqlPeriodEnd(true, supportsDateTime, true)
                ) + ","
                : null;

            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{dsd.SqlDeletedMetadataTable(targetVersion)}](
[DF_ID] int not null,
{timeDimensionColumns}
{dimensionColumnsWithType},
{metaAttributeColumns}
[{DbExtensions.LAST_UPDATED_COLUMN}] [datetime] NOT NULL
)";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (dsd.DataCompression != DataCompressionEnum.NONE)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{dsd.SqlDeletedMetadataTable(targetVersion)}
ON [{dotStatDb.DataSchema}].[{dsd.SqlDeletedMetadataTable(targetVersion)}]
WITH(DATA_COMPRESSION = {dsd.DataCompression})", cancellationToken);
            }
            else
            {
                sqlCommand = $@"CREATE NONCLUSTERED INDEX I_{dsd.SqlDeletedMetadataTable(targetVersion)} 
ON [{dotStatDb.DataSchema}].[{dsd.SqlDeletedMetadataTable(targetVersion)}]([DF_ID],[LAST_UPDATED])";

                await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
            }
        }

        protected override async Task BuildDynamicDatasetMetaDataTable(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var metaAttributeColumns = dsd.Msd.MetadataAttributes
                .ToColumnList(true);

            if (!string.IsNullOrWhiteSpace(metaAttributeColumns))
            {
                metaAttributeColumns += ',';
            }

            var table = dsd.SqlMetadataDataSetTable(targetVersion);
            var constraint = dsd.DataCompression != DataCompressionEnum.NONE ? "" : $", CONSTRAINT [PK_{table}] PRIMARY KEY CLUSTERED (DF_ID)";
            var sqlCommand = $@"CREATE TABLE [{dotStatDb.DataSchema}].[{table}](
                        DF_ID INT NOT NULL,
                        {metaAttributeColumns}
                        [{DbExtensions.LAST_UPDATED_COLUMN}] [datetime] NOT NULL
                        {constraint}                        
                )";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);

            //Apply data compression
            if (dsd.DataCompression != DataCompressionEnum.NONE)
            {
                await dotStatDb.ExecuteNonQuerySqlAsync(
                    $@"CREATE CLUSTERED COLUMNSTORE INDEX CCI_{table}
ON [{dotStatDb.DataSchema}].[{table}]
WITH(DATA_COMPRESSION = {dsd.DataCompression})", cancellationToken);
            }
        }

        protected override async ValueTask<bool> BuildDynamicMetaDataDsdView(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The codelist table doesnt exist, therefore the db is corrupted or the method is called by the unit tests
            foreach (var dimension in dsd.Dimensions.Where(x => x.Base.HasCodedRepresentation()))
            {
                if (!await dotStatDb.TableExists($"CL_{dimension.Codelist.DbId}", cancellationToken))
                {
                    return false;
                }
            }

            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dsd.SqlMetadataDataStructureTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dsd.SqlMetadataDataSetTable(targetVersion)}", cancellationToken))
            {
                return false;
            }

            //The view already exists, therefore no need to recreate it.
            if (await dotStatDb.ViewExists($"{dsd.SqlMetaDataDsdViewName(targetVersion)}", cancellationToken))
                return false;

            var dimensions = dsd.Dimensions.Where(dim => !dim.Base.TimeDimension).ToList();
            
            // SELECT ----------------------------------------------------------------

            var selectStatement = new StringBuilder();
            var dsSelectStatement = new StringBuilder();
            //dimensions
            selectStatement
                .Append(string.Join(", ", dimensions.Select(d =>
                d.Base.HasCodedRepresentation() ? 
                    $"CASE WHEN [CL_{d.Code}].[ID] IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE [CL_{d.Code}].[ID] END AS [{d.Code}]" 
                    : $"CASE WHEN [ME].{d.SqlColumn()} IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE [ME].{d.SqlColumn()} END AS [{d.Code}]")));

            dsSelectStatement
                .Append(string.Join(", ", dimensions.Select(d => $"'{DbExtensions.DimensionSwitchedOff}' AS [{d.Code}]")));

            //time dimension
            var timeDim = dsd.TimeDimension;
            var (sqlMin, sqlMax) = dsd.Base.HasSupportDateTimeAnnotation()
                ? (DateTime.MinValue.ToString("yyyy-MM-dd HH:mm:ss"), DateTime.MaxValue.ToString("yyyy-MM-dd HH:mm:ss"))
                : (DateTime.MinValue.ToString("yyyy-MM-dd"), DateTime.MaxValue.ToString("yyyy-MM-dd"));

            if (timeDim != null)
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(",",
                        $"CASE WHEN [ME].{timeDim.SqlColumn()} IS NULL THEN '{DbExtensions.DimensionSwitchedOff}' ELSE [ME].{timeDim.SqlColumn()} END AS [{dsd.Base.TimeDimension.Id}]",
                        DbExtensions.SqlPeriodStart(),
                        DbExtensions.SqlPeriodEnd()
                    ));

                dsSelectStatement
                    .Append(", ")
                    .Append(string.Join(", ",
                        $"'{DbExtensions.DimensionSwitchedOff}' AS [{dsd.Base.TimeDimension.Id}]",
                        $"'{sqlMax}' AS [{DbExtensions.SqlPeriodStart()}]",
                        $"'{sqlMin}' AS [{DbExtensions.SqlPeriodEnd()}]"
                    ));
            }

            //metadata attributes
            if (dsd.Msd.MetadataAttributes.Any())
            {
                selectStatement
                    .Append(",")
                    .Append(string.Join(", ", dsd.Msd.MetadataAttributes.Select(a => $"[ME].{a.SqlColumn()} AS [{a.HierarchicalId}]"
                )));

                dsSelectStatement
                    .Append(",")
                    .Append(string.Join(", ", dsd.Msd.MetadataAttributes.Select(a => $"{a.SqlColumn()} AS [{a.HierarchicalId}]"
                    )));
            }

            // LAST_UPDATED
            selectStatement.Append($",[ME].{DbExtensions.LAST_UPDATED_COLUMN}");
            dsSelectStatement.Append($",{DbExtensions.LAST_UPDATED_COLUMN}");

            // JOIN ------------------------------------------------------------------
            var joinStatement = new StringBuilder();

            //dimensions
            joinStatement.AppendLine(string.Join("\n", dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [ME].{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                )
            ));

            var dsdLevel = $@"
                        SELECT {selectStatement}
                        FROM [{dotStatDb.DataSchema}].[{dsd.SqlMetadataDataStructureTable(targetVersion)}] AS [ME]
                        {joinStatement}";

            var dsLevel = $@"
                        SELECT {dsSelectStatement}
                        FROM [{dotStatDb.DataSchema}].[{dsd.SqlMetadataDataSetTable(targetVersion)}]
                        WHERE [DF_ID] = -1";

            var sqlCommand =
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dsd.SqlMetaDataDsdViewName(targetVersion)} 
                   AS
                        {dsdLevel}
                        UNION ALL
                        {dsLevel}
                ";

            return await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken) > 0;
        }

        protected override async Task BuildDeletedView(Dsd dsd, char targetVersion, SqlDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //The dependent table does not exists, therefore the db is corrupted or the method is called by the unit tests
            if (!await dotStatDb.TableExists($"{dsd.SqlDeletedTable(targetVersion)}", cancellationToken))
            {
                return;
            }

            //The view already exists, therefore no need to recreate it.
            if (await dotStatDb.ViewExists($"{dsd.SqlDeletedMetadataDsdViewName(targetVersion)}", cancellationToken))
                return;

            var dimensions = dsd.Dimensions
                .Where(dim => !dim.Base.TimeDimension)
                .ToList();

            //Time dimension columns
            var timeDimensionColumns = dsd.TimeDimension != null
                    ? ", " + string.Join(", ",
                            $"[DEL].[{DbExtensions.TimeDimColumn}] AS [{dsd.Base.TimeDimension.Id}]",
                            DbExtensions.SqlPeriodStart(),
                            DbExtensions.SqlPeriodEnd()
                        )
                    : string.Empty;

            //Dimensions
            var dimensionsColumns = dimensions.Any()
                        ? ", " + string.Join(", ", dimensions.Select(d => d.Base.HasCodedRepresentation() ? $"[CL_{d.Code}].[ID] AS [{d.Code}]" : $"[DEL].{d.SqlColumn()} AS [{d.Code}]"))
                        : string.Empty;

            //Attributes
            var attributeColumns = dsd.Attributes.Any()
                ? "," + string.Join(",", dsd.Attributes.Select(a => $"[DEL].{a.SqlColumn()} as [{a.Code}]"))
                : string.Empty;

            var joinStatements = string.Join(Environment.NewLine, dimensions.Where(d => d.Base.HasCodedRepresentation()).Select(d =>
                    $"LEFT JOIN [{dotStatDb.ManagementSchema}].[CL_{d.Codelist.DbId}] AS [CL_{d.Code}] ON [DEL].{d.SqlColumn()} = [CL_{d.Code}].[ITEM_ID]"
                )
            );

            var sqlCommand =
                $@"CREATE VIEW [{dotStatDb.DataSchema}].{dsd.SqlDeletedMetadataDsdViewName(targetVersion)} 
                   AS
                       SELECT [DEL].{DbExtensions.ValueColumn} AS [OBS_VALUE]
                            {timeDimensionColumns}
                            {dimensionsColumns}
                            {attributeColumns}                            
                            ,{DbExtensions.LAST_UPDATED_COLUMN}
                       FROM [{dotStatDb.DataSchema}].{dsd.SqlDeletedTable(targetVersion)} AS [DEL]
                            {joinStatements}
                       WHERE [DEL].[DF_ID]= -1";

            await dotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }
        #endregion

    }
}
