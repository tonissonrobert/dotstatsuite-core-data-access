﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Domain;

namespace DotStat.Db.Engine
{
    public abstract class MetadataDataflowEngineBase<TDotStatDb> : ArtefactEngineBase<Dataflow, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected MetadataDataflowEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public override async Task<bool> CreateDynamicDbObjects(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dataflow == null)
            {
                throw new ArgumentNullException(nameof(dataflow));
            }

            //Set A
            await BuildDynamicMetadataDataFlowTable(dataflow, (char)DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDynamicMetadataDataFlowView(dataflow, (char) DbTableVersion.A, dotStatDb, cancellationToken);
            await BuildDeletedView(dataflow, (char)DbTableVersion.A, dotStatDb, cancellationToken);

            //Set B
            await BuildDynamicMetadataDataFlowTable(dataflow, (char)DbTableVersion.B, dotStatDb, cancellationToken);
            await BuildDynamicMetadataDataFlowView(dataflow, (char) DbTableVersion.B, dotStatDb, cancellationToken);
            return await BuildDeletedView(dataflow, (char)DbTableVersion.B, dotStatDb, cancellationToken);

        }

        public override Task CleanUp(Dataflow dataflow, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        protected abstract Task BuildDynamicMetadataDataFlowTable(Dataflow dataflow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task<bool> BuildDynamicMetadataDataFlowView(Dataflow dataFlow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task<bool> BuildDeletedView(Dataflow dataFlow, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

    }
}