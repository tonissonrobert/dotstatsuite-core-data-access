﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Engine
{
    public abstract class AttributeEngineBase<TDotStatDb> : ComponentEngineBase<Attribute, TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected AttributeEngineBase(IGeneralConfiguration generalConfiguration) : base(generalConfiguration)
        {
        }

        public async Task CreateDynamicDbObjects(Dsd dsd, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            if (dsd == null)
            {
                throw new ArgumentNullException(nameof(dsd));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (dsd.Attributes != null && dsd.Attributes.Any())
            {
                //Set A
                await BuildDynamicDatasetAttributeTable(dsd, (char)DbTableVersion.A, dotStatDb, cancellationToken);
                await BuildDynamicDimensionGroupAttributeTable(dsd, (char)DbTableVersion.A, dotStatDb, cancellationToken);
                
                //Set B
                await BuildDynamicDatasetAttributeTable(dsd, (char)DbTableVersion.B, dotStatDb, cancellationToken);
                await BuildDynamicDimensionGroupAttributeTable(dsd, (char)DbTableVersion.B, dotStatDb, cancellationToken);
            }
        }

        public override async Task CleanUp(Attribute component, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            //Does not delete nor changes structure of related attribute table!

            if (component == null)
            {
                throw new ArgumentNullException(nameof(component));
            }

            if (dotStatDb == null)
            {
                throw new ArgumentNullException(nameof(dotStatDb));
            }

            if (component.DbId < 1)
            {
                throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeInvalidDbId),
                        component.Code, component.DbId)
                );
            }

            await DeleteFromComponentTableByDbId(component.DbId, dotStatDb, cancellationToken);
        }

        public async Task AlterTextAttributeColumns(Dsd dsd, int newMaxAttributeLength, TDotStatDb dotStatDb, CancellationToken cancellationToken)
        {
            var affectedAttributes = dsd.Attributes.Where(a =>
                    !a.Base.HasCodedRepresentation() && a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet)
                .ToList();

            // Alter tables only if there are affected attributes
            if (affectedAttributes.Any())
            {
                await AlterTextAttributeColumnsInDatasetAttributeTable(dsd, affectedAttributes, newMaxAttributeLength, (char)DbTableVersion.A, dotStatDb, cancellationToken);
                await AlterTextAttributeColumnsInDatasetAttributeTable(dsd, affectedAttributes, newMaxAttributeLength, (char)DbTableVersion.B, dotStatDb, cancellationToken);
            }

            affectedAttributes = dsd.Attributes.Where(a =>
                !a.Base.HasCodedRepresentation() &&
                (a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || a.Base.AttachmentLevel == AttributeAttachmentLevel.Group) && !a.Base.DimensionReferences.Contains(DimensionObject.TimeDimensionFixedId))
                .ToList();

            // Alter tables only if there are affected attributes
            if (affectedAttributes.Any())
            {
                await AlterTextAttributeColumnsInDimensionGroupAttributeTable(dsd, affectedAttributes, newMaxAttributeLength, (char)DbTableVersion.A, dotStatDb, cancellationToken);
                await AlterTextAttributeColumnsInDimensionGroupAttributeTable(dsd, affectedAttributes, newMaxAttributeLength, (char)DbTableVersion.B, dotStatDb, cancellationToken);
            }
        }

        protected abstract Task BuildDynamicDatasetAttributeTable(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task BuildDynamicDimensionGroupAttributeTable(Dsd dsd, char targetVersion, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task AlterTextAttributeColumnsInDatasetAttributeTable(Dsd dsd,
            IList<Attribute> attributes, int newMaxAttributeLength, char targetVersion,
            TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task AlterTextAttributeColumnsInDimensionGroupAttributeTable(Dsd dsd,
            IList<Attribute> attributes, int newMaxAttributeLength, char targetVersion,
            TDotStatDb dotStatDb, CancellationToken cancellationToken);

    }
}
