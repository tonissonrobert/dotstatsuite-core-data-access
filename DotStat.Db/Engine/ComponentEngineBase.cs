using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.DB;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace DotStat.Db.Engine
{
    public abstract class ComponentEngineBase<TComponent, TDotStatDb> 
        where TComponent : IDotStatIdentifiable
        where TDotStatDb : IDotStatDb
    {
        protected readonly IGeneralConfiguration GeneralConfiguration;

        protected ComponentEngineBase(IGeneralConfiguration generalConfiguration)
        {
            GeneralConfiguration = generalConfiguration;
        }
        
        public async Task<int> GetDbId(object component, TDotStatDb dotStatDb, CancellationToken cancellationToken) 
        {
            var comp = component as CodeListBasedIdentifiableDotStatObject<IIdentifiableObject>;

            if (comp == null)
            {
                throw new ArgumentException(nameof(component));
            }

            if (comp.Dsd == null)
            {
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoDSDMappedComponent,
                    comp.Code)
                );
            }

            return await GetDbId(comp.Code, comp.Dsd.DbId, dotStatDb, cancellationToken);
        }

        public abstract Task<int> GetDbId(string sdmxId, int parentIdId, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        public abstract Task<int> InsertToComponentTable(TComponent component, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        public abstract Task CleanUp(TComponent component, TDotStatDb dotStatDb, CancellationToken cancellationToken);

        protected abstract Task DeleteFromComponentTableByDbId(int dbId, TDotStatDb dotStatDb, CancellationToken cancellationToken);
    }
}
