﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

namespace DotStat.Db.Repository
{
    public interface IObservationRepository
    {
        IAsyncEnumerable<ObservationRow> GetObservations(
            IDataQuery dataQuery,
            IImportReferenceableStructure referencedStructure,
            DbTableVersion tableVersion,
            bool isDataQuery,
            CancellationToken cancellationToken
        );

        Task<long> GetObservationCount(
            Dataflow dataFlow,
            DbTableVersion tableVersion,
            string dataFlowWhereClause,
            CancellationToken cancellationToken);

        string BuildReplaceSqlQuery(
            IDataQuery dataQuery,
            IImportReferenceableStructure referencedStructure,
            IList<string> components,
            DbTableVersion tableVersion,
            bool isDataQuery,
            bool filterLastUpdated);
    }
}