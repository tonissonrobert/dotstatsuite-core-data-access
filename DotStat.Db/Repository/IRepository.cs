﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DotStat.DB.Repository
{
    public interface IRepository<T> where T : class
    {
        Task<T> Get(int id);
        IAsyncEnumerable<T> GetAll();
        Task<int> Add(T entity);
        Task<int> Delete(int id);
        Task<int> Update(T entity);
    }
}
