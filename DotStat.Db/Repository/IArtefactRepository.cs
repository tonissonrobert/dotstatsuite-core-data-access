﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Db.DB;
using DotStat.Db.Dto;
using DotStat.Db.Engine.SqlServer;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.MappingStore;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;

namespace DotStat.Db.Repository
{
    public interface IArtefactRepository
    {
        Task CheckManagementTables(CancellationToken cancellationToken);
        ValueTask<bool> CreateNewDsdAftefact(Dsd dsd, CancellationToken cancellationToken, bool logError = true);

        ValueTask<bool> CleanUpDsd(int dsdDbId, IList<ArtefactItem> dataflowArtefacts, IList<ComponentItem> allComponents, CancellationToken cancellationToken);
        ValueTask<bool> CleanUpMsdOfDsd(int dsdDbId, IList<ArtefactItem> dataflowArtefacts, IList<ComponentItem> allComponents, IList<MetadataAttributeItem> allMetadataAttributeComponents, CancellationToken cancellationToken);
        ValueTask<bool> CleanUpCodelist(int clId, CancellationToken cancellationToken);
        Task<bool> UpdatePITInfoOfDsdArtefact(Dsd dsd, CancellationToken cancellationToken);

        Task<int?> GetDsdMsdDbId(int dsdDbId, CancellationToken cancellationToken);
        ValueTask<bool> DeleteArtefact(int dbId, CancellationToken cancellationToken);
        ValueTask<bool> DeleteArtefacts(IEnumerable<int> dbIds, CancellationToken cancellationToken);
        Task<ArtefactItem> GetArtefactByDbId(int dbId, CancellationToken cancellationToken, bool errorIfNotFound = false);
        Task<int> GetArtefactDbId(IDotStatMaintainable artefact, CancellationToken cancellationToken, bool errorIfNotFound = false);
        Task<int> GetArtefactDbId(IMaintainableObject artefact, CancellationToken cancellationToken);
        Task<int> GetArtefactDbId(string agencyId, string artefactId, SdmxVersion version, SDMXArtefactType sdmxDbType, CancellationToken cancellationToken);
        Task<IList<ArtefactItem>> GetListOfArtefacts(CancellationToken cancellationToken, string type = null);
        Task<ArtefactItem> GetArtefact(string agencyId, string artefactId, SdmxVersion version, SDMXArtefactType sdmxDbType, CancellationToken cancellationToken, bool errorIfNotFound = true);
        //Task<bool> GetDataExistsForDataflow(ArtefactItem dfArtefact, IDataStoreRepository dataStoreRepository, DbTableVersion tableVersion, CancellationToken cancellationToken);
        Task<IList<ArtefactItem>> GetListOfDataflowArtefactsForDsd(int dsdDbId, CancellationToken cancellationToken);
        Task<IList<ArtefactItem>> GetListOfCodelistsWithoutDsdAndMsd(CancellationToken cancellationToken);

        Task<JObject> GetDSDPITInfo(Dataflow dataFlow, CancellationToken cancellationToken);
        Task FillMeta(Dsd dsd, CancellationToken cancellationToken, bool errorIfNotFound = true);
        Task FillMeta(Msd msd, CancellationToken cancellationToken);
        Task UpdateMsdOfDsd(int dsdDbId, int? msdDbId, CancellationToken cancellationToken);
        Task UpdateDataCompressionOfDsd(int dsdDbId, DataCompressionEnum dataCompression, CancellationToken cancellationToken);
        Task UpdateKeepHistoryOfDsd(int dsdDbId, bool keepHistory, CancellationToken cancellationToken);
        Task VerifyAndCreateMeasures(Dsd dsd, IList<ComponentItem> componentItems,
            bool factTableExists, CancellationToken cancellationToken, bool throwConcurrencyError = true);
        Task VerifyAndCreateOrUpdateDynamicTables(Dsd dsd, bool factTableExists, CancellationToken cancellationToken);
        
        Task SetKeepHistoryOn(Dsd dsd, CancellationToken cancellationToken);

        Task VerifyAndCreateDataflow(Dataflow dataflow, CancellationToken cancellationToken);

        Task SetKeepHistoryOn(Dataflow dataflow, CancellationToken cancellationToken);

        ValueTask VerifyAndCreateOrUpdateMetadataDynamicTables(Dsd dsd, bool metaDataTableExists,
            CancellationToken cancellationToken);

        ValueTask VerifyAndCreateCodelistOfComponent(Dsd dsd, IDotStatCodeListBasedIdentifiable component,
            ComponentItem componentInDb, bool factTableExists, CancellationToken cancellationToken, bool throwConcurrencyError = true);

        Task<ComponentItem> TryCreateDimensionComponent(Dimension dim, SDMXArtefactType dimType,
            CancellationToken cancellationToken);
        
        Task GetOrCreateMsd(Msd msd, CancellationToken cancellationToken);
        Task CreateMetadataAttribute(MetadataAttribute attr, CancellationToken cancellationToken);

        Task<bool> CreateMetaDataFlow(Dataflow dataFlow, bool dataFlowMetaDataTableExists,
            CancellationToken cancellationToken);



        Task<bool> CheckSupportOfTimeAtTimeDimension(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken, string columnName = null);

        Task<AvailabilityHelper> CreateDataAvailabilityOfDataFlow(Dataflow dataFlow, string dataFlowWhereClause, DbTableVersion tableVersion, CancellationToken cancellationToken);

    }
}
