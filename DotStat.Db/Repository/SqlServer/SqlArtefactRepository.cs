using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Common.Localization;
using DotStat.Common.Util;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Codelist;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.DataStructure;
using DotStat.Db.DB;
using DotStat.Db.Dto;
using DotStat.Db.Exception;
using DotStat.Domain;
using System.Threading.Tasks;
using System.Threading;
using DotStat.Common.Logger;
using DotStat.Db.Engine.SqlServer;
using DotStat.Db.Helpers;
using DotStat.Db.Util;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Constants;
using DotStat.Common.Exceptions;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;

namespace DotStat.Db.Repository.SqlServer
{
    public class SqlArtefactRepository : DatabaseRepositoryBase<SqlDotStatDb>, IArtefactRepository
    {
        public SqlArtefactRepository(SqlDotStatDb dotStatDb,  IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }

        public async Task CheckManagementTables(CancellationToken cancellationToken)
        {
            var tableNames = new[]
            {
                "ARTEFACT",
                "COMPONENT",
                "METADATA_ATTRIBUTE",
                "ENUMERATIONS",
                "DSD_TRANSACTION",
                "LOGS",
                "DF_Output_Timestamp",
                "ATTR_DIM_SET"
            };

            foreach (var tableName in tableNames)
            {
                if (!await DotStatDb.TableExists(tableName, cancellationToken))
                {
                    throw new DatabaseTableNotFoundException(
                            string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DatabaseTableNotFound), tableName));
                }
            }
        }
        public async ValueTask<bool> CreateNewDsdAftefact(Dsd dsd, CancellationToken cancellationToken, bool logError = true)
        {
            var dsdBuilder = new SqlDsdEngine(GeneralConfiguration);
            try
            {
                var dsdId = await dsdBuilder.GetDbId(dsd, DotStatDb, cancellationToken);

                if (dsdId == -1)
                {
                    Log.Debug("DSD is not in ARTEFACT table, inserting.");
                    dsd.DbId = await dsdBuilder.InsertToArtefactTable(dsd, DotStatDb, cancellationToken);
                    Log.Debug($"DSD [{dsd.DbId}] inserted into ARTEFACT table.");
                }
                else
                {
                    dsd.DbId = dsdId;
                }

                return true;
            }
            catch (SqlException ex) when (ex.Number == 2627 || ex.Number == 2601)
            {
                //2601 - Cannot insert duplicate key row in object '%.*ls' with unique index '%.*ls'.The duplicate key value is % ls.
                //2627 - Violation of % ls constraint '%.*ls'.Cannot insert duplicate key in object '%.*ls'.The duplicate key value is % ls.
                //Concurrent process already inserted row for the same dsd, so unique key constraint violation.
                if (logError)
                {
                    Log.Error(
                        new DotStatException(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TransactionAbortedDueToConcurrentArtefactAccess), dsd.FullId), ex));
                }

                return false;
            }
        }

        public async ValueTask<bool> CleanUpDsd(int dsdDbId, IList<ArtefactItem> dataflowArtefacts, IList<ComponentItem> allComponents, CancellationToken cancellationToken)
        {
            if (dsdDbId <= 0)
            {
                return false;
            }

            // Drop views
            foreach (var dataflow in dataflowArtefacts)
            {
                if (dataflow.DfDsdId == null) continue;

                //Drop [data].[VI_CurrentDataDataFlow_{dataflowDbId}_A/B] views
                await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDataDataFlowViewName(dataflow.DbId, (char)DbTableVersion.A), cancellationToken);
                await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDataDataFlowViewName(dataflow.DbId, (char)DbTableVersion.B), cancellationToken);

                //Drop [data].VI_CurrentDataReplaceDataFlow_{dataflowDbId}_A/B] views
                await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDataReplaceDataFlowViewName(dataflow.DbId, (char)DbTableVersion.A), cancellationToken);
                await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDataReplaceDataFlowViewName(dataflow.DbId, (char)DbTableVersion.B), cancellationToken);

                //Drop [data].VI_DeletedDataDataFlow_{dataflowDbId}_A/B] views
                await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDeletedDataViewName(dataflow.DbId, (char)DbTableVersion.A), cancellationToken);
                await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDeletedDataViewName(dataflow.DbId, (char)DbTableVersion.B), cancellationToken);

                //Temporal tables
                //Drop [data].VI_CurrentDataDataFlow_{dataflowDbId}_A/B_IncludeHistory] views
                await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDataIncludeHistoryDataFlowViewName(dataflow.DbId, (char)DbTableVersion.A), cancellationToken);
                await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDataIncludeHistoryDataFlowViewName(dataflow.DbId, (char)DbTableVersion.B), cancellationToken);
                
            }

            //Drop [data].[VI_CurrentDataDsd_{dsdDbId}_A/B] views
            await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDataDsdViewName(dsdDbId, (char)DbTableVersion.A), cancellationToken);
            await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDataDsdViewName(dsdDbId, (char)DbTableVersion.B), cancellationToken);

            //Temporal tables
            //var allComponents = (await GetAllComponents(cancellationToken)).ToList();
            var dsdComponents = allComponents.Where(x => x.DsdId == dsdDbId).ToArray();

            // Clear [management].[ATTR_DIM_SET]
            foreach (var component in dsdComponents.Where(x => x.IsDimension))
            {
                await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                    $"DELETE FROM [{DotStatDb.ManagementSchema}].[ATTR_DIM_SET] WHERE [DIM_ID] = @DimId",
                    cancellationToken,
                    new SqlParameter("DimId", SqlDbType.Int) { Value = component.DbId });
            }

            var clIds = dsdComponents.Select(x => x.CodelistId);
            var clIdsToKeep = allComponents.Where(x => x.DsdId != dsdDbId && clIds.Contains(x.CodelistId)).Select(x => x.CodelistId);

            foreach (var clId in clIds.Except(clIdsToKeep).Where(x => x.HasValue))
            {
                // Drop [management].[CL_id] tables (only if not used elsewhere)
                await DotStatDb.ExecuteNonQuerySqlAsync($@"DROP TABLE IF EXISTS [{DotStatDb.ManagementSchema}].[CL_{clId}]", cancellationToken);

                // Delete corresponding row from [management].[ARTEFACT] table
                await DeleteArtefact(clId.Value, cancellationToken);
            }

            // Clear dataflows + dsd form [management].[ARTEFACT] table
            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $"DELETE FROM [{DotStatDb.ManagementSchema}].[ARTEFACT] WHERE [DF_DSD_ID] = @DsdId",
                cancellationToken,
                new SqlParameter("DsdId", SqlDbType.Int) { Value = dsdDbId });

            await DeleteArtefact(dsdDbId, cancellationToken);

            // Clear [management].[COMPONENT] table
            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $"DELETE FROM [{DotStatDb.ManagementSchema}].[COMPONENT] WHERE [DSD_ID] = @DsdId",
                cancellationToken,
                new SqlParameter("DsdId", SqlDbType.Int) { Value = dsdDbId });

            // Turn off Temporal Tables
            var temporalTables = new List<string>
            {
                DbExtensions.SqlFactTable(dsdDbId,(char)DbTableVersion.A),
                DbExtensions.SqlFactTable(dsdDbId,(char)DbTableVersion.B),
                DbExtensions.SqlDimGroupAttrTable(dsdDbId,(char)DbTableVersion.A),
                DbExtensions.SqlDimGroupAttrTable(dsdDbId,(char)DbTableVersion.B),
                DbExtensions.SqlDsdAttrTable(dsdDbId,(char)DbTableVersion.A),
                DbExtensions.SqlDsdAttrTable(dsdDbId,(char)DbTableVersion.B),
            };

            foreach (var temporalTable in temporalTables)
            {
                await DotStatDb.RemoveTemporalTableSupport(DotStatDb.DataSchema, temporalTable, cancellationToken);
            }

            // Drop [data].[ATTR_id], [data].[FACT_id], [data].[FILT_id] tables
            var tableNames = new List<string>();
            using (var dr = await DotStatDb.ExecuteReaderSqlAsync(
                $"SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES " +
                $"WHERE (TABLE_NAME LIKE 'ATTR[_]{dsdDbId}[_]%' OR TABLE_NAME LIKE 'FACT[_]{dsdDbId}[_]%' OR TABLE_NAME LIKE 'FILT_{dsdDbId}') OR TABLE_NAME LIKE 'DELETED[_]{dsdDbId}[_]%'" +
                $"AND TABLE_SCHEMA = '{DotStatDb.DataSchema}'", cancellationToken))
            {
                while (await dr.ReadAsync(cancellationToken))
                {
                    tableNames.Add(dr.GetString(0));
                }
            }

            foreach (var table in tableNames)
            {
                await DotStatDb.ExecuteNonQuerySqlAsync($@"DROP TABLE [{DotStatDb.DataSchema}].[{table}]", cancellationToken);
            }

            return true;
        }

        public async ValueTask<bool> CleanUpMsdOfDsd(int dsdDbId, IList<ArtefactItem> dataflowArtefacts, IList<ComponentItem> allComponents, IList<MetadataAttributeItem> allMetadataAttributeComponents, CancellationToken cancellationToken)
        {
            if (dsdDbId <= 0)
            {
                return false;
            }

            var linkedMsdDbId = await GetDsdMsdDbId(dsdDbId, cancellationToken);
            //No linked MSD
            if (linkedMsdDbId == null || linkedMsdDbId <= 0)
            {
                return false;
            }

            var msd = await GetArtefactByDbId((int)linkedMsdDbId, cancellationToken, false);

            // Invalid or missing MSD id
            if (msd == null || msd.DbId == -1)
            {
                await UpdateMsdOfDsd(dsdDbId, null, cancellationToken);

                return false;
            }

            // Drop views
            foreach (var dataflow in dataflowArtefacts)
            {
                if (dataflow.DfDsdId == null) continue;

                //Drop [data].VI_DeletedMetadataDataFlow_{dataflowDbId}_A/B] views
                await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDeletedMetadataDataFlowViewName(dataflow.DbId, (char)DbTableVersion.A), cancellationToken);
                await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDeletedMetadataDataFlowViewName(dataflow.DbId, (char)DbTableVersion.B), cancellationToken);

                //Drop [data].[VI_MetadataDataFlow_{dataflowDbId}_A/B] views
                await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlMetadataDataFlowViewName(dataflow.DbId, (char)DbTableVersion.A), cancellationToken);
                await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlMetadataDataFlowViewName(dataflow.DbId, (char)DbTableVersion.B), cancellationToken);

                //Drop [data].[META_DF_{dataflowDbId}_A/B] Tables
                await DotStatDb.DropTable(DotStatDb.DataSchema, DbExtensions.SqlMetadataDataFlowTable(dataflow.DbId, (char)DbTableVersion.A), cancellationToken);
                await DotStatDb.DropTable(DotStatDb.DataSchema, DbExtensions.SqlMetadataDataFlowTable(dataflow.DbId, (char)DbTableVersion.B), cancellationToken);

            }

            //Drop [data].[VI_MetadataDsd_{dsdDbId}_A/B] views
            await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlMetaDataDsdViewName(dsdDbId, (char)DbTableVersion.A), cancellationToken);
            await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlMetaDataDsdViewName(dsdDbId, (char)DbTableVersion.B), cancellationToken);

            //Drop [data].[VI_DeletedMetadataDsd_{dsdDbId}_A/B] views
            await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDeletedMetadataDsdViewName(dsdDbId, (char)DbTableVersion.A), cancellationToken);
            await DotStatDb.DropView(DotStatDb.DataSchema, DbExtensions.SqlDeletedMetadataDsdViewName(dsdDbId, (char)DbTableVersion.B), cancellationToken);

            //Drop [data].[META_DS_{dsdDbId}_A/B] Tables
            await DotStatDb.DropTable(DotStatDb.DataSchema, DbExtensions.SqlMetadataDataSetTable(dsdDbId, (char)DbTableVersion.A), cancellationToken);
            await DotStatDb.DropTable(DotStatDb.DataSchema, DbExtensions.SqlMetadataDataSetTable(dsdDbId, (char)DbTableVersion.B), cancellationToken);

            //Drop [data].[META_DSD_{dsdDbId}_A/B] Tables
            await DotStatDb.DropTable(DotStatDb.DataSchema, DbExtensions.SqlMetadataDataStructureTable(dsdDbId, (char)DbTableVersion.A), cancellationToken);
            await DotStatDb.DropTable(DotStatDb.DataSchema, DbExtensions.SqlMetadataDataStructureTable(dsdDbId, (char)DbTableVersion.B), cancellationToken);

            //Drop [data].[DELETED_META_{dsdDbId}_A/B] Tables
            await DotStatDb.DropTable(DotStatDb.DataSchema, DbExtensions.SqlDeletedMetadataTable(dsdDbId, (char)DbTableVersion.A), cancellationToken);
            await DotStatDb.DropTable(DotStatDb.DataSchema, DbExtensions.SqlDeletedMetadataTable(dsdDbId, (char)DbTableVersion.B), cancellationToken);


            //Remove remove msd information
            await UpdateMsdOfDsd(dsdDbId, null, cancellationToken);

            var linkedDsds = await GetDsdDbIdsLinkedToMsd(msd.DbId, cancellationToken);
            //MSD linked to multiple DSDs
            if (linkedDsds.Any())
                return true;

            //var allComponents = (await GetAllComponents(cancellationToken)).ToList();
            //var allMetadataAttributeComponents = (await GetAllMetadataAttributeComponents(cancellationToken)).ToList();

            var clIds = allMetadataAttributeComponents.Where(x => x.MsdId == msd.DbId).Select(x => x.CodelistId).ToArray();
            var clIdsToKeep = allComponents.Where(x => clIds.Contains(x.CodelistId)).Select(x => x.CodelistId).ToArray();
            clIdsToKeep = clIdsToKeep.Concat(allMetadataAttributeComponents.Where(x => x.MsdId != msd.DbId).Select(x => x.CodelistId).ToList()).ToArray();

            foreach (var clId in clIds.Except(clIdsToKeep).Where(x => x.HasValue))
            {
                // Drop [management].[CL_id] tables (only if not used elsewhere)
                await DotStatDb.ExecuteNonQuerySqlAsync(
                    $@"DROP TABLE IF EXISTS [{DotStatDb.ManagementSchema}].[CL_{clId}]", cancellationToken);

                // Delete corresponding row from [management].[ARTEFACT] table
                await DeleteArtefact(clId.Value, cancellationToken);
            }

            // Clear msd form [management].[ARTEFACT] table
            await DeleteArtefact(msd.DbId, cancellationToken);

            // Clear [management].[METADATA_ATTRIBUTE] table
            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $"DELETE FROM [{DotStatDb.ManagementSchema}].[METADATA_ATTRIBUTE] WHERE [MSD_ID] = @MsdId",
                cancellationToken,
                new SqlParameter("MsdId", SqlDbType.Int) { Value = msd.DbId });

            return true;
        }

        public async ValueTask<bool> CleanUpCodelist(int clId, CancellationToken cancellationToken)
        {
            if (clId <= 0)
            {
                return false;
            }

            // Delete corresponding row from [management].[ARTEFACT] table
            await DeleteArtefact(clId, cancellationToken);

            // Drop [management].[CL_id] tables (only if not used elsewhere)
            await DotStatDb.ExecuteNonQuerySqlAsync($@"DROP TABLE IF EXISTS [{DotStatDb.ManagementSchema}].[CL_{clId}]", cancellationToken);

            return true;
        }

        public async Task<int?> GetDsdMaxTextAttributeLength(Dsd dsd, CancellationToken cancellationToken)
        {
            var maxAttributeLength = await DotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT [DSD_MAX_TEXT_ATTR_LENGTH]
                    FROM [{DotStatDb.ManagementSchema}].[ARTEFACT] a
                   WHERE a.[ID] = @Id AND a.[AGENCY] = @Agency AND a.[TYPE] = 'DSD'
                     AND a.[VERSION_1] = @Version1 AND a.[VERSION_2] = @Version2
                     AND (a.[VERSION_3] = @Version3 OR (a.[VERSION_3] IS NULL AND @Version3 IS NULL))",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.VarChar) { Value = dsd.Code },
                new SqlParameter("Agency", SqlDbType.VarChar) { Value = dsd.AgencyId },
                new SqlParameter("Version1", SqlDbType.Int) { Value = dsd.Version.Major },
                new SqlParameter("Version2", SqlDbType.Int) { Value = dsd.Version.Minor },
                new SqlParameter("Version3", SqlDbType.Int) { Value = ((object)dsd.Version.Patch) ?? DBNull.Value }
            );

            // When the DSD exists in database table and max length is DBNull, then the original hard-coded value (150) should be returned
            return maxAttributeLength == DBNull.Value ? 150 : (int?) maxAttributeLength;
        }
        
        public async Task<bool> UpdatePITInfoOfDsdArtefact(Dsd dsd, CancellationToken cancellationToken)
        {
            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE [{DotStatDb.ManagementSchema}].[ARTEFACT]
                      SET [DSD_LIVE_VERSION] = @LiveVersion,
                          [DSD_PIT_VERSION] = @PITVersion,
                          [DSD_PIT_RELEASE_DATE] = @PITReleaseDate,
                          [DSD_PIT_RESTORATION_DATE] = @PITRestorationDate,
                          [LAST_UPDATED] = @DT_Now  
                    WHERE [ART_ID] = @Id",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.Int) { Value = dsd.DbId },
                new SqlParameter("LiveVersion", SqlDbType.Char) { Value = (object)dsd.LiveVersion ?? DBNull.Value },
                new SqlParameter("PITVersion", SqlDbType.Char) { Value = (object)dsd.PITVersion ?? DBNull.Value },
                new SqlParameter("PITReleaseDate", SqlDbType.DateTime) { Value = (object)dsd.PITReleaseDate ?? DBNull.Value },
                new SqlParameter("PITRestorationDate", SqlDbType.DateTime) { Value = (object)dsd.PITRestorationDate ?? DBNull.Value },
                new SqlParameter("DT_Now", DateTime.Now)
            ) > 0;
        }
        
        public async Task<bool> UpdateMaxAttributeLengthOfDsdArtefact(Dsd dsd, CancellationToken cancellationToken)
        {
            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE [{DotStatDb.ManagementSchema}].[ARTEFACT]
                      SET [DSD_MAX_TEXT_ATTR_LENGTH] = @MaxTextAttrLength,
                          [LAST_UPDATED] = @DT_Now  
                    WHERE [ART_ID] = @Id",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.Int) { Value = dsd.DbId },
                new SqlParameter("MaxTextAttrLength", SqlDbType.Int) { Value = (object)dsd.MaxTextAttributeLength ?? DBNull.Value },
                new SqlParameter("DT_Now", DateTime.Now)
            ) > 0;
        }

        public async Task<JObject> GetDSDPITInfo(Dataflow dataFlow, CancellationToken cancellationToken)
        {
            if (dataFlow == null)
            {
                throw new ArgumentNullException(nameof(dataFlow));
            }

            if (dataFlow.Dsd == null)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowParameterWithoutDsd),
                    dataFlow.FullId)
                );
            }

            await FillMeta(dataFlow.Dsd, cancellationToken);
            
            if (dataFlow.Dsd.PITVersion != null)
            {
                if (dataFlow.Dsd.PITReleaseDate == null || dataFlow.Dsd.PITReleaseDate > DateTime.Now)
                    return new JObject
                        {
                             new JProperty("PITVersion", dataFlow.Dsd.PITVersion?.ToString()),
                             new JProperty("PITReleaseDate", dataFlow.Dsd.PITReleaseDate?.ToString("yyyy-MM-ddTHH:mm:ss.FFFzzz", CultureInfo.InvariantCulture)),
                             new JProperty("PITRestorationDate", dataFlow.Dsd.PITRestorationDate?.ToString("yyyy-MM-ddTHH:mm:ss.FFFzzz", CultureInfo.InvariantCulture))
                        };
                //throw PIT already released
                throw new PointInTimeReleaseException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.PITReleased),
                    dataFlow.Dsd.FullId));
            }
            //throw non existing PIT
            throw new TableVersionException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NonExistingPITVersion),
                dataFlow.Dsd.FullId));
        }

        public async Task<int?> GetDsdMsdDbId(int dsdDbId, CancellationToken cancellationToken)
        {
            var msdDbId = await DotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"SELECT [MSD_ID]
                    FROM [{DotStatDb.ManagementSchema}].[ARTEFACT] a
                   WHERE a.[ART_ID] = @DsdDbId AND a.[TYPE] = @Type",
                cancellationToken,
                new SqlParameter("DsdDbId", SqlDbType.VarChar) { Value = dsdDbId },
                new SqlParameter("Type", SqlDbType.VarChar) { Value = DbTypes.GetDbType(SDMXArtefactType.Dsd) }
            );

            return (msdDbId == DBNull.Value || msdDbId == null) ? null : (int?)msdDbId;
        }

        public async ValueTask<bool> DeleteArtefact(int dbId, CancellationToken cancellationToken)
        {
            return await DeleteArtefacts(dbId.AsEnumerable(), cancellationToken);
        }

        public async ValueTask<bool> DeleteArtefacts(IEnumerable<int> dbIds, CancellationToken cancellationToken)
        {
            var ids = dbIds.ToArray();

            if (ids.Length == 0)
            {
                return true;
            }

            return await DotStatDb.ExecuteNonQuerySqlAsync(
                $"DELETE FROM [{DotStatDb.ManagementSchema}].[ARTEFACT] WHERE [ART_ID] IN " + GetValueListString(ids),
                cancellationToken) > 0;
        }

        public async Task<int> GetArtefactDbId(IDotStatMaintainable artefact, CancellationToken cancellationToken, bool errorIfNotFound = false)
        {
            return await GetArtefactDbId(artefact.AgencyId, artefact.Code, artefact.Version, artefact.DbType, cancellationToken, errorIfNotFound);
        }

        public async Task<int> GetArtefactDbId(IMaintainableObject artefact, CancellationToken cancellationToken)
        {
            SDMXArtefactType dbType;

            if (artefact is IDataflowObject)
            {
                dbType = SDMXArtefactType.Dataflow;
            }
            else if (artefact is IDataStructureObject)
            {
                dbType = SDMXArtefactType.Dsd;
            }
            else if (artefact is ICodelistObject)
            {
                dbType = SDMXArtefactType.CodeList;
            }
            else
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ArtefactTypeNotRecognized),
                    artefact.GetType(), artefact.Id, artefact.AgencyId, artefact.Version)
                );
            }

            return await GetArtefactDbId(artefact.AgencyId, artefact.Id, new SdmxVersion(artefact.Version), dbType, cancellationToken);
        }

        public async Task<int> GetArtefactDbId(string agencyId, string artefactId, SdmxVersion version, SDMXArtefactType sdmxDbType, CancellationToken cancellationToken)
        {
            return await GetArtefactDbId(agencyId, artefactId, version, DbTypes.GetDbType(sdmxDbType), cancellationToken);
        }

        public async Task<ArtefactItem> GetArtefact(string agencyId, string artefactId, SdmxVersion version, SDMXArtefactType sdmxDbType, CancellationToken cancellationToken, bool errorIfNotFound = true)
        {
            return await GetArtefact(agencyId, artefactId, version, DbTypes.GetDbType(sdmxDbType), cancellationToken, errorIfNotFound);
        }

        public async Task<ArtefactItem> GetArtefactByDbId(int dbId, CancellationToken cancellationToken, bool errorIfNotFound = true)
        {
            var sqlCommand =
                $@"SELECT [ART_ID], [TYPE], [ID], [AGENCY], [VERSION_1], [VERSION_2], [VERSION_3], [DSD_LIVE_VERSION], [DSD_PIT_VERSION]
                            , [DSD_PIT_RELEASE_DATE], [DSD_PIT_RESTORATION_DATE], [DF_DSD_ID], [MSD_ID], [DF_WHERE_CLAUSE], [DSD_MAX_TEXT_ATTR_LENGTH], [LAST_UPDATED],[DATA_COMPRESSION]
                                        FROM [{DotStatDb.ManagementSchema}].[ARTEFACT] WHERE [ART_ID] = " + dbId;
            
            using (var dr = await DotStatDb.ExecuteReaderSqlAsync(sqlCommand, cancellationToken))
            {
                if (dr.Read())
                {
                    return new ArtefactItem
                    {
                        DbId = dr.ColumnValue<int>("ART_ID"),
                        Type = dr["TYPE"].ToString(),
                        Id = dr["ID"].ToString(),
                        Agency = dr["AGENCY"].ToString(),
                        Version = new SdmxVersion((int)dr["VERSION_1"], (int)dr["VERSION_2"],
                            dr.GetNullableValue<int>("VERSION_3")).ToString(),
                        LiveVersion = dr.GetNullableString("DSD_LIVE_VERSION")?[0],
                        PITVersion = dr.GetNullableString("DSD_PIT_VERSION")?[0],
                        PITReleaseDate = dr.GetNullableValue<DateTime>("DSD_PIT_RELEASE_DATE"),
                        PITRestorationDate = dr.GetNullableValue<DateTime>("DSD_PIT_RESTORATION_DATE"),
                        DfDsdId = dr.GetNullableValue<int>("DF_DSD_ID"),
                        DsdMsdId = dr.GetNullableValue<int>("MSD_ID"),
                        DfWhereClause = dr.GetNullableString("DF_WHERE_CLAUSE"),
                        MaxTextAttributeLength = dr.GetNullableValue<int>("DSD_MAX_TEXT_ATTR_LENGTH"),
                        LastModified = dr.GetNullableValue<DateTime>("LAST_UPDATED"),
                        DataCompression = Enum.TryParse(dr.ColumnValue<string>("DATA_COMPRESSION"), true, out DataCompressionEnum dataCompression)
                            ? dataCompression
                            : DataCompressionEnum.NONE
                    };
                }
            }

            if (errorIfNotFound)
            {
                throw new ArtefactNotFoundException();
            }

            return null;
        }

        public async Task<IList<ArtefactItem>> GetListOfArtefacts(CancellationToken cancellationToken, string type = null)
        {
            var result = new List<ArtefactItem>();
            var sqlCommand =
                    $@"SELECT [ART_ID], [TYPE], [ID], [AGENCY], [VERSION_1], [VERSION_2], [VERSION_3], [DSD_LIVE_VERSION], [DSD_PIT_VERSION]
                            , [DSD_PIT_RELEASE_DATE], [DSD_PIT_RESTORATION_DATE], [DF_DSD_ID], [MSD_ID], [DF_WHERE_CLAUSE], [DSD_MAX_TEXT_ATTR_LENGTH], [LAST_UPDATED],[DATA_COMPRESSION]
                                        FROM [{DotStatDb.ManagementSchema}].[ARTEFACT]";

            if (!string.IsNullOrEmpty(type))
            {
                sqlCommand += $" WHERE [TYPE] = '{type}'";
            }

            using (var dr = await DotStatDb.ExecuteReaderSqlAsync(sqlCommand, cancellationToken))
            {
                while (dr.Read())
                {
                    var item = new ArtefactItem
                    {
                        DbId = dr.ColumnValue<int>("ART_ID"),
                        Type = dr["TYPE"].ToString(),
                        Id = dr["ID"].ToString(),
                        Agency = dr["AGENCY"].ToString(),
                        Version = new SdmxVersion((int)dr["VERSION_1"], (int)dr["VERSION_2"],
                            dr.GetNullableValue<int>("VERSION_3")).ToString(),
                        LiveVersion = dr.GetNullableString("DSD_LIVE_VERSION")?[0],
                        PITVersion = dr.GetNullableString("DSD_PIT_VERSION")?[0],
                        PITReleaseDate = dr.GetNullableValue<DateTime>("DSD_PIT_RELEASE_DATE"),
                        PITRestorationDate = dr.GetNullableValue<DateTime>("DSD_PIT_RESTORATION_DATE"),
                        DfDsdId = dr.GetNullableValue<int>("DF_DSD_ID"),
                        DsdMsdId = dr.GetNullableValue<int>("MSD_ID"),
                        DfWhereClause = dr.GetNullableString("DF_WHERE_CLAUSE"),
                        MaxTextAttributeLength = dr.GetNullableValue<int>("DSD_MAX_TEXT_ATTR_LENGTH"),
                        LastModified = dr.GetNullableValue<DateTime>("LAST_UPDATED"),
                        DataCompression = Enum.TryParse(dr.ColumnValue<string>("DATA_COMPRESSION"), true, out DataCompressionEnum dataCompression) 
                            ? dataCompression
                            : DataCompressionEnum.NONE
                    };

                    result.Add(item);
                }
            }

            return result;
        }

        public async Task<IList<ArtefactItem>> GetListOfDataflowArtefactsForDsd(int dsdDbId, CancellationToken cancellationToken)
        {
            var result = new List<ArtefactItem>();
            var sqlCommand =
                $@"SELECT [ART_ID], [TYPE], [ID], [AGENCY], [VERSION_1], [VERSION_2], [VERSION_3], [DSD_LIVE_VERSION], [DSD_PIT_VERSION]
                            , [DSD_PIT_RELEASE_DATE], [DSD_PIT_RESTORATION_DATE], [DF_DSD_ID], [MSD_ID], [DF_WHERE_CLAUSE], [LAST_UPDATED]
                                        FROM [{DotStatDb.ManagementSchema}].[ARTEFACT]
                                        WHERE [TYPE] = 'DF' AND [DF_DSD_ID] = " + dsdDbId;

            using (var dr = await DotStatDb.ExecuteReaderSqlAsync(sqlCommand, cancellationToken))
            {
                while (dr.Read())
                {
                    var item = new ArtefactItem
                    {
                        DbId = dr.ColumnValue<int>("ART_ID"),
                        Type = dr["TYPE"].ToString(),
                        Id = dr["ID"].ToString(),
                        Agency = dr["AGENCY"].ToString(),
                        Version = new SdmxVersion((int)dr["VERSION_1"], (int)dr["VERSION_2"],
                            dr.GetNullableValue<int>("VERSION_3")).ToString(),
                        LiveVersion = dr.GetNullableValue<char>("DSD_LIVE_VERSION"),
                        PITVersion = dr.GetNullableValue<char>("DSD_PIT_VERSION"),
                        PITReleaseDate = dr.GetNullableValue<DateTime>("DSD_PIT_RELEASE_DATE"),
                        PITRestorationDate = dr.GetNullableValue<DateTime>("DSD_PIT_RESTORATION_DATE"),
                        DfDsdId = dr.GetNullableValue<int>("DF_DSD_ID"),
                        DsdMsdId = dr.GetNullableValue<int>("MSD_ID"),
                        DfWhereClause = dr.GetNullableString("DF_WHERE_CLAUSE"),
                        LastModified = dr.GetNullableValue<DateTime>("LAST_UPDATED")
                    };

                    result.Add(item);
                }
            }
            
            return result;
        }

        public async Task<IList<ArtefactItem>> GetListOfCodelistsWithoutDsdAndMsd(CancellationToken cancellationToken)
        {
            var result = new List<ArtefactItem>();
            var sqlCommand =
                $@"SELECT [ART_ID], [TYPE], [ID], [AGENCY], [VERSION_1], [VERSION_2], [VERSION_3], [DSD_LIVE_VERSION], [DSD_PIT_VERSION],
                          [DSD_PIT_RELEASE_DATE], [DSD_PIT_RESTORATION_DATE], [DF_DSD_ID], [MSD_ID], [DF_WHERE_CLAUSE], [LAST_UPDATED]
                     FROM [{DotStatDb.ManagementSchema}].[ARTEFACT] a
                    WHERE a.[TYPE] = 'CL'
                      AND NOT EXISTS (SELECT 1 FROM [{DotStatDb.ManagementSchema}].[COMPONENT] WHERE CL_ID = a.ART_ID
                                       UNION
                                      SELECT 1 FROM [{DotStatDb.ManagementSchema}].[METADATA_ATTRIBUTE] WHERE  CL_ID = a.ART_ID)";

            using (var dr = await DotStatDb.ExecuteReaderSqlAsync(sqlCommand, cancellationToken))
            {
                while (dr.Read())
                {
                    var item = new ArtefactItem
                    {
                        DbId = dr.ColumnValue<int>("ART_ID"),
                        Type = dr["TYPE"].ToString(),
                        Id = dr["ID"].ToString(),
                        Agency = dr["AGENCY"].ToString(),
                        Version = new SdmxVersion((int)dr["VERSION_1"], (int)dr["VERSION_2"],
                            dr.GetNullableValue<int>("VERSION_3")).ToString(),
                        LiveVersion = dr.GetNullableValue<char>("DSD_LIVE_VERSION"),
                        PITVersion = dr.GetNullableValue<char>("DSD_PIT_VERSION"),
                        PITReleaseDate = dr.GetNullableValue<DateTime>("DSD_PIT_RELEASE_DATE"),
                        PITRestorationDate = dr.GetNullableValue<DateTime>("DSD_PIT_RESTORATION_DATE"),
                        DfDsdId = dr.GetNullableValue<int>("DF_DSD_ID"),
                        DsdMsdId = dr.GetNullableValue<int>("MSD_ID"),
                        DfWhereClause = dr.GetNullableString("DF_WHERE_CLAUSE"),
                        LastModified = dr.GetNullableValue<DateTime>("LAST_UPDATED")
                    };

                    result.Add(item);
                }
            }

            return result;
        }

        public async Task FillMeta(Dsd dsd, CancellationToken cancellationToken, bool errorIfNotFound = true)
        {
            var sqlQuery = $@"SELECT [ART_ID],[DSD_LIVE_VERSION],[DSD_PIT_VERSION],[DSD_PIT_RELEASE_DATE],[DSD_PIT_RESTORATION_DATE],[DSD_MAX_TEXT_ATTR_LENGTH],[MSD_ID],[DATA_COMPRESSION],[KEEP_HISTORY]
                    FROM [{DotStatDb.ManagementSchema}].[ARTEFACT] a
                  WHERE a.[ID] = @Id AND a.[AGENCY] = @Agency AND a.[TYPE] = 'DSD'
                     AND a.[VERSION_1] = @Version1 AND a.[VERSION_2] = @Version2
                     AND (a.[VERSION_3] = @Version3 OR (a.[VERSION_3] IS NULL AND @Version3 IS NULL))";

            var sqlParameters = new DbParameter[]
            {
                new SqlParameter("Id", SqlDbType.VarChar) {Value = dsd.Code},
                new SqlParameter("Agency", SqlDbType.VarChar) {Value = dsd.AgencyId},
                new SqlParameter("Version1", SqlDbType.Int) {Value = dsd.Version.Major},
                new SqlParameter("Version2", SqlDbType.Int) {Value = dsd.Version.Minor},
                new SqlParameter("Version3", SqlDbType.Int) {Value = ((object) dsd.Version.Patch) ?? DBNull.Value}
            };

            await using (var dr = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlQuery, cancellationToken, parameters: sqlParameters))
            {
                if (!await dr.ReadAsync(cancellationToken))
                {
                    if(errorIfNotFound)
                        throw new DotStatException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DsdNotFound),
                            dsd.AgencyId, dsd.Code, dsd.Version)
                        );

                    return;
                }

                //TODO: proper char validation
                dsd.DbId = dr.GetInt32(0);
                dsd.LiveVersion = dr.GetNullableString("DSD_LIVE_VERSION")?[0];
                dsd.PITVersion = dr.GetNullableString("DSD_PIT_VERSION")?[0];
                dsd.PITReleaseDate = dr.GetNullableValue<DateTime>("DSD_PIT_RELEASE_DATE");
                dsd.PITRestorationDate = dr.GetNullableValue<DateTime>("DSD_PIT_RESTORATION_DATE");
                dsd.MaxTextAttributeLength = dr.GetNullableValue<int>("DSD_MAX_TEXT_ATTR_LENGTH");
                dsd.MsdDbId = dr.GetNullableValue<int>("MSD_ID");
                dsd.DataCompression = Enum.TryParse(dr.ColumnValue<string>("DATA_COMPRESSION"), true, out DataCompressionEnum dataCompression)
                            ? dataCompression
                            : DataCompressionEnum.NONE;
                dsd.KeepHistory = dr.GetBoolean("KEEP_HISTORY");
            }
        }

        public async Task UpdateMsdOfDsd(int dsdDbId, int? msdDbId, CancellationToken cancellationToken)
        {
            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE [{DotStatDb.ManagementSchema}].[ARTEFACT]
                      SET [MSD_ID] = @MsdDbId,
                          [LAST_UPDATED] = @DT_Now
                    WHERE [ART_ID] = @Id",
                cancellationToken,
                new SqlParameter("MsdDbId", SqlDbType.Int) { Value = (object)msdDbId ?? DBNull.Value },
                new SqlParameter("Id", SqlDbType.Int) { Value = dsdDbId },
                new SqlParameter("DT_Now", DateTime.Now)
             );
        }

        public async Task UpdateDataCompressionOfDsd(int dsdDbId, DataCompressionEnum dataCompression, CancellationToken cancellationToken)
        {
            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE [{DotStatDb.ManagementSchema}].[ARTEFACT]
                      SET [DATA_COMPRESSION] = @DataCompression,
                          [LAST_UPDATED] = @DT_Now
                    WHERE [ART_ID] = @Id",
                cancellationToken, 
                new SqlParameter("DataCompression", dataCompression.ToString()),
                new SqlParameter("Id", SqlDbType.Int) { Value = dsdDbId },
                new SqlParameter("DT_Now", DateTime.Now)
             );
        }
        
        public async Task UpdateKeepHistoryOfDsd(int dsdDbId, bool keepHistory, CancellationToken cancellationToken)
        {
            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE [{DotStatDb.ManagementSchema}].[ARTEFACT]
                      SET [KEEP_HISTORY] = @KeepHistory,
                          [LAST_UPDATED] = @DT_Now
                    WHERE [ART_ID] = @Id",
                cancellationToken,
                new SqlParameter("KeepHistory", keepHistory),
                new SqlParameter("Id", SqlDbType.Int) { Value = dsdDbId },
                new SqlParameter("DT_Now", DateTime.Now)
            );
        }

        public async Task FillMeta(Msd msd, CancellationToken cancellationToken)
        {
            var sqlQuery = $@"SELECT [ART_ID]
                    FROM [{DotStatDb.ManagementSchema}].[ARTEFACT] a
                  WHERE a.[ID] = @Id AND a.[AGENCY] = @Agency AND a.[TYPE] = @Type
                     AND a.[VERSION_1] = @Version1 AND a.[VERSION_2] = @Version2
                     AND (a.[VERSION_3] = @Version3 OR (a.[VERSION_3] IS NULL AND @Version3 IS NULL))";

            var sqlParameters = new DbParameter[]
            {
                new SqlParameter("Id", SqlDbType.VarChar) {Value = msd.Code},
                new SqlParameter("Agency", SqlDbType.VarChar) {Value = msd.AgencyId},
                new SqlParameter("Version1", SqlDbType.Int) {Value = msd.Version.Major},
                new SqlParameter("Version2", SqlDbType.Int) {Value = msd.Version.Minor},
                new SqlParameter("Version3", SqlDbType.Int) {Value = ((object) msd.Version.Patch) ?? DBNull.Value},
                new SqlParameter("Type", SqlDbType.VarChar) {Value = DbTypes.GetDbType(SDMXArtefactType.Msd)},
            };

            await using (var dr = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlQuery, cancellationToken, CommandBehavior.SingleRow, parameters: sqlParameters))
            {
                if (!await dr.ReadAsync(cancellationToken))
                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MsdNotFound),
                        msd.AgencyId, msd.Code, msd.Version)
                    );

                msd.DbId = dr.GetInt32(0);
            }
        }

        public async Task<bool> CheckSupportOfTimeAtTimeDimension(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken, string columnName = null)
        {
            var factTableName = dsd.SqlFactTable((char)tableVersion);

            if (string.IsNullOrEmpty(columnName))
            {
                columnName = DbExtensions.SqlPeriodStart();
            }

            var dataTypeUsedInDb = (string)await DotStatDb.ExecuteScalarSqlAsync(
                $"SELECT typ.name FROM sys.all_columns c JOIN sys.tables t ON c.object_id = t.object_id AND t.name = '{factTableName}' JOIN sys.types typ ON typ.system_type_id = c.system_type_id WHERE c.name = '{columnName}'",
                cancellationToken
            );

            if (dataTypeUsedInDb == null)
            {
                // When column does not exist use the DSD annotation to determine the support
                return dsd.Base.HasSupportDateTimeAnnotation();
            }

            return string.Equals(dataTypeUsedInDb, "datetime2", StringComparison.InvariantCultureIgnoreCase);
        }

        public async Task<AvailabilityHelper> CreateDataAvailabilityOfDataFlow(Dataflow dataFlow, string dataFlowWhereClause, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            var availabilityHelper = new AvailabilityHelper(dataFlow.Dsd);
            var viewName = dataFlow.Dsd.SqlDataDsdViewName((char)tableVersion);
            var where = !string.IsNullOrWhiteSpace(dataFlowWhereClause) ? $"\nWHERE {dataFlowWhereClause}" : null;

            //Dimensions
            var nonTimeDimensions = dataFlow.Dsd.Dimensions
                .Where(x => !x.Base.TimeDimension).ToList();

            //Enable Micro-data use cases which do not need the available codes to be added to the ACC for non-coded dimensions.
            if (GeneralConfiguration.ExcludeNonCodedDimensionsDuringConstraintCalculation)
            {
                nonTimeDimensions = nonTimeDimensions.Where(d => d.Base.HasCodedRepresentation()).ToList();
            }

            var noTimeDimQuery = string.Join("\n\tUNION ALL\n",
                nonTimeDimensions.Select(
                    dim => $@"SELECT DISTINCT '{dim.Code}' AS [ID_Dim], [{dim.Code}] as [ITEM_ID]
FROM [{DotStatDb.DataSchema}].[{viewName}] {where}"));

            if (!string.IsNullOrEmpty(noTimeDimQuery))
            {
                await using var r = await DotStatDb.ExecuteReaderSqlAsync(noTimeDimQuery, cancellationToken, tryUseReadOnlyConnection: true);
                while (await r.ReadAsync(cancellationToken))
                    availabilityHelper.AddDimKey(r.GetString(0), r.GetString(1));
            }

            //Time dimension
            var timeDim = dataFlow.Dsd.Dimensions.FirstOrDefault(x => x.Base.TimeDimension);
            var timeDimQuery = timeDim is null ?
                null : $@"SELECT min({DbExtensions.SqlPeriodStart()}),max({DbExtensions.SqlPeriodEnd()}) 
FROM [{DotStatDb.DataSchema}].[{viewName}] {where}";

            if (!string.IsNullOrEmpty(timeDimQuery))
            {
                await using var r = await DotStatDb.ExecuteReaderSqlAsync(timeDimQuery, cancellationToken, tryUseReadOnlyConnection: true, commandBehavior: CommandBehavior.SingleRow);
                if (await r.ReadAsync(cancellationToken))
                    availabilityHelper.AddTime(
                        r.GetNullableValue<DateTime>(0),
                        r.GetNullableValue<DateTime>(1)
                    );
            }

            return availabilityHelper;
        }

        public async Task VerifyAndCreateMeasures(Dsd dsd, IList<ComponentItem> componentItems,
            bool factTableExists, CancellationToken cancellationToken, bool throwConcurrencyError = true)
        {
            var dataTypeEnumHelper = new DataTypeEnumHelper(DotStatDb, tryUseReadOnlyConnection: false);

            var measuresInDb = componentItems.Where(ci => ci.IsPrimaryMeasure)
                .ToDictionary(ci => ci.Id, StringComparer.InvariantCultureIgnoreCase);

            var measure = dsd.PrimaryMeasure;

            measuresInDb.TryGetValue(measure.Code, out var measureInManagementDb);

            measuresInDb.Remove(measure.Code);

            // check & warn if DSD has no measure representation
            if (!factTableExists && measure.Codelist == null && measure.IsMissingRepresentation)
            {
                Log.Warn(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoMeasureRepresentationWarning), dsd.FullId, measure.Code));
            }

            if (factTableExists && measureInManagementDb == null)
            {
                throw new ChangeInDsdException(dsd.FullId,
                    string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ComponentNotFound),
                        measure.FullId, dsd.FullId));
            }

            if (measureInManagementDb != null)
            {
                // When the measure already exists in data db then check if there are any changes

                if (measureInManagementDb.IsCoded ^ measure.Base.HasCodedRepresentation())
                {

                    string representationInDb;
                    if (measureInManagementDb.CodelistId.HasValue)
                    {
                        var codeListInDb = await GetArtefactByDbId(measureInManagementDb.CodelistId.Value, cancellationToken);

                        representationInDb = codeListInDb.ToString();
                    }
                    else
                    {
                        representationInDb = dataTypeEnumHelper.GetValueFromId(measureInManagementDb.EnumId.Value);
                    }

                    var newRepresentation = measure.Base.HasCodedRepresentation()
                        ? measure.Codelist.FullId
                        : measure.TextFormat.TextType.EnumType.ToString();

                    throw new ChangeInDsdException(dsd.FullId,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .ChangeInDsdMeasureCodedRepresentationChanged), measure.FullId, representationInDb,
                            newRepresentation));
                }

                if (!measure.Base.HasCodedRepresentation())
                {
                    var measureTextFormat = measure.TextFormat.TextType?.EnumType.ToString() ??
                                            TextEnumType.String.ToString();

                    if (dataTypeEnumHelper.GetIdFromValue(measureTextFormat) != measureInManagementDb.EnumId)
                    {
                        throw new ChangeInDsdException(dsd.FullId,
                            string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                    .ChangeInDsdMeasureTextFormatChanged),
                                measure.FullId,
                                dataTypeEnumHelper.GetValueFromId(measureInManagementDb.EnumId.Value),
                                measureTextFormat));
                    }

                    if (measure.TextFormat.Pattern != measureInManagementDb.Pattern)
                    {
                        throw new ChangeInDsdException(dsd.FullId,
                            string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                    .ChangeInDsdMeasurePatternFacetChanged),
                                measure.FullId,
                                measureInManagementDb.Pattern ?? "null",
                                measure.TextFormat.Pattern ?? "null"));
                    }

                    if (measure.TextFormat.MinLength != measureInManagementDb.MinLength)
                    {
                        throw new ChangeInDsdException(dsd.FullId,
                            string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                    .ChangeInDsdMeasureMinLengthFacetChanged),
                                measure.FullId,
                                measureInManagementDb.MinLength?.ToString() ?? "null",
                                measure.TextFormat.MinLength?.ToString() ?? "null"));
                    }

                    if (measure.TextFormat.MaxLength != measureInManagementDb.MaxLength)
                    {
                        throw new ChangeInDsdException(dsd.FullId,
                            string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                    .ChangeInDsdMeasureMaxLengthFacetChanged),
                                measure.FullId,
                                measureInManagementDb.MaxLength?.ToString() ?? "null",
                                measure.TextFormat.MaxLength?.ToString() ?? "null"));
                    }

                    if (measure.TextFormat.MinValue != measureInManagementDb.MinValue)
                    {
                        throw new ChangeInDsdException(dsd.FullId,
                            string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                    .ChangeInDsdMeasureMinValueFacetChanged),
                                measure.FullId,
                                measureInManagementDb.MinValue?.ToString() ?? "null",
                                measure.TextFormat.MinValue?.ToString() ?? "null"));
                    }

                    if (measure.TextFormat.MaxValue != measureInManagementDb.MaxValue)
                    {
                        throw new ChangeInDsdException(dsd.FullId,
                            string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                    .ChangeInDsdMeasureMaxValueFacetChanged),
                                measure.FullId,
                                measureInManagementDb.MaxValue?.ToString() ?? "null",
                                measure.TextFormat.MaxValue?.ToString() ?? "null"));
                    }
                }
            }

            if (measure.Base.HasCodedRepresentation())
            {
                await VerifyAndCreateCodelistOfComponent(dsd, measure, measureInManagementDb, factTableExists, cancellationToken, throwConcurrencyError);
            }

            var measureBuilder = new SqlPrimaryMeasureEngine(GeneralConfiguration, dataTypeEnumHelper);

            try
            {
                measure.DbId = (measureInManagementDb?.DbId ?? -1) < 1
                    ? await measureBuilder.InsertToComponentTable(measure, DotStatDb, cancellationToken)
                    : measureInManagementDb.DbId;
            }
            catch
            {
                await measureBuilder.CleanUp(measure, DotStatDb, cancellationToken);

                throw;
            }

            if (measuresInDb.Count > 0)
            {
                throw new ChangeInDsdException(
                    dsd.FullId,
                    string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ChangeInDsdMeasureRemoved),
                        "change in dsd - measure removed {0}",
                        string.Join(", ", measuresInDb.Select(m => m.Value.Id))));
            }
        }

        public async Task VerifyAndCreateOrUpdateDynamicTables(Dsd dsd, bool factTableExists, CancellationToken cancellationToken)
        {
            var attributeBuilder = new SqlAttributeEngine(GeneralConfiguration);
            var dsdBuilder = new SqlDsdEngine(GeneralConfiguration);

            var newMaxAttributeLength = dsd.GetMaxLengthOfTextAttributeFromConfig(GeneralConfiguration.MaxTextAttributeLength);

            Log.Debug($"MaxAttributeLength value used: {newMaxAttributeLength}");

            if (!factTableExists)
            {

                //Calculate and set current max text attribute length
                dsd.MaxTextAttributeLength = newMaxAttributeLength;
                await UpdateMaxAttributeLengthOfDsdArtefact(dsd, cancellationToken);

                await attributeBuilder.CreateDynamicDbObjects(dsd, DotStatDb, cancellationToken);

                await dsdBuilder.CreateDynamicDbObjects(dsd, DotStatDb, cancellationToken);
            }
            else
            {
                // Check if max length of text attribute has changed since the last transaction (either in dsd annotation or general configuration) and alter tables if there is change
                var dsdMaxAttributeLengthInDb = await GetDsdMaxTextAttributeLength(dsd, cancellationToken);

                if (newMaxAttributeLength != dsdMaxAttributeLengthInDb)
                {
                    // If there are non-coded attributes defined on DSD then related tables need to be checked and altered
                    if (dsd.Attributes.Any(a => !a.Base.HasCodedRepresentation()))
                    {
                        // If the new value is less then the previous one then database content needs to be checked as data can be lost when shrinking column size
                        if (newMaxAttributeLength > 0 && (dsdMaxAttributeLengthInDb == 0 || newMaxAttributeLength < dsdMaxAttributeLengthInDb))
                        {
                            // This option is currently not supported.
                            Log.Warn(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MaximumTextAttributeLengthDecreaseNotSupported), dsdMaxAttributeLengthInDb, newMaxAttributeLength));
                        }
                        else
                        {
                            dsd.MaxTextAttributeLength = newMaxAttributeLength;

                            await dsdBuilder.AlterTextAttributeColumns(dsd, newMaxAttributeLength, DotStatDb, cancellationToken);

                            await attributeBuilder.AlterTextAttributeColumns(dsd, newMaxAttributeLength, DotStatDb, cancellationToken);

                            await UpdateMaxAttributeLengthOfDsdArtefact(dsd, cancellationToken);

                            Log.Notice(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MaximumTextAttributeLengthNewValueApplied), newMaxAttributeLength, dsd.FullId));
                        }
                    }
                }
            }
        }

        public async Task SetKeepHistoryOn(Dsd dsd, CancellationToken cancellationToken)
        {
            var tables = new Dictionary<string, string>() {
                {$"{dsd.SqlFactTable((char)DbTableVersion.A)}",$"{dsd.SqlFactHistoryTable((char)DbTableVersion.A)}"},
                {$"{dsd.SqlFactTable((char)DbTableVersion.B)}",$"{dsd.SqlFactHistoryTable((char)DbTableVersion.B)}"}
            };

            //Check if dsd has dim group attribute tables
            if (dsd.Attributes
                .Where(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.Group or AttributeAttachmentLevel.DimensionGroup)
                .Any(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)))
            {
                tables.Add($"{dsd.SqlDimGroupAttrTable((char)DbTableVersion.A)}",
                    $"{dsd.SqlDimGroupAttrHistoryTable((char)DbTableVersion.A)}");
                tables.Add($"{dsd.SqlDimGroupAttrTable((char)DbTableVersion.B)}",
                    $"{dsd.SqlDimGroupAttrHistoryTable((char)DbTableVersion.B)}");
            }

            //Check if dsd has dsd attribute tables
            if (dsd.Attributes
                .Any(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null))
            {
                tables.Add($"{dsd.SqlDsdAttrTable((char)DbTableVersion.A)}",
                    $"{dsd.SqlDsdAttrHistoryTable((char)DbTableVersion.A)}");
                tables.Add($"{dsd.SqlDsdAttrTable((char)DbTableVersion.B)}",
                    $"{dsd.SqlDsdAttrHistoryTable((char)DbTableVersion.B)}");
                
                //Add PK to DsdAttrTables if not exists (it is a requirement of SQL temporal tables feature)
                const string sqlCommand = @"
IF NOT EXISTS(SELECT 1 FROM sys.objects WHERE type = 'PK' AND  parent_object_id = OBJECT_ID ('{tableName}'))
BEGIN
ALTER TABLE {tableName}
ADD PRIMARY KEY (DF_ID)
END;";
                var tableA = sqlCommand.Replace("{tableName}", $"[{DotStatDb.DataSchema}].[{dsd.SqlDsdAttrTable((char)DbTableVersion.A)}]");
                await DotStatDb.ExecuteNonQuerySqlAsync(tableA, cancellationToken);

                var tableB = sqlCommand.Replace("{tableName}", $"[{DotStatDb.DataSchema}].[{dsd.SqlDsdAttrTable((char)DbTableVersion.B)}]");
                await DotStatDb.ExecuteNonQuerySqlAsync(tableB, cancellationToken);
            }

            //Create temporal tables
            foreach (var (tableName, historyTableName) in tables)
            {
                await DotStatDb.AddTemporalTableSupport(DotStatDb.DataSchema, tableName, historyTableName, cancellationToken);
            }

            //Create temporal views
            await UpdateKeepHistoryOfDsd(dsd.DbId, keepHistory: true, cancellationToken);

            dsd.KeepHistory = true;
        }

        public async Task VerifyAndCreateDataflow(Dataflow dataflow, CancellationToken cancellationToken)
        {
            var dbId = await GetArtefactDbId(dataflow, cancellationToken);
            if (dbId <= 0)
            {
                var dataflowBuilder = new SqlDataflowEngine(GeneralConfiguration);

                try
                {
                    dataflow.DbId = await dataflowBuilder.InsertToArtefactTable(dataflow, DotStatDb, cancellationToken);

                    await dataflowBuilder.CreateDynamicDbObjects(dataflow, DotStatDb, cancellationToken);
                }
                catch
                {
                    await dataflowBuilder.CleanUp(dataflow, DotStatDb, cancellationToken);

                    throw;
                }
            }
            else
            {
                dataflow.DbId = dbId;
            }
        }

        public async Task SetKeepHistoryOn(Dataflow dataflow, CancellationToken cancellationToken)
        {
            //Create temporal views
            var dataflowEngine = new SqlDataflowEngine(GeneralConfiguration);

            await dataflowEngine.CreateHistoryViews(dataflow, DotStatDb, cancellationToken);
           
        }

        public async ValueTask VerifyAndCreateOrUpdateMetadataDynamicTables(Dsd dsd, bool metaDataTableExists, CancellationToken cancellationToken)
        {
            if (metaDataTableExists) return;

            var msdBuilder = new SqlMsdEngine(GeneralConfiguration);
            await msdBuilder.CreateDynamicDbObjects(dsd, DotStatDb, cancellationToken);
        }

        public async ValueTask VerifyAndCreateCodelistOfComponent(Dsd dsd, IDotStatCodeListBasedIdentifiable component,
            ComponentItem componentInDb, bool factTableExists, CancellationToken cancellationToken, bool throwConcurrencyError = true)
        {
            if (component.Codelist == null)
            {
                // If component has no codelist there is nothing to check.
                return;
            }

            var codelistBuilder = new SqlCodelistEngine(GeneralConfiguration);

            var clId = await GetArtefactDbId(component.Codelist, cancellationToken);

            if (clId <= 0) // Code list not present in ARTEFACT table
            {
                if (factTableExists)
                {
                    // When fact table exists then all components and artifact were already created, so
                    //   if code list of component not found in ARTEFACT table it means the code list of component has
                    //   changed to another codelist not present id data db yet.
                    throw new ChangeInDsdException(dsd.FullId, string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            component.ResourceIdOfCodelistChanged), component.FullId, component.Codelist.FullId));
                }

                try
                {
                    component.Codelist.DbId = await codelistBuilder.InsertToArtefactTable(component.Codelist, DotStatDb, cancellationToken);
                    await codelistBuilder.CreateDynamicDbObjects(component.Codelist, DotStatDb, cancellationToken);
                }
                catch (SqlException ex) when (ex.Number == 2627 || ex.Number == 2601)
                {
                    // 2601 - Cannot insert duplicate key row in object '%.*ls' with unique index '%.*ls'.The duplicate key value is % ls.
                    // 2627 - Violation of % ls constraint '%.*ls'.Cannot insert duplicate key in object '%.*ls'.The duplicate key value is % ls.
                    // Concurrent process already inserted row for the same code list, so unique key constraint violation.

                    await codelistBuilder.CleanUp(component.Codelist, DotStatDb, cancellationToken);
                    if (throwConcurrencyError)
                    {
                        throw new DotStatException(
                            string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                    .TransactionAbortedDueToConcurrentArtefactAccess), component.Codelist.FullId),
                            ex);
                    }
                }
                catch
                {
                    await codelistBuilder.CleanUp(component.Codelist, DotStatDb, cancellationToken);

                    throw;
                }
            }
            else // Codelist is present in ARTEFACT table
            {
                if (componentInDb != null &&
                    (!componentInDb.CodelistId.HasValue || componentInDb.CodelistId.Value != clId))
                {
                    throw new ChangeInDsdException(dsd.FullId, string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            component.ResourceIdOfCodelistChanged), component.FullId, component.Codelist.FullId));
                }

                component.Codelist.DbId = clId;

                await CheckCodesOfCodedComponent(dsd.FullId, component, cancellationToken, codelistBuilder);
            }
        }

        public async Task GetOrCreateMsd(Msd msd, CancellationToken cancellationToken)
        {
            var dbId = await GetArtefactDbId(msd, cancellationToken);
            if (dbId <= 0)
            {
                var msdBuilder = new SqlMsdEngine(GeneralConfiguration);

                try
                {
                    msd.DbId = await msdBuilder.InsertToArtefactTable(msd, DotStatDb, cancellationToken);
                }
                catch
                {
                    await msdBuilder.CleanUp(msd, DotStatDb, cancellationToken);

                    throw;
                }
            }
            else
            {
                msd.DbId = dbId;
            }
        }

        public async Task CreateMetadataAttribute(MetadataAttribute attr, CancellationToken cancellationToken)
        {
            var metadataAttributeBuilder = new SqlMetadataAttributeEngine(GeneralConfiguration);
            try
            {
                attr.DbId = await metadataAttributeBuilder.InsertToComponentTable(attr, DotStatDb, cancellationToken);
            }
            catch
            {
                await metadataAttributeBuilder.CleanUp(attr, DotStatDb, cancellationToken);
                throw;
            }
        }
        
        public async Task<bool> CreateMetaDataFlow(Dataflow dataFlow, bool dataFlowMetaDataTableExists, CancellationToken cancellationToken)
        {
            if (dataFlowMetaDataTableExists) return false;
            var metaDataFlowBuilder = new SqlMetadataDataflowEngine(GeneralConfiguration);

            try
            {
                return await metaDataFlowBuilder.CreateDynamicDbObjects(dataFlow, DotStatDb, cancellationToken);
            }
            catch
            {
                await metaDataFlowBuilder.CleanUp(dataFlow, DotStatDb, cancellationToken);
                throw;
            }
        }

        public async Task<ComponentItem> TryCreateDimensionComponent(Dimension dim, SDMXArtefactType dimType, CancellationToken cancellationToken)
        {
            var dimensionBuilder = new SqlDimensionEngine(GeneralConfiguration, new DataTypeEnumHelper(DotStatDb, tryUseReadOnlyConnection: false));

            try
            {
                dim.DbId = await dimensionBuilder.InsertToComponentTable(dim, DotStatDb, cancellationToken);
                return new ComponentItem
                {
                    DbId = dim.DbId,
                    DsdId = dim.Dsd.DbId,
                    Id = dim.Base.Id,
                    Type = DbTypes.GetDbType(dimType),
                    CodelistId = dim.Codelist?.DbId
                };
            }
            catch
            {
                await dimensionBuilder.CleanUp(dim, DotStatDb, cancellationToken);

                throw;
            }
        }

        #region Private methods

        private string GetValueListString<T>(T[] values)
        {
            if (values.Length == 0)
            {
                throw new ArgumentException("values");
            }

            var sb = new StringBuilder();
            sb.Append("(");
            sb.Append(string.Join(", ", values));
            sb.Append(")");

            return sb.ToString();
        }
        
        private async Task<int> GetArtefactDbId(string agencyId, string artefactId, SdmxVersion version, string dbType, CancellationToken cancellationToken, bool errorIfNotFound = false)
        {
            var sql = new StringBuilder($@"SELECT [ART_ID]
                FROM [{DotStatDb.ManagementSchema}].[ARTEFACT] 
                WHERE [ID] = @Id AND [AGENCY] = @Agency AND [TYPE] = @Type
                    AND [VERSION_1] = @Version1 AND [VERSION_2] = @Version2");

            var @params = new List<DbParameter>()
            {
                new SqlParameter("Id", SqlDbType.VarChar) {Value = artefactId},
                new SqlParameter("Agency", SqlDbType.VarChar) {Value = agencyId},
                new SqlParameter("Type", SqlDbType.VarChar) {Value = dbType},
                new SqlParameter("Version1", SqlDbType.Int) {Value = version.Major},
                new SqlParameter("Version2", SqlDbType.Int) {Value = version.Minor}
            };

            if (version.Patch.HasValue)
            {
                sql.Append(" AND [VERSION_3] = @Version3");
                @params.Add(new SqlParameter("Version3", SqlDbType.Int) { Value = version.Patch.Value });
            }
            else
            {
                sql.Append(" AND [VERSION_3] IS NULL");
            }

            var result = await DotStatDb.ExecuteScalarSqlWithParamsAsync(sql.ToString(),cancellationToken, @params.ToArray());

            if (result != null)
                return (int) result;

            if (!errorIfNotFound)
                return -1;

            throw new ArtefactNotFoundException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ArtefactNotFoundInManagementDb),
                agencyId,artefactId,version,dbType)
            );
        }

        private async Task<ArtefactItem> GetArtefact(string agencyId, string artefactId, SdmxVersion version, string dbType, CancellationToken cancellationToken, bool errorIfNotFound = false)
        {
            var sqlCommand =
                $@"SELECT * FROM [{DotStatDb.ManagementSchema}].[ARTEFACT] 
                    WHERE [ID] = @Id AND [AGENCY] = @Agency AND [TYPE] = @Type
                        AND [VERSION_1] = @Version1 AND [VERSION_2] = @Version2
                        {(version.Patch.HasValue?" AND[VERSION_3] = @Version3": " AND [VERSION_3] IS NULL")}";

            var sqlParameters = new DbParameter[]
            {
                new SqlParameter("Id", SqlDbType.VarChar) {Value = artefactId},
                new SqlParameter("Agency", SqlDbType.VarChar) {Value = agencyId},
                new SqlParameter("Version1", SqlDbType.Int) {Value = version.Major},
                new SqlParameter("Version2", SqlDbType.Int) {Value = version.Minor},
                new SqlParameter("Version3", SqlDbType.Int) {Value = ((object) version.Patch) ?? DBNull.Value},
                new SqlParameter("Type", SqlDbType.VarChar) {Value = dbType},
            };

            await using (var dr = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlCommand, cancellationToken, CommandBehavior.SingleRow, parameters: sqlParameters))
            {
                if (await dr.ReadAsync(cancellationToken))
                {
                    return new ArtefactItem
                    {
                        DbId = dr.ColumnValue<int>("ART_ID"),
                        Type = dr["TYPE"].ToString(),
                        Id = dr["ID"].ToString(),
                        Agency = dr["AGENCY"].ToString(),
                        Version = new SdmxVersion((int)dr["VERSION_1"], (int)dr["VERSION_2"],
                            dr.GetNullableValue<int>("VERSION_3")).ToString(),
                        LiveVersion = dr.GetNullableString("DSD_LIVE_VERSION")?[0],
                        PITVersion = dr.GetNullableString("DSD_PIT_VERSION")?[0],
                        PITReleaseDate = dr.GetNullableValue<DateTime>("DSD_PIT_RELEASE_DATE"),
                        PITRestorationDate = dr.GetNullableValue<DateTime>("DSD_PIT_RESTORATION_DATE"),
                        DfDsdId = dr.GetNullableValue<int>("DF_DSD_ID"),
                        DsdMsdId = dr.GetNullableValue<int>("MSD_ID"),
                        DfWhereClause = dr.GetNullableString("DF_WHERE_CLAUSE"),
                        MaxTextAttributeLength = dr.GetNullableValue<int>("DSD_MAX_TEXT_ATTR_LENGTH"),
                        LastModified = dr.GetNullableValue<DateTime>("LAST_UPDATED")
                    };
                }
            }

            if (!errorIfNotFound)
                return new ArtefactItem();

            throw new ArtefactNotFoundException(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ArtefactNotFoundInManagementDb),
                agencyId, artefactId, version, dbType)
            );
        }

        private async Task<IEnumerable<int>> GetDsdDbIdsLinkedToMsd(int msdDbId, CancellationToken cancellationToken)
        {
            var result = new HashSet<int>();
            var sqlCommand =
                $@"SELECT [ART_ID] 
                        FROM [{DotStatDb.ManagementSchema}].[ARTEFACT] 
                           WHERE [MSD_ID] = {msdDbId} AND [TYPE] = '{DbTypes.GetDbType(SDMXArtefactType.Dsd)}'";

            using (var dr = await DotStatDb.ExecuteReaderSqlAsync(sqlCommand, cancellationToken))
            {
                while (dr.Read())
                {
                    result.Add((int)dr["ART_ID"]);
                }
            }

            return result;
        }
        
        private async Task CheckCodesOfCodedComponent(string parentArtFullId, IDotStatCodeListBasedIdentifiable component, CancellationToken cancellationToken
            , SqlCodelistEngine codelistBuilder = null, bool isDsd = true)
        {
            if (codelistBuilder == null)
            {
                codelistBuilder = new SqlCodelistEngine(GeneralConfiguration);
            }

            var codesInDb = new HashSet<string>(await codelistBuilder.GetCodesOfCodeList(component.Codelist, DotStatDb, cancellationToken));

            var codesInCodelist = new HashSet<string>(component.Codelist.Codes.Select(c => c.Code));

            // Check if any codes were deleted
            var deletedCodes = codesInDb.Except(codesInCodelist, StringComparer.InvariantCultureIgnoreCase).ToArray();

            if (deletedCodes.Length > 0)
            {
                throw isDsd
                    ? new ChangeInDsdException(parentArtFullId,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(component.ResourceIdOfCodelistCodeRemoved),
                            component.Codelist.FullId, component.FullId, string.Join(", ", deletedCodes)))
                    : new ChangeInMsdException(parentArtFullId,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(component.ResourceIdOfCodelistCodeRemoved),
                            component.Codelist.FullId, component.FullId, string.Join(", ", deletedCodes)));
            }

            // Add new codes
            var newCodes = codesInCodelist.Except(codesInDb, StringComparer.InvariantCultureIgnoreCase).ToArray();

            if (newCodes.Length > 0)
            {
                await codelistBuilder.InsertCodes(component.Codelist, newCodes, DotStatDb, cancellationToken);

                Log.Notice(string.Format(
                    LocalizationRepository.GetLocalisedResource(component.ResourceIdOfCodelistCodeAdded),
                    component.Codelist.FullId, component.FullId, string.Join(", ", newCodes)));
            }
        }
        

        #endregion
    }
}
