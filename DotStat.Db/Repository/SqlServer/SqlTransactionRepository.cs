using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Common.Logger;
using Transaction = DotStat.Domain.Transaction;
using DotStat.Db.Dto;
using System.Threading.Tasks;
using System.Threading;
using DotStat.Db.DB;
using TransactionStatus = DotStat.Domain.TransactionStatus;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using DotStat.Common.Exceptions;
using System.Transactions;

namespace DotStat.Db.Repository.SqlServer
{
    public class SqlTransactionRepository : DatabaseRepositoryBase<SqlDotStatDb>, ITransactionRepository
    {
        public SqlTransactionRepository(SqlDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }
        
        public async Task<bool> UpdateTableVersionOfTransaction(int transactionId, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION]
                     SET [TABLE_VERSION] = @TargetVersion
                   WHERE [TRANSACTION_ID] = @Id",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.Int) { Value = transactionId },
                new SqlParameter("TargetVersion", SqlDbType.Char) { Value = (char)tableVersion }
            ) > 0;
        }

        public async Task<bool> UpdateFinalTargetVersionOfTransaction(int transactionId, TargetVersion targetVersion, CancellationToken cancellationToken)
        {
            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION]
                     SET [FINAL_TARGET_VERSION] = @TargetVersion
                   WHERE [TRANSACTION_ID] = @Id",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.Int) { Value = transactionId },
                new SqlParameter("TargetVersion", SqlDbType.VarChar) { Value = targetVersion }
            ) > 0;
        }

        public async Task<bool> UpdateArtefactFullIdOfTransaction(int transactionId, string artefactFullId, CancellationToken cancellationToken)
        {
            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION]
                     SET [ARTEFACT_FULL_ID] = @ArtefactFull
                   WHERE [TRANSACTION_ID] = @Id",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.Int) { Value = transactionId },
                new SqlParameter("ArtefactFull", SqlDbType.VarChar) { Value = artefactFullId }
            ) > 0;
        }

        public async Task<bool> TryLockNewTransaction(Transaction transaction, bool isTransactionWithNoDsd, DbTableVersion origTransferTargetTableVersion, CancellationToken cancellationToken, bool logErrors = true)
        {
            var dbId = transaction.ArtefactDbId ?? -1;
            try
            {
                Log.Debug($"Attempting to lock a new transaction for DSD id: {dbId} [{transaction.ArtefactFullId ?? SpecialTransactionArtefactIds.None}]");

                if (!await LockTransaction(transaction.Id, dbId, transaction.ArtefactChildDbId, origTransferTargetTableVersion, transaction.RequestedTargetVersion, cancellationToken))
                    return false;

                if (dbId > 0 && !await EnsureOnlySingleDsdTransaction(transaction, cancellationToken))
                    return false;

                //New transaction trying to block all the service
                if (dbId == -1)
                {
                    var ongoingTransactions = await GetNonCompletedTransactions(transaction.Id, cancellationToken);
                    //Check that there are no running transactions for any artefact
                    if (ongoingTransactions.Count > 0)
                    {
                        if (logErrors)
                        {
                            Log.Error(new DotStatException(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MaintenanceTransactionFailedOngoingTransactions),
                                transaction.Type.ToString(),
                                ongoingTransactions.Count,
                                string.Join(",", ongoingTransactions))));
                        }

                        return false;
                    }
                }
                //New transaction for specific artefact
                else
                {
                    var blockingTransactionId = await GetNonCompletedTransactionBlockingAll(transaction.Id, cancellationToken);
                    //Check that there are no running transactions blocking all the service
                    if (blockingTransactionId > -1)
                    {
                        if (logErrors)
                        {
                            Log.Error(new DotStatException(string.Format(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.OngoingTransactionBlockingAll),
                                blockingTransactionId)));
                        }

                        return false;
                    }
                }

                return true;
            }
            catch (SqlException ex) when (ex.Number == 2627 || ex.Number == 2601)
            {
                //2601 - Cannot insert duplicate key row in object '%.*ls' with unique index '%.*ls'.The duplicate key value is % ls.
                //2627 - Violation of % ls constraint '%.*ls'.Cannot insert duplicate key in object '%.*ls'.The duplicate key value is % ls.
                //Concurrent process already inserted row for the same dsd, so primary key constraint violation.
                var transactionInProgress = await GetInProgressTransactions(transaction.Id, dbId, cancellationToken).FirstOrDefaultAsync(cancellationToken);

                if (logErrors)
                {
                    var errorMsg = isTransactionWithNoDsd
                    ? string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                            .TransactionAbortedDueToConcurrentTransactionWithNoDsd),
                        transaction.Id,
                        transactionInProgress?.Id.ToString() ?? "unknown")
                    : string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                            .TransactionAbortedDueToConcurrentTransaction),
                        transaction.Id,
                        transaction.ArtefactFullId,
                        transactionInProgress?.Id.ToString() ?? "unknown");

                    Log.Error(new DotStatException(errorMsg, ex));
                }

                return false;
            }
        }

        public async Task<int> GetNextTransactionId(CancellationToken cancellationToken)
        {
            var ret = await DotStatDb.ExecuteScalarSqlAsync(
                $@"SELECT NEXT VALUE FOR [{DotStatDb.ManagementSchema}].[TRANSFER_SEQUENCE]",
                cancellationToken
            );

            return (int)ret;
        }

        public async Task<bool> MarkTransactionReadyForValidation(int transactionId, CancellationToken cancellationToken)
        {
            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION]
                      SET [EXECUTION_END] = @DT_Now
                    WHERE [TRANSACTION_ID] = @Id",
                cancellationToken,
                new SqlParameter("DT_Now", DateTime.UtcNow),
                new SqlParameter("Id", transactionId)
            ) > 0;
        }

        public async IAsyncEnumerable<TransactionLog> GetTransactionLogs(int transactionId, bool includeFatal, [EnumeratorCancellation] CancellationToken cancellationToken, bool tryUseReadOnlyConnection = false)
        {
            var sqlCommand = $@"SELECT [DATE],[LEVEL],[SERVER],[LOGGER],[MESSAGE],[EXCEPTION],[USEREMAIL] 
                        FROM [{DotStatDb.ManagementSchema}].[LOGS] WITH (READPAST)
                        WHERE [TRANSACTION_ID] = @Id {(!includeFatal ? " AND [LEVEL] != 'FATAL' " : string.Empty)}
                        ORDER BY [DATE]";
            
            await using (var dr = await DotStatDb.ExecuteReaderSqlWithParamsAsync(
                sqlCommand, 
                cancellationToken,
                tryUseReadOnlyConnection: tryUseReadOnlyConnection,
                parameters: new SqlParameter("Id", SqlDbType.Int) { Value = transactionId }))
            {
                while (await dr.ReadAsync(cancellationToken))
                {
                    yield return new TransactionLog
                    {
                        Id = transactionId,
                        Date = GetDateTime(dr, 0)?.ToString(Transaction.TransactionDatetimeFormat),
                        Level = GetString(dr, 1),
                        Server = GetString(dr, 2),
                        Logger = GetString(dr, 3),
                        Message = GetString(dr, 4),
                        Exception = GetString(dr, 5),
                        UserEmail = GetString(dr, 6)
                    };

                }
            }
        }

        public async Task<Transaction> CreateTransactionItem(int transactionId, ArtefactItem artefact, DotStatPrincipal principal, string sourceDataSpace, string dataSource, TransactionType type, TargetVersion? requestedTargetVersion, string serviceId, CancellationToken cancellationToken, bool blockAllTransactions = false)
        {
            return await CreateTransactionItem(transactionId, artefact?.ToString(), principal, sourceDataSpace, dataSource, type, requestedTargetVersion, serviceId, cancellationToken, blockAllTransactions);
        }
       
        public async Task<Transaction> CreateTransactionItem(int transactionId, string atrFullId, DotStatPrincipal principal, string sourceDataSpace, string dataSource, TransactionType type, TargetVersion? requestedTargetVersion, string serviceId, CancellationToken cancellationToken, bool blockAllTransactions = false)
        {
            var transactionCreated =await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"INSERT INTO [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION](
                    [TRANSACTION_ID],
                    [ARTEFACT_FULL_ID],
                    [QUEUED_DATETIME],
                    [EXECUTION_START],
                    [EXECUTION_END],
                    [USER_EMAIL],
                    [SOURCE_DATASPACE],
                    [DATA_SOURCE],
                    [TYPE],
                    [STATUS],
                    [REQUESTED_TARGET_VERSION],
                    [BLOCK_ALL_TRANSACTIONS],
                    [SERVICE_ID],
                    [LAST_UPDATED]
                )
                VALUES (@Id, @AtrefactFullId, @DT_Now, NULL, NULL, @Email, @SourceDataSpace, @DataSource, @Type, @Status, @RequestedTargetVersion, @BlockAllTransactions, @ServiceId, @DT_Now)",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.Int) { Value = transactionId },
                new SqlParameter("AtrefactFullId", SqlDbType.VarChar) { Value = (object)atrFullId ?? SpecialTransactionArtefactIds.None },
                new SqlParameter("Email", SqlDbType.VarChar) { Value = (object)principal?.Email ?? DBNull.Value },
                new SqlParameter("SourceDataSpace", SqlDbType.VarChar) { Value = (object)sourceDataSpace ?? DBNull.Value },
                new SqlParameter("DataSource", SqlDbType.VarChar) { Value = (object)dataSource ?? DBNull.Value },
                new SqlParameter("Type", SqlDbType.VarChar) { Value = type.ToString() },
                new SqlParameter("Status", SqlDbType.VarChar) { Value = TransactionStatus.Queued.ToString() },
                new SqlParameter("RequestedTargetVersion", SqlDbType.VarChar) { Value = (object)requestedTargetVersion ?? DBNull.Value },
                new SqlParameter("BlockAllTransactions", SqlDbType.Bit) { Value = blockAllTransactions },
                new SqlParameter("ServiceId", SqlDbType.VarChar) { Value = !string.IsNullOrWhiteSpace(serviceId) ? serviceId : DBNull.Value },
                new SqlParameter("DT_Now", DateTime.UtcNow)
            );


            if (transactionCreated>0)
            {
                return new Transaction()
                {
                    Id = transactionId,
                    ArtefactFullId = atrFullId,
                    UserEmail = principal?.Email,
                    SourceDataspace = sourceDataSpace,
                    DataSource = dataSource,
                    Type = type,
                    Status = TransactionStatus.Queued,
                    RequestedTargetVersion = requestedTargetVersion,
                    FinalTargetVersion = requestedTargetVersion,
                    ServiceId = serviceId,
                    BlockAllTransactions = blockAllTransactions
                };
            }

            return null;
        }

        public async Task<Transaction> GetTransactionById(int transactionId, CancellationToken cancellationToken, bool tryUseReadOnlyConnection = false)
        {
            var sqlCommand =
                $@"SELECT {GetTransactionTableDbFields()}
                   FROM [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION]
                   WHERE [TRANSACTION_ID] = @Id";

            var transactions= LoadTransactionByCommand(
                sqlCommand, 
                cancellationToken, 
                tryUseReadOnlyConnection,
                new SqlParameter("Id", SqlDbType.Int) { Value = transactionId });

            return await transactions.FirstOrDefaultAsync(cancellationToken);
        }

        public IAsyncEnumerable<Transaction> GetTransactions(string userEmail, string artFullId, DateTime start, DateTime end, TransactionStatus? status, CancellationToken cancellationToken, bool tryUseReadOnlyConnection = false)
        {
            var criteria = new StringBuilder();
            var @params = new List<DbParameter>()
            {
                new SqlParameter("@start", SqlDbType.DateTime) { Value = start },
                new SqlParameter("@end", SqlDbType.DateTime) { Value = end }
            };

            if (!string.IsNullOrEmpty(userEmail))
            {
                criteria.AppendLine("[USER_EMAIL] = @userEmail AND ");
                @params.Add(new SqlParameter("userEmail", SqlDbType.VarChar) { Value = userEmail });
            }

            if (!string.IsNullOrEmpty(artFullId))
            {
                criteria.AppendLine("[ARTEFACT_FULL_ID] = @artFullId AND ");
                @params.Add(new SqlParameter("artFullId", SqlDbType.VarChar) { Value = artFullId });
            }

            if (status != null)
            {
                criteria.AppendLine("[STATUS] = @status AND ");
                @params.Add(new SqlParameter("status", SqlDbType.VarChar) { Value = status.ToString() });
            }

            var sqlCommand = $@"SELECT {GetTransactionTableDbFields()}
                     FROM [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION] WITH (READPAST)
                     WHERE {criteria}
                    (                     
                        --Completed transactions
                        ([QUEUED_DATETIME] >= @start AND [EXECUTION_END]  <= @end) 
                        --Failed/Timed out transactions
                        OR ([EXECUTION_END] IS NULL AND [QUEUED_DATETIME] BETWEEN @start AND @end)
                    );";

            return LoadTransactionByCommand(
                sqlCommand,
                cancellationToken,
                tryUseReadOnlyConnection,
                @params.ToArray()
            );
        }

        public IAsyncEnumerable<Transaction> GetInProgressTransactions(int transactionId, int dsdId, CancellationToken cancellationToken)
        {
            var artIdFilter = dsdId == -1 ? 
                "[ART_ID] IS NOT NULL" : //Get any transaction in progress regardless if is maintenance or targeting a particular artefact
                $"([ART_ID] = {dsdId} OR [ART_ID] = -1)"; //Get any transaction in progress for this dsdId or a maintenance transaction

            var sqlCommand = $@"SELECT {GetTransactionTableDbFields()}
                     FROM [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION]
                     WHERE [TRANSACTION_ID] <> {transactionId} AND {artIdFilter} AND [SUCCESSFUL] IS NULL";

            return LoadTransactionByCommand(sqlCommand, cancellationToken);            
        }


        public async Task<bool> LockTransaction(
            int transactionId, 
            int dsdDbId,
            int? dsdChildDbId,
            DbTableVersion tableVersion, 
            TargetVersion? FinalTargetVersion, 
            CancellationToken cancellationToken
        )
        {
            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION]
                   SET [EXECUTION_START]=@DT_Now, 
                        [ART_ID]=@ArtId,
                        [ART_CHILD_ID]=@ArtChildId,
                        [TABLE_VERSION]=@TableVersion,
                        [STATUS]=@Status, 
                        [FINAL_TARGET_VERSION]=@TargetVersion, 
                        [LAST_UPDATED]=@DT_Now
                    WHERE [TRANSACTION_ID] = @Id",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.Int) { Value = transactionId },
                new SqlParameter("ArtId", SqlDbType.Int) { Value = dsdDbId },
                new SqlParameter("ArtChildId", (object) dsdChildDbId ?? DBNull.Value),
                new SqlParameter("TableVersion", SqlDbType.Char) { Value = (char)tableVersion },
                new SqlParameter("Status", SqlDbType.VarChar) { Value = TransactionStatus.InProgress.ToString() },
                new SqlParameter("TargetVersion", SqlDbType.VarChar) { Value = (object)FinalTargetVersion ?? DBNull.Value },
                new SqlParameter("DT_Now", DateTime.UtcNow)
            ) > 0;
        }
        private async Task<bool> EnsureOnlySingleDsdTransaction(Transaction transaction, CancellationToken cancellationToken)
        {
            return (int) await DotStatDb.ExecuteScalarSqlWithParamsAsync(
                $@"select count(*) FROM [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION] where [SUCCESSFUL] IS NULL 
AND [ART_ID] = @ArtId AND [ART_CHILD_ID] IS {(transaction.ArtefactChildDbId == null ? "NOT" : "")} NULL",
                cancellationToken,
                new SqlParameter("ArtId", SqlDbType.Int) { Value = transaction.ArtefactDbId }
            ) == 0;
        }

        public async Task<bool> MarkTransactionAsTimedOut(int transactionId)
        {
            return await UpdateTransactionStatus(transactionId, TransactionStatus.TimedOut, false, CancellationToken.None);
        }

        public async Task<bool> MarkTransactionAsCanceled(int transactionId)
        {
            return await UpdateTransactionStatus(transactionId, TransactionStatus.Canceled, false, CancellationToken.None);
        }

        public async Task<bool> CancelTransactions(string serviceId)
        {
            //Keep backwards compatibility for environments which do not have the setting 'SERVICE_ID' configured.
            //In this case, no transactions are Canceled
            if (string.IsNullOrEmpty(serviceId)) return true;
            
            var sqlCommand = $@"SELECT [TRANSACTION_ID]
                FROM [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION]
                WHERE [SUCCESSFUL] IS NULL AND [SERVICE_ID] = @ServiceId";

            await using (var dr = await DotStatDb.ExecuteReaderSqlWithParamsAsync(
                sqlCommand, 
                CancellationToken.None,
                tryUseReadOnlyConnection:true,
                parameters:new SqlParameter("ServiceId", SqlDbType.VarChar) { Value = serviceId }
            ))
            {
                while (await dr.ReadAsync(CancellationToken.None))
                { 
                    var transactionId = dr.GetInt32(0);
                    LogHelper.RecordNewTransaction(transactionId, DotStatDb.DataSpace);
                    Log.Warn(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CanceledTransactionOnStartup), transactionId));
                }
            }

            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION]
                  SET [EXECUTION_END] = @DT_Now, [SUCCESSFUL] = @Successful, [STATUS]=@Status, [LAST_UPDATED]=@DT_Now
                  WHERE [SUCCESSFUL] IS NULL AND [SERVICE_ID] = @ServiceId",
                CancellationToken.None,
                new SqlParameter("ServiceId", SqlDbType.VarChar) { Value = serviceId },
                new SqlParameter("Successful", SqlDbType.Bit) { Value = false },
                new SqlParameter("Status", SqlDbType.VarChar) { Value = TransactionStatus.Canceled.ToString() },
                new SqlParameter("DT_Now", DateTime.UtcNow)
            ) > 0;
        }

        public async Task<bool> MarkTransactionAsCompleted(int transactionId, bool successful)
        {
            return await UpdateTransactionStatus(transactionId, TransactionStatus.Completed, successful, CancellationToken.None);
        }

        #region private methods

        private async Task<List<int>> GetNonCompletedTransactions(int transactionId, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"SELECT [TRANSACTION_ID]
                FROM [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION]
                WHERE [TRANSACTION_ID] <> {transactionId} AND [ART_ID] IS NOT NULL AND [SUCCESSFUL] IS NULL;";

            var transactionIds = new List<int>();
            using (var dr = await DotStatDb.ExecuteReaderSqlAsync(sqlCommand, cancellationToken))
            {
                while (await dr.ReadAsync(cancellationToken))
                {
                    transactionIds.Add(dr.GetInt32(0));
                }
            }

            return transactionIds;
        }

        private async Task<int> GetNonCompletedTransactionBlockingAll(int transactionId, CancellationToken cancellationToken)
        {
            var sqlCommand = $@"SELECT TOP(1) [TRANSACTION_ID]
            FROM [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION]
            WHERE [TRANSACTION_ID] <> {transactionId} AND [ART_ID] IS NOT NULL
                AND [BLOCK_ALL_TRANSACTIONS] = 1 AND [SUCCESSFUL] IS NULL;";

            return (int?)await DotStatDb.ExecuteScalarSqlAsync(sqlCommand, cancellationToken) ?? -1;
        }

        private static string GetTransactionTableDbFields()
        {
            return @" [TRANSACTION_ID],[ART_ID],[ARTEFACT_FULL_ID],[TABLE_VERSION],[QUEUED_DATETIME],[EXECUTION_START],
                [EXECUTION_END],[SUCCESSFUL],[USER_EMAIL],[SOURCE_DATASPACE],[DATA_SOURCE],[TYPE],[STATUS],
                [REQUESTED_TARGET_VERSION],[FINAL_TARGET_VERSION],[BLOCK_ALL_TRANSACTIONS],[LAST_UPDATED],[ART_CHILD_ID],[SERVICE_ID]";
        }

        private async ValueTask<bool> UpdateTransactionStatus(int transactionId, TransactionStatus status, bool? successful, CancellationToken cancellationToken)
        {
            var successfulPart = "";
            var executionStart = "";
            var executionEnd = "";
            switch (status)
            {
                case TransactionStatus.Queued:
                    return false;
                case TransactionStatus.InProgress:
                    return false;
                case TransactionStatus.Completed:
                    executionEnd = "[EXECUTION_END] = @DT_Now,";
                    successfulPart = "[SUCCESSFUL] = @Successful,";
                    break;
                case TransactionStatus.Canceled:
                    executionEnd = "[EXECUTION_END] = @DT_Now,";
                    successful = false;
                    successfulPart = "[SUCCESSFUL] = @Successful,";
                    break;
                case TransactionStatus.TimedOut:
                    executionEnd = "[EXECUTION_END] = @DT_Now,";
                    successful = false;
                    successfulPart = "[SUCCESSFUL] = @Successful,";
                    break;
                case TransactionStatus.Unknown:
                    return false;
            }

            return await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                $@"UPDATE [{DotStatDb.ManagementSchema}].[DSD_TRANSACTION]
                  SET {executionStart} {executionEnd} {successfulPart} [STATUS]=@Status, [LAST_UPDATED]=@DT_Now
                  WHERE [TRANSACTION_ID] = @Id",
                cancellationToken,
                new SqlParameter("Id", SqlDbType.Int) { Value = transactionId },
                new SqlParameter("Successful", SqlDbType.Bit) { Value = (object)successful ?? DBNull.Value },
                new SqlParameter("Status", SqlDbType.VarChar) { Value = status.ToString() },
                new SqlParameter("DT_Now", DateTime.UtcNow)
            ) > 0;

        }

        private async IAsyncEnumerable<Transaction> LoadTransactionByCommand(
            string sqlCommand,
            [EnumeratorCancellation] CancellationToken cancellationToken,
            bool tryUseReadOnlyConnection = false,
            params DbParameter[] parameters)
        {
            using (var dbDataReader = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlCommand, cancellationToken, tryUseReadOnlyConnection: tryUseReadOnlyConnection, parameters: parameters))
            {
                while (await dbDataReader.ReadAsync(cancellationToken))
                {
                    yield return new Transaction()
                    {
                        Id = dbDataReader.GetInt32(0),
                        ArtefactDbId = dbDataReader.ColumnValue<int?>(1),
                        ArtefactFullId = dbDataReader.ColumnValue<string>(2),
                        TableVersion =
                            Enum.TryParse(dbDataReader.ColumnValue<string>(3), true, out DbTableVersion tableVersion)
                                ? tableVersion
                                : DbTableVersions.GetDbTableVersion(null),
                        QueuedDateTime = dbDataReader.GetDateTime(4),
                        ExecutionStart = dbDataReader.ColumnValue<DateTime?>(5),
                        ExecutionEnd = dbDataReader.ColumnValue<DateTime?>(6),
                        Successful = dbDataReader.ColumnValue<bool?>(7),
                        UserEmail = dbDataReader.ColumnValue<string>(8),
                        SourceDataspace = dbDataReader.ColumnValue<string>(9),
                        DataSource = dbDataReader.ColumnValue<string>(10),
                        Type = Enum.TryParse(dbDataReader.ColumnValue<string>(11), true, out TransactionType type) ? type : TransactionType.Unknown,
                        Status = Enum.TryParse(dbDataReader.ColumnValue<string>(12), true, out TransactionStatus status) ? status : TransactionStatus.Unknown,
                        RequestedTargetVersion = Enum.TryParse(dbDataReader.ColumnValue<string>(13), true, out TargetVersion requestedTargetVersion) ? requestedTargetVersion : null as TargetVersion?,
                        FinalTargetVersion = Enum.TryParse(dbDataReader.ColumnValue<string>(14), true, out TargetVersion finalTargetVersion) ? finalTargetVersion : null as TargetVersion?,
                        BlockAllTransactions = dbDataReader.GetBoolean(15),
                        LastUpdated = dbDataReader.GetDateTime(16),
                        ArtefactChildDbId = dbDataReader.ColumnValue<int?>(17),
                        ServiceId = dbDataReader.ColumnValue<string>(18)
                    };
                }
            }
        }

        #endregion
    }
}
