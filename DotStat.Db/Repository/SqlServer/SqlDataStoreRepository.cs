﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.DB;
using DotStat.Db.Reader;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using System.Threading;
using System.Threading.Tasks;
using Attribute = DotStat.Domain.Attribute;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using DotStat.DB.Repository.SqlServer;
using DotStat.Db.Validation;
using DotStat.Db.Exception;
using System.Reflection;
using System.Text.RegularExpressions;

namespace DotStat.Db.Repository.SqlServer
{
    public class SqlDataStoreRepository : SqlDataMergerBase, IDataStoreRepository
    {
        public SqlDataStoreRepository(SqlDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }
        
        public async Task<BulkImportResult> BulkInsertData(IAsyncEnumerable<ObservationRow> observations,
            ReportedComponents reportedComponents,
            CodeTranslator translator,
            Dataflow dataflow,
            bool fullValidation,
            bool isTimeAtTimeDimensionSupported,
            bool isXMLSource,
            CancellationToken cancellationToken)
        {
            await using var connection = DotStatDb.GetConnection();
            using var bulkCopy = new SqlBulkCopy(connection,
                SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.UseInternalTransaction, null);

            bulkCopy.DestinationTableName = $"[{DotStatDb.DataSchema}].[{dataflow.Dsd.SqlStagingTable()}]";
            bulkCopy.BatchSize = 30000;
            bulkCopy.BulkCopyTimeout = DotStatDb.DatabaseCommandTimeout;
            if (DotStatDb.DataSpace.NotifyImportBatchSize > 0)
            {
                bulkCopy.SqlRowsCopied += OnSqlRowsCopied;
                bulkCopy.NotifyAfter = DotStatDb.DataSpace.NotifyImportBatchSize;
            }

            using var observationReader = new SdmxObservationReader(observations, reportedComponents, dataflow, translator, GeneralConfiguration, fullValidation, isTimeAtTimeDimensionSupported, isXMLSource);

            // if staging contains calculated ROW_ID column
            if (dataflow.Dimensions.Count > 32)
                for (var i = 0; i < observationReader.FieldCount; i++)
                {
                    bulkCopy.ColumnMappings.Add(i, i + 1);
                }
            try
            {
                await bulkCopy.WriteToServerAsync(observationReader, cancellationToken);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                // Generic exception handling for the case when the bulk copy operation fails due to the length of the value written to database
                if (!ex.Message.Contains("Received an invalid column length from the bcp client for colid"))
                    throw;

                var match = Regex.Match(ex.Message.ToString(), @"\d+");
                var index = Convert.ToInt32(match.Value) - 1;

                var fi = typeof(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic | BindingFlags.Instance);
                var sortedColumns = fi.GetValue(bulkCopy);
                var items = (object[])sortedColumns.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sortedColumns);

                var itemData = items[index].GetType().GetField("_metadata", BindingFlags.NonPublic | BindingFlags.Instance);
                var metaData = itemData.GetValue(items[index]);

                var maxLength = metaData.GetType().GetField("length", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metaData);

                var msg = string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.InvalidValueLengthExceedsStorageSize),
                    observationReader.GetName(index), // {0} - component name
                    observationReader.CurrentIndex, // {1} - observation index
                    observationReader.CurrentIndex + 1, // {2} row no. in a csv file
                    maxLength); // {4} - the max. length allowed by the SQL data type

                throw new BulkCopyException(msg);
            }

            var errors = observationReader.GetErrors();
            var rowsCopied = observationReader.GetObservationsCount();
            var batchActions = observationReader.GetBatchActions();

            Log.Notice(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ReadingSourceFinished),
                rowsCopied));

            var datasetMetadataAttributesRows = observationReader.DataSetAttributes.Select(d => d.Value).ToList();

            return new BulkImportResult(datasetMetadataAttributesRows, errors, rowsCopied, batchActions);
        }

        public async Task DropStagingTables(Dsd dsd, CancellationToken cancellationToken)
        {
            var tableName = dsd.SqlStagingTable();
            await DotStatDb.DropTable(DotStatDb.DataSchema, tableName, cancellationToken);
        }

        public async Task DropStagingTables(int dsdDbId, CancellationToken cancellationToken)
        {
            var tableName = DbExtensions.SqlStagingTable(dsdDbId);
            await DotStatDb.DropTable(DotStatDb.DataSchema, tableName, cancellationToken);
        }

        public async Task RecreateStagingTables(Dsd dsd, ReportedComponents reportedComponents, bool isTimeAtTimeDimensionSupported, CancellationToken cancellationToken)
        {
            await DropStagingTables(dsd, cancellationToken);
            await BuildStagingTables(dsd, reportedComponents, isTimeAtTimeDimensionSupported, cancellationToken);
        }

        public async Task AddIndexStagingTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken)
        {
            //No index required for stating table; Request with only dataflow level attributes
            if (!reportedComponents.IsPrimaryMeasureReported && reportedComponents.ObservationAttributes.Count == 0 && reportedComponents.SeriesAttributesWithNoTimeDim.Count == 0)
                return;

            var dimensionColumns = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension).ToColumnList();
            var timeDimColumns = reportedComponents.TimeDimension is not null
                ? string.Join(",", reportedComponents.TimeDimension ,DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd()) : null;

            var indexColumns = "[ROW_ID]";
            var indexName = "[idx_ROW_ID]";
            //32 is the SQL limit of columns that can participate in a Unique index
            if (reportedComponents.Dimensions.Count + 1 <= 32)
            {
                indexColumns = $"{dimensionColumns}, [BATCH_NUMBER]";
                indexName = "[idx_dimensions]";
            }

            var obsAttrColumns = reportedComponents.ObservationAttributes.Any()
                ? "," + reportedComponents.ObservationAttributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength) : null;
            var measure = reportedComponents.IsPrimaryMeasureReported ? ", [VALUE]" : null;
            var actionColumns = ", [REAL_ACTION_FACT], [REAL_ACTION_ATTR]";

            await DotStatDb.ExecuteNonQuerySqlAsync($@"
                CREATE NONCLUSTERED INDEX {indexName} 
                ON [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}]({indexColumns})
                --INCLUDE ({timeDimColumns}{measure}{obsAttrColumns}{actionColumns})
            ", cancellationToken);
        }
        
        public async Task AddUniqueIndexStagingTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken)
        {
            var dimensionColumns = reportedComponents.Dimensions.ToColumnList();

            var indexColumns = "[ROW_ID]";
            var indexName = "[UI_ROW_ID]";
            //32 is the SQL limit of columns that can participate in a Unique index
            if (reportedComponents.Dimensions.Count + 1 <= 32)
            {
                indexColumns = $"{dimensionColumns}, [BATCH_NUMBER]";
                indexName = "[UI_dimensions]";
            }
            
            try
            { 
                await DotStatDb.ExecuteNonQuerySqlAsync($@"
                    CREATE UNIQUE INDEX {indexName} 
                    ON [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}]({indexColumns})
                ", cancellationToken);

            }
            catch (SqlException e)
            {
                switch (e.Number)
                {
                    case 1505://duplicates in the staging table insert
                        throw new ConsumerValidationException(new List<ValidationError> { new ObservationError(){ Type = ValidationErrorType.DuplicatedRowsInStagingTable } });
                    default:
                        throw;
                }
            }
        }

        public async Task DeleteData(IImportReferenceableStructure referencedStructure, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            if (referencedStructure is not Dsd dsd)
            {
                //Dsd level delete not required for data
                return;
            }

            if (DotStatDb.DataSpace.KeepHistory)
            {
                //Temporal tables do not allow Truncate commands
                await DeleteAllFromDsdTables(dsd, tableVersion, cancellationToken);
                await DeleteAllFromAttributeTables(dsd, tableVersion, cancellationToken);
            }
            else
            {
                await TruncateDsdTables(dsd, tableVersion, cancellationToken);
                await TruncateAttributeTables(dsd, tableVersion, cancellationToken);
            }
        }

        public async Task CopyDataToNewVersion(Dsd dsd, DbTableVersion sourceDbTableVersion, DbTableVersion targetDbTableVersion, CancellationToken cancellationToken)
        {
            if (sourceDbTableVersion == targetDbTableVersion)
                throw new ArgumentException($"The same value was provided for the parameters 'sourceDbTableVersion' and 'targetDbTableVersion'");

            var timeDim = dsd.TimeDimension;
            var timeDimColumns = timeDim != null
                ? "," + string.Join(",", timeDim.SqlColumn(), DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd())
                : null;

            var obsAttributes = dsd.Attributes
                .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation ||
                            (a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || a.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                            a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToArray();

            var attributeColumns = obsAttributes.Any()
                ? "," + obsAttributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength)
                : null;

            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@" INSERT
                      INTO [{DotStatDb.DataSchema}].[{dsd.SqlFactTable((char)targetDbTableVersion)}] 
                      ([SID], [VALUE], [{DbExtensions.LAST_UPDATED_COLUMN}] {timeDimColumns} {attributeColumns} )
                    SELECT [SID], [VALUE], [{DbExtensions.LAST_UPDATED_COLUMN}] {timeDimColumns} {attributeColumns} 
                      FROM [{DotStatDb.DataSchema}].[{dsd.SqlFactTable((char)sourceDbTableVersion)}]"
                      , cancellationToken
            );
        }

        public async Task UpdateStatisticsOfFactTable(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@"UPDATE STATISTICS [{DotStatDb.DataSchema}].[{dsd.SqlFactTable((char)tableVersion)}] WITH FULLSCAN, ALL",
                cancellationToken
            );
        }

        public async Task UpdateStatisticsOfFilterTable(Dsd dsd, CancellationToken cancellationToken)
        {
            await DotStatDb.ExecuteNonQuerySqlAsync(
                $@"UPDATE STATISTICS [{DotStatDb.DataSchema}].[{dsd.SqlFilterTable()}] WITH FULLSCAN, ALL",
                cancellationToken
            );
        }

        public async ValueTask CopyAttributesToNewVersion(Dsd dsd, DbTableVersion sourceDbTableVersion, DbTableVersion targetDbTableVersion, CancellationToken cancellationToken)
        {
            if (sourceDbTableVersion == targetDbTableVersion)
                throw new ArgumentException($"The same value was provided for the parameters 'sourceDbTableVersion' and 'targetDbTableVersion'");

            if (!dsd.Attributes.Any())
                return;

            var dimGroupAttributes = dsd.Attributes.Where(a =>
                (a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup ||
                a.Base.AttachmentLevel == AttributeAttachmentLevel.Group) &&
                !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)
            ).ToArray();

            if (dimGroupAttributes.Any())
            {
                var dimGroupAttributeColumns = dimGroupAttributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength);

                await DotStatDb.ExecuteNonQuerySqlAsync(
                    $@" INSERT
                    INTO [{DotStatDb.DataSchema}].[{dsd.SqlDimGroupAttrTable((char)targetDbTableVersion)}] (SID, [{DbExtensions.LAST_UPDATED_COLUMN}], {dimGroupAttributeColumns})
                    SELECT SID, [{DbExtensions.LAST_UPDATED_COLUMN}], {dimGroupAttributeColumns}
                    FROM [{DotStatDb.DataSchema}].[{dsd.SqlDimGroupAttrTable((char)sourceDbTableVersion)}]",
                    cancellationToken
                );
            }

            var datasetAttributes = dsd.Attributes.Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet).ToArray();

            if (datasetAttributes.Any())
            {
                var datasetAttributeColumns = datasetAttributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength);

                await DotStatDb.ExecuteNonQuerySqlAsync(
                    $@" INSERT
                    INTO [{DotStatDb.DataSchema}].[{dsd.SqlDsdAttrTable((char)targetDbTableVersion)}] ([DF_ID],[{DbExtensions.LAST_UPDATED_COLUMN}], {datasetAttributeColumns})
                    SELECT [DF_ID],[{DbExtensions.LAST_UPDATED_COLUMN}], {datasetAttributeColumns}
                    FROM [{DotStatDb.DataSchema}].[{dsd.SqlDsdAttrTable((char)sourceDbTableVersion)}]",
                    cancellationToken
                );
            }
        }

        protected override async ValueTask MergeDsdStagingToDimensionsTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken)
        {

            var noTimeDimensions = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension).ToList();
            if (!noTimeDimensions.Any())
                return;

            var dimensionColumnsNoTimeDim = noTimeDimensions.ToColumnList();
            var dimensionColumnsNoTimeDimJoin = string.Join(" AND ", noTimeDimensions.Select(a => string.Format("Filt.{0} = Import.{0}", a.SqlColumn())));
            var dimensionColumnsNoTimeNotNull = string.Join(" AND ", noTimeDimensions.Select(a => $"{a.SqlColumn()} IS NOT NULL"));

            var sqlCommand = $@"
MERGE INTO [{DotStatDb.DataSchema}].[{dsd.SqlFilterTable()}] Filt
USING (
    SELECT DISTINCT {dimensionColumnsNoTimeDim} FROM [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}] 
    WHERE {dimensionColumnsNoTimeNotNull} 
    AND [REQUESTED_ACTION] IN (@Merge, @Replace)
) 
    Import
ON {dimensionColumnsNoTimeDimJoin}
WHEN NOT MATCHED THEN
    INSERT ({dimensionColumnsNoTimeDim})
    VALUES ({dimensionColumnsNoTimeDim});
";

            //Add Sql parameters
            var sqlParameters = new List<DbParameter>
            {
                new SqlParameter("Merge", SqlDbType.Int) { Value = (int)StagingRowActionEnum.Merge },
                new SqlParameter("Replace", SqlDbType.Int) { Value = (int)StagingRowActionEnum.Replace }
            };

            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, sqlParameters.ToArray());
        }

        public override async ValueTask<MergeResult> MergeStagingToObservationsTable(IImportReferenceableStructure referencedStructure,
            ReportedComponents reportedComponents, DbTableVersion tableVersion,
            BatchAction batchAction, bool includeSummary, ICodeTranslator codeTranslator, DateTime dateTime,
            CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();
            if (referencedStructure is not Dataflow dataFlow)
            {
                //Dsd level merge not required for data
                return mergeResult;
            }
            
            var action = batchAction.Action;
            var batchNumber = batchAction.BatchNumber;
            var isWildCarded = batchAction.IsWildCarded;

            if (action is StagingRowActionEnum.Skip || action is StagingRowActionEnum.DeleteAll)
                return mergeResult;

            //There are no reported dimensions, no action required at this level
            if (!reportedComponents.Dimensions.Any())
                return mergeResult;

            var dsd = dataFlow.Dsd;

            //No reported components at observation level
            //Delete and Replace actions can set the observation level components to NULL if the components are not present in the import file
            if (!reportedComponents.IsPrimaryMeasureReported && !reportedComponents.ObservationAttributes.Any() && action == StagingRowActionEnum.Merge)
                return mergeResult;

            //Dimensions columns
            var dimensionColumnsNoTimeDimJoinTemplate = action switch
            {//Delete action allows for wildcard dimensions and Replaces can have attributes not referencing all dimensions 
                //Note that for Replaces, the replace will not be wildcarded because in the validation CheckReferenceDimensionsPresent, all referenced dimensions must be present
                StagingRowActionEnum.Delete or StagingRowActionEnum.Replace when isWildCarded => " Filt.{0} = ISNULL(Import.{0}, Filt.{0}) ",
                _ => " Filt.{0} = Import.{0} "
            };

            var noTimeDimensions = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension).ToList();
            var dimensionColumnsNoTimeDimJoin =
                string.Join(" AND ", noTimeDimensions.Select(a => string.Format(dimensionColumnsNoTimeDimJoinTemplate, a.SqlColumn())));
            
            //Time dimension columns
            string timeDimColumns = null;
            string timeDimColumnsJoin = null;
            string sdmxTimeDimNotNull = null;
            if (reportedComponents.TimeDimension is not null)
            {
                timeDimColumns = "," + string.Join(",", reportedComponents.TimeDimension.SqlColumn(), DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd());

                var timeDimColumnsJoinTemplate = action switch
                {
                    //Delete action allows for wildcard dimensions 
                    StagingRowActionEnum.Delete when isWildCarded => " Fact.{0} = ISNULL(Import.{0}, Fact.{0}) ",
                    _ => "Fact.{0} = Import.{0}"
                };

                timeDimColumnsJoin = @$"AND (
                    { string.Format(timeDimColumnsJoinTemplate, DbExtensions.SqlPeriodStart())} AND 
                    {string.Format(timeDimColumnsJoinTemplate, DbExtensions.SqlPeriodEnd())})";

                sdmxTimeDimNotNull = string.Format("AND Import.{0} IS NOT NULL", reportedComponents.TimeDimension.SqlColumn());
            }

            var updateCondition = new List<string>();
            string updateDiffCheck = null;
            //Measure columns
            string measureColumn = null;
            string measureSelect = null;
            string measureColumnUpdate = null;
            switch (action)
            {
                case StagingRowActionEnum.Merge when reportedComponents.IsPrimaryMeasureReported:
                    measureSelect = measureColumn = ", [VALUE]";
                    measureColumnUpdate = "Fact.[VALUE] = CASE WHEN Import.[VALUE] IS NOT NULL THEN Import.[VALUE] ELSE Fact.[VALUE] END,";
                    updateCondition.Add($"(Import.[VALUE] IS NOT NULL AND (Import.[VALUE] <> Fact.[VALUE] {dsd.PrimaryMeasure.SqlCollate()} OR Fact.[VALUE] IS NULL))");
                    break;
                case StagingRowActionEnum.Replace:
                    measureSelect = reportedComponents.IsPrimaryMeasureReported ? ", [VALUE]" : ", NULL as [VALUE]";
                    measureColumn = ", [VALUE]";
                    measureColumnUpdate = "Fact.[VALUE] = Import.[VALUE],";
                    updateCondition.Add(reportedComponents.IsPrimaryMeasureReported ? $"((Import.[VALUE] IS NULL AND Fact.[VALUE] IS NOT NULL) OR (Import.[VALUE] IS NOT NULL AND Fact.[VALUE] IS NULL) OR Fact.[VALUE] <> Import.[VALUE] {dsd.PrimaryMeasure.SqlCollate()})" : "Fact.[VALUE] IS NOT NULL");
                    break;
                case StagingRowActionEnum.Delete:
                    measureSelect = reportedComponents.IsPrimaryMeasureReported ? ", [VALUE]" : ", NULL as [VALUE]";
                    measureColumn = ", [VALUE]";
                    measureColumnUpdate = "Fact.[VALUE] = CASE WHEN Import.[VALUE] IS NOT NULL THEN NULL ELSE Fact.[VALUE] END,";
                    updateCondition.Add("(Import.[VALUE] IS NOT NULL AND Fact.[VALUE] IS NOT NULL)");
                    break;
            }

            //Attributes columns
            var reportedAttributes = action == StagingRowActionEnum.Merge ? 
                reportedComponents.ObservationAttributes : 
                dsd.Attributes.Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                    || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

            string obsAttrColumns = reportedAttributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength);
            string obsAttrColumnsUpdate = null;
            
            switch (action)
            {
                case StagingRowActionEnum.Merge:
                    obsAttrColumnsUpdate = string.Join(",\n", reportedAttributes
                        .Select(a => $"Fact.{a.SqlColumn()} = CASE WHEN Import.{a.SqlColumn()} IS NOT NULL THEN Import.{a.SqlColumn()} ELSE Fact.{a.SqlColumn()} END"));
                    updateCondition
                        .AddRange(reportedAttributes
                            .Select(a => $"(Import.{a.SqlColumn()} IS NOT NULL AND (Import.{a.SqlColumn()} <> Fact.{a.SqlColumn()} {a.SqlCollate()} OR Fact.{a.SqlColumn()} IS NULL))"));
                    break;
                case StagingRowActionEnum.Delete:
                    obsAttrColumnsUpdate = string.Join(",\n", reportedAttributes
                        .Select(a => $"Fact.{a.SqlColumn()} = CASE WHEN Import.{a.SqlColumn()} IS NOT NULL THEN NULL ELSE Fact.{a.SqlColumn()} END"));
                    updateCondition
                        .AddRange(reportedAttributes
                            .Select(a => $"(Import.{a.SqlColumn()} IS NOT NULL AND Fact.{a.SqlColumn()} IS NOT NULL)"));
                    break;
                case StagingRowActionEnum.Replace:
                    obsAttrColumnsUpdate = string.Join(",\n", reportedAttributes.Select(a => @$"Fact.{a.SqlColumn()} = Import.{a.SqlColumn()}"));

                    updateCondition
                        .AddRange(reportedAttributes
                            .Select(a => string.Format(
                                    reportedComponents.ObservationAttributes.Any(x => x.Code == a.Code)
                                        ? "((Import.{0} IS NOT NULL AND Fact.{0} IS NULL) OR (Import.{0} IS NULL AND Fact.{0} IS NOT NULL) OR Fact.{0} <> Import.{0} {1})"
                                        : "Fact.{0} IS NOT NULL"
                                    ,a.SqlColumn()
                                    ,a.SqlCollate()
                                )));

                    break;
            };

            if (!string.IsNullOrEmpty(obsAttrColumns))
                obsAttrColumns = "," + obsAttrColumns;

            if (!string.IsNullOrEmpty(obsAttrColumnsUpdate))
                obsAttrColumnsUpdate += ",";

            if (updateCondition.Any())
                updateDiffCheck = $"AND ({string.Join(" OR \n", updateCondition)})";

            //Calculate summary
            var summaryStatement = string.Empty;
            if (includeSummary)
            {
                summaryStatement = action == StagingRowActionEnum.Delete ?
                "SELECT 0 as [INSERTED], @updated as [UPDATED], @@ROWCOUNT - @updated as [DELETED], @@ROWCOUNT as [TOTAL];" :
                "SELECT @@ROWCOUNT - @updated as [INSERTED], @updated as [UPDATED], 0 AS [DELETED], @@ROWCOUNT as [TOTAL];";
            }
            else
            {
                summaryStatement = "SELECT CASE WHEN @@ROWCOUNT > 0 THEN  CAST(1 AS BIT) ELSE CAST(0 AS BIT) END;";
            }

            //Merge staging table to Fact table
            var actionStatement = action == StagingRowActionEnum.Delete ?
//Delete actions
$@"WHEN MATCHED AND Import.[REAL_ACTION_FACT] = @Merge {updateDiffCheck} THEN --Soft Delete set values to NULL
    UPDATE SET 
    {measureColumnUpdate}
    {obsAttrColumnsUpdate}
    [{DbExtensions.LAST_UPDATED_COLUMN}] = @CurrentDateTime
    {(includeSummary ? ", @updated = @updated + 1" : "")} 
WHEN MATCHED AND Import.[REAL_ACTION_FACT] = @Delete THEN DELETE; --Real delete" :
// Replace and Merge actions
$@"WHEN MATCHED {updateDiffCheck} THEN --Updates
    UPDATE SET 
    {measureColumnUpdate}
    {obsAttrColumnsUpdate}
    [{DbExtensions.LAST_UPDATED_COLUMN}] = @CurrentDateTime
    {(includeSummary ? ", @updated = @updated + 1" : "")} 
WHEN NOT MATCHED {sdmxTimeDimNotNull} THEN -- Inserts
INSERT ([SID]{timeDimColumns}{measureColumn}{obsAttrColumns}, [{DbExtensions.LAST_UPDATED_COLUMN}] )
VALUES ([SID]{timeDimColumns}{measureColumn}{obsAttrColumns}, @CurrentDateTime );";

            //Filter allowed content constraint
            var nonTimeConstraint = action == StagingRowActionEnum.Delete ?
                Predicate.BuildWhereForObservationLevel(dataQuery: null, dataFlow, codeTranslator, useExternalValues: false, includeTimeConstraints: false, tableAlias: "Filt")
                : string.Empty;
            if (!string.IsNullOrEmpty(nonTimeConstraint))
                nonTimeConstraint = $"AND {nonTimeConstraint}";

            var timeDimConstraint = new List<string>();
            Predicate.AddTimePredicate(timeDimConstraint, dataFlow, null, "Fact");
            var timeConstraint = action is StagingRowActionEnum.Delete && timeDimConstraint.Any() ? " AND " + string.Join(" AND ", timeDimConstraint) : null;

            var sqlCommand = $@"
            {(includeSummary ? "declare @updated as int = 0, @deleted as int = 0;" : "")}
            
MERGE INTO [{DotStatDb.DataSchema}].[{dsd.SqlFactTable((char)tableVersion)}] Fact
USING
(
    SELECT Filt.[SID] {timeDimColumns}{measureSelect}{obsAttrColumns}, [REAL_ACTION_FACT]
    FROM [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}] Import
    LEFT JOIN [{DotStatDb.DataSchema}].[{dsd.SqlFilterTable()}] Filt 
    ON {dimensionColumnsNoTimeDimJoin}
    WHERE Import.[BATCH_NUMBER] = @BatchNumber AND [REAL_ACTION_FACT] <> @Skip
    {nonTimeConstraint}
) Import
ON Fact.[SID] = Import.[SID] {timeDimColumnsJoin}
{timeConstraint}
{actionStatement}
{summaryStatement}";

            //Add Sql parameters
            var sqlParameters = new List<DbParameter>
            {
                new SqlParameter("CurrentDateTime", SqlDbType.DateTime) { Value = dateTime },
                new SqlParameter("Merge", SqlDbType.Int) { Value = (int)StagingRowActionEnum.Merge },
                new SqlParameter("Delete", SqlDbType.Int) { Value = (int)StagingRowActionEnum.Delete },
                new SqlParameter("Skip", SqlDbType.Int) { Value = (int)StagingRowActionEnum.Skip },
                new SqlParameter("BatchNumber", SqlDbType.Int) { Value = batchNumber }
            };

            return await ExecuteMerge(sqlCommand, sqlParameters, includeSummary, cancellationToken, dsd, reportedComponents);            
        }

        public override async ValueTask<MergeResult> MergeStagingToSeriesTable(IImportReferenceableStructure referencedStructure,
            ReportedComponents reportedComponents, DbTableVersion tableVersion,
            BatchAction batchAction, bool includeSummary, ICodeTranslator codeTranslator, DateTime dateTime,
            CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();
            if (referencedStructure is not Dataflow dataFlow)
            {
                //Dsd level merge not required for data
                return mergeResult;
            }

            var noTimeDimensions = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension).ToList();
            //There are no dimensions (excluding time) reported, therefore no changes required a series/group series level
            //Action takes place at given the scope of the reported dimensions; No reported dimensions means either delete all or only at dataflow level
            if (!noTimeDimensions.Any())
                return mergeResult;

            var action = batchAction.Action;
            var batchNumber = batchAction.BatchNumber;
            var isWildCarded = batchAction.IsWildCarded;

            if (action is StagingRowActionEnum.Skip || action is StagingRowActionEnum.DeleteAll || 
                (!reportedComponents.SeriesAttributesWithNoTimeDim.Any() && action is StagingRowActionEnum.Merge))
                return mergeResult;

            var dsd = dataFlow.Dsd;

            //Dimensions columns
            var dimensionColumnsNoTimeDimJoinTemplate = action switch
            {
                //Delete action allows for wildcard dimensions and Replaces can have attributes not referencing all dimensions 
                //Note that for Replaces, the replace will not be wildcarded because in the validation CheckReferenceDimensionsPresent, all referenced dimensions must be present
                StagingRowActionEnum.Delete or StagingRowActionEnum.Replace when isWildCarded => " Filt.{0} = ISNULL(Import.{0}, Filt.{0}) ",
                _ => " Filt.{0} = Import.{0} "
            };
            
            var dimensionColumnsNoTimeDimJoin =
                string.Join(" AND ", noTimeDimensions.Select(a => string.Format(dimensionColumnsNoTimeDimJoinTemplate, a.SqlColumn())));


            //Attributes columns
            var reportedAttributes = action == StagingRowActionEnum.Merge? 
                reportedComponents.SeriesAttributesWithNoTimeDim : 
                dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DimensionGroup or AttributeAttachmentLevel.Group)
                    .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId));

            if(!reportedAttributes.Any())
                return mergeResult;

            var seriesAttrColumns = reportedAttributes.Any() ?
                ", " + reportedAttributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength) : string.Empty;

            var seriesAttrColumnsUpdate = action switch
            {
                //When Replacing data, then higher-level attributes (series, group, dataflow) are not to be replaced but only updated or added if related values are present in the upload file.
                //It means that they cannot be NULLed through a Replace action.
                //Only Delete actions can NULL those attributes.
                StagingRowActionEnum.Merge or StagingRowActionEnum.Replace =>
                    string.Join(",\n", reportedAttributes
                        .Select(a => $"Attr.{a.SqlColumn()} = CASE WHEN Import.{a.SqlColumn()} IS NOT NULL THEN Import.{a.SqlColumn()} ELSE Attr.{a.SqlColumn()} END")),

                StagingRowActionEnum.Delete =>
                    string.Join(",\n", reportedAttributes
                        .Select(a => $"Attr.{a.SqlColumn()} = CASE WHEN Import.{a.SqlColumn()} IS NOT NULL THEN NULL ELSE Attr.{a.SqlColumn()} END")),
                _ => string.Empty
            };

            var seriesAttrColumnsWithMin = string.Join(",", reportedAttributes.Select(a => string.Format("MIN({0}) {0}", a.SqlColumn())));

            if (!string.IsNullOrEmpty(seriesAttrColumnsWithMin))
                seriesAttrColumnsWithMin = "," + seriesAttrColumnsWithMin;

            var updateDiffCheck = action switch
            {
                StagingRowActionEnum.Merge or StagingRowActionEnum.Replace =>
                    string.Join(" OR\n", reportedAttributes
                        .Select(a => $"(Import.{a.SqlColumn()} IS NOT NULL AND (Attr.{a.SqlColumn()} <> Import.{a.SqlColumn()} {a.SqlCollate()} OR Attr.{a.SqlColumn()} IS NULL))")),

                StagingRowActionEnum.Delete =>
                    string.Join(" OR\n", reportedAttributes
                        .Select(a => $"(Import.{a.SqlColumn()} IS NOT NULL AND Attr.{a.SqlColumn()} IS NOT NULL)")),
                _ => string.Empty
            };

            if (!string.IsNullOrEmpty(updateDiffCheck))
                updateDiffCheck = $"AND ({updateDiffCheck})";

            //Calculate summary
            var summaryStatement = string.Empty;
            if (includeSummary)
            {
                summaryStatement = action == StagingRowActionEnum.Delete ?
                "SELECT 0 as [INSERTED], @updated as [UPDATED], @@ROWCOUNT - @updated as [DELETED], @@ROWCOUNT as [TOTAL];" :
                "SELECT @@ROWCOUNT - @updated as [INSERTED], @updated as [UPDATED], 0 AS [DELETED], @@ROWCOUNT as [TOTAL];";
            }
            else
            {
                summaryStatement = "SELECT CASE WHEN @@ROWCOUNT > 0 THEN  CAST(1 AS BIT) ELSE CAST(0 AS BIT) END;";
            }

            //Merge staging table to Attr table
            var actionStatement = action == StagingRowActionEnum.Delete ?
//Delete actions
$@"WHEN MATCHED AND Import.[REAL_ACTION_ATTR] = @Merge {updateDiffCheck} THEN --Soft Delete set values to NULL
    UPDATE SET 
    {seriesAttrColumnsUpdate}
    , [{DbExtensions.LAST_UPDATED_COLUMN}] = @CurrentDateTime
    {(includeSummary ? ", @updated = @updated + 1" : "")} 
WHEN MATCHED AND Import.[REAL_ACTION_ATTR] = @Delete THEN DELETE; --Real delete" :
// Replace and Merge actions
$@"WHEN MATCHED {updateDiffCheck} THEN --Updates
    UPDATE SET 
    {seriesAttrColumnsUpdate}
    , [{DbExtensions.LAST_UPDATED_COLUMN}] = @CurrentDateTime
    {(includeSummary ? ", @updated = @updated + 1" : "")} 
WHEN NOT MATCHED THEN -- Inserts
INSERT ([SID]{seriesAttrColumns},[{DbExtensions.LAST_UPDATED_COLUMN}])
VALUES ([SID]{seriesAttrColumns},@CurrentDateTime);";

            //Filter allowed content constraint
            var nonTimeConstraint = action == StagingRowActionEnum.Delete ?
                Predicate.BuildWhereForObservationLevel(dataQuery: null, dataFlow, codeTranslator, useExternalValues: false, includeTimeConstraints: false, tableAlias: "Filt")
                : string.Empty;
            if(!string.IsNullOrEmpty(nonTimeConstraint))
                nonTimeConstraint = $"AND {nonTimeConstraint}";

            //Merge staging table to Attributes table            
            var sqlCommand = $@"
            {(includeSummary ? "declare @updated as int = 0;" : "")}
            {(includeSummary ? "declare @deleted as int = 0;" : "")}

MERGE INTO [{DotStatDb.DataSchema}].[{dsd.SqlDimGroupAttrTable((char)tableVersion)}] Attr
USING
(    
    SELECT Filt.[SID] {seriesAttrColumnsWithMin}, MIN([REAL_ACTION_ATTR]) [REAL_ACTION_ATTR]
    FROM [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}] Import
    LEFT JOIN [{DotStatDb.DataSchema}].[{dsd.SqlFilterTable()}] Filt 
    ON {dimensionColumnsNoTimeDimJoin}
    WHERE Import.[BATCH_NUMBER] = @BatchNumber AND [REAL_ACTION_ATTR] <> @Skip
    {nonTimeConstraint}
	GROUP BY Filt.[SID]
) Import
ON Attr.[SID] = Import.[SID]
{actionStatement}
{summaryStatement}";

            //Add Sql parameters
            var sqlParameters = new List<DbParameter>
            {
                new SqlParameter("CurrentDateTime", SqlDbType.DateTime) { Value = dateTime },
                new SqlParameter("Merge", SqlDbType.Int) { Value = (int)StagingRowActionEnum.Merge },
                new SqlParameter("Delete", SqlDbType.Int) { Value = (int)StagingRowActionEnum.Delete },
                new SqlParameter("Skip", SqlDbType.Int) { Value = (int)StagingRowActionEnum.Skip },
                new SqlParameter("BatchNumber", SqlDbType.Int) { Value = batchNumber }
            };

            //EXECUTE MERGE

            return await ExecuteMerge(sqlCommand, sqlParameters, includeSummary, cancellationToken, dsd, reportedComponents);
        }

        public override async ValueTask<MergeResult> MergeStagingToDatasetTable(IImportReferenceableStructure referencedStructure,
            IList<IKeyValue> attributes,
            CodeTranslator translator, DbTableVersion tableVersion, StagingRowActionEnum action, bool includeSummary,
            DateTime dateTime, CancellationToken cancellationToken)
        {
            var mergeResult = new MergeResult();
            if (referencedStructure is not Dataflow dataFlow)
            {
                //Dsd level merge not required for data
                return mergeResult;
            }

            if (action is StagingRowActionEnum.Skip || action is StagingRowActionEnum.DeleteAll)
                return mergeResult;

            //No dataSet level attributes to process
            if (!attributes.Any())
                return mergeResult;

            var datasetAttributesOfDds = dataFlow.Dsd.Attributes
                .Where(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null).ToList();

            //No dsd attributes
            if (datasetAttributesOfDds.Count == 0)
                return mergeResult;

            //Attributes
            var attributesToModify = new Dictionary<Attribute, object>();
            var i = 0;

            var attributesPresent = new bool[datasetAttributesOfDds.Count];
            foreach (var attributeKeyValue in attributes.Where(x => !string.IsNullOrEmpty(x.Code)))
            {
                var attribute = datasetAttributesOfDds.First(a =>
                    a.Base.Id.Equals(attributeKeyValue.Concept, StringComparison.InvariantCultureIgnoreCase));

                //Translate to internal codes
                var attrValue = attribute.GetObjectFromString(attributeKeyValue.Code, translator, action);
                attributesToModify[attribute] = attrValue;
                attributesPresent[i++] = true;
            }

            if (!attributesToModify.Any())
                return mergeResult;

            var realAction =  action switch
            {
                StagingRowActionEnum.Delete when attributesPresent.Distinct().Count() != 1 =>
                    StagingRowActionEnum.Merge,
                StagingRowActionEnum.Replace when !attributesToModify.Any() => 
                    StagingRowActionEnum.Delete,
                StagingRowActionEnum.Merge when !attributesToModify.Any() => 
                    StagingRowActionEnum.Skip,
                _ => action
            };

            if (realAction == StagingRowActionEnum.Skip)
                    return mergeResult;

            //DELETE
            if (realAction == StagingRowActionEnum.Delete)
            {
                if (includeSummary)
                {
                    mergeResult.DeleteCount += (int)await DotStatDb.ExecuteScalarSqlAsync(
                        $"DELETE FROM [{DotStatDb.DataSchema}].[{dataFlow.Dsd.SqlDsdAttrTable((char)tableVersion)}] WHERE [DF_ID] = {dataFlow.DbId}; SELECT @@ROWCOUNT AS [ROW_COUNT];", cancellationToken);
                    mergeResult.TotalCount =
                        mergeResult.UpdateCount + mergeResult.DeleteCount + mergeResult.InsertCount;
                }
                else
                {
                    await DotStatDb.ExecuteNonQuerySqlAsync(
                        $"DELETE FROM [{DotStatDb.DataSchema}].[{dataFlow.Dsd.SqlDsdAttrTable((char)tableVersion)}] WHERE [DF_ID] = {dataFlow.DbId}", cancellationToken);
                }

                return mergeResult;
            }

            //ATTRIBUTES
            var columnNames = string.Join(",", attributesToModify.Select(kvp => kvp.Key.SqlColumn()));

            var attributeColumns = string.Join(",", attributesToModify.Select(kvp => $"@Comp{kvp.Key.DbId} AS {kvp.Key.SqlColumn()}"));
            
            var attrColumnsUpdate = action switch
            {
                //When Replacing data, then higher-level attributes (series, group, dataflow) are not to be replaced but only updated or added if related values are present in the upload file.
                //It means that they cannot be NULLed through a Replace action.
                //Only Delete actions can NULL those attributes.
                StagingRowActionEnum.Merge or StagingRowActionEnum.Replace =>
                    string.Join(",\n", attributesToModify.Select(kvp => kvp.Key)
                        .Select(a => $"Fact.{a.SqlColumn()} = CASE WHEN Import.{a.SqlColumn()} IS NOT NULL THEN Import.{a.SqlColumn()} ELSE Fact.{a.SqlColumn()} END")),
                StagingRowActionEnum.Delete =>
                    string.Join(",\n", attributesToModify.Select(kvp => kvp.Key)
                        .Select(a => $"Fact.{a.SqlColumn()} = CASE WHEN Import.{a.SqlColumn()} IS NOT NULL THEN NULL ELSE Fact.{a.SqlColumn()} END")),
                _ => string.Empty
            };

            var updateDiffCheck = action switch
            {
                StagingRowActionEnum.Merge or StagingRowActionEnum.Replace =>
                    string.Join(" OR\n", attributesToModify.Select(kvp => kvp.Key)
                        .Select(a => $"(Import.{a.SqlColumn()} IS NOT NULL AND (Fact.{a.SqlColumn()} <> Import.{a.SqlColumn()} {a.SqlCollate()} OR Fact.{a.SqlColumn()} IS NULL))")),

                StagingRowActionEnum.Delete =>
                    string.Join(" OR\n", attributesToModify.Select(kvp => kvp.Key)
                        .Select(a => $"(Import.{a.SqlColumn()} IS NOT NULL AND Fact.{a.SqlColumn()} IS NOT NULL)")),
                _ => string.Empty
            };

            if (!string.IsNullOrEmpty(updateDiffCheck))
                updateDiffCheck = $"AND ({updateDiffCheck})";

            //Merge staging table to DsdAttr table
            var actionStatement = action == StagingRowActionEnum.Delete ?
//Delete actions
$@"WHEN MATCHED {updateDiffCheck} THEN --Soft Delete set values to NULL
    UPDATE SET 
    {attrColumnsUpdate}
    , [{DbExtensions.LAST_UPDATED_COLUMN}] = @CurrentDateTime
    {(includeSummary ? ", @updated = @updated + 1" : "")};" :
// Replace and Merge actions
$@"WHEN MATCHED {updateDiffCheck} THEN --Updates
    UPDATE SET 
    {attrColumnsUpdate}
    , [{DbExtensions.LAST_UPDATED_COLUMN}] = @CurrentDateTime
    {(includeSummary ? ", @updated = @updated + 1" : "")} 
WHEN NOT MATCHED THEN -- Inserts
INSERT ([DF_ID], {columnNames}, [{DbExtensions.LAST_UPDATED_COLUMN}])
VALUES ([DF_ID], {columnNames}, @CurrentDateTime);";

            var sqlCommand = $@"
            {(includeSummary ? "declare @updated as int = 0;" : "")}
MERGE
INTO [{DotStatDb.DataSchema}].[{dataFlow.Dsd.SqlDsdAttrTable((char)tableVersion)}] AS Fact
USING (SELECT @DfId AS [DF_ID], {attributeColumns}) AS Import
    ON Fact.[DF_ID] = Import.[DF_ID]
{actionStatement}

{(includeSummary ? "SELECT @@ROWCOUNT - @updated AS [INSERTED], @updated as [UPDATED], 0 as [DELETED], @@ROWCOUNT as [TOTAL];" : "SELECT CASE WHEN @@ROWCOUNT > 0 THEN 1 ELSE 0 END;")}";
            
            //SQL PARAMETERS
            var sqlParameters = new List<DbParameter>
            {
                new SqlParameter("CurrentDateTime", SqlDbType.DateTime) { Value = dateTime }
            };

            sqlParameters.AddRange(attributesToModify.Select(kvp =>
                new SqlParameter($"Comp{kvp.Key.DbId}", kvp.Key.GetSqlType())
                {
                    Value = kvp.Value
                } as DbParameter).Concat(new[] { new SqlParameter("DfId", SqlDbType.Int) { Value = dataFlow.DbId } as DbParameter }).ToArray());

            //EXECUTE MERGE

            return await ExecuteMerge(sqlCommand, sqlParameters, includeSummary, cancellationToken);
        }
        
        public async Task DropDsdTables(Dsd dsd, CancellationToken cancellationToken)
        {
            await DropDsdTables(dsd, DbTableVersion.A, cancellationToken);
            await DropDsdTables(dsd, DbTableVersion.B, cancellationToken);
        }

        protected override async Task<bool> CleanUpFullyEmptyObservations(IImportReferenceableStructure referencedStructure, DbTableVersion tableVersion,
            CancellationToken cancellationToken)
        {
            if (referencedStructure is not Dataflow dataFlow)
            {
                //Dsd level cleanup not required for data
                return true;
            }

            var dsd = dataFlow.Dsd;
            if (!dsd.Attributes.Any())
            {
                return false;
            }

            var attributeColumns = dsd.Attributes
                .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToColumnListWithIsNull();

            if (!string.IsNullOrWhiteSpace(attributeColumns))
            {
                attributeColumns += " AND ";
            }

            // Fact table
            //Delete fully empty rows at obs level
            var sqlCommand = $@"DELETE FROM [{DotStatDb.DataSchema}].[{dsd.SqlFactTable((char)tableVersion)}] WHERE 
                {attributeColumns}
                [VALUE] IS NULL; 

                SELECT CASE WHEN @@ROWCOUNT > 0 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END;";

            var coordinatesDeleted = await DotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken) > 0;

            // Group attribute table
            attributeColumns = dsd.Attributes
                .Where(x => !x.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                .ToColumnListWithIsNull(new[] { AttributeAttachmentLevel.DimensionGroup, AttributeAttachmentLevel.Group });

            if (!string.IsNullOrEmpty(attributeColumns))
            {
                sqlCommand = $@"DELETE FROM [{DotStatDb.DataSchema}].[{dsd.SqlDimGroupAttrTable((char)tableVersion)}] WHERE {attributeColumns}; 
                SELECT CASE WHEN @@ROWCOUNT > 0 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END; ";

                var attrRowsDeleted = await DotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken) > 0;
                coordinatesDeleted = coordinatesDeleted || attrRowsDeleted; 
            }

            // Dataset level attribute table
            attributeColumns = dsd.Attributes.ToColumnListWithIsNull(new[] { AttributeAttachmentLevel.DataSet, AttributeAttachmentLevel.Null });

            if (!string.IsNullOrEmpty(attributeColumns))
            {
                await DotStatDb.ExecuteNonQuerySqlAsync($@"DELETE FROM [{DotStatDb.DataSchema}].[{dsd.SqlDsdAttrTable((char)tableVersion)}] WHERE {attributeColumns}", cancellationToken);
            }

            //Indicate if there are coordinates that were deleted at observation or series level.
            return coordinatesDeleted;
        }

        private async Task DropDsdTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            if (DotStatDb.DataSpace.KeepHistory)
            {
                await DotStatDb.RemoveTemporalTableSupport(DotStatDb.DataSchema, dsd.SqlFactTable((char)tableVersion),
                    cancellationToken);
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlFactHistoryTable((char)tableVersion), cancellationToken);
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlFactTable((char)tableVersion), cancellationToken);
            }
            else
            {
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlFactTable((char)tableVersion), cancellationToken);
            }

            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlFilterTable(), cancellationToken);
            await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlDeletedTable((char)tableVersion), cancellationToken);
        }
        
        public async Task DropAttributeTables(Dsd dsd, CancellationToken cancellationToken)
        {
            if (DotStatDb.DataSpace.KeepHistory)
            {
                await DotStatDb.RemoveTemporalTableSupport(DotStatDb.DataSchema, dsd.SqlDimGroupAttrTable((char)DbTableVersion.A),
                    cancellationToken);
                await DotStatDb.RemoveTemporalTableSupport(DotStatDb.DataSchema, dsd.SqlDimGroupAttrTable((char)DbTableVersion.B),
                    cancellationToken);
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlDimGroupAttrHistoryTable((char)DbTableVersion.A),
                    cancellationToken);
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlDimGroupAttrHistoryTable((char)DbTableVersion.B),
                    cancellationToken);
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlDimGroupAttrTable((char)DbTableVersion.A),
                    cancellationToken);
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlDimGroupAttrTable((char)DbTableVersion.B),
                    cancellationToken);
            }
            else
            {
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlDimGroupAttrTable((char)DbTableVersion.A),
                    cancellationToken);
                await DotStatDb.DropTable(DotStatDb.DataSchema, dsd.SqlDimGroupAttrTable((char)DbTableVersion.B),
                    cancellationToken);
            }
        }

        protected override async Task DeleteAll(IImportReferenceableStructure referencedStructure, DbTableVersion tableVersion, bool includeSummary, ImportSummary importSummary, ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken)
        {
            if (referencedStructure is not Dataflow dataFlow)
            {
                //Dsd level delete all not required for data
                return;
            }

            var noTimeDimConstraint = Predicate.BuildWhereForObservationLevel(dataQuery: null, dataFlow, codeTranslator, useExternalValues: false, includeTimeConstraints: false);
            var filteredTimeDim = new List<string>();
            Predicate.AddTimePredicate(filteredTimeDim, dataFlow, null, null);

            var filterConstraintObs = !string.IsNullOrEmpty(noTimeDimConstraint) || filteredTimeDim.Any() ? "WHERE " : string.Empty;
            if(!string.IsNullOrEmpty(noTimeDimConstraint))
            {
                filterConstraintObs += $@" [SID] IN (
SELECT [SID] FROM [{DotStatDb.DataSchema}].{dataFlow.Dsd.SqlFilterTable()} 
WHERE {noTimeDimConstraint}
)";
            }

            if (filteredTimeDim.Any())
            {
                filterConstraintObs += !string.IsNullOrEmpty(noTimeDimConstraint) ? " AND " : "";
                filterConstraintObs += string.Join(" AND ", filteredTimeDim);
            }

            var mergeResult = await DeleteAll(dataFlow.Dsd.SqlFactTable((char)tableVersion), filterConstraintObs, includeSummary, cancellationToken);

            if (mergeResult.Errors.Any())
            {
                importSummary.Errors.AddRange(mergeResult.Errors);
                return;
            }

            importSummary.ObservationLevelMergeResult.Add(mergeResult);

            if (dataFlow.Dsd.Attributes.Any(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DimensionGroup or AttributeAttachmentLevel.Group))
            {
                var filterConstraintSeries = !string.IsNullOrEmpty(noTimeDimConstraint) ? $@"WHERE [SID] IN (
SELECT [SID] FROM [{DotStatDb.DataSchema}].{dataFlow.Dsd.SqlFilterTable()} 
WHERE {noTimeDimConstraint}
)" :string.Empty;

                    mergeResult = await DeleteAll(dataFlow.Dsd.SqlDimGroupAttrTable((char)tableVersion), filterConstraintSeries, includeSummary, cancellationToken);

                if (mergeResult.Errors.Any())
                {
                    importSummary.Errors.AddRange(mergeResult.Errors);
                    return;
                }

                importSummary.SeriesLevelMergeResult.Add(mergeResult);
            }
            
            if (dataFlow.Dsd.Attributes.Any(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null))
            {
                mergeResult = await DeleteAll(dataFlow.Dsd.SqlDsdAttrTable((char)tableVersion), filterAllowedContentConstraint: null, includeSummary, cancellationToken);

                if (mergeResult.Errors.Any())
                {
                    importSummary.Errors.AddRange(mergeResult.Errors);
                    return;
                }

                importSummary.DataFlowLevelMergeResult.Add(mergeResult);
            }

            Log.Warn(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DeleteAllActionPerformed));

            await InsertDeletedAllRow(dataFlow.DbId, dataFlow.Dsd.SqlDeletedTable((char)tableVersion), dateTime, cancellationToken);
            
        }
       
        public async Task TruncateDsdTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.SqlFactTable((char)tableVersion), cancellationToken);
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.SqlDeletedTable((char)tableVersion), cancellationToken);
        }

        public async Task DeleteAllFromDsdTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.DeleteAllFromTable(DotStatDb.DataSchema, dsd.SqlFactTable((char)tableVersion), cancellationToken);
            await DotStatDb.DeleteAllFromTable(DotStatDb.DataSchema, dsd.SqlDeletedTable((char)tableVersion), cancellationToken);
        }

        public async Task TruncateAttributeTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.SqlDsdAttrTable((char) tableVersion), cancellationToken);
            await DotStatDb.TruncateTable(DotStatDb.DataSchema, dsd.SqlDimGroupAttrTable((char) tableVersion), cancellationToken);
        }

        public async Task DeleteAllFromAttributeTables(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken)
        {
            await DotStatDb.DeleteAllFromTable(DotStatDb.DataSchema, dsd.SqlDsdAttrTable((char)tableVersion), cancellationToken);
            await DotStatDb.DeleteAllFromTable(DotStatDb.DataSchema, dsd.SqlDimGroupAttrTable((char)tableVersion), cancellationToken);
        }

        #region MappingSetParams
        public MappingSetParams GetMappingSetParams(Dataflow dataflow, char? tableVersion)
        {
            var emptyDataQuery = this.GetEmptyDataViewQuery(dataflow);

            return new MappingSetParams()
            {
                IsMetadata = false,
                ActiveQuery = tableVersion.HasValue ? GetDataViewQuery(dataflow, tableVersion.Value) : emptyDataQuery,
                ReplaceQuery = tableVersion.HasValue ? GetReplacedViewQuery(dataflow, tableVersion.Value) : emptyDataQuery,
                DeleteQuery = tableVersion.HasValue ? GetDeletedDataViewQuery(dataflow, tableVersion.Value) : emptyDataQuery,
                IncludeHistoryQuery = tableVersion.HasValue ? GetIncludeHistoryViewQuery(dataflow, tableVersion.Value) : emptyDataQuery,
                OrderByTimeAsc = GetOrderByClause(dataflow),
                OrderByTimeDesc = GetOrderByClause(dataflow, false)
            };
        }

        private string GetDataViewQuery(Dataflow dataflow, char tableVersion)
        {
            var columns = new List<string>();

            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"[TIME_PERIOD] AS [{dim.Code}]");
                    columns.Add($"[{DbExtensions.SqlPeriodStart()}]");
                    columns.Add($"[{DbExtensions.SqlPeriodEnd()}]");
                }
                else
                {
                    columns.Add($"[{dim.Code}]");
                }
            }

            columns.Add($"[{dataflow.Dsd.Base.PrimaryMeasure.Id}]");

            foreach (var attr in dataflow.Dsd.Attributes)
            {
                columns.Add($"[{attr.Code}]");
            }

            columns.Add($"[{DbExtensions.LAST_UPDATED_COLUMN}]");

            return $"SELECT [SID], {string.Join(",", columns)} FROM [{DotStatDb.DataSchema}].[{dataflow.SqlDataDataFlowViewName(tableVersion)}]";
        }
        private string GetReplacedViewQuery(Dataflow dataflow, char tableVersion)
        {
            var columns = new List<string>();

            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"[TIME_PERIOD] AS [{dim.Code}]");
                    columns.Add($"[{DbExtensions.SqlPeriodStart()}]");
                    columns.Add($"[{DbExtensions.SqlPeriodEnd()}]");
                }
                else
                {
                    columns.Add($"[{dim.Code}]");
                }
            }

            columns.Add($"[{dataflow.Dsd.Base.PrimaryMeasure.Id}]");

            foreach (var attr in dataflow.Dsd.Attributes)
            {
                columns.Add($"[{attr.Code}]");
            }

            columns.Add($"[{DbExtensions.LAST_UPDATED_COLUMN}]");

            return $"SELECT [SID], {string.Join(",", columns)} FROM [{DotStatDb.DataSchema}].[{dataflow.SqlDataReplaceDataFlowViewName(tableVersion)}]";
        }
        private string GetIncludeHistoryViewQuery(Dataflow dataflow, char tableVersion)
        {
            var columns = new List<string>();

            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"[TIME_PERIOD] AS [{dim.Code}]");
                    columns.Add($"[{DbExtensions.SqlPeriodStart()}]");
                    columns.Add($"[{DbExtensions.SqlPeriodEnd()}]");
                }
                else
                {
                    columns.Add($"[{dim.Code}]");
                }
            }

            columns.Add($"[{dataflow.Dsd.Base.PrimaryMeasure.Id}]");

            foreach (var attr in dataflow.Dsd.Attributes)
            {
                columns.Add($"[{attr.Code}]");
            }

            columns.Add($"[{DbExtensions.LAST_UPDATED_COLUMN}]");

            columns.Add("[ValidTo]");

            return $"SELECT [SID], {string.Join(",", columns)} FROM [{DotStatDb.DataSchema}].[{dataflow.SqlDataIncludeHistoryDataFlowViewName(tableVersion)}]";
        }
        private string GetDeletedDataViewQuery(Dataflow dataflow, char tableVersion)
        {
            var columns = new List<string>();

            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"[TIME_PERIOD] AS [{dim.Code}]");
                    columns.Add($"[{DbExtensions.SqlPeriodStart()}]");
                    columns.Add($"[{DbExtensions.SqlPeriodEnd()}]");
                }
                else
                {
                    columns.Add($"[{dim.Code}]");
                }
            }

            columns.Add($"[{dataflow.Dsd.Base.PrimaryMeasure.Id}]");

            foreach (var attr in dataflow.Dsd.Attributes)
            {
                columns.Add($"[{attr.Code}]");
            }

            columns.Add($"[{DbExtensions.LAST_UPDATED_COLUMN}]");

            return $"SELECT 0 as [SID], {string.Join(",", columns)} FROM [{DotStatDb.DataSchema}].[{dataflow.SqlDeletedDataViewName(tableVersion)}]";
        }
        private string GetEmptyDataViewQuery(Dataflow dataflow)
        {
            var columns = new List<string>();

            foreach (var dim in dataflow.Dsd.Dimensions)
            {
                if (dim.Base.TimeDimension)
                {
                    columns.Add($"NULL AS [{dim.Code}]");
                    columns.Add($"NULL AS [{DbExtensions.SqlPeriodStart()}]");
                    columns.Add($"NULL AS [{DbExtensions.SqlPeriodEnd()}]");
                }
                else
                {
                    columns.Add($"NULL AS [{dim.Code}]");
                }
            }

            columns.Add($"NULL AS [{dataflow.Dsd.Base.PrimaryMeasure.Id}]");

            foreach (var attr in dataflow.Dsd.Attributes)
            {
                columns.Add($"NULL AS [{attr.Code}]");
            }

            columns.Add($"NULL as [{DbExtensions.LAST_UPDATED_COLUMN}]");
            
            if(dataflow.Dsd.KeepHistory)
                columns.Add("NULL as [ValidTo]");

            return "SELECT TOP 0 NULL AS [SID], " + string.Join(",", columns);
        }
        private string GetOrderByClause(Dataflow dataflow, bool isTimeAscending = true)
        {
            var columns = new List<string>{"[SID] ASC"};

            if (dataflow.Dsd.TimeDimension != null)
            {
                columns.Add($"[{DbExtensions.SqlPeriodStart()}] {(isTimeAscending ? "ASC":"DESC")}");
                columns.Add($"[{DbExtensions.SqlPeriodEnd()}] {(isTimeAscending ? "DESC" : "ASC")}");
            }

            return string.Join(",", columns);
        }
        
        #endregion

        public async Task<bool> DataTablesExist(Dsd dsd, CancellationToken cancellationToken)
        {
            return
                await DotStatDb.TableExists(dsd.SqlFactTable((char)DbTableVersion.A), cancellationToken)
                || await DotStatDb.TableExists(dsd.SqlFactTable((char)DbTableVersion.B), cancellationToken);
        }

        public async Task ReorganizeColumnStoreIndexes(Dsd dsd, char targetVersion, CancellationToken cancellationToken)
        {
            var indexPrefix = dsd.DataCompression == DataCompressionEnum.NONE
                ? "NCCI_"
                : "CCI_";

            var tables = dsd.DataCompression == DataCompressionEnum.NONE 
                ? new List<(string Table, string Index)>
                {
                    new($"{dsd.SqlFilterTable()}", null),
                    new($"{dsd.SqlFactTable(targetVersion)}", $"NCCI_TIME_DIM_{dsd.DbId}_{targetVersion}"),
                }
                : new List<(string Table, string Index)>
                {
                    new($"{dsd.SqlFilterTable()}", null),
                    new($"{dsd.SqlFactTable(targetVersion)}", null),
                    new($"{dsd.SqlDeletedTable(targetVersion)}", null)
                };


            if (dsd.DataCompression != DataCompressionEnum.NONE && dsd.Attributes
                    .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                    .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                    .Any())
            {
                tables.Add(new($"{dsd.SqlDimGroupAttrTable(targetVersion)}", null));
            }

            foreach (var t in tables)
            {
                await DotStatDb.ReorganizeColumnStoreIndex(DotStatDb.DataSchema, t.Table, t.Index ?? (indexPrefix + t.Table), cancellationToken);
            }
        }

        protected override async Task StoreDeleteInstructions(
            IImportReferenceableStructure referencedStructure,
            ReportedComponents reportedComponents,
            IList<DataSetAttributeRow> dataSetAttributeRows,
            DbTableVersion tableVersion,
            DateTime dateTime,
            CancellationToken cancellationToken)
        {
            if (referencedStructure is not Dataflow dataFlow)
            {
                //Dsd level delete all not required for data
                return;
            }

            var dimensions = new List<string>();
            var components = new List<string>();

            if (reportedComponents.Dimensions.Any())
            {
                dimensions.Add(reportedComponents.Dimensions.ToColumnList());
            }

            if (reportedComponents.TimeDimension != null)
            {
                dimensions.Add(DbExtensions.SqlPeriodStart());
                dimensions.Add(DbExtensions.SqlPeriodEnd());
            }

            if (reportedComponents.IsPrimaryMeasureReported)
            {
                components.Add(DbExtensions.ValueColumn);
            }

            if (dataFlow.Dsd.Attributes.Any())
            {
                components.AddRange(dataFlow.Dsd.Attributes.Select(attr => attr.SqlColumn()));
            }

            var dsd = dataFlow.Dsd;

            if (!dimensions.Any() && !components.Any())
                return;

            // Store deleted instructions as indicated by the user in the import file
            var dimensionsString = dimensions.Any() ? "," + string.Join(",", dimensions) : "";

            var sqlParameters = new List<DbParameter>()
            {
                new SqlParameter("DF_ID", dataFlow.DbId),
                new SqlParameter("Delete", (int) StagingRowActionEnum.Delete),
                new SqlParameter("DT_Now", dateTime)
            };

            var sql = @$"INSERT INTO [{DotStatDb.DataSchema}].[{dsd.SqlDeletedTable((char)tableVersion)}]([DF_ID],[{DbExtensions.LAST_UPDATED_COLUMN}]{dimensionsString},{string.Join(",", components)}) 
                SELECT @DF_ID,@DT_Now {dimensionsString},{string.Join(",", components)} FROM [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}] 
                WHERE [REQUESTED_ACTION] = @Delete";

            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sql, cancellationToken, sqlParameters.ToArray());



            // Store fully empty rows in deletions table as a result of a partial delete or a replace to all nulls at observation level
            var attributesStoredAtObsLevel = dsd.Attributes
                .Where(x => (x.Base.AttachmentLevel == AttributeAttachmentLevel.Observation || x.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup || x.Base.AttachmentLevel == AttributeAttachmentLevel.Group)
                    && x.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId));

            var attributeColumns = attributesStoredAtObsLevel.ToColumnListWithIsNull();
            if (!string.IsNullOrWhiteSpace(attributeColumns))
            {
                attributeColumns += " AND ";
            }

            var selectColumns = attributesStoredAtObsLevel.ToColumnList();
            if (!string.IsNullOrWhiteSpace(selectColumns))
            {
                selectColumns = "," + selectColumns;
            }
            selectColumns += ", [VALUE]";


            var selectPresentColumns = string.Join(", ", attributesStoredAtObsLevel.Select(attr => $"@PresentColumn AS {attr.SqlColumn()}"));
            if (!string.IsNullOrWhiteSpace(selectPresentColumns))
            {
                selectPresentColumns = "," + selectPresentColumns;
            }
            selectPresentColumns += ", @PresentColumn AS [VALUE]";

            sqlParameters = new List<DbParameter>()
            {
                new SqlParameter("DF_ID", dataFlow.DbId),
                new SqlParameter("DT_Now", DateTime.UtcNow),
                new SqlParameter("PresentColumn", DbExtensions.ColumnPresentDbValue.ToString())
            };

            var sqlCommand = $@"INSERT INTO [{DotStatDb.DataSchema}].[{dsd.SqlDeletedTable((char)tableVersion)}]([DF_ID],[{DbExtensions.LAST_UPDATED_COLUMN}]{dimensionsString}{selectColumns}) 
SELECT @DF_ID,@DT_Now {dimensionsString}{selectPresentColumns} 
FROM [{DotStatDb.DataSchema}].[{dsd.SqlFactTable((char)tableVersion)}] FA
JOIN [{DotStatDb.DataSchema}].[{dsd.SqlFilterTable()}] FI
ON FA.[SID] = FI.[SID]
WHERE {attributeColumns} [VALUE] IS NULL";
            await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, sqlParameters.ToArray());

        }

        #region Private methods

        private async Task BuildStagingTables(Dsd dsd, ReportedComponents reportedComponents, bool isTimeAtTimeDimensionSupported, CancellationToken cancellationToken)
        {
            //Dimensions
            //32 is the SQL limit of columns that can participate in a Unique index
            var rowId = reportedComponents.Dimensions.Count > 32 ? $"[ROW_ID] AS ({dsd.SqlBuildRowIdFormula()}) PERSISTED NOT NULL," : null;

            var dimensionColumns = reportedComponents.Dimensions?.ToColumnList(true, true);
            if (!string.IsNullOrWhiteSpace(dimensionColumns))
                dimensionColumns += ',';

            var timePeriodColumns = reportedComponents.TimeDimension is not null
                ? string.Join(",",
                      DbExtensions.SqlPeriodStart(true, isTimeAtTimeDimensionSupported, true),
                      DbExtensions.SqlPeriodEnd(true, isTimeAtTimeDimensionSupported, true)
                ) + "," : null;

            //Measure
            var measure = reportedComponents.IsPrimaryMeasureReported ? $"[VALUE] {dsd.PrimaryMeasure.GetSqlType(dsd.SupportsIntentionallyMissingValues)} NULL," : null;

            //Attributes
            var attributeColumns = dsd.Attributes.OrderBy(a => a.DbId).ToColumnList(dsd.GetMaxLengthOfTextAttributeFromConfig(GeneralConfiguration.MaxTextAttributeLength), withType: true, applyNotNull: false);

            if (!string.IsNullOrWhiteSpace(attributeColumns))
                attributeColumns += ',';

            //Create table
            var sqlCommand = $@"CREATE TABLE [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}](
                    {rowId}
                    {timePeriodColumns}
                    {dimensionColumns}
                    {measure}
                    {attributeColumns}
                    [REQUESTED_ACTION] [TINYINT] NOT NULL,
                    [REAL_ACTION_FACT] [TINYINT] NOT NULL,
                    [REAL_ACTION_ATTR] [TINYINT] NOT NULL,
                    [BATCH_NUMBER] [INT] NULL
                )";

            await DotStatDb.ExecuteNonQuerySqlAsync(sqlCommand, cancellationToken);
        }

        private void OnSqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            var batchNumber = e.RowsCopied / DotStatDb.DataSpace.NotifyImportBatchSize;
            Log.Notice(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ProcessingObservations),
                e.RowsCopied, batchNumber));
        }

        private async Task<MergeResult> ExecuteMerge(string sqlCommand, List<DbParameter> sqlParams, bool includeSummary, CancellationToken cancellationToken, Dsd dsd = null, ReportedComponents reportedComponents = null)
        {
            var mergeResult = new MergeResult();
            if (string.IsNullOrEmpty(sqlCommand))
                return mergeResult;

            try
            {
                if (includeSummary)
                {
                    await using var dbDataReader = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlCommand, cancellationToken, commandBehavior: CommandBehavior.SingleRow, parameters: sqlParams.ToArray());

                    if (await dbDataReader.ReadAsync(cancellationToken))
                    {
                        mergeResult.InsertCount = dbDataReader.GetInt32(0);
                        mergeResult.UpdateCount = dbDataReader.GetInt32(1);
                        mergeResult.DeleteCount = dbDataReader.GetInt32(2);
                        mergeResult.TotalCount = dbDataReader.GetInt32(3);
                        mergeResult.RowsAddedDeleted = mergeResult.InsertCount > 0 || mergeResult.DeleteCount > 0;
                    }
                }
                else
                {
                    mergeResult.RowsAddedDeleted = await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(sqlCommand, cancellationToken, sqlParams.ToArray()) > 0;
                }
            }
            catch (SqlException e)
            {
                switch (e.Number)
                {
                    case 2627://duplicates in the staging table insert
                        mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.DuplicatedRowsInStagingTable });
                        break;
                    case 8672://Overlapping rows or duplicates
                        if (await StagingTableHasDuplicates(dsd, reportedComponents, cancellationToken))
                            mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.DuplicatedRowsInStagingTable });
                        else
                            mergeResult.Errors.Add(new ObservationError { Type = ValidationErrorType.MultipleRowsModifyingTheSameRegionInStagingTable });
                        break;
                    default:
                        throw;

                }
            }

            return mergeResult;
        }

        private async Task<bool> StagingTableHasDuplicates(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken)
        {
            if (dsd is null || reportedComponents is null)
                return false;

            //Dimensions columns
            var noTimeDimensions = reportedComponents.Dimensions.Where(d => !d.Base.TimeDimension
                                                                            && dsd.Dimensions.Any(rd => rd.Code.Equals(d.Code, StringComparison.CurrentCultureIgnoreCase)));

            var dimensionColumnsNoTimeDimJoin =
                string.Join(", ", noTimeDimensions.Select(a => a.SqlColumn()));

            string timeDimColumns = null;
            if (reportedComponents.TimeDimension is not null)
            {
                timeDimColumns = "," + string.Join(",", DbExtensions.SqlPeriodStart(), DbExtensions.SqlPeriodEnd());
            }

            var sqlCommand = $@"SELECT TOP 1 1 FROM [{DotStatDb.DataSchema}].[{dsd.SqlStagingTable()}]
GROUP BY {dimensionColumnsNoTimeDimJoin} {timeDimColumns}, [REQUESTED_ACTION]
HAVING COUNT(*) > 1";

            return ((int?)await DotStatDb.ExecuteScalarSqlAsync(sqlCommand, cancellationToken) ?? 0) > 0;
        }

        #endregion
    }
}