﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.DB;

namespace DotStat.Db.Repository.SqlServer
{
    public class SqlCodelistRepository : DatabaseRepositoryBase<SqlDotStatDb>, ICodelistRepository
    {
        public SqlCodelistRepository(SqlDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }

        public async Task<string[]> GetDimensionCodesFromDb(int codeListId, CancellationToken cancellationToken)
        {
            return await GetCodelist(
                $"SELECT ITEM_ID, ID FROM [{DotStatDb.ManagementSchema}].[{DbExtensions.GetCodelistTableName(codeListId)}] ORDER BY ITEM_ID DESC",
                (id, r) => r.GetString(1), cancellationToken);
        }

        #region Private methods

        private async Task<T[]> GetCodelist<T>(string sql, Func<int, IDataReader, T> readCode, CancellationToken cancellationToken)
        {
            T[] codelist;

            using (var dr = await DotStatDb.ExecuteReaderSqlAsync(sql, cancellationToken))
            {
                if (!dr.Read())
                    throw new DotStatException(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EmptyCodelist));

                var id = dr.GetInt32(0);
                codelist = new T[id + 1]; // dimension desc order, first row has max capasity
                codelist[id] = readCode(id, dr);

                while (dr.Read())
                {
                    id = dr.GetInt32(0);
                    codelist[id] = readCode(id, dr);
                }
            }

            return codelist;
        }
        #endregion

    }
}