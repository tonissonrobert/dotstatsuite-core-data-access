﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.DB;
using DotStat.Db.Dto;
using DotStat.Db.Engine.SqlServer;
using DotStat.Db.Exception;
using DotStat.Db.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Attribute = DotStat.Domain.Attribute;

namespace DotStat.Db.Repository.SqlServer
{
    public class SqlAttributeRepository : DatabaseRepositoryBase<SqlDotStatDb>, IAttributeRepository
    {
        public SqlAttributeRepository(SqlDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration) :
            base(dotStatDb, generalConfiguration)
        {
        }

        public async Task CreateAttribute(Attribute attr, Dsd dsd, IList<ComponentItem> componentItems, CancellationToken cancellationToken)
        {
            var attributeBuilder = new SqlAttributeEngine(GeneralConfiguration);

            try
            {
                attr.DbId = await attributeBuilder.InsertToComponentTable(attr, DotStatDb, cancellationToken);
                await InsertToAttributeDimensionSetTable(attr, dsd, componentItems, cancellationToken);
            }
            catch
            {
                await attributeBuilder.CleanUp(attr, DotStatDb, cancellationToken);

                throw;
            }
        }

        public async IAsyncEnumerable<IKeyValue> GetDatasetAttributes(
            Dataflow dataflow,
            ICodeTranslator codeTranslator,
            DbTableVersion tableVersion,
            [EnumeratorCancellation] CancellationToken cancellationToken
        )
        {
            var datasetAttributes =
                dataflow.Dsd.Attributes
                .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet || a.Base.AttachmentLevel == AttributeAttachmentLevel.Null).ToArray();

            if (!datasetAttributes.Any())
            {
                yield break;
            }
            
            var sqlQuery = BuildDatasetAttributeSqlQuery(dataflow, datasetAttributes, (char)tableVersion);
            var sqlParameters = new DbParameter[] {new SqlParameter("DfId", SqlDbType.Int) {Value = dataflow.DbId}};

            await using (var dr = await DotStatDb.ExecuteReaderSqlWithParamsAsync(sqlQuery, cancellationToken, parameters:sqlParameters, tryUseReadOnlyConnection: true))
            {
                while (await dr.ReadAsync(cancellationToken))
                {
                    foreach (var attribute in datasetAttributes)
                    {
                        var keyValue = await GetAttributeKeyValue(
                            attribute,
                            codeTranslator,
                            dr,
                            cancellationToken
                        );

                        if (keyValue != null)
                        {
                            yield return keyValue;
                        }
                    }
                }
            }
        }

        #region Private methods

        private async Task InsertToAttributeDimensionSetTable(Domain.Attribute attribute, Dsd dsd, IList<ComponentItem> dsdComponentsInDb, CancellationToken cancellationToken)
        {
            IList<string> dimensionReferences = null;

            if (attribute.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
            {
                dimensionReferences = attribute.Base.DimensionReferences;
            }
            else if (attribute.Base.AttachmentLevel == AttributeAttachmentLevel.Group)
            {
                dimensionReferences = dsd.Base.Groups.FirstOrDefault(g =>
                    g.Id.Equals(attribute.Base.AttachmentGroup, StringComparison.InvariantCultureIgnoreCase))?.DimensionRefs;
            }

            if (dimensionReferences == null)
            {
                return;
            }

            foreach (var dimensionId in dimensionReferences)
            {
                var dimension = dsd.Dimensions.FirstOrDefault(d => d.Code.Equals(dimensionId, StringComparison.InvariantCultureIgnoreCase));

                if (dimension == null)
                {
                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoDimensionFound),
                        dimensionId,
                        dsd.FullId,
                        attribute.Code)
                    );
                }

                var dimensionComponent = dsdComponentsInDb.FirstOrDefault(d =>
                        d.IsDimension && d.Id.Equals(dimensionId, StringComparison.InvariantCultureIgnoreCase));

                if (dimensionComponent == null || dimensionComponent.DbId <= 0)
                {
                    throw new DimensionNotFoundByTranslatorException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoDimensionFoundInManagementDb),
                        dimensionId,
                        dsd.FullId,
                        attribute.Code)
                    );
                }

                await DotStatDb.ExecuteNonQuerySqlWithParamsAsync(
                    $@"INSERT
                            INTO [{DotStatDb.ManagementSchema}].[ATTR_DIM_SET]
                                 ([ATTR_ID], [DIM_ID])
                          VALUES (@AttributeId, @DimensionId)",
                    cancellationToken,
                    new SqlParameter("AttributeId", SqlDbType.Int) { Value = attribute.DbId },
                    new SqlParameter("DimensionId", SqlDbType.Int) { Value = dimensionComponent.DbId });
            }
        }


        private string BuildDatasetAttributeSqlQuery(
            Dataflow dataflow,
            IList<Attribute> datasetAttributes,
            char tableVersion)
        {
            if (!datasetAttributes.Any())
            {
                return null;
            }

            var sbSql =
                $@"SELECT {datasetAttributes.ToColumnList(GeneralConfiguration.MaxTextAttributeLength)}
                     FROM [{DotStatDb.DataSchema}].[{dataflow.Dsd.SqlDsdAttrTable(tableVersion)}]
                    WHERE [DF_ID] = @DfId
                    ";

            return sbSql;
        }

        private async ValueTask<IKeyValue> GetAttributeKeyValue(Attribute attribute, ICodeTranslator codeTranslator, IDataReader dr, CancellationToken cancellationToken)
        {
            var sqlColumnName = attribute.SqlColumn().Trim("[]".ToCharArray());
            var codelistProjection = attribute.Base.HasCodedRepresentation() ? await codeTranslator[attribute, cancellationToken] : null;

            var keyValue = GetKeyValue(sqlColumnName, codelistProjection, attribute.Base, dr);

            return keyValue;
        }

        private IKeyValue GetKeyValue(string sqlColumn, ICodelistProjection codelistProjection, IComponent component, IDataReader dr)
        {
            var value = dr.ColumnValue<string>(sqlColumn);

            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            if (!component.HasCodedRepresentation() || codelistProjection == null)
            {
                return new KeyValueImpl(value, component.Id);
            }

            var componentCode = codelistProjection[Convert.ToInt32(value)];

            return new KeyValueImpl(componentCode, component.Id);
        }

        #endregion
    }
}