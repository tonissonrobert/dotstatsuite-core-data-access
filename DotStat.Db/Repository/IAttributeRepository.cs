﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.Dto;
using DotStat.Db.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Db.Repository
{
    public interface IAttributeRepository
    {

        Task CreateAttribute(Domain.Attribute attr, Dsd dsd, IList<ComponentItem> componentItems,
            CancellationToken cancellationToken);

        IAsyncEnumerable<IKeyValue> GetDatasetAttributes(
            Dataflow dataflow,
            ICodeTranslator codeTranslator,
            DbTableVersion tableVersion,
            CancellationToken cancellationToken
        );
    }
}