﻿using System;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.DB;
using DotStat.Db.Util;
using DotStat.Db.Validation;
using DotStat.DB.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace DotStat.DB.Repository
{
    public abstract class DataMergerBase<TDotStatDb>
        where TDotStatDb : IDotStatDb
    {
        protected readonly TDotStatDb DotStatDb;
        protected readonly IGeneralConfiguration GeneralConfiguration;

        protected DataMergerBase(TDotStatDb dotStatDb, IGeneralConfiguration generalConfiguration)
        {
            DotStatDb = dotStatDb;
            GeneralConfiguration = generalConfiguration;
        }
        
        public async Task<ImportSummary> MergeStagingTable(IImportReferenceableStructure referencedStructure, ReportedComponents reportedComponents, Dictionary<int, BatchAction> obsLevelBatchActions, IList<DataSetAttributeRow> dataSetAttributeRows, CodeTranslator translator,
            DbTableVersion tableVersion, bool includeSummary, CancellationToken cancellationToken)
        {
            var importSummary = new ImportSummary
            {
                //ObservationsCount = bulkImportResult.RowsCopied,
                Errors = new List<IValidationError>()
            };

            if (!obsLevelBatchActions.Any() && !dataSetAttributeRows.Any())
                return importSummary;

            var hasMerges = obsLevelBatchActions.Any(a=> a.Value.Action == StagingRowActionEnum.Merge);
            var hasReplaces = obsLevelBatchActions.Any(a => a.Value.Action == StagingRowActionEnum.Replace);
            var hasDeletions = obsLevelBatchActions.Any(a => a.Value.Action == StagingRowActionEnum.Delete);
            var hasDeleteAll = obsLevelBatchActions.Any(a => a.Value.Action == StagingRowActionEnum.DeleteAll);

            if (hasMerges || hasReplaces)
            {
                //Insert new series found in the import file
                await MergeStagingToDimensionsTable(referencedStructure, reportedComponents, cancellationToken);
            }

            using (TransactionScope scope = DotStatDb.GetTransactionScope())
            {
                //Use the same datetime for the LAST_UPDATED column for all changes within the same import/transfer

                var dateTime = DateTime.UtcNow;
                //Merge non dataset level components 
                foreach (var (_, batchAction) in obsLevelBatchActions)
                {
                    var action = batchAction.Action;

                    //Special case DeleteAll 
                    if (action is StagingRowActionEnum.DeleteAll)
                    {
                        await DeleteAll(referencedStructure, tableVersion, includeSummary, importSummary, translator, dateTime, cancellationToken);

                        importSummary.RequiresRecalculationOfActualContentConstraint = true;
                        if (importSummary.Errors.Count > 0)
                            return importSummary;
                    }
                    else
                    {
                        //Process observation level components (primary measure and attributes which reference all dimensions, including time)
                        var mergeResult = await MergeStagingToObservationsTable(referencedStructure, reportedComponents, tableVersion,
                            batchAction, includeSummary, translator, dateTime, cancellationToken);

                        if (mergeResult.Errors.Any())
                        {
                            importSummary.Errors.AddRange(mergeResult.Errors);
                            return importSummary;
                        }

                        importSummary.RequiresRecalculationOfActualContentConstraint = importSummary.RequiresRecalculationOfActualContentConstraint || mergeResult.RowsAddedDeleted;
                        importSummary.ObservationLevelMergeResult.Add(mergeResult);

                        //Process attributes that reference all dimensions, except time dimension
                        mergeResult = await MergeStagingToSeriesTable(referencedStructure, reportedComponents,
                            tableVersion, batchAction, includeSummary, translator, dateTime, cancellationToken);

                        if (mergeResult.Errors.Any())
                        {
                            importSummary.Errors.AddRange(mergeResult.Errors);
                            return importSummary;
                        }

                        importSummary.RequiresRecalculationOfActualContentConstraint = importSummary.RequiresRecalculationOfActualContentConstraint || mergeResult.RowsAddedDeleted;
                        importSummary.SeriesLevelMergeResult.Add(mergeResult);
                    }
                }

                //Merge  dataset level components 
                foreach (var dataSetAttributeRow in dataSetAttributeRows)
                {
                    var action = dataSetAttributeRow.Action;
                    hasReplaces = hasReplaces || action == StagingRowActionEnum.Replace;
                    hasDeletions = hasDeletions || action == StagingRowActionEnum.Delete;
                    hasDeleteAll = hasDeleteAll || action == StagingRowActionEnum.DeleteAll;

                    //Special case DeleteAll 
                    if (action is StagingRowActionEnum.DeleteAll)
                        continue;
                    
                    var mergeResult = await MergeStagingToDatasetTable(referencedStructure, dataSetAttributeRow.Attributes,
                                    translator, tableVersion, action, includeSummary, dateTime, cancellationToken);

                    if (mergeResult.Errors.Any())
                    {
                        importSummary.Errors.AddRange(mergeResult.Errors);
                        return importSummary;
                    }

                    importSummary.DataFlowLevelMergeResult.Add(mergeResult);
                }

                //Store delete instructions
                if (hasReplaces || hasDeletions || hasDeleteAll)
                {
                    await StoreDeleteInstructions(
                        referencedStructure,
                        reportedComponents,
                        dataSetAttributeRows,
                        tableVersion,
                        dateTime,
                        cancellationToken
                    );
                }

                //Merge action cannot set components to NULL, no cleanup needed
                //Merge & Replace actions cannot set components to NULL at series not dataset level, no cleanup needed
                if (hasReplaces || hasDeletions)
                {
                    //Clean up fully deleted observations
                    var observationsRemoved = await CleanUpFullyEmptyObservations(referencedStructure, tableVersion, cancellationToken);
                    importSummary.RequiresRecalculationOfActualContentConstraint = importSummary.RequiresRecalculationOfActualContentConstraint || observationsRemoved;
                }

                scope.Complete();
            }

            return importSummary;
        }

        public async ValueTask MergeStagingToDimensionsTable(IImportReferenceableStructure referencedStructure, ReportedComponents reportedComponents,
            CancellationToken cancellationToken)
        {
            if(referencedStructure is Dsd dsd)
                await MergeDsdStagingToDimensionsTable(dsd, reportedComponents, cancellationToken);
            else
                await MergeDsdStagingToDimensionsTable((referencedStructure as Dataflow).Dsd, reportedComponents, cancellationToken);
        }

        public abstract ValueTask<MergeResult> MergeStagingToObservationsTable(IImportReferenceableStructure referencedStructure, ReportedComponents reportedComponents, DbTableVersion tableVersion, BatchAction batchAction, bool includeSummary, ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken);

        public abstract ValueTask<MergeResult> MergeStagingToSeriesTable(IImportReferenceableStructure referencedStructure, ReportedComponents reportedComponents, DbTableVersion tableVersion, BatchAction batchAction, bool includeSummary, ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken);

        public abstract ValueTask<MergeResult> MergeStagingToDatasetTable(IImportReferenceableStructure referencedStructure, IList<IKeyValue> attributes, CodeTranslator translator, DbTableVersion tableVersion, StagingRowActionEnum action, bool includeSummary, DateTime dateTime, CancellationToken cancellationToken);

        protected abstract ValueTask MergeDsdStagingToDimensionsTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken);

        protected abstract Task DeleteAll(IImportReferenceableStructure referencedStructure, DbTableVersion tableVersion, bool includeSummary, ImportSummary importSummary, ICodeTranslator codeTranslator, DateTime dateTime, CancellationToken cancellationToken);

        protected abstract Task StoreDeleteInstructions(IImportReferenceableStructure referencedStructure, ReportedComponents reportedComponents, IList<DataSetAttributeRow> dataSetAttributeRows, DbTableVersion tableVersion, DateTime dateTime, CancellationToken cancellationToken);

        protected abstract Task<bool> CleanUpFullyEmptyObservations(IImportReferenceableStructure referencedStructure, DbTableVersion tableVersion, CancellationToken cancellationToken);
     }
}
