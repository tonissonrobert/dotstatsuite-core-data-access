﻿using System;
using System.Reflection;
using System.Transactions;
using DotStat.Db.DB;
using DotStat.Db.Repository;

namespace DotStat.DB.Repository
{
    public delegate IUnitOfWork UnitOfWorkResolver(string dataSpace);

    public class UnitOfWork : IUnitOfWork
    {
        static UnitOfWork()
        {
            // Remove System MaxTimeout from Transaction, so transaction scope will use either DefaultTimeout or transaction scope object's Timeout if specified without limitation of 10 min
            DisableGlobalTransactionTimeout();
        }

        /// <summary>
        /// TransactionManager.MaxTimeout should become assignable in the next version of .NET 7
        /// </summary>
        /// https://github.com/dotnet/runtime/blob/release/6.0/src/libraries/System.Transactions.Local/src/System/Transactions/TransactionManager.cs#L351
        /// https://github.com/dotnet/runtime/issues/1418#issuecomment-1130202520
        static void DisableGlobalTransactionTimeout()
        {
            SetTransactionManagerField("s_cachedMaxTimeout", true);
            SetTransactionManagerField("s_maximumTimeout", TimeSpan.Zero);

            void SetTransactionManagerField(string fieldName, object value) =>
                typeof(TransactionManager)
                    .GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Static)?
                    .SetValue(null, value);
        }

        public string DataSpace { get; }

        public int MaxDataImportTimeInMinutes { get; }

        public int DatabaseCommandTimeout { get; }

        public IDotStatDb DotStatDb { get; }

        public IArtefactRepository ArtefactRepository { get; }

        public IAttributeRepository AttributeRepository { get; }

        public IDataStoreRepository DataStoreRepository { get; }

        public IMetadataComponentRepository MetaMetadataComponentRepository { get; }

        public IMetadataStoreRepository MetadataStoreRepository { get; }

        public IObservationRepository ObservationRepository { get; }

        public ITransactionRepository TransactionRepository { get; }

        public IComponentRepository ComponentRepository { get; }

        public ICodelistRepository CodeListRepository { get; }
        
        public UnitOfWork(
            IDotStatDb dotStatDb,
            IArtefactRepository artefactRepository,
            IAttributeRepository attributeRepository,
            IDataStoreRepository dataStoreRepository,
            IMetadataStoreRepository metadataStoreRepository,
            IMetadataComponentRepository metaMetadataComponentRepository,
            IObservationRepository observationRepository,
            ITransactionRepository transactionRepository,
            IComponentRepository componentRepository,
            ICodelistRepository codeListRepository)
        {
            DataSpace = dotStatDb.Id;
            MaxDataImportTimeInMinutes = dotStatDb.DataSpace.DataImportTimeOutInMinutes;
            DatabaseCommandTimeout = dotStatDb.DataSpace.DatabaseCommandTimeoutInSec;

            DotStatDb = dotStatDb;
            ArtefactRepository = artefactRepository;
            AttributeRepository = attributeRepository;
            DataStoreRepository = dataStoreRepository;
            MetaMetadataComponentRepository = metaMetadataComponentRepository;
            MetadataStoreRepository = metadataStoreRepository;
            ObservationRepository = observationRepository;
            TransactionRepository = transactionRepository;
            ComponentRepository = componentRepository;
            CodeListRepository = codeListRepository;
        }
    }
}
