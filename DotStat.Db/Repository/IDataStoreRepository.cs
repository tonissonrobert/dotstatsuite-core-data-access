﻿using System.Collections.Generic;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Domain;
using System.Threading;
using System.Threading.Tasks;
using DotStat.DB.Repository;

namespace DotStat.Db.Repository
{
    public interface IDataStoreRepository : IDatabaseRepository, IDataMerger
    {
        Task<BulkImportResult> BulkInsertData(IAsyncEnumerable<ObservationRow> observations,
            ReportedComponents reportedComponents,
            CodeTranslator translator,
            Dataflow dataflow,
            bool fullValidation,
            bool isTimeAtTimeDimensionSupported,
            bool isXMLSource,
            CancellationToken cancellationToken);

        Task RecreateStagingTables(Dsd dsd, ReportedComponents reportedComponents, bool isTimeAtTimeDimensionSupported, CancellationToken cancellationToken);
        Task DropStagingTables(Dsd dsd, CancellationToken cancellationToken);
        Task DropStagingTables(int dsdDbId, CancellationToken cancellationToken);
        Task AddIndexStagingTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken);
        Task AddUniqueIndexStagingTable(Dsd dsd, ReportedComponents reportedComponents, CancellationToken cancellationToken);
        Task DeleteData(IImportReferenceableStructure referencedStructure, DbTableVersion tableVersion, CancellationToken cancellationToken);

        Task CopyDataToNewVersion(Dsd dsd, DbTableVersion sourceDbTableVersion, DbTableVersion targetDbTableVersion, CancellationToken cancellationToken);

        ValueTask CopyAttributesToNewVersion(Dsd dsd, DbTableVersion sourceDbTableVersion, DbTableVersion targetDbTableVersion, CancellationToken cancellationToken);
      
        Task UpdateStatisticsOfFactTable(Dsd dsd, DbTableVersion tableVersion, CancellationToken cancellationToken);
        Task UpdateStatisticsOfFilterTable(Dsd dsd, CancellationToken cancellationToken);

        Task DropDsdTables(Dsd dsd, CancellationToken cancellationToken);

        Task DropAttributeTables(Dsd dsd, CancellationToken cancellationToken);
        Task<bool> DataTablesExist(Dsd dsd, CancellationToken cancellationToken);
        Task ReorganizeColumnStoreIndexes(Dsd dsd, char targetVersion, CancellationToken cancellationToken);
        MappingSetParams GetMappingSetParams(Dataflow dataflow, char? tableVersion);
    }
}
