﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.Exception;
using DotStat.Db.Util;
using DotStat.Db.Validation;
using DotStat.DB.Reader;
using DotStat.DB.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DotStat.Db.Reader
{
    public sealed class SdmxMetadataObservationReader : BulkCopyDataReader
    {
        private readonly IAsyncEnumerator<ObservationRow> _enumerator;
        private readonly object[] _values;
        private bool Eof { get; set; }
        private ObservationRow CurrentRecord { get; set; }
        public int CurrentIndex { get; private set; }
        private int _errorCount;
        private int _batchCount = 1;
        private Dictionary<int, BatchAction> _batchActions = new();

        private Dsd Dsd { get; }

        private readonly DsdMetadataBuilder _metadata;
        private readonly ICodeTranslator _codeTranslator;
        private readonly MetadataValidator _metadataValidator;
        private readonly IGeneralConfiguration _configuration;
        private readonly ReportedComponents _reportedComponents;
        private int _attributesCount = 0;
        private readonly bool _supportsDateTime;

        public Dictionary<int, DataSetAttributeRow> DatasetMetadataAttributes { get; set; }

        public SdmxMetadataObservationReader(IAsyncEnumerable<ObservationRow> observations, ReportedComponents reportedComponents, Dsd dsd, ICodeTranslator codeTranslator, IGeneralConfiguration configuration, bool fullValidation, bool isTimeAtTimeDimensionSupported)
        {
            _enumerator = observations?.GetAsyncEnumerator();
            _codeTranslator = codeTranslator;

            DatasetMetadataAttributes = new Dictionary<int, DataSetAttributeRow>();

            Dsd = dsd;
            _attributesCount = Dsd.Msd.MetadataAttributes.Count;
            _configuration = configuration;
            _metadata = new DsdMetadataBuilder(dsd, reportedComponents);
            _metadataValidator = new MetadataValidator(dsd, configuration, fullValidation, isTimeAtTimeDimensionSupported, _metadata);
            _reportedComponents = reportedComponents;
            _values = new object[FieldCount];
            CurrentIndex = 0;
            _errorCount = 0;
            _supportsDateTime = Dsd.Base.HasSupportDateTimeAnnotation();
        }

        public override int FieldCount => _metadata.DimensionCount + //Only reported dimensions
                                          _attributesCount + // all attributes
                                          1 + // Requested Action
                                          1 + // Real Action
                                          1;  // Batch number  

        public override object GetValue(int i) => _values[i];

        public override bool Read()
        {
            if (Eof)
            {
                return false;
            }

            do
            {
                var isValid = false;

                try
                {
                    if (_enumerator.MoveNextAsync().AsTask().Result)
                    {
                        CurrentRecord = _enumerator.Current;
                        var action = CurrentRecord.Action;

                        var isObsNull = _enumerator.Current.Observation is null;
                        var allDimensionsOmitted = isObsNull ||
                            string.IsNullOrEmpty(CurrentRecord.Observation.ObsTime) && CurrentRecord.Observation.SeriesKey.Key.All(x => string.IsNullOrEmpty(x.Code));
                        
                        if (IsDeleteAll(isObsNull, allDimensionsOmitted))
                        {
                            action = StagingRowActionEnum.DeleteAll;
                        }

                        if (!_batchActions.TryGetValue(CurrentRecord.BatchNumber, out _))
                        {
                            _batchActions.TryAdd(CurrentRecord.BatchNumber, new BatchAction(CurrentRecord.BatchNumber, action, CurrentRecord.IsWildCarded));
                        }

                        if (action == StagingRowActionEnum.DeleteAll)
                        {
                            continue;
                        }

                        CurrentIndex += 1;
                        isValid = _metadataValidator.ValidateObservation(CurrentRecord.Observation, CurrentRecord.BatchNumber, CurrentIndex, action);

                        var allDimensionsSwitchedOff = isObsNull ||
                                                       (!string.IsNullOrEmpty(CurrentRecord.Observation.ObsTime) && CurrentRecord.Observation.ObsTime.Equals(DbExtensions.DimensionSwitchedOff) && CurrentRecord.Observation.SeriesKey.Key.All(x => x.Code.Equals(DbExtensions.DimensionSwitchedOff)));

                        if (allDimensionsSwitchedOff || allDimensionsOmitted)
                        { 
                            AddDsdLevelMetadata();
                        }

                        if (CurrentRecord.Action == StagingRowActionEnum.Merge && (allDimensionsSwitchedOff || allDimensionsOmitted))
                        {
                            continue;
                        }

                        if (CurrentRecord.Action is StagingRowActionEnum.Delete or StagingRowActionEnum.Replace && allDimensionsSwitchedOff)
                        {
                            continue;
                        }
                    }
                    else
                    {
                        Eof = true;
                        return false;
                    }
                }
                catch (ObservationReadException e)
                {
                    _metadataValidator.AddError(e.Error, CurrentIndex);
                }
                // Unrecoverable error while reading, we stop here
                catch (System.Exception e)
                {
                    _metadataValidator.AddError(ValidationErrorType.Undefined, CurrentIndex, null, e.Message);
                }

                if (isValid && _errorCount == 0)
                {
                    break;
                }

                if (!isValid && _configuration.MaxTransferErrorAmount > 0 && ++_errorCount >= _configuration.MaxTransferErrorAmount)
                {
                    Eof = true;
                    return false;
                }
            }
            while (true);

            Fill(_values);

            return !Eof;
        }

        public override bool IsClosed => Eof;

        public override string GetName(int i)
        {
            if (i < 0)
                return string.Empty;

            var dimensionOffset = 0;

            if (_reportedComponents.Dimensions.Count > 32)
            {
                // When name of ROW_ID column is requested
                if (i == 0)
                    return "ROW_ID";

                dimensionOffset = 1;
            }

            if (_metadata.HasTime)
            {
                // Period Start or End columns requested
                if (i < 2 + dimensionOffset)
                    return i == 0 + dimensionOffset ? DbExtensions.SqlPeriodStart() : DbExtensions.SqlPeriodEnd();

                dimensionOffset += 2;
            }

            var dimensions = _metadata.Dims.ToArray();

            // Dimension ID requested
            if (i < dimensions.Length + dimensionOffset)
            {
                var d = dimensions.ElementAtOrDefault(i - dimensionOffset);

                return d?.Dimension.Code ?? string.Empty;
            }

            var metaAttributeOffset = dimensionOffset + dimensions.Length;
            var metaAttributes = _metadata.MetaAttrs.ToArray();

            // ID of meta attribute requested
            if (metaAttributes.Length != 0 && (i - metaAttributeOffset) < metaAttributes.Length)
            {
                var ma = metaAttributes.ElementAtOrDefault(i - metaAttributeOffset);

                return ma?.MetadataAttribute.Code ?? string.Empty;
            }

            // Any of the additional column IDs requested
            if (i == metaAttributeOffset + metaAttributes.Length)
                return "REQUESTED_ACTION";
            if (i == metaAttributeOffset + metaAttributes.Length + 1)
                return "REAL_ACTION_FACT";
            if (i == metaAttributeOffset + metaAttributes.Length + 2)
                return "BATCH_NUMBER";

            // Requested ID is out of bounds
            return string.Empty;
        }

        private void Fill(object[] values)
        {
            Array.Clear(_values, 0, values.Length);

            if (CurrentRecord.BatchNumber > _batchCount)
                _batchCount = CurrentRecord.BatchNumber;

            SetTimeDimensionValues(values);
            SetDimensionValues(values);

            var currentAttributeList = CurrentRecord.Observation.Attributes.Where(a =>
                _reportedComponents.MetadataAttributes.Any(r =>
                    r.HierarchicalId.Equals(a.Concept, StringComparison.InvariantCultureIgnoreCase))).ToList();

            SetMetadataAttributeValues(values, currentAttributeList);

            values[FieldCount - 3] = (int)CurrentRecord.Action;

            StagingRowActionEnum realAction;
            switch (CurrentRecord.Action)
            {
                case StagingRowActionEnum.Delete when !currentAttributeList.Any() ||
                                                      (currentAttributeList.Count == Dsd.Msd.MetadataAttributes.Count &&
                                                       (currentAttributeList.All(x => string.IsNullOrEmpty(x.Code)) || currentAttributeList.All(x => !string.IsNullOrEmpty(x.Code)))):
                    realAction = StagingRowActionEnum.Delete;
                    break;
                case StagingRowActionEnum.Merge when currentAttributeList.All(x => string.IsNullOrEmpty(x.Code)):
                    realAction = StagingRowActionEnum.Skip;
                    break;
                case StagingRowActionEnum.Replace:
                    realAction = StagingRowActionEnum.Replace;
                    break;
                default:
                    realAction = StagingRowActionEnum.Merge;
                    break;
            }

            values[FieldCount - 2] = (int)realAction;
            values[FieldCount - 1] = CurrentRecord.BatchNumber;
        }

        private void SetMetadataAttributeValues(IList<object> values, List<IKeyValue> currentAttributeList)
        {
            var currentIndex = Dsd.Dimensions.Count + (_reportedComponents.TimeDimension is not null ? 2 : 0);

            // metadata attribute values
            foreach (var attr in currentAttributeList)
            {
                var attrMeta = _metadata.MetaAttr(attr.Concept);

                if (string.IsNullOrEmpty(attr.Code))
                {
                    values[currentIndex + attrMeta.Index] = null;
                }
                else
                {
                    values[currentIndex + attrMeta.Index] = CurrentRecord.Action == StagingRowActionEnum.Delete
                        ? DbExtensions.ColumnPresentDbValue.ToString()
                        : attr.Code;
                }
            }
        }

        private void SetDimensionValues(IList<object> values)
        {
            foreach (var dim in Dsd.Dimensions.Where(dim => !dim.Base.TimeDimension))
            {
                var key = CurrentRecord.Observation.SeriesKey.Key.FirstOrDefault(k => k.Concept.Equals(dim.Code, StringComparison.InvariantCultureIgnoreCase));

                if (key == null || string.IsNullOrEmpty(key.Code))
                {
                    // Missing or empty -> null (anything including NULL)
                    values[_metadata[dim.Code].Index] = null;
                }
                else if (dim.Codelist != null && key.Code.Equals(DbExtensions.DimensionWildCarded, StringComparison.InvariantCultureIgnoreCase))
                {
                    // * -> NOT NULL
                    values[_metadata[dim.Code].Index] = DbExtensions.DimensionWildCardedDbValue;
                }
                else if (dim.Codelist != null && key.Code.Equals(DbExtensions.DimensionSwitchedOff, StringComparison.InvariantCultureIgnoreCase))
                {
                    // - -> only NULL
                    values[_metadata[dim.Code].Index] = DbExtensions.DimensionSwitchedOffDbValue;
                }
                else
                {
                    // Actual dim value
                    values[_metadata[dim.Code].Index] = dim.Codelist != null
                        ? _codeTranslator.TranslateCodeToId(key.Concept, key.Code)
                        : key.Code;
                }
            }
        }

        private void SetTimeDimensionValues(IList<object> values)
        {
            if (_metadata.TimeDim == null)
            {
                return;
            }

            if (string.IsNullOrEmpty(CurrentRecord.Observation.ObsTime))
            {
                // Missing or empty -> anything including NULL
                values[_metadata[_metadata.TimeDim.Code].Index] = null;
                values[_metadata[_metadata.TimeDim.Code].Index - 2] =
                    CurrentRecord.Action is StagingRowActionEnum.Delete or StagingRowActionEnum.Replace
                        ? null
                        : DateTime.MaxValue.Date;
                values[_metadata[_metadata.TimeDim.Code].Index - 1] =
                    CurrentRecord.Action is StagingRowActionEnum.Delete or StagingRowActionEnum.Replace
                        ? null
                        : DateTime.MinValue.Date;
            }
            else if (CurrentRecord.Observation.ObsTime.Equals(DbExtensions.DimensionWildCarded, StringComparison.InvariantCultureIgnoreCase))
            {
                // * -> NOT NULL
                values[_metadata[_metadata.TimeDim.Code].Index] = null;
                values[_metadata[_metadata.TimeDim.Code].Index - 2] = _supportsDateTime
                    ? DateTime.MaxValue.AddYears(-1)
                    : DateTime.MaxValue.Date.AddYears(-1);
                values[_metadata[_metadata.TimeDim.Code].Index - 1] = _supportsDateTime
                    ? DateTime.MinValue.AddYears(1)
                    : DateTime.MinValue.Date.AddYears(1);
            }
            else if (CurrentRecord.Observation.ObsTime.Equals(DbExtensions.DimensionSwitchedOff, StringComparison.InvariantCultureIgnoreCase))
            {
                // - -> only NULL
                values[_metadata[_metadata.TimeDim.Code].Index] = null;
                values[_metadata[_metadata.TimeDim.Code].Index - 2] = _supportsDateTime
                    ? DateTime.MaxValue 
                    : DateTime.MaxValue.Date;
                values[_metadata[_metadata.TimeDim.Code].Index - 1] = _supportsDateTime
                    ? DateTime.MinValue 
                    : DateTime.MinValue.Date;
            }
            else
            {
                // Actual dim value
                var reportingYearStartDateString = _metadata.HasReportingYearStartDayAttr ? CurrentRecord.Observation.GetAttribute(AttributeObject.Repyearstart)?.Code : null;
                var key = $"{CurrentRecord.Observation.ObsTime}-{reportingYearStartDateString}";

                _metadataValidator.ValidatedTimeValues.TryGetValue(key, out var timeDimExtendedProperties);

                values[_metadata[_metadata.TimeDim.Code].Index] = CurrentRecord.Observation.ObsTime;
                values[_metadata[_metadata.TimeDim.Code].Index - 2] = _supportsDateTime ? timeDimExtendedProperties.PeriodStart : timeDimExtendedProperties.PeriodStart.Date;
                values[_metadata[_metadata.TimeDim.Code].Index - 1] = _supportsDateTime ? timeDimExtendedProperties.PeriodEnd : timeDimExtendedProperties.PeriodEnd.Date;
            }
        }

        private void AddDsdLevelMetadata()
        {
            if (!CurrentRecord.Observation.Attributes.Any())
            {
                foreach (var metadataAttribute in _reportedComponents.MetadataAttributes)
                {
                    var specialValue = CurrentRecord.Action == StagingRowActionEnum.Delete ?
                        DbExtensions.ColumnPresentDbValue.ToString() : null;
                    AddDatasetMetadataAttribute(new KeyValueImpl(specialValue, metadataAttribute.HierarchicalId));
                }

                return;
            }

            foreach (var attr in CurrentRecord.Observation.Attributes.Where(a => _reportedComponents.MetadataAttributes.Any(r =>
                         r.HierarchicalId.Equals(a.Concept, StringComparison.InvariantCultureIgnoreCase))))
            {
                AddDatasetMetadataAttribute(attr);
            }
        }

        private void AddDatasetMetadataAttribute(IKeyValue attr)
        {
            if (!DatasetMetadataAttributes.TryGetValue(CurrentRecord.BatchNumber, out var dataSetAttributeRow))
            {
                DatasetMetadataAttributes.Add(
                    CurrentRecord.BatchNumber, new DataSetAttributeRow(CurrentRecord.BatchNumber, CurrentRecord.Action) { Attributes = new List<IKeyValue> { attr } });
            }
            else
            {
                var existingAttribute = dataSetAttributeRow.Attributes.FirstOrDefault(a =>
                    a.Concept.Equals(attr.Concept, StringComparison.CurrentCultureIgnoreCase));

                if (existingAttribute == null)
                {
                    DatasetMetadataAttributes[CurrentRecord.BatchNumber].Attributes.Add(attr);
                }
                else if (existingAttribute.Code != attr.Code)
                {
                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MultipleValuesForDatasetAttribute),
                        existingAttribute.Code, attr.Code, attr.Concept));
                }
            }
        }

        public List<IValidationError> GetErrors() => _metadataValidator.GetErrors();

        public int GetObservationsCount() => CurrentIndex;
        public Dictionary<int, BatchAction> GetBatchActions() => _batchActions;

        public override async void Close()
        {
            await _enumerator.DisposeAsync();
        }


        private bool IsDeleteAll(bool isObsNull, bool allDimensionsOmitted)
        {
            switch (CurrentRecord.Action)
            {
                //Special action Delete all operationif
                case StagingRowActionEnum.DeleteAll:
                    return true;
                //When either all components are omitted or all are present and all dimensions are omitted
                // All dimensions omited
                case StagingRowActionEnum.Delete when allDimensionsOmitted:
                {
                    var allComponentsOmitted = isObsNull ||
                                               CurrentRecord.Observation.Attributes.Count == 0 //No attributes reported
                                               || _metadata.Dsd.Msd.MetadataAttributes.All(attribute =>
                                                   CurrentRecord.Observation.Attributes
                                                       .Where(a => string.IsNullOrWhiteSpace(a.Code)) //All attributes present
                                                       .Any(a => a.Concept.Equals(attribute.HierarchicalId, StringComparison.CurrentCultureIgnoreCase)));

                    if (allComponentsOmitted)
                    {
                        return true;
                    }
                    
                    var allComponentsPresent = isObsNull ||
                                               (CurrentRecord.Observation.Attributes.Count ==
                                                _metadata.Dsd.Msd.MetadataAttributes.Count //All attributes reported
                                                && _metadata.Dsd.Msd.MetadataAttributes.All(attribute =>
                                                    CurrentRecord.Observation.Attributes.Where(a => !string.IsNullOrWhiteSpace(a.Code)) //All attributes omited
                                                        .Any(a => a.Concept.Equals(attribute.HierarchicalId, StringComparison.CurrentCultureIgnoreCase))));
                    if (allComponentsPresent)
                    {
                        return true;
                    }


                    break;
                }
                default:
                    return false;
            }

            return false;
        }
    }
}