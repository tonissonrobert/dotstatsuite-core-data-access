﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db.Exception;
using DotStat.Db.Util;
using DotStat.Db.Validation;
using DotStat.DB.Reader;
using DotStat.DB.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace DotStat.Db.Reader
{
    public sealed class SdmxObservationReader : BulkCopyDataReader
    {
        private readonly IAsyncEnumerator<ObservationRow> _enumerator;
        private readonly object[] _values;
        private readonly bool[] _observationLvlComponentsPresent;
        private readonly bool[] _seriesLvlComponentsPresent;
        private readonly int _observationLvlComponentsCount;
        private readonly int _seriesLvlComponentsCount;

        private bool Eof { get; set; }
        private ObservationRow CurrentRecord { get; set; }
        public int CurrentIndex { get; private set; }
        private int _errorCount;
        private int _batchCount = 1;
        private Dictionary<int, BatchAction> _batchActions = new();

        private Dataflow Dataflow { get; }

        private readonly DsdMetadataBuilder _metadata;
        private readonly CodeTranslator _codeTranslator;
        private readonly ObservationValidator _observationValidator;
        private readonly IGeneralConfiguration _configuration;
        private readonly ReportedComponents _reportedComponents;
        private bool _allComponentsOmitted;

        private readonly bool _isXmlSource;

        public Dictionary<int, DataSetAttributeRow> DataSetAttributes { get; set; }

        public SdmxObservationReader(IAsyncEnumerable<ObservationRow> observationRows,
                                     ReportedComponents reportedComponents,
                                     Dataflow dataflow,
                                     CodeTranslator codeTranslator,
                                     IGeneralConfiguration configuration,
                                     bool fullValidation,
                                     bool isTimeAtTimeDimensionSupported,
                                     bool isXmlSource)
        {
            _enumerator = observationRows?.GetAsyncEnumerator();

            _codeTranslator = codeTranslator;
            Dataflow = dataflow;
            _configuration = configuration;
            _isXmlSource = isXmlSource;

            DataSetAttributes = new Dictionary<int, DataSetAttributeRow>();

            _metadata = new DsdMetadataBuilder(dataflow.Dsd, reportedComponents);
            _observationValidator = new ObservationValidator(dataflow, reportedComponents, configuration, fullValidation, isTimeAtTimeDimensionSupported, _metadata);
            _reportedComponents = reportedComponents;
            _errorCount = 0;

            CurrentIndex = 0;

            var observationLvlComponents = dataflow.Dsd.Attributes.Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                                                                               || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();
            
            _observationLvlComponentsCount = observationLvlComponents.Count + 1;//Primary measure
            _observationLvlComponentsPresent = new bool[_observationLvlComponentsCount];

            var seriesLvlComponents = dataflow.Dsd.Attributes
                    .Where(a => 
                        (a.Base.AttachmentLevel is AttributeAttachmentLevel.Group or AttributeAttachmentLevel.DimensionGroup) &&
                        !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)
                     ).ToList();
            _seriesLvlComponentsCount = seriesLvlComponents.Count;
            _seriesLvlComponentsPresent = new bool[_seriesLvlComponentsCount];

            _values = new object[FieldCount];
        }

        #region BulkCopyDataReader

        public override int FieldCount => _metadata.DimensionCount + // only reported dimensions
                                          Dataflow.Dsd.Attributes.Count + // attributes 
                                          (_reportedComponents.IsPrimaryMeasureReported ? 1 : 0) + // Measure 
                                          1 + //Requested Action Column 
                                          1 + // Real Action Fact
                                          1 + // Real Action Attr
                                          1;  // Batch number  

        public override object GetValue(int i) => _values[i];

        public override bool Read()
        {
            if (Eof)
                return false;

            do
            {
                var isValid = false;
                try
                {
                    if (_enumerator.MoveNextAsync().AsTask().Result)
                    {
                        CurrentRecord = _enumerator.Current;
                        CurrentIndex++;
                        var action = CurrentRecord.Action;

                        if (CurrentRecord.Action == StagingRowActionEnum.Delete)
                        {
                            _allComponentsOmitted = (CurrentRecord.Observation.Attributes.Count == 0 //No attributes reported
                                                     || !_isXmlSource //In XML all are omitted only if none of them is present, null attribute value means intentionally missing
                                                     && CurrentRecord.Observation.Attributes.All(a => string.IsNullOrWhiteSpace(a.Code))) //All attributes omitted
                                                    && string.IsNullOrWhiteSpace(_enumerator.Current.Observation.ObservationValue); // Observation value omitted
                        }
                        else
                        {
                            _allComponentsOmitted = false;
                        }
                        
                        if (IsDeleteAll())
                        {
                            action = StagingRowActionEnum.DeleteAll;
                        }

                        if (!_batchActions.TryGetValue(CurrentRecord.BatchNumber, out _))
                        {
                            _batchActions.TryAdd(CurrentRecord.BatchNumber, new BatchAction(CurrentRecord.BatchNumber, action, CurrentRecord.IsWildCarded));
                        }

                        if (action == StagingRowActionEnum.DeleteAll)
                            continue;
                        
                        AddDataFlowAttributes();

                        isValid = _observationValidator.ValidateObservation(CurrentRecord.Observation, CurrentRecord.BatchNumber, CurrentIndex, _enumerator.Current.Action);
                    }
                    else
                    {
                        Eof = true;
                        return false;
                    }
                }
                catch (ObservationReadException e)
                {
                    _observationValidator.AddError(e.Error, CurrentIndex);
                }
                // Unrecoverable error while reading, we stop here
                catch (System.Exception e)
                {
                    _observationValidator.AddError(ValidationErrorType.Undefined, CurrentIndex, null, e.Message);
                }

                if (isValid && _errorCount == 0)
                {
                    break;
                }

                if (!isValid && _configuration.MaxTransferErrorAmount > 0 && ++_errorCount >= _configuration.MaxTransferErrorAmount)
                {
                    Eof = true;
                    return false;
                }
            }
            while (true);

            Fill(_values);

            return !Eof;
        }

        public override bool IsClosed => Eof;

        #endregion

        private void Fill(object[] values)
        {
            Array.Clear(_values, 0, values.Length);
            Array.Clear(_observationLvlComponentsPresent, 0, _observationLvlComponentsCount);
            Array.Clear(_seriesLvlComponentsPresent, 0, _seriesLvlComponentsCount);

            if (CurrentRecord.BatchNumber > _batchCount)
                _batchCount = CurrentRecord.BatchNumber;

            //Dimensions
            var reportedDimensions = new List<Dimension>();
            var timeDimReported = false;
            //time dimension, index - 0
            if (_reportedComponents.TimeDimension is not null && !string.IsNullOrEmpty(CurrentRecord.Observation.ObsTime))
            {
                timeDimReported = true;
                reportedDimensions.Add(_metadata.TimeDim);

                var reportingYearStartDateString = _metadata.HasReportingYearStartDayAttr ? CurrentRecord.Observation.GetAttribute(AttributeObject.Repyearstart)?.Code : null;
                var key = $"{CurrentRecord.Observation.ObsTime}-{reportingYearStartDateString}";

                _observationValidator.ValidTimeValues.TryGetValue(key, out var timeDimExtendedProperties);

                values[_metadata[_metadata.TimeDim.Code].Index - 2] = timeDimExtendedProperties.PeriodStart;
                values[_metadata[_metadata.TimeDim.Code].Index - 1] = timeDimExtendedProperties.PeriodEnd;
                values[_metadata[_metadata.TimeDim.Code].Index] = CurrentRecord.Observation.ObsTime;
            }

            // dimensions should respect order of DSD, used on table creation
            var seriesLevelDimReported = false;
            foreach (var key in CurrentRecord.Observation.SeriesKey.Key)
            {
                if (_metadata.HasTime &&
                    key.Concept.Equals(_metadata.TimeDim.Code, StringComparison.CurrentCultureIgnoreCase))
                    continue;

                if (string.IsNullOrEmpty(key.Code)) continue;

                seriesLevelDimReported = true;  
                var dim = _metadata[key.Concept].Dimension;
                reportedDimensions.Add(dim);
                values[_metadata[key.Concept].Index] = dim.Codelist != null
                    ? _codeTranslator.TranslateCodeToId(key.Concept, key.Code)
                    : key.Code;
            }


            //Measure
            var obsLevelCount = 0;
            var strValue = CurrentRecord.Observation.ObservationValue;
            if (_allComponentsOmitted)
            {
                strValue = !reportedDimensions.Any() ? 
                    null : DbExtensions.ColumnPresentDbValue.ToString();
            }
            
            var obsValue = Dataflow.Dsd.GetPrimaryMeasureObjectFromString(strValue, CurrentRecord.Action, _reportedComponents.CultureInfo ?? CultureInfo.InvariantCulture);

            if (obsValue is not null)
            {
                _observationLvlComponentsPresent[obsLevelCount++] = true;
            }

            var ii = _metadata.DimensionCount;
            values[ii++] = obsValue;

            //Attributes
            var seriesLevelCount = 0;
            var hasDataSetLevelComponentsPresent = false;
            var attributes = GetAttributes(timeDimReported, seriesLevelDimReported, reportedDimensions.Any());
            foreach (var attr in attributes)
            {
                var attrMeta = _metadata.Attr(attr.Concept);
                var attrValue = attrMeta.Attribute.GetObjectFromString(attr.Code, _codeTranslator, CurrentRecord.Action, _isXmlSource);
                
                //DataSet Level attributes
                if (attrMeta.Attribute.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null)
                {
                    //skip omitted attributes 
                    if (attrValue is null)
                        continue;

                    hasDataSetLevelComponentsPresent = true;

                    //Store delete actions in staging table
                    if (CurrentRecord.Action == StagingRowActionEnum.Delete)
                    {
                        values[ii + attrMeta.Index] = attrValue;
                    } 
                }
                else
                {
                    //skip omitted attributes 
                    if (attrValue is null)
                        continue;

                    values[ii + attrMeta.Index] = attrValue;

                    //Keep track of the components that are present to calculate the real action
                    if (attrMeta.Attribute.Base.AttachmentLevel is AttributeAttachmentLevel.Observation ||
                        _metadata.DimensionsReferenceGroups[attr.Concept].Contains(DimensionObject.TimeDimensionFixedId))
                    {
                        _observationLvlComponentsPresent[obsLevelCount++] = true;
                    }
                    else
                    {
                        _seriesLvlComponentsPresent[seriesLevelCount++] = true;
                    }
                }
            }
           
            // --------------------------------------------------------------

            // Requested Action
            values[FieldCount - 4] = (int)CurrentRecord.Action;

            // Real Action observation level (stored in the fact table)
            var observationLevelRealAction = CurrentRecord.Action switch
            {
                //No observation level components present -> skip merge
                StagingRowActionEnum.Merge when _observationLvlComponentsPresent.All(a => !a) => StagingRowActionEnum.Skip,

                //No observation level components present and is not a delete all -> skip delete
                StagingRowActionEnum.Delete when _observationLvlComponentsPresent.All(a => !a) &&
                     (hasDataSetLevelComponentsPresent || _seriesLvlComponentsPresent.Any(a => a)) => StagingRowActionEnum.Skip,
                
                //Not all observation level components present then cannot physically delete all the coordinate (update to null the present components)
                StagingRowActionEnum.Delete when _observationLvlComponentsPresent.Distinct().Count() != 1 => StagingRowActionEnum.Merge,

                _ => CurrentRecord.Action
            };

            values[FieldCount - 3] = (int)observationLevelRealAction;

            // Real Action series level (stored in attr table) 
            var seriesLevelRealAction = CurrentRecord.Action switch
            {
                //No series level components present -> skip
                StagingRowActionEnum.Merge or StagingRowActionEnum.Replace
                    when _seriesLvlComponentsPresent.All(a => !a) => StagingRowActionEnum.Skip,

                //Replace behaves as merge in higher level attributes
                StagingRowActionEnum.Replace => StagingRowActionEnum.Merge,

                //No group level components present and is not a delete all -> skip delete
                StagingRowActionEnum.Delete when _seriesLvlComponentsPresent.All(a => !a) &&
                     (hasDataSetLevelComponentsPresent || _observationLvlComponentsPresent.Any(a => a)) => StagingRowActionEnum.Skip,
                
                //Not all group level components present then cannot physically delete all the coordinate (update to null the present components)
                StagingRowActionEnum.Delete when _seriesLvlComponentsPresent.Distinct().Count() != 1 => StagingRowActionEnum.Merge,
                
                _ => CurrentRecord.Action
            };

            values[FieldCount - 2] = (int)seriesLevelRealAction;
            values[FieldCount - 1] = CurrentRecord.BatchNumber;
        }

        public List<IValidationError> GetErrors() => _observationValidator.GetErrors();

        public int GetObservationsCount() => CurrentIndex;
        public Dictionary<int, BatchAction> GetBatchActions() => _batchActions;

        public override async void Close()
        {
            await _enumerator.DisposeAsync();
        }

        public override string GetName(int i)
        {
            if (i < 0)
                return string.Empty;

            var dimensionOffset = 0;

            if (_reportedComponents.Dimensions.Count > 32)
            {
                // When name of ROW_ID column is requested
                if (i == 0)
                    return "ROW_ID"; 

                dimensionOffset = 1;
            }

            if (_metadata.HasTime)
            {
                // Period Start or End columns requested
                if (i < 2 + dimensionOffset)
                    return i == 0 + dimensionOffset ? DbExtensions.SqlPeriodStart() : DbExtensions.SqlPeriodEnd();

                dimensionOffset += 2;
            }

            var dimensions = _metadata.Dims.ToArray();

            // Dimension ID requested
            if (i < dimensions.Length + dimensionOffset)
            {
                var d = dimensions.ElementAtOrDefault(i - dimensionOffset);

                return d?.Dimension.Code ?? string.Empty;
            }

            var attributeOffset = dimensionOffset + dimensions.Length;

            // ID of primary measure requested
            if (_reportedComponents.IsPrimaryMeasureReported)
            {
                if (i == dimensions.Length + dimensionOffset)
                    return "VALUE";

                attributeOffset++;
            }

            var attributes = _metadata.Attrs.ToArray();

            // ID of attribute requested
            if (attributes.Length != 0 && (i - attributeOffset) < attributes.Length)
            {
                var a = attributes.ElementAtOrDefault(i - attributeOffset);
                
                return a?.Attribute.Code ?? string.Empty;
            } 

            // Any of the additional column IDs requested
            if (i == attributeOffset + attributes.Length)
                return "REQUESTED_ACTION";
            if (i == attributeOffset + attributes.Length + 1)
                return "REAL_ACTION_FACT";
            if (i == attributeOffset + attributes.Length + 2)
                return "REAL_ACTION_ATTR";
            if (i == attributeOffset + attributes.Length + 3)
                return "BATCH_NUMBER";

            // Requested ID is out of bounds
            return string.Empty;
        }

        private void AddDataFlowAttributes()
        {
            foreach (var attr in CurrentRecord.Observation.Attributes)
            {
                var attrMeta = _metadata.Attr(attr.Concept);                
                //DataSet Level attributes
                if (attrMeta.Attribute.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null)
                {
                    TryAddDataFlowAttribute(attr);
                }
            }
        }

        private void TryAddDataFlowAttribute(IKeyValue attr)
        {
            if (!DataSetAttributes.TryGetValue(CurrentRecord.BatchNumber, out var dataSetAttributeRow))
            {
                DataSetAttributes.Add(
                    CurrentRecord.BatchNumber,
                    new DataSetAttributeRow(CurrentRecord.BatchNumber, CurrentRecord.Action) { Attributes = new List<IKeyValue> { attr } });
            }
            else
            {
                var existingAttribute = dataSetAttributeRow.Attributes.FirstOrDefault(a =>
                    a.Concept.Equals(attr.Concept, StringComparison.CurrentCultureIgnoreCase));
                if (existingAttribute == null)
                {
                    DataSetAttributes[CurrentRecord.BatchNumber].Attributes.Add(attr);
                }
                else if (!string.IsNullOrEmpty(existingAttribute.Code) &&
                         !string.IsNullOrEmpty(attr.Code) &&
                         !existingAttribute.Code.Equals(attr.Code, StringComparison.OrdinalIgnoreCase))
                {
                    throw new DotStatException(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MultipleValuesForDatasetAttribute),
                        existingAttribute.Code, attr.Code, attr.Concept));
                }
                else
                {
                    DataSetAttributes[CurrentRecord.BatchNumber].Action = CurrentRecord.Action;
                    DataSetAttributes[CurrentRecord.BatchNumber].Attributes = new List<IKeyValue>() { attr };
                }
            }
        }

        private bool IsDeleteAll()
        {
            switch (CurrentRecord.Action)
            {
                case StagingRowActionEnum.DeleteAll:
                //Special action Delete all operation
                //When either all components are omitted or all are present and all dimensions are omitted
                //All dimensions omitted
                //All omitted or all present
                // Observation value present
                case StagingRowActionEnum.Delete when
                    (_enumerator.Current.Observation is null ||
                     string.IsNullOrWhiteSpace(CurrentRecord.Observation.ObsTime) //Time dimension omitted
                     && CurrentRecord.Observation.SeriesKey.Key.All(a => string.IsNullOrWhiteSpace(a.Code))) && (_allComponentsOmitted ||
                        (_metadata.Dsd.Attributes.All(attribute =>
                             CurrentRecord.Observation.Attributes.Where(a => !string.IsNullOrWhiteSpace(a.Code))
                                 .Any(a => a.Concept.Equals(attribute.Code, StringComparison.CurrentCultureIgnoreCase))) //All attributes present
                         && !string.IsNullOrWhiteSpace(_enumerator.Current.Observation?.ObservationValue))):
                    return true;
                default:
                    return false;
            }
        }

        private List<IKeyValue> GetAttributes(bool timeDimReported, bool seriesLevelDimReported, bool hasReportedDimensions)
        {
            if(!_allComponentsOmitted)
                return CurrentRecord.Observation.Attributes.ToList();

            //Add present value for attributes referencing the present dimensions
            List<IKeyValue> attributesReferencingPresentDimensions = new();
            foreach (var attr in Dataflow.Dsd.Attributes)
            {
                var attrMeta = _metadata.Attr(attr.Code);
                var referencesTimeDim = _metadata.DimensionsReferenceGroups[attr.Code]
                    .Contains(DimensionObject.TimeDimensionFixedId);
                //Attributes stored at obs level
                if (hasReportedDimensions && (attrMeta.Attribute.Base.AttachmentLevel is AttributeAttachmentLevel.Observation || referencesTimeDim))
                {
                    attributesReferencingPresentDimensions.Add(new KeyValueImpl(DbExtensions.ColumnPresentDbValue.ToString(), attr.Code));
                }
                //Attributes stored at series level
                else if (seriesLevelDimReported && !timeDimReported && attrMeta.Attribute.Base.AttachmentLevel is AttributeAttachmentLevel.DimensionGroup or AttributeAttachmentLevel.Group && !referencesTimeDim)
                {
                    attributesReferencingPresentDimensions.Add(new KeyValueImpl(DbExtensions.ColumnPresentDbValue.ToString(), attr.Code));
                }
                //Attributes stored at dataflow level
                else if (!hasReportedDimensions && !seriesLevelDimReported && attrMeta.Attribute.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null)
                {
                    attributesReferencingPresentDimensions.Add(new KeyValueImpl(DbExtensions.ColumnPresentDbValue.ToString(), attr.Code));
                }
            }

            return attributesReferencingPresentDimensions;
        }
    }
}