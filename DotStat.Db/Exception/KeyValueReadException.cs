﻿using System.Diagnostics.CodeAnalysis;
using DotStat.DB.Exception;
using DotStat.Db.Validation;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    public class KeyValueReadException : SdmxValidationException
    {
        public KeyValueReadException(ValidationErrorType errorType, IKeyValue keyValue, params string[] argument) 
            : base(new KeyValueError()
            {
                Type = errorType,
                Value = keyValue == null ? null : $"{keyValue.Concept}:{keyValue.Code}",
                Argument = argument
            })
        {

        }
    }
}