﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    public class TransactionKilledException : DotStatException
    {
        public TransactionKilledException()
        {
        }

        public TransactionKilledException(string message) : base(message)
        {
        }

        public TransactionKilledException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
