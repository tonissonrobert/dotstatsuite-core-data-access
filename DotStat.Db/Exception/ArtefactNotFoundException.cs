﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    public class ArtefactNotFoundException : DotStatException
    {
        public ArtefactNotFoundException()
        {
        }

        public ArtefactNotFoundException(string message) : base(message)
        {
        }

        public ArtefactNotFoundException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
