﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using DotStat.Common.Exceptions;
using DotStat.Db.Validation;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    public class ConsumerValidationException : DotStatException
    {
        private readonly IEnumerable<IValidationError> _errors;

        public ConsumerValidationException(IEnumerable<IValidationError> errors)
        {
            _errors = errors;
        }

        public override string Message
        {
            get
            {
                var sb = new StringBuilder();

                foreach (var error in _errors)
                {
                    sb.AppendLine(error.ToString());
                }

                return sb.ToString();
            }
        }
    }
}