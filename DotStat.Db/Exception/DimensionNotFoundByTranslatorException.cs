﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    class DimensionNotFoundByTranslatorException : DotStatException
    {
        public DimensionNotFoundByTranslatorException()
        {
        }

        public DimensionNotFoundByTranslatorException(string message) : base(message)
        {
        }

        public DimensionNotFoundByTranslatorException(string message, System.Exception innerException) : base(message, innerException)
        {
        }

    }
}
