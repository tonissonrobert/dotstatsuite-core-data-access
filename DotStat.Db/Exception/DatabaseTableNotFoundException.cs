﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    public class DatabaseTableNotFoundException: DotStatException
    {
        public DatabaseTableNotFoundException()
        {
        }

        public DatabaseTableNotFoundException(string message) : base(message)
        {
        }

        public DatabaseTableNotFoundException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
