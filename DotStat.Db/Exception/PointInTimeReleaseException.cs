﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Db.Exception
{
    [ExcludeFromCodeCoverage]
    public class PointInTimeReleaseException : DotStatException
    {
        public PointInTimeReleaseException()
        {
        }

        public PointInTimeReleaseException(string message) : base(message)
        {
        }

        public PointInTimeReleaseException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
