﻿using System.Diagnostics.CodeAnalysis;

namespace DotStat.Db.Dto
{
    [ExcludeFromCodeCoverage]
    public class MetadataAttributeItem
    {
        public int DbId { get; set; }
        public string Id { get; set; }
        public int MsdId { get; set; }
        public int? CodelistId { get; set; }
        public long? EnumId { get; set; }
        public int? Parent { get; set; }
        public int? MinOccurs { get; set; }
        public int? MaxOccurs { get; set; }
        public int? IsRepresentational { get; set; }
        public bool IsCoded => CodelistId.HasValue;
}
}
