﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class DurationValidator : PatternValidator
    {
        private const string ValidationExpression = 
            @"^-?P( ( ( [0-9]+Y([0-9]+M)?([0-9]+D)?
                      | ([0-9]+M)([0-9]+D)?
                      | ([0-9]+D)
                      )
                      (T ( ([0-9]+H)([0-9]+M)?([0-9]+(\.[0-9]+)?S)?
                         | ([0-9]+M)([0-9]+(\.[0-9]+)?S)?
                         | ([0-9]+(\.[0-9]+)?S)
                         )
                      )?
                   )
                 | (T ( ([0-9]+H)([0-9]+M)?([0-9]+(\.[0-9]+)?S)?
                      | ([0-9]+M)([0-9]+(\.[0-9]+)?S)?
                      | ([0-9]+(\.[0-9]+)?S)
                      )
                   )
                 )$";

        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotDuration;

        public DurationValidator(bool allowNullValue = true) : base (ValidationExpression, allowNullValue)
        {
        }
    }
}
;