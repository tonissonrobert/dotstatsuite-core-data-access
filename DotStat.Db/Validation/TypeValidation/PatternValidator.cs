﻿using System;
using System.Text.RegularExpressions;
using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class PatternValidator : SimpleSdmxTypeValidator
    {
        protected readonly string RegexPattern;

        protected readonly Regex RegexObject;

        protected virtual ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatPatternMismatch;

        public PatternValidator(string regexPattern, bool allowNullValue = true) : base(allowNullValue)
        {
            if (string.IsNullOrWhiteSpace(regexPattern))
            {
                throw new ArgumentNullException(nameof(regexPattern));
            }

            RegexPattern = regexPattern;

            RegexObject = new Regex(regexPattern, RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);
        }

        public override bool IsValid(string observationValue)
        {
            ValidationError = ValidationErrorType.Undefined;

            if ((AllowNullValue && string.IsNullOrEmpty(observationValue)) || RegexObject.IsMatch(observationValue))
            {
                return true;
            }

            ValidationError = TypeSpecificValidationError;

            ExtraErrorMessageParameters = new[] { RegexPattern };

            return false;
        }
    }
}
