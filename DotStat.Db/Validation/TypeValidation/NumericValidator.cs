﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class NumericValidator : PatternValidator
    {
        private const string ValidationExpression = @"^[0-9]+$";

        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotNumeric;

        public NumericValidator(bool allowNullValue = true) : base(ValidationExpression, allowNullValue)
        {
        }
    }
}
