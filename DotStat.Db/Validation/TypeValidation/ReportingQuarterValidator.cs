﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class ReportingQuarterValidator : PatternValidator
    {
        private const string ValidationExpression = @"^\d{4}\-Q[1-4](Z|(\+|\-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$";

        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotReportingQuarter;

        public ReportingQuarterValidator(bool allowNullValue = true) : base (ValidationExpression, allowNullValue)
        {
        }
    }
}
