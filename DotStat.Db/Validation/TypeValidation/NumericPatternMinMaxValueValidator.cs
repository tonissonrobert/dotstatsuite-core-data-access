﻿using System.Globalization;
using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class NumericPatternMinMaxValueValidator : PatternValidator
    {
        protected readonly decimal? MinValue;
        protected readonly decimal? MaxValue;

        public NumericPatternMinMaxValueValidator(string regexNumericPattern, decimal? minValue, decimal? maxValue, bool allowNullValue = true) : base(regexNumericPattern, allowNullValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
        }

        public override bool IsValid(string observationValue)
        {
            if (!base.IsValid(observationValue))
            {
                return false;
            }

            // No need of min/max value check if observation value is valid and is null 
            if (AllowNullValue && string.IsNullOrEmpty(observationValue))
            {
                return true;
            }

            // When observation value is within decimal boundaries (internal type used by SdmxSource to store min/max value)
            if (decimal.TryParse(observationValue, out var decimalValue)) 
            {
                if (MinValue.HasValue && MinValue > decimalValue)
                {
                    ValidationError = ValidationErrorType.InvalidValueFormatMinValue;

                    ExtraErrorMessageParameters = new[] { MinValue.Value.ToString(CultureInfo.InvariantCulture) };

                    return false;
                }

                if (MaxValue.HasValue && MaxValue < decimalValue)
                {
                    ValidationError = ValidationErrorType.InvalidValueFormatMaxValue;

                    ExtraErrorMessageParameters = new[] { MaxValue.Value.ToString(CultureInfo.InvariantCulture) };

                    return false;
                }

                return true;
            }

            // When observation value is outside of decimal boundaries (internal type used by SdmxSource to store min/max value)
            bool isNegative = observationValue.StartsWith("-");

            if (isNegative && MinValue.HasValue)
            {
                ValidationError = ValidationErrorType.InvalidValueFormatMinValue;

                ExtraErrorMessageParameters = new[] { MinValue.Value.ToString(CultureInfo.InvariantCulture) };

                return false;
            }

            if (!isNegative && MaxValue.HasValue)
            {
                ValidationError = ValidationErrorType.InvalidValueFormatMaxValue;

                ExtraErrorMessageParameters = new[] { MaxValue.Value.ToString(CultureInfo.InvariantCulture) };

                return false;
            }

            return true;
        }
    }
}
