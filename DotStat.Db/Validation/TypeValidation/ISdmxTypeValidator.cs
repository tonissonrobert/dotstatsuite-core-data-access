﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public interface ISdmxTypeValidator
    {
        bool IsValid(string observationValue);

        ValidationErrorType ValidationError { get; }

        string[]  ExtraErrorMessageParameters { get; }
    }
}
