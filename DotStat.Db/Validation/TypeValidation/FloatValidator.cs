﻿using System;
using System.Globalization;
using DotStat.Db.Validation;
using DotStat.Domain;

namespace DotStat.DB.Validation.TypeValidation
{
    public class FloatValidator : SimpleSdmxMinMaxValueValidator
    {
        private readonly CultureInfo _cultureInfo;
        private readonly string _validIntentionallyMissingValue;
        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotFloat;

        public FloatValidator(decimal? minValue, decimal? maxValue, bool supportsIntentionallyMissingValue, CultureInfo cultureInfo, bool allowNullValue = true) : base(minValue, maxValue, allowNullValue)
        {
            _cultureInfo = cultureInfo;
            _validIntentionallyMissingValue = supportsIntentionallyMissingValue ? IntentionallyMissingValue.Numerical.Value : null;
        }

        protected override bool TryParseDecimalOutFunction(string observationValue, out decimal? decimalForMinMaxCheck, out bool isNegative)
        {
            decimalForMinMaxCheck = null;
            isNegative = false;

            if (_validIntentionallyMissingValue != null &&
                observationValue.Equals(_validIntentionallyMissingValue, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            if (!float.TryParse(observationValue, NumberStyles.Float, _cultureInfo.NumberFormat, out var floatValue) || !float.IsFinite(floatValue) || float.IsNaN(floatValue))
            {
                return false;
            }

            isNegative = floatValue < 0;
            try
            {
                decimalForMinMaxCheck = (decimal)floatValue;
            }
            catch (OverflowException)
            {
                decimalForMinMaxCheck = (floatValue < 1 && floatValue > -1) ?
                    new decimal(1, 0, 0, isNegative, 28)
                    : (decimal?)null;
            }
            
            return true;
        }
    }
}
