﻿using DotStat.Db.Validation;
using System.Globalization;

namespace DotStat.DB.Validation.TypeValidation
{
    public abstract class SimpleSdmxMinMaxValueValidator : SimpleSdmxTypeValidator
    {
        protected readonly decimal? MinValue;
        protected readonly decimal? MaxValue;

        protected abstract ValidationErrorType TypeSpecificValidationError { get; }

        protected SimpleSdmxMinMaxValueValidator(decimal? minValue, decimal? maxValue, bool allowNullValue = true) : base(allowNullValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
        }

        protected abstract bool TryParseDecimalOutFunction(string observationValue, out decimal? decimalValueForMinMaxCheck, out bool isNegative);

        public override bool IsValid(string observationValue)
        {
            ValidationError = ValidationErrorType.Undefined;

            if (AllowNullValue && string.IsNullOrEmpty(observationValue))
            {
                return true;
            }

            if (TryParseDecimalOutFunction(observationValue, out var decimalValue, out var isNegative))
            {
                if (MinValue.HasValue && ((!decimalValue.HasValue && isNegative) || MinValue > decimalValue))
                {
                    ValidationError = ValidationErrorType.InvalidValueFormatMinValue;

                    ExtraErrorMessageParameters = new[] { MinValue.Value.ToString(CultureInfo.InvariantCulture) };

                    return false;
                }

                if (MaxValue.HasValue && ((!decimalValue.HasValue &&  !isNegative) || MaxValue < decimalValue))
                {
                    ValidationError = ValidationErrorType.InvalidValueFormatMaxValue;

                    ExtraErrorMessageParameters = new[] { MaxValue.Value.ToString(CultureInfo.InvariantCulture) };

                    return false;
                }

                return true;
            }

            ValidationError = TypeSpecificValidationError;

            return false;
        }
    }
}
