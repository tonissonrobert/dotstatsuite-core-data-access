﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class ShortValidator : SimpleSdmxMinMaxValueValidator
    {
        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotShort;

        public ShortValidator(decimal? minValue, decimal? maxValue, bool allowNullValue = true) : base(minValue, maxValue, allowNullValue)
        {
        }

        protected override bool TryParseDecimalOutFunction(string observationValue, out decimal? decimalForMinMaxCheck, out bool isNegative)
        {
            decimalForMinMaxCheck = null;
            isNegative = false;

            if (!short.TryParse(observationValue, out var shortValue))
            {
                return false;
            }

            isNegative = shortValue < 0;
            decimalForMinMaxCheck = shortValue;

            return true;
        }
    }
}
