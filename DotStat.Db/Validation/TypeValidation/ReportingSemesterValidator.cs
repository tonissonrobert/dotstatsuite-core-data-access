﻿using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class ReportingSemesterValidator : PatternValidator
    {
        private const string ValidationExpression = @"^\d{4}\-S[1-2](Z|(\+|\-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$";

        protected override ValidationErrorType TypeSpecificValidationError => ValidationErrorType.InvalidValueFormatNotReportingSemester;

        public ReportingSemesterValidator(bool allowNullValue = true) : base (ValidationExpression, allowNullValue)
        {
        }
    }
}
