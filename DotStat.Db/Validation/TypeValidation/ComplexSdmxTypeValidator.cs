﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public enum ComplexValidationMode
    {
        Any = 0,
        All = 1
    }

    public abstract class ComplexSdmxTypeValidator : SimpleSdmxTypeValidator
    {
        private readonly List<ISdmxTypeValidator> _validators;

        private readonly ComplexValidationMode _validationMode;

        protected ComplexSdmxTypeValidator(IEnumerable<ISdmxTypeValidator> validators,
            ComplexValidationMode validationMode = ComplexValidationMode.Any, bool allowNullValue = true) : base(allowNullValue)
        {
            _validators = validators.ToList();
            _validationMode = validationMode;
        }

        public override bool IsValid(string observationValue)
        {
            ValidationError = ValidationErrorType.Undefined;

            foreach (var validator in _validators)
            {
                var isValid = validator.IsValid(observationValue);

                if (_validationMode == ComplexValidationMode.Any && isValid)
                {
                    return true;
                }

                if (_validationMode == ComplexValidationMode.All && !isValid)
                {
                    ValidationError = validator.ValidationError;
                    ExtraErrorMessageParameters = validator.ExtraErrorMessageParameters;

                    return false;
                }
            }

            // When reached here then
            // - if ComplexValidationMode.Any then no validation was successful
            // - if ComplexValidationMode.All then all validations were successful
            return _validationMode == ComplexValidationMode.All;
        }
    }
}
