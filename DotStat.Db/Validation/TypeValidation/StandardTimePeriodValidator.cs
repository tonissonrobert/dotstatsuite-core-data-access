﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using DotStat.Db.Validation;

namespace DotStat.DB.Validation.TypeValidation
{
    public class StandardTimePeriodValidator : ComplexSdmxTypeValidator
    {
        public StandardTimePeriodValidator(bool allowNullValue = true) : base(
            new ISdmxTypeValidator[]
            {
                new BasicTimePeriodValidator(allowNullValue), 
                new ReportingTimePeriodValidator(allowNullValue)
            }, ComplexValidationMode.Any, allowNullValue)
        {
        }

        public override bool IsValid(string observationValue)
        {
            if (base.IsValid(observationValue))
            {
                return true;
            }

            ValidationError = ValidationErrorType.InvalidValueFormatNotStandardTimePeriod;

            return false;
        }
    }
}
