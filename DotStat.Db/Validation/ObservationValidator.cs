﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.DB.Util;
using DotStat.DB.Validation.TypeValidation;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Attribute = DotStat.Domain.Attribute;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using DotStat.DB;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Db.Validation
{
    public class ObservationValidator : Validator
    {
        private readonly Dataflow _dataflow;
        private readonly Dictionary<string, (List<Dimension> Dimensions, List<Attribute> Attributes)> _attributesWithSameDimensionReferences;
        private readonly List<Attribute> _reportedNonDsdMandatoryAttributes;
        private readonly Dictionary<string, Attribute> _reportedNonDsdAttributes;
        private readonly Dictionary<string, Attribute> _allDsdAttributes;
        private readonly List<Attribute> _mandatoryNonDsdAttributes;
        private readonly DsdMetadataBuilder _metadata;
        private readonly List<ISdmxTypeValidator> _measureValidators;
        private readonly int _maxTextAttributeLength;
        private readonly bool _validateObservationValue;
        private readonly bool _isTimeAtTimeDimensionSupported;
        private readonly string _listOfAllDimensionIds;
        public readonly Dictionary<string, TimeDimensionExtendedProperties> ValidTimeValues;
        public readonly Dictionary<string, ValidationErrorType> InvalidTimeValues;


        public ObservationValidator(Dataflow dataflow, ReportedComponents reportedComponents,
            IGeneralConfiguration configuration, bool fullValidation, bool isTimeAtTimeDimensionSupported,
            DsdMetadataBuilder metadata)
            : base(configuration, fullValidation)
        {
            _dataflow = dataflow;
            _metadata = metadata;
            _allDsdAttributes = dataflow.Dsd.Attributes.ToDictionary(a => a.Code, a => a);
            var reportedNonDsdAttributes = reportedComponents.SeriesAttributesWithNoTimeDim
                .Concat(reportedComponents.ObservationAttributes).ToList();

            _attributesWithSameDimensionReferences = reportedNonDsdAttributes.GetAttributeGroupsOfSameDimensionRelations(dataflow.Dsd);

            _reportedNonDsdAttributes = reportedNonDsdAttributes.ToDictionary(a => a.Code, a => a);

            _reportedNonDsdMandatoryAttributes = reportedNonDsdAttributes.Where(a => a.Base.Mandatory).ToList();

            _mandatoryNonDsdAttributes = dataflow.Dsd.Attributes.Where(a => a.Base.Mandatory)
                .Where(a => a.Base.AttachmentLevel is not AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null).ToList();

            _measureValidators = new SdmxTypeValidatorBuilder()
                .BuildFromPrimaryMeasure(dataflow.Dsd.PrimaryMeasure, dataflow.Dsd.SupportsIntentionallyMissingValues, reportedComponents.CultureInfo ?? CultureInfo.InvariantCulture).ToList();

            _maxTextAttributeLength = dataflow.Dsd.MaxTextAttributeLength ??
                                      dataflow.Dsd.GetMaxLengthOfTextAttributeFromConfig(configuration
                                          .MaxTextAttributeLength);

            _validateObservationValue = reportedComponents.IsPrimaryMeasureReported;

            _isTimeAtTimeDimensionSupported = isTimeAtTimeDimensionSupported;
            ValidTimeValues = new Dictionary<string, TimeDimensionExtendedProperties>();
            InvalidTimeValues = new Dictionary<string, ValidationErrorType>();

            _listOfAllDimensionIds = string.Join(",", dataflow.Dsd.Dimensions.Select(d => d.Code));
        }

        public override bool ValidateObservation(IObservation observation, int batchNumber, int row = 0,
            StagingRowActionEnum action = StagingRowActionEnum.Merge)
        {
            if (action is StagingRowActionEnum.Skip or StagingRowActionEnum.DeleteAll)
                return true;

            // simple and fast checks 

            // validity of reported time period value to be checked first
            if (!TimeDimensionCheck(observation, row))
                return false;

            if (!DimensionConstraintCheck(observation, row))
                return false;

            (string PrimaryMeasureId, HashSet<string> AttributIds) presentComponents = (null, new HashSet<string>());
            if (_validateObservationValue)
            {
                if (!ObservationValueTypeCheck(observation, row))
                    return false;

                if (FullValidation)
                {
                    if (!Duplicates.TryGetValue(batchNumber, out var duplicatesHashSet))
                        Duplicates[batchNumber] = duplicatesHashSet = new HashSet<string>();

                    if (!duplicatesHashSet.Add(observation.GetKeyStartEndPeriod(_metadata.HasReportingYearStartDayAttr)))
                        return AddError(ValidationErrorType.DuplicatedCoordinate, row, observation);
                }

                if (!string.IsNullOrEmpty(observation.ObservationValue.HarmonizeSpecialCaseValues()))
                    presentComponents.PrimaryMeasureId = _metadata.Dsd.Base.PrimaryMeasure.Id;
            }
            
            var reportedDimensions =
                observation.SeriesKey.Key.ToDictionary(k => k.Concept, k => k.Code, StringComparer.OrdinalIgnoreCase);

            if (_metadata.HasTime && observation.ObsTime is not null)
                reportedDimensions.Add(_metadata.TimeDim.Code, observation.ObsTime);
            
            if (!MandatoryAttributesCheck(observation, row, action, reportedDimensions))
                return false;

            foreach (var attribute in observation.Attributes)
            {
                if (!AttributeTypeCheck(attribute, observation, row, action))
                    return false;

                if (!string.IsNullOrEmpty(attribute.Code.HarmonizeSpecialCaseValues()))
                    presentComponents.AttributIds.Add(attribute.Concept);
            }

            return !IsMaxErrorLimitReached && CheckReferenceDimensionsPresent(presentComponents, observation, row, action, reportedDimensions);
        }

        public bool DimensionConstraintCheck(IObservation observation, int row)
        {
            var seriesKey = observation.GetKey(false);

            if (Series.Contains(seriesKey))
                return true;

            foreach (var key in observation.SeriesKey.Key)
            {
                var codeIsNullOrEmpty = string.IsNullOrEmpty(key.Code);

                var meta = _metadata[key.Concept];

                if(!meta.Dimension.Base.HasCodedRepresentation())
                    continue;
                
                if (!codeIsNullOrEmpty && !meta.Dimension.HasCode(key.Code))
                    return AddError(ValidationErrorType.UnknownCodeMember, row, observation,
                        key.Concept + ":" + key.Code);

                if (!codeIsNullOrEmpty && !meta.Dimension.IsAllowed(key.Code))
                    return AddError(ValidationErrorType.DimensionConstraintViolation, row, observation,
                        key.Concept + ":" + key.Code);
            }

            Series.Add(seriesKey);

            return true;
        }

        public bool TimeDimensionCheck(IObservation observation, int row)
        {
            if (string.IsNullOrEmpty(observation.ObsTime))
                return true;

            var reportingYearStartDateString = _metadata.HasReportingYearStartDayAttr ? observation.GetAttribute(AttributeObject.Repyearstart)?.Code : null;
            var key = $"{observation.ObsTime}-{reportingYearStartDateString}";

            // Check if current time dim value was already identified as a valid one
            if (ValidTimeValues.TryGetValue(key, out _))
                return true;

            // Check if current time dim value was already identified as an invalid one
            if (InvalidTimeValues.TryGetValue(key, out var validationErrorType))
                return AddError(validationErrorType, row, observation);

            try
            {
                var timeDimension = observation.ObsAsTimeDate;
                // If full validation then log error when time is not supported by DSD but observation's time period contains a time fragment
                // Note that reporting trimester also contains 'T', e.g. '2021-T2', '2021-T3Z', '2022-T1+02:00' etc.
                if (timeDimension != null && FullValidation && !_isTimeAtTimeDimensionSupported &&
                    (timeDimension.Value.TimeOfDay.Ticks > 0 || observation.ObsTime.Contains("T00")))
                {
                    InvalidTimeValues.Add(key, ValidationErrorType.TimeAtTimeDimensionNotSupported);

                    return AddError(ValidationErrorType.TimeAtTimeDimensionNotSupported, row, observation);
                }

                ValidTimeValues.Add(key, new TimeDimensionExtendedProperties(observation.ObsTime, reportingYearStartDateString));
            }
            catch (SdmxSemmanticException)
            {
                InvalidTimeValues.Add(key, ValidationErrorType.TimeDimensionFormatNotRecognized);

                return AddError(ValidationErrorType.TimeDimensionFormatNotRecognized, row, observation);
            }

            return true;
        }

        public bool ObservationValueTypeCheck(IObservation observation, int row)
        {
            var obsVal = observation.ObservationValue.HarmonizeSpecialCaseValues();
            foreach (ISdmxTypeValidator validator in _measureValidators)
            {
                if (!validator.IsValid(obsVal))
                {
                    return AddError(validator.ValidationError, row, observation, validator.ExtraErrorMessageParameters);
                }
            }

            return true;
        }

        public bool MandatoryAttributesCheck(IObservation observation, int row, StagingRowActionEnum action, Dictionary<string, string> reportedDimensions)
        {
            var isValid = true;
            if (!FullValidation)
                return true;
            if (action == StagingRowActionEnum.Delete)
                return true;
            if (_mandatoryNonDsdAttributes.Count == 0)
                return true;

            //Non reported
            var missingMandatoryAttributes = _mandatoryNonDsdAttributes.Where(mandatoryAttribute =>
                !observation.Attributes.Any(a => a.Concept.Equals(mandatoryAttribute.Code,
                    StringComparison.InvariantCultureIgnoreCase)));
            
            var presentDimensionsNotNull = reportedDimensions
                .Where(d => !string.IsNullOrEmpty(d.Value))
                .Select(d => d.Key);

            foreach (var attribute in missingMandatoryAttributes)
            {
                var referencedDimensions = _metadata.DimensionsReferenceGroups[attribute.Code];
                if (presentDimensionsNotNull.ToHashSet().IsSupersetOf(referencedDimensions))
                {
                    isValid = false;
                    AddError(ValidationErrorType.MandatoryAttributeMissing, row, observation, attribute.Code);
                }
            }

            //Reported with null value
            var mandatoryAttributesNullValue = _reportedNonDsdMandatoryAttributes
                .Where(mandatoryAttribute => observation.Attributes.Any(a =>
                    a.Concept.Equals(mandatoryAttribute.Code,
                        StringComparison.InvariantCultureIgnoreCase) &&
                    string.IsNullOrWhiteSpace(a.Code.HarmonizeSpecialCaseValues()))).ToList();

            foreach (var attribute in mandatoryAttributesNullValue)
            {
                var referencedDimensions = _metadata.DimensionsReferenceGroups[attribute.Code];
                if (presentDimensionsNotNull.ToHashSet().IsSupersetOf(referencedDimensions))
                {
                    isValid = false;
                    AddError(ValidationErrorType.MandatoryAttributeWithNullValueInStaging, row, observation,
                        attribute.Code);
                }
            }

            return isValid;
        }

        public bool AttributeTypeCheck(IKeyValue attribute, IObservation observation, int row, StagingRowActionEnum action)
        {
            if (!_allDsdAttributes.TryGetValue(attribute.Concept, out var attrMeta))
            {
                //Skip Ignore unknown attributes
                observation.Attributes.Remove(attribute);
                return AddError(ValidationErrorType.AttributeNotInDsd, row, observation, attribute.Concept,
                    _dataflow.Dsd.FullId);
            }

            //skip dsd attributes
            if (attrMeta.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet or AttributeAttachmentLevel.Null)
                return true;

            var isAttributeCodeEmpty = string.IsNullOrEmpty(attribute.Code.HarmonizeSpecialCaseValues());

            var hasCodeRepresentation = attrMeta.Base.HasCodedRepresentation();
            if (hasCodeRepresentation && action != StagingRowActionEnum.Delete)
            {
                if (!isAttributeCodeEmpty && attrMeta.FindMember(attribute.Code, false) == null)
                    return AddError(ValidationErrorType.UnknownAttributeCodeMember, row, observation, attribute.Concept,
                        attribute.Code);

                //Check allowed content constraint
                if (!isAttributeCodeEmpty && !attrMeta.IsAllowed(attribute.Code))
                    return AddError(ValidationErrorType.AttributeConstraintViolation, row, observation,
                        attribute.Concept + ":" + attribute.Code);

            }
            else
            {
                var maxTextAttributeLength = attrMeta.Dsd.MaxTextAttributeLength ?? _maxTextAttributeLength;

                if (!isAttributeCodeEmpty &&
                    maxTextAttributeLength is > 0 and <= 4000 && // MaxTextAttributeLength annotation determines only the storage size, not the validation rule
                    attribute.Code.Length > maxTextAttributeLength)
                {
                    return AddError(ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit, row, observation,
                        attribute.Concept, attribute.Code,
                        attribute.Code.Length.ToString(),
                        maxTextAttributeLength.ToString());
                }

                // Currently attribute can be coded or text. Additional type check should be placed here 
                // when other types will be supported. (eg. valid date, integer, double, etc.)
            }

            return true;
        }

        public bool CheckReferenceDimensionsPresent(
            (string PrimaryMeasureId, HashSet<string> AttributIds) presentedComponents,
            IObservation observation, int row, StagingRowActionEnum action, Dictionary<string, string> presentDimensions)
        {
            if (action == StagingRowActionEnum.Delete)
                return true;
            
            // When all dimensions are present and none of them are null then no need to check the components individually
            if (_metadata.Dsd.Dimensions.Count == presentDimensions.Count &&
                presentDimensions.All(d => !string.IsNullOrEmpty(d.Value)))
            {
                return true;
            }

            // When primary measure is presented then none of the dimensions allowed to be missing or to be empty
            if (!string.IsNullOrEmpty(presentedComponents.PrimaryMeasureId))
            {
                // If primary measure is OK, then all attributes are OK, too
                return CheckMissingReferencedDimensionsOfComponents(
                    new List<string>() { presentedComponents.PrimaryMeasureId },
                    _metadata.Dsd.Dimensions, _listOfAllDimensionIds,
                    presentDimensions, observation, row, ValidationErrorType.ObservationReferencedDimensionMissing);
            }

            var remainingAttributesToCheck = new List<string>(presentedComponents.AttributIds);
            var validationResult = true;

            foreach (var attributesWithSameDimensionSet in _attributesWithSameDimensionReferences)
            {
                // If no more attributes left to be checked then validation is OK
                if (remainingAttributesToCheck.Count == 0)
                    return validationResult;

                // If error limit reached stop validation
                if (IsMaxErrorLimitReached)
                    return false;

                var currentAttributesToCheck = (
                    from attribute
                        in attributesWithSameDimensionSet.Value.Attributes
                    where remainingAttributesToCheck.Remove(attribute.Code)
                    select attribute.Code).ToList();

                // If there are no attributes presented matching the current set of attributes then take the next group of attributes
                if (currentAttributesToCheck.Count == 0)
                    continue;

                validationResult &= CheckMissingReferencedDimensionsOfComponents(currentAttributesToCheck,
                    attributesWithSameDimensionSet.Value.Dimensions, attributesWithSameDimensionSet.Key,
                    presentDimensions, observation, row, ValidationErrorType.AttributeReferencedDimensionMissing);
            }

            return validationResult;
        }

        private bool CheckMissingReferencedDimensionsOfComponents(IEnumerable<string> presentedComponentIds, IReadOnlyCollection<Dimension> relatedDimensions, string listOfRelatedDimensionIds,
            IDictionary<string, string> dimensionsPresent, IObservation observation, int row, ValidationErrorType validationErrorType)
        {
            if (relatedDimensions.Count == 0)
                return true;

            var referencedDimensionsMissing = new List<string>();

            foreach (var relatedDimension in relatedDimensions)
            {
                if (dimensionsPresent.TryGetValue(relatedDimension.Code, out var presentedDimensionCode))
                {
                    if (string.IsNullOrEmpty(presentedDimensionCode))
                        return AddError(ValidationErrorType.MissingCodeMember, row, observation, relatedDimension.Code);
                }
                else
                {
                    referencedDimensionsMissing.Add(relatedDimension.Code);
                }
            }

            if (referencedDimensionsMissing.Count == 0)
                return true;

            foreach (var attributeId in presentedComponentIds)
            {
                AddError(validationErrorType, row, observation, attributeId,
                    listOfRelatedDimensionIds,
                    string.Join(",", referencedDimensionsMissing));
            }

            return false;
        }
    }
}