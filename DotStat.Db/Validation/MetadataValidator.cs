﻿using DotStat.Common.Configuration.Interfaces;
using DotStat.DB;
using DotStat.DB.Util;
using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DotStat.Db.Validation
{
    public class MetadataValidator : Validator
    {
        private readonly Dsd _dsd;
        private readonly bool _isTimeAtTimeDimensionSupported;
        private readonly DsdMetadataBuilder _metadata;
        public readonly Dictionary<string, TimeDimensionExtendedProperties> ValidatedTimeValues;

        public MetadataValidator(Dsd dsd, IGeneralConfiguration configuration, bool fullValidation, bool isTimeAtTimeDimensionSupported,
            DsdMetadataBuilder metadata) : base(configuration, fullValidation)
        {
            _dsd = dsd;
            _isTimeAtTimeDimensionSupported = isTimeAtTimeDimensionSupported;
            _metadata = metadata;

            ValidatedTimeValues = new Dictionary<string, TimeDimensionExtendedProperties>();
        }

        public override bool ValidateObservation(IObservation observation, int batchNumber, int row=0, StagingRowActionEnum action = StagingRowActionEnum.Merge)
        {
            if (observation == null)
            {
                return true;
            }

            if (!ValidateDimensionValues(action, observation, row))
            {
                return false;
            }

            // validity of reported time period value to be checked first
            if (!TimeDimensionCheck(observation, row))
            {
                return false;
            }

            if (FullValidation)
            {
                if (!Duplicates.TryGetValue(batchNumber, out var duplicatesHashSet))
                    Duplicates[batchNumber] = duplicatesHashSet = new HashSet<string>();

                if (!duplicatesHashSet.Add(observation.GetKeyStartEndPeriod(false)))
                    return AddError(ValidationErrorType.DuplicatedCoordinate, row, observation);
            }

            return DimensionConstraintCheck(observation, row);
        }

        private bool ValidateDimensionValues(StagingRowActionEnum action, IObservation observation, int row)
        {
            if (action == StagingRowActionEnum.Delete)
            {
                return true;
            }

            var invalidDimensions = new List<string>();

            if (_dsd.TimeDimension != null &&
                (string.IsNullOrWhiteSpace(observation.ObsTime) || observation.ObsTime.Equals(DbExtensions.DimensionWildCarded, StringComparison.OrdinalIgnoreCase)))
            {
                invalidDimensions.Add(_dsd.TimeDimension.Code);
            }

            foreach (var dim in _dsd.Dimensions.Where(dim => !dim.Base.TimeDimension))
            {
                var key = observation.SeriesKey.Key.FirstOrDefault(k => k.Concept.Equals(dim.Code, StringComparison.InvariantCultureIgnoreCase));

                if (key == null || string.IsNullOrEmpty(key.Code) || key.Code.Equals(DbExtensions.DimensionWildCarded, StringComparison.InvariantCultureIgnoreCase))
                {
                    invalidDimensions.Add(dim.Code);
                }
            }

            if (invalidDimensions.Any())
            {
                AddError(ValidationErrorType.MissingDimensionForMetadataImport, row, observation, string.Join(", ", invalidDimensions));
                return false;
            }

            return true;
        }

        private bool TimeDimensionCheck(IObservation observation, int row)
        {
            if (!_metadata.HasTime)
                return true;

            var reportingYearStartDateString = _metadata.HasReportingYearStartDayAttr ? observation.GetAttribute(AttributeObject.Repyearstart)?.Code : null;
            var key = $"{observation.ObsTime}-{reportingYearStartDateString}";

            try
            {
                if (string.IsNullOrWhiteSpace(observation.ObsTime))
                    return true;

                if (observation.ObsTime.Equals(DbExtensions.DimensionWildCarded, StringComparison.OrdinalIgnoreCase) ||
                    observation.ObsTime.Equals(DbExtensions.DimensionSwitchedOff, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }

                if (ValidatedTimeValues.TryGetValue(key, out _))
                    return true;

                var timeDimension = observation.ObsAsTimeDate;

                // If full validation then log error when time is not supported by DSD but observation's time period contains a time fragment
                // Note that reporting trimester also contains 'T', e.g. '2021-T2', '2021-T3Z', '2022-T1+02:00' etc.
                if (timeDimension != null && FullValidation && !_isTimeAtTimeDimensionSupported &&
                    (timeDimension.Value.TimeOfDay.Ticks > 0 || observation.ObsTime.Contains("T00")))
                {

                    return AddError(ValidationErrorType.TimeAtTimeDimensionNotSupported, row, observation);
                }

                ValidatedTimeValues.Add(key, new TimeDimensionExtendedProperties(observation.ObsTime, reportingYearStartDateString));
            }
            catch (SdmxSemmanticException)
            {
                return AddError(ValidationErrorType.TimeDimensionFormatNotRecognized, row, observation);
            }

            return true;
        }

        private bool DimensionConstraintCheck(IObservation observation, int row)
        {
            var seriesKey = observation.GetKey(false);

            if (Series.Contains(seriesKey))
            {
                return true;
            }

            foreach (var key in observation.SeriesKey.Key)
            {
                if (string.IsNullOrEmpty(key.Code) || key.Code.Equals(DbExtensions.DimensionWildCarded, StringComparison.OrdinalIgnoreCase) || key.Code.Equals(DbExtensions.DimensionSwitchedOff, StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }

                var dimension = _dsd.Dimensions.First(x => x.Code.Equals(key.Concept));

                if (dimension.Base.HasCodedRepresentation() && !dimension.HasCode(key.Code))
                {
                    return AddError(ValidationErrorType.UnknownCodeMember, row, observation, key.Concept + ":" + key.Code);
                }
            }

            Series.Add(seriesKey);

            return true;
        }
    }
}