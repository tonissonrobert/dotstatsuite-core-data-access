﻿using System;
using System.Linq;
using DotStat.Common.Localization;

namespace DotStat.Db.Validation
{
    public enum ValidationErrorType
    {
        Undefined,
        MissingCodeMember,
        UnknownCodeMember,
        DimensionConstraintViolation,
        AttributeConstraintViolation,
        NullValue,
        DuplicatedCoordinate,
        DuplicatedRowsInStagingTable,
        DuplicatedRowsInMetadataStagingTable,
        MultipleRowsModifyingTheSameRegionInStagingTable,
        OverlappingCoordinates,
        AttributeNotInDsd,
        AttributeReportedAtMultipleLevels,
        WrongSetOfDimensionsInKeyOfKeyable,
        MandatoryAttributeWithNullValueInDatabase,
        MandatoryAttributeWithNullValueInStaging,
        MultipleValuesForAttributeInDatabase,
        MultipleValuesForAttributeInStaging,
        DuplicatedAttributeKey,
        DuplicatedDatasetAttribute,
        UnknownAttributeCodeMember,
        ObservationAttributeAtNonObservationLevel,
        DimGroupAttributeAtDatasetLevel,
        DatasetAttributeAtKeyable, 
        MandatoryAttributeMissing,
        TextAttributeValueLengthExceedsMaxLimit,
        AttributeReferencedDimensionMissing,
        ObservationReferencedDimensionMissing,
        InvalidValueFormatNotAlpha,
        InvalidValueFormatNotAlphaNumeric,
        InvalidValueFormatNotNumeric,
        InvalidValueFormatNotBigInteger,
        InvalidValueFormatNotInteger,
        InvalidValueFormatNotLong,
        InvalidValueFormatNotShort,
        InvalidValueFormatNotDecimal,
        InvalidValueFormatNotFloat,
        InvalidValueFormatNotDouble,
        InvalidValueFormatNotBoolean,
        InvalidValueFormatNotUri,
        InvalidValueFormatNotCount,
        InvalidValueFormatNotObservationalTimePeriod,
        InvalidValueFormatNotStandardTimePeriod,
        InvalidValueFormatNotBasicTimePeriod,
        InvalidValueFormatNotGregorianTimePeriod,
        InvalidValueFormatNotGregorianYear,
        InvalidValueFormatNotGregorianYearMonth,
        InvalidValueFormatNotGregorianDay,
        InvalidValueFormatNotReportingTimePeriod,
        InvalidValueFormatNotReportingYear,
        InvalidValueFormatNotReportingSemester,
        InvalidValueFormatNotReportingTrimester,
        InvalidValueFormatNotReportingQuarter,
        InvalidValueFormatNotReportingMonth,
        InvalidValueFormatNotReportingWeek,
        InvalidValueFormatNotReportingDay,
        InvalidValueFormatNotDateTime,
        InvalidValueFormatNotTimeRange,
        InvalidValueFormatNotMonth,
        InvalidValueFormatNotMonthDay,
        InvalidValueFormatNotDay,
        InvalidValueFormatNotTime,
        InvalidValueFormatNotDuration,
        InvalidValueFormatPatternMismatch,
        InvalidValueFormatMaxLength,
        InvalidValueFormatMinLength,
        InvalidValueFormatMaxValue,
        InvalidValueFormatMinValue,
        InvalidValueFormatUnknownCodeMember,
        TimeAtTimeDimensionNotSupported,
        TimeDimensionFormatNotRecognized,
        MissingDimensionForMetadataImport
    }

    public interface IValidationError
    {
        ValidationErrorType Type { get; set; }
        int Row { get; set; }
        string Coordinate { get; set; }
        string Value { get; set; }
        string[] Argument { get; set; }

        string ToString();
    }

    public abstract class ValidationError : IValidationError
    {
        public ValidationErrorType Type { get; set; }
        public int Row { get; set; }
        public string Coordinate { get; set; }
        public string Value { get; set; }
        public string[] Argument { get; set; }

        protected abstract string GetTemplate();

        public override string ToString()
        {
            var x = new object[4 + (Argument?.Length ?? 0)];
            x[0] = Row;
            x[1] = Coordinate;
            x[2] = Value;
            x[3] = Row + 1; // Line number in CSV
            
            Argument?.CopyTo(x, 4);

            string customizedMessage;
            string template = null;

            try
            {
                template = (Type == ValidationErrorType.Undefined) ? x[4].ToString() : GetTemplate();
                customizedMessage = string.Format(template, x);
            }
            catch (System.Exception e)
            {
                customizedMessage = string.IsNullOrEmpty(template)
                    ? $"Could not get message template for validation error. Validation error: [{GetType().Name}.{Type.ToString()}] Parameters: {string.Join(",", x.Select(i => $"[{i}]"))} Exception: {e.Message}"
                    : $"Could not customize message template for validation error with the provided parameters. Validation error: [{GetType().Name}.{Type.ToString()}] Template: [{template}] Parameters: {string.Join(",", x.Select(i => $"[{i}]"))} Exception: {e.Message}";
            }

            return customizedMessage;
        }
    }

    public class ObservationError : ValidationError
    {
        /// <summary>
        /// Template arguments:
        /// 0 - Processed row
        /// 1 - Coordinate
        /// 2 - Observation value
        /// 3 - csv Line
        /// 4 -> n - Arguments or generic message in case of unknown exception
        /// </summary>
        /// <returns></returns>
        protected override string GetTemplate()
        {
            switch (Type)
            {
                case ValidationErrorType.MissingCodeMember:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingCodeMember);

                case ValidationErrorType.UnknownCodeMember:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnknownCodeMember);

                case ValidationErrorType.DimensionConstraintViolation:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DimensionConstraintViolation);

                case ValidationErrorType.AttributeNotInDsd:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeNotDefinedInDsd);

                case ValidationErrorType.AttributeConstraintViolation:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeConstraintViolation);

                case ValidationErrorType.UnknownAttributeCodeMember:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnknownAttributeCodeMemberWithCoordinate);

                case ValidationErrorType.NullValue:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NullValue);

                case ValidationErrorType.MandatoryAttributeMissing:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MandatoryObservationAttributeMissing);

                case ValidationErrorType.MandatoryAttributeWithNullValueInStaging:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MandatoryObservationAttributeWithNullValueInStaging);

                case ValidationErrorType.DuplicatedCoordinate:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DuplicatedCoordinate);

                case ValidationErrorType.DuplicatedRowsInStagingTable:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DuplicatedRowsInStagingTable);

                case ValidationErrorType.DuplicatedRowsInMetadataStagingTable:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DuplicatedRowsInStagingTable);

                case ValidationErrorType.MultipleRowsModifyingTheSameRegionInStagingTable:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MultipleRowsModifyingTheSameRegionInStagingTable);

                case ValidationErrorType.OverlappingCoordinates:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.OverlappingCoordinates);

                case ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TextAttributeValueLengthExceedsMaxLimitWithCoordinate);

                case ValidationErrorType.AttributeReferencedDimensionMissing:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeReferencedDimensionMissing);

                case ValidationErrorType.ObservationReferencedDimensionMissing:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ObservationReferencedDimensionMissing);

                case ValidationErrorType.InvalidValueFormatNotAlpha:
                case ValidationErrorType.InvalidValueFormatNotAlphaNumeric:
                case ValidationErrorType.InvalidValueFormatNotNumeric:
                case ValidationErrorType.InvalidValueFormatNotBigInteger:
                case ValidationErrorType.InvalidValueFormatNotInteger:
                case ValidationErrorType.InvalidValueFormatNotLong:
                case ValidationErrorType.InvalidValueFormatNotShort:
                case ValidationErrorType.InvalidValueFormatNotDecimal:
                case ValidationErrorType.InvalidValueFormatNotFloat:
                case ValidationErrorType.InvalidValueFormatNotDouble:
                case ValidationErrorType.InvalidValueFormatNotBoolean:
                case ValidationErrorType.InvalidValueFormatNotUri:
                case ValidationErrorType.InvalidValueFormatNotCount:
                case ValidationErrorType.InvalidValueFormatNotObservationalTimePeriod:
                case ValidationErrorType.InvalidValueFormatNotStandardTimePeriod:
                case ValidationErrorType.InvalidValueFormatNotBasicTimePeriod:
                case ValidationErrorType.InvalidValueFormatNotGregorianTimePeriod:
                case ValidationErrorType.InvalidValueFormatNotGregorianYear:
                case ValidationErrorType.InvalidValueFormatNotGregorianYearMonth:
                case ValidationErrorType.InvalidValueFormatNotGregorianDay:
                case ValidationErrorType.InvalidValueFormatNotReportingTimePeriod:
                case ValidationErrorType.InvalidValueFormatNotReportingYear:
                case ValidationErrorType.InvalidValueFormatNotReportingSemester:
                case ValidationErrorType.InvalidValueFormatNotReportingTrimester:
                case ValidationErrorType.InvalidValueFormatNotReportingQuarter:
                case ValidationErrorType.InvalidValueFormatNotReportingMonth:
                case ValidationErrorType.InvalidValueFormatNotReportingWeek:
                case ValidationErrorType.InvalidValueFormatNotReportingDay:
                case ValidationErrorType.InvalidValueFormatNotDateTime:
                case ValidationErrorType.InvalidValueFormatNotTimeRange:
                case ValidationErrorType.InvalidValueFormatNotMonth:
                case ValidationErrorType.InvalidValueFormatNotMonthDay:
                case ValidationErrorType.InvalidValueFormatNotDay:
                case ValidationErrorType.InvalidValueFormatNotTime:
                case ValidationErrorType.InvalidValueFormatNotDuration:
                case ValidationErrorType.InvalidValueFormatPatternMismatch:
                case ValidationErrorType.InvalidValueFormatMaxLength:
                case ValidationErrorType.InvalidValueFormatMinLength:
                case ValidationErrorType.InvalidValueFormatMaxValue:
                case ValidationErrorType.InvalidValueFormatMinValue:
                case ValidationErrorType.InvalidValueFormatUnknownCodeMember:
                    return LocalizationRepository.GetLocalisedResource(Enum.Parse<Localization.ResourceId>(Type.ToString()));

                case ValidationErrorType.TimeAtTimeDimensionNotSupported:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TimeAtTimeDimensionNotSupported);

                case ValidationErrorType.TimeDimensionFormatNotRecognized:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TimeDimensionFormatNotRecognized);

                case ValidationErrorType.MissingDimensionForMetadataImport:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingDimensionMetadataImport);

                default:
                    return string.Join(",", Argument);
            }
        }
    }

    public class KeyableError : ValidationError
    {
        /// <summary>
        /// Template arguments:
        /// 0 - Processed row
        /// 1 - Coordinate
        /// 2 - Observation value
        /// 3 - csv Line
        /// 4 -> n - Arguments or generic message in case of unknown exception
        /// </summary>
        /// <returns></returns>
        protected override string GetTemplate()
        {
            switch (Type)
            {
                case ValidationErrorType.MissingCodeMember:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingCodeMemberWithCoordinate);

                case ValidationErrorType.DimensionConstraintViolation:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DimensionConstraintViolation);

                case ValidationErrorType.AttributeConstraintViolation:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeConstraintViolation);

                case ValidationErrorType.UnknownCodeMember:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnknownCodeMemberWithCoordinate);

                case ValidationErrorType.UnknownAttributeCodeMember:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnknownAttributeCodeMemberWithCoordinate);

                case ValidationErrorType.AttributeNotInDsd:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeNotDefinedInDsd);

                case ValidationErrorType.AttributeReportedAtMultipleLevels:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeReportedAtMultipleLevels);

                case ValidationErrorType.WrongSetOfDimensionsInKeyOfKeyable:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongSetOfDimensionsInKeyOfKeyable);
                
                case ValidationErrorType.MandatoryAttributeMissing:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MandatoryDimGroupAttributeMissing);

                case ValidationErrorType.MandatoryAttributeWithNullValueInDatabase:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MandatoryAttributeWithNullValueInDatabase);

                case ValidationErrorType.MandatoryAttributeWithNullValueInStaging:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MandatoryDimGroupAttributeWithNullValueInStaging);

                case ValidationErrorType.DuplicatedAttributeKey:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DuplicatedAttributeKey);

                case ValidationErrorType.ObservationAttributeAtNonObservationLevel:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ObservationAttributeReportedAtKeyables);

                case ValidationErrorType.DatasetAttributeAtKeyable:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DatasetAttributeReportedAtKeyables);

                case ValidationErrorType.MultipleValuesForAttributeInDatabase:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MultipleValuesForDimGroupAttributeInDatabase);

                case ValidationErrorType.MultipleValuesForAttributeInStaging:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MultipleValuesForDimGroupAttributeInStaging);

                case ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TextAttributeValueLengthExceedsMaxLimitWithCoordinate);

                default:
                    return string.Join(",", Argument);
            }
        }
    }

    public class KeyValueError : ValidationError
    {
        /// <summary>
        /// Template arguments:
        /// 0 - Index of processed KeyValue
        /// 1 - Coordinate - always null for dataset attributes
        /// 2 - Attribute key-value 
        /// 3 - csv Line
        /// 4 -> n - Arguments or generic message in case of unknown exception
        /// </summary>
        /// <returns></returns>
        protected override string GetTemplate()
        {
            switch (Type)
            {
                case ValidationErrorType.UnknownAttributeCodeMember:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnknownAttributeCodeMemberWithoutCoordinate);

                case ValidationErrorType.AttributeNotInDsd:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeNotDefinedInDsd);

                case ValidationErrorType.AttributeReportedAtMultipleLevels:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeReportedAtMultipleLevels);
                
                case ValidationErrorType.MandatoryAttributeMissing:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MandatoryDatasetAttributeMissing);

                case ValidationErrorType.MandatoryAttributeWithNullValueInDatabase:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MandatoryDatasetAttributeWithNullValueInDatabase);

                case ValidationErrorType.MandatoryAttributeWithNullValueInStaging:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MandatoryDatasetAttributeWithNullValueInStaging);

                case ValidationErrorType.DuplicatedDatasetAttribute:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DuplicatedDatasetAttribute);

                case ValidationErrorType.ObservationAttributeAtNonObservationLevel:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ObservationAttributeReportedAtDataset);

                case ValidationErrorType.DimGroupAttributeAtDatasetLevel:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DimGroupAttributeReportedAtDataset);
                    
                case ValidationErrorType.UnknownCodeMember:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnknownCodeMemberWithoutCoordinate);

                case ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TextAttributeValueLengthExceedsMaxLimitWithoutCoordinate);

                case ValidationErrorType.AttributeConstraintViolation:
                    return LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeConstraintViolation);

                default:
                    return string.Join(",", Argument);
            }
        }
    }
}