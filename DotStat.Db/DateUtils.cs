﻿using System;
using Org.Sdmxsource.Sdmx.Api.Model.Base;
using Org.Sdmxsource.Sdmx.Util.Date;

namespace DotStat.DB
{
    public static class DateUtils
    {
        private static readonly ReportingTimePeriod ReportingTimePeriod = new ReportingTimePeriod();

        public static (DateTime start, DateTime end) GetPeriod(string sdmxTime, string reportingYearStartDateString = null)
        {
            var reportingPeriod = ReportingTimePeriod.ToGregorianPeriod(sdmxTime, reportingYearStartDateString);

            var start = reportingPeriod.PeriodStart.DateTime;
            var period = PeriodicityFactory.Create(reportingPeriod.Frequency);
            var end = period is TimeRangePeriodicity
                            ? period.ToDateTimeOffsetDuration(sdmxTime).ToEnd()
                            : period.AddPeriod(start).AddSeconds(-1);

            return (start, end);
        }

        private static DateTime ToEnd(this DateTimeOffsetDuration dt)
        {
            return dt.Date
                .AddYears(dt.Duration.Years)
                .AddMonths(dt.Duration.Months)
                .AddDays(dt.Duration.Days)
                .AddHours(dt.Duration.Hours)
                .AddMinutes(dt.Duration.Minutes)
                .AddSeconds(dt.Duration.Seconds - 1)
                .DateTime;
        }

        public static DateTime? EndDate(this ISdmxDate date)
        {
            if (date == null)
                return null;

            var duration = date.Duration;

            return date.Date
                .AddYears(duration.Years)
                .AddMonths(duration.Months)
                .AddDays(duration.Days)
                .AddHours(duration.Hours)
                .AddMinutes(duration.Minutes);
        }
    }

    public class TimeDimensionExtendedProperties
    {
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }

        public TimeDimensionExtendedProperties(string sdmxTime, string reportingYearStartDateString)
        {
            (PeriodStart, PeriodEnd) = DateUtils.GetPeriod(sdmxTime, reportingYearStartDateString);
        }
    }
}
