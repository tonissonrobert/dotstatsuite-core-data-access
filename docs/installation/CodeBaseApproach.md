# DotStatSuiteCore Data / Common Database - Source Installation
The following installation description represents the steps to initialize and upgrade a DotStatSuiteCore Data / Common database. The data database is used for the data storage of a single dataspace, the common database holds common information among the databases [**See more**](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/blob/develop/README.md)

## Prerequisites
* **SQL server 2017** or higher
* SQL authentication enabled
* Named pipe enable 
* SQL browser service running
* SQL Server Agent running
* **Visual Studio 2022** or higher
* **Microsoft SQL server management studio** (Optional)

## Installation
The *DotStat.DbUp.csproj* project is a .net core 6.0 console application managing the database initialization and upgrades using [**DbUp**]([https://www.nuget.org/packages/dbup-sqlserver/](https://www.nuget.org/packages/dbup-sqlserver/)). It is part of the *DotStat.DataAccess.sln* solution along with a separate project (*DotStat.DbUp.MsSql.csproj*) which stores the embedded SQL scripts corresponding to MS SQL Server.

The tool has the following parameters:
* **executed**: List already executed scripts 
e.g.: DotStat.DbUp.exe executed --connectionString "Server=.; Database=MyDataDb; Trusted_connection=true" --dataDb
  * *connectionString*: Required. The connection string for the database
  * *executionTimeout*: (Default: 30) The execution timeout in minutes
  * *commonDb*: Required. The type of the database, can be either --commonDb or --dataDb or --mappingStoreDb
  * *dataDb*: Required. The type of the database, can be either --commonDb or --dataDb or --mappingStoreDb
  * *mappingStoreDb*: Required. The type of the database, can be either --commonDb or --dataDb or --mappingStoreDb
* **tobeexecuted**: List scripts which will be executed
e.g.: DotStat.DbUp.exe tobeexecuted --connectionString "Server=.; Database=MyDataDb; Trusted_connection=true" --dataDb
  * *connectionString*: Required. The connection string for the database
  * *executionTimeout*: (Default: 30) The execution timeout in minutes
  * *commonDb*: Required. The type of the database, can be either --commonDb or --dataDb or --mappingStoreDb
  * *dataDb*: Required. The type of the database, can be either --commonDb or --dataDb or --mappingStoreDb
  * *mappingStoreDb*: Required. The type of the database, can be either --commonDb or --dataDb or --mappingStoreDb
* **upgrade**: Initialize / upgrade the specified database
e.g.: DotStat.DbUp.exe upgrade --connectionString "Server=.; Database=MyDataDb; Trusted_connection=true" --dataDb --loginName dataWriter --loginPwd 1234abcd!@#$ --force
  * *force*: (Default: false) Forces execution of upgrade without asking user confirmation
  * *loginName*: (Default: dataWriter) The name of the login
  * *loginPwd*: (Default: 1234abcd!@#$) The password of the login
  * *ROloginName*: (Default: \<empty\>) The name of the *optional* read-only login. When not provided, no read-only user is created.
  * *ROloginPwd*: (Default: 1234abcd!@#$) The password of the *optional* read-only login
  * *alterPassword*: (Default: false) Changes the passwords of existing *loginName* and *ROloginName* logins in database
  * *dropDb*: (Default: false) Drops the database before creating it
  * *connectionString*: Required. The connection string for the database
  * *executionTimeout*: (Default: 30) The execution timeout in minutes
  * *withoutDbaScripts*: (Default: false) The DBA scripts are not executed: it will not create logins / users, won't put the DB in restricted mode while running
  * *commonDb*: Required. The type of the database, can be either --commonDb or --dataDb or --mappingStoreDb
  * *dataDb*: Required. The type of the database, can be either --commonDb or --dataDb or --mappingStoreDb
  * *mappingStoreDb*: Required. The type of the database, can be either --commonDb or --dataDb or --mappingStoreDb
  * *googleProjectId*: (Default: \<empty\>) Google.Cloud logging [projectId](https://cloud.google.com/dotnet/docs/reference/Google.Cloud.Logging.Log4Net/latest/configuration#projectid)
  * *googleLogId*: (Default: DbUp) Google.Cloud logging [logId](https://cloud.google.com/dotnet/docs/reference/Google.Cloud.Logging.Log4Net/latest/configuration#logid)
* **help** Display more information on a specific command.

The database is automatically created if it does not exists, a table called *dbo.SchemaVersions* is used to track the executed SQL scripts.

The *mappingStoreDb* option is only added to be able to generate the mappingstore database along with the user and login as the [**tool provided by Eurostat**](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/blob/develop/docs/installation/CodeBaseApproach.md#databases-setup) does not have this feature yet.

The version of the database is stored in the *dbo.DB_VERSION* table.

**The database name and user login information** corresponds to the `DotStatSuiteCoreDataDbConnectionString` configuration node, used in the [**dotstatsuite-core-transfer service**](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer) and the [**dotstatsuite-core-sdmxri-nsi-plugin**](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin)
The login name and password should be set when the tool is executed for the first time with the upgrade option.

## Google logging (Optional)
Optionally, it's possible to log activity to [Google.Gloud](https://cloud.google.com/logging/docs) by providing *googleProjectId* option in *upgrade* action with existing project ID in Google Cloud Platform.

**NB!** If a tool is executed otside of Google Cloud Platform there should be environment variable *GOOGLE_APPLICATION_CREDENTIALS* with a path to service account JSON file for an authentication, see more [here](https://cloud.google.com/dotnet/docs/reference/Google.Cloud.Logging.Log4Net/latest/#authentication)

## Access rights
The user used in the connectionString parameter of the tool has to be either sysadmin or needs to have to following rights:

### Common and Data DBs
```sql
USE [master]
GO
GRANT CREATE DATABASE TO [yourUser]
GO
GRANT ALTER ANY LOGIN TO [yourUser]
GO
```

### Data DBs
```sql
USE [msdb]
GO
GRANT SELECT ON [dbo].[sysjobs] TO [yourUser]
GO
GRANT EXECUTE ON [dbo].[sp_add_job] TO [yourUser]
GO
GRANT EXECUTE ON [dbo].[sp_add_jobserver] TO [yourUser]
GO
GRANT EXECUTE ON [dbo].[sp_attach_schedule] TO [yourUser]
GO
GRANT EXECUTE ON [dbo].[sp_add_jobstep] TO [yourUser]
GO
GRANT EXECUTE ON [dbo].[sp_add_schedule] TO [yourUser]
GO
GRANT EXECUTE ON [dbo].[sp_add_jobschedule] TO [yourUser]
GO
```
