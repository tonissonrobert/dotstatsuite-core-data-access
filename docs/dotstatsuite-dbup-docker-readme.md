# About this Image

The official DbUp tool image of SIS-CC .Stat Suite, configured to be ready for using in a [.Stat Suite](https://sis-cc.gitlab.io/dotstatsuite-documentation/) installation. 
This tool is used to initialize and/or update the dotstatsuite-data and the dotstatsuite-common database.
It can also be used to initialize an empty MappingStore database.

Source repository: https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access

# Environment Variables
*  ***EXECUTION_PARAMETERS***.- Single environment variable, which takes the value of a string containing the execution options.

For a complete list of supported execution options, please see the installation section of the documentation: https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/blob/develop/docs/installation/CodeBaseApproach.md?ref_type=heads#installation
Please note, that these options are not mapped to individual environment variables, therefore this container takes a single environment variable (*EXECUTION_PARAMETERS*) where all these options can be included in a single value.

# How to use this image
Start dotstatsuite-dbup instance using the latest release. 

IMPORTANT NOTE: If you are using PowerShell on Windows to run these commands use double quotes instead of single quotes.

Examples:

**Update a dotstatsuite-data database**
```
docker run -e 'EXECUTION_PARAMETERS=upgrade --connectionString Server=db;Database=DesignDataDb;User=sa;Password=yourStrong(!)Password; --dataDb --loginName testLogin --loginPwd testLogin(!)Password --force' -d siscc/dotstatsuite-dbup:master
```
**Reinitialize a dotstatsuite-data database**
```
docker run -e 'EXECUTION_PARAMETERS=upgrade --connectionString Server=db;Database=DesignDataDb;User=sa;Password=yourStrong(!)Password; --dataDb --loginName testLogin --loginPwd testLogin(!)Password --force --dropDb' -d siscc/dotstatsuite-dbup:master
```


**Update a dotstatsuite-common database**
```
docker run -e 'EXECUTION_PARAMETERS=upgrade --connectionString Server=db;Database=commonDb;User=sa;Password=yourStrong(!)Password; --commonDb --loginName testLogin --loginPwd testLogin(!)Password --force' -d siscc/dotstatsuite-dbup:master
```
**Reinitialize a dotstatsuite-common database**
```
docker run -e 'EXECUTION_PARAMETERS=upgrade --connectionString Server=db;Database=commonDb;User=sa;Password=yourStrong(!)Password; --commonDb --loginName testLogin --loginPwd testLogin(!)Password --force --dropDb' -d siscc/dotstatsuite-dbup:master
```

**Initialize a mappingStore database**
> Please note, that this option is used only to create an empty database and a login/database user. To initialize or update the actual mapping store database objects, the [MAAPI tool](https://gitlab.com/sis-cc/eurostat-sdmx-ri/maapi.net.mirrored/-/tree/master/src/Estat.Sri.Mapping.Tool?ref_type=heads) has to be runned against the it. E.g. the [siscc/sdmxri-nsi-maapi](https://hub.docker.com/r/siscc/sdmxri-nsi-maapi) image having the MAAPI tool integrated can serve this purpose.

```
docker run -e 'EXECUTION_PARAMETERS=upgrade --connectionString Server=db;Database=mappingStoreDb;User=sa;Password=yourStrong(!)Password; --mappingStoreDb--loginName testLogin --loginPwd testLogin(!)Password --force --dropDb' -d siscc/dotstatsuite-dbup:master
```


