﻿using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Db.Engine.SqlServer;
using DotStat.Db.Util;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Engine
{
    [TestFixture, Parallelizable(ParallelScope.Fixtures)]
    public class SqlDimensionEngineTests : BaseDbIntegrationTests
    {
        private SqlDimensionEngine _engine;
        private SqlDotStatDb _db;
        private Domain.Dataflow _dataflow;

        public SqlDimensionEngineTests() : base(@"sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml")
        {
            _engine = new SqlDimensionEngine(Configuration, new DataTypeEnumHelper(DotStatDb));
            _dataflow = this.GetDataflow();
            _db = DotStatDb;

            InitDataForTests();
        }

        private void InitDataForTests()
        {
            _dataflow.Dsd.DbId = 5;

            var i = 100;

            foreach (var dim in _dataflow.Dsd.Dimensions)
                dim.DbId = i++;

            foreach (var attr in _dataflow.Dsd.Attributes)
                attr.DbId = i++;

            _dataflow.Dsd.PrimaryMeasure.DbId = i;
        }

        [Test, Order(1)]
        public async Task No_Artifact_Record_Should_Exist()
        {
            foreach(var dim in _dataflow.Dsd.Dimensions)
                Assert.AreEqual(-1, await _engine.GetDbId(dim.Code, _dataflow.Dsd.DbId, _db, CancellationToken));
        }

        [Test, Order(2)]
        public async Task Insert_Artifact()
        {
            foreach (var dim in _dataflow.Dsd.Dimensions)
                Assert.IsTrue((dim.DbId = await _engine.InsertToComponentTable(dim, _db, CancellationToken)) > 0);
        }

        [Test, Order(3)]
        public async Task CleanUp()
        {
            foreach (var dim in _dataflow.Dsd.Dimensions)
            {
                Assert.IsTrue(await _engine.GetDbId(dim.Code, _dataflow.Dsd.DbId, _db, CancellationToken) > 0);

                await _engine.CleanUp(dim, _db, CancellationToken);

                Assert.AreEqual(-1, await _engine.GetDbId(dim.Code, _dataflow.Dsd.DbId, _db, CancellationToken));
            }
        }
    }
}
