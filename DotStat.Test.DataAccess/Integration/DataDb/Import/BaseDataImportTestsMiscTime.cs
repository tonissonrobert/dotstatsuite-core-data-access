﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;

namespace DotStat.Test.DataAccess.Integration.DataDb.Import
{
    public abstract class BaseDataImportTestsMiscTime : BaseDbIntegrationTests
    {
        protected readonly Dataflow Dataflow;

        protected BaseDataImportTestsMiscTime(string dfPath = null) : base(dfPath)
        {
            Dataflow = this.GetDataflow();
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(Dataflow.Dsd);
        }

        protected async Task Import(bool isTimeAtTimeDimensionSupported, bool fullValidation, CancellationToken cancellationToken, int expectedNoOfValidationErrors = 0)
        {
            var tableVersion = DbTableVersion.A;
            const string dataSource = "testFile.csv";
            
            var targetVersion = TargetVersion.Live;

            Dataflow.Base.Annotations.Add(null);
            
            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, Dataflow.Dsd.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, cancellationToken);

            // create dataflow DB objects 
            var tryNewTransactionResult =
                await DotStatDbService.TryNewTransaction(transaction, Dataflow, MsAccess, cancellationToken);
            Assert.IsTrue(tryNewTransactionResult, "Creation of the transaction failed.");

            // generate observations
            var observations = GetObservations(StagingRowActionEnum.Merge);
            var observationsList = await observations.ToListAsync(cancellationToken);
            var obsCount = observationsList.Count;

            var reportedComponents = GetReportedComponents(Dataflow);

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(Dataflow.Dsd, reportedComponents, isTimeAtTimeDimensionSupported, cancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(Dataflow, cancellationToken);
            var bulkImportResult= await UnitOfWork.DataStoreRepository.BulkInsertData(observations, reportedComponents, codeTranslator, Dataflow,
                fullValidation, isTimeAtTimeDimensionSupported, false, cancellationToken);

            Assert.AreEqual(expectedNoOfValidationErrors, bulkImportResult.Errors.Count, $"No of bulk insert errors. \n [{ string.Join("\n", bulkImportResult.Errors) }]");

            // merge series
            var importSumary = await UnitOfWork.DataStoreRepository.MergeStagingTable(Dataflow, reportedComponents,
                bulkImportResult.BatchActions, bulkImportResult.DataSetLevelAttributeRows, codeTranslator, tableVersion, fullValidation, CancellationToken);

            if (fullValidation)
            {
                Assert.AreEqual(0, importSumary.Errors.Count, "No of merge errors.");
                Assert.AreEqual(obsCount - expectedNoOfValidationErrors, importSumary.ObservationLevelMergeResult.TotalCount,
                    "Count of observations written.");
                Assert.AreEqual(obsCount - expectedNoOfValidationErrors, importSumary.ObservationLevelMergeResult.InsertCount,
                    "Count of observations inserted.");
                Assert.AreEqual(0, importSumary.ObservationLevelMergeResult.DeleteCount, "Count of observations deleted.");
                Assert.AreEqual(0, importSumary.ObservationLevelMergeResult.UpdateCount, "Count of observations updated.");
            }
            else
            {
                Assert.AreEqual(0, importSumary.ObservationLevelMergeResult.Errors.Count, "No of merge errors.");
                Assert.AreEqual(0, importSumary.ObservationLevelMergeResult.TotalCount, "Count of observations written.");
                Assert.AreEqual(0, importSumary.ObservationLevelMergeResult.InsertCount, "Count of observations inserted.");
                Assert.AreEqual(0, importSumary.ObservationLevelMergeResult.DeleteCount, "Count of observations deleted.");
                Assert.AreEqual(0, importSumary.ObservationLevelMergeResult.UpdateCount, "Count of observations updated.");
            }

            // close transaction
            Dataflow.Dsd.LiveVersion = (char?) tableVersion;
            var success = await DotStatDbService.CloseTransaction(transaction, Dataflow, false, bulkImportResult.RowsCopied > 0, true,
                MsAccess, cancellationToken);
            Assert.IsTrue(success);

            // --------------------------------------------------------------------------

            // check view is created & if observations are returned
            var view = Dataflow.Dsd.SqlDataDsdViewName((char)tableVersion);
            Assert.IsTrue(await DotStatDb.ViewExists(view, cancellationToken));

            var dbObsCount = (int)(await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}",cancellationToken));
            Assert.AreEqual(obsCount - expectedNoOfValidationErrors, dbObsCount);

            // ---------------------------------
            
            var dataQueryStr = "....../?startPeriod=1900&endPeriod=2200";

            var dataQuery = new DataQueryImpl(
                new RESTDataQueryCore($"data/{Dataflow.AgencyId},{Dataflow.Code},{Dataflow.Version}/{dataQueryStr}"),
                new InMemoryRetrievalManager(new SdmxObjectsImpl(Dataflow.Dsd.Base, Dataflow.Base)));

            var dbObservations = await UnitOfWork.ObservationRepository.GetObservations(
                    dataQuery,
                    Dataflow,
                    tableVersion,
                    isDataQuery: true,
                    cancellationToken
                )
                .ToListAsync(cancellationToken);

            //TODO: change back when proper validation of fractional seconds are implemented in .Stat instead of SdmxSource
            //Assert.AreEqual(dbObsCount - 2, dbObservations.Length);  
            Assert.AreEqual(dbObsCount - 2 - (fullValidation && !isTimeAtTimeDimensionSupported ? 0 : 1), dbObservations.Count);  

            Assert.AreEqual(Dataflow.Dimensions.Count(x => !x.Base.TimeDimension), observationsList[0].Observation.SeriesKey.Key.Count);

            dataQueryStr = "....../?startPeriod=1000&endPeriod=1100";

            dataQuery = new DataQueryImpl(
                new RESTDataQueryCore($"data/{Dataflow.AgencyId},{Dataflow.Code},{Dataflow.Version}/{dataQueryStr}"),
                new InMemoryRetrievalManager(new SdmxObjectsImpl(Dataflow.Dsd.Base, Dataflow.Base)));

            dbObservations = await UnitOfWork.ObservationRepository.GetObservations(
                    dataQuery,
                    Dataflow,
                    tableVersion,
                    isDataQuery: true,
                    cancellationToken
                )
                .ToListAsync(cancellationToken);

            Assert.AreEqual(2, dbObservations.Count);
        }

        /// <summary>
        ///  runs only after Import Test
        /// </summary>
        protected async Task CopyToNewVersion(CancellationToken cancellationToken)
        {
            var sourceVersion = DbTableVersion.A;
            var targetVersion = DbTableVersion.B;

            await UnitOfWork.DataStoreRepository.DeleteData(Dataflow.Dsd, targetVersion, cancellationToken);
            await UnitOfWork.DataStoreRepository.CopyDataToNewVersion(Dataflow.Dsd, sourceVersion, targetVersion, cancellationToken);
            await UnitOfWork.DataStoreRepository.CopyAttributesToNewVersion(Dataflow.Dsd, sourceVersion, targetVersion, cancellationToken);

            var view = Dataflow.Dsd.SqlDataDsdViewName((char)targetVersion);

            var dbObsCount = (int) await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", cancellationToken);
            Assert.IsTrue(dbObsCount > 0);
        }

        protected async Task CheckSupportOfTimeAtTimeDimension(bool expectedResult, CancellationToken cancellationToken)
        {
            Assert.AreEqual(expectedResult, await UnitOfWork.ArtefactRepository.CheckSupportOfTimeAtTimeDimension(Dataflow.Dsd, DbTableVersion.A, cancellationToken,"PERIOD_START"));

            Assert.AreEqual(expectedResult, await UnitOfWork.ArtefactRepository.CheckSupportOfTimeAtTimeDimension(Dataflow.Dsd, DbTableVersion.A, cancellationToken,"PERIOD_END"));
        }

        private async IAsyncEnumerable<ObservationRow> GetObservations(
            StagingRowActionEnum action,
            [EnumeratorCancellation] CancellationToken cancellationToken = default)
        {
            var seriesKey = new KeyableImpl(
                Dataflow.Base,
                Dataflow.Dsd.Base,
                new List<IKeyValue>
                {
                    new KeyValueImpl("1", "MEASURE"),
                    new KeyValueImpl("THRPEB", "INDEX"),
                    new KeyValueImpl("1", "SECTOR"),
                    new KeyValueImpl("B", "INDUSTRY"),
                    new KeyValueImpl("AUS", "REGION"),
                    new KeyValueImpl("10", "TSEST"),
                    new KeyValueImpl("Q", "FREQUENCY"),
                },
                null
            );

            var mandatoryAttributes = new IKeyValue[]
            {              
                new KeyValueImpl("AUD", "UNIT_MEASURE")
            };
            var attributes = new IKeyValue[]
            {
                new KeyValueImpl("--07-01", "REPORTING_YEAR_START_DAY"),                //Mandatory attributes
                new KeyValueImpl("AUD", "UNIT_MEASURE")
            };

            var observations = new List<IObservation>
            {
                new ObservationImpl(seriesKey, "1997-A1", "1", attributes),
                new ObservationImpl(seriesKey, "2011-W35", "2", attributes),
                new ObservationImpl(seriesKey, "1997-Q1", "3", attributes),
                new ObservationImpl(seriesKey, "1997-Q2", "4", attributes),
                new ObservationImpl(seriesKey, "1997-Q3", "5", attributes),
                new ObservationImpl(seriesKey, "1997-Q4", "6", attributes),
                new ObservationImpl(seriesKey, "1997", "7", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2000-S2", "8", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2000-T1", "9", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2020-01", "10", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2020-12", "11", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2021-M01", "12", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2021-M12", "13", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2011-W01", "14", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2011-W36", "15", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2011-W52", "16", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2021-06-01", "17", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2021-06-02T12:00:00", "18", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2021-06-03T13:30:00", "19", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2021-D004", "20", mandatoryAttributes),
                new ObservationImpl(seriesKey, "2000-01-01T00:00:00/P5Y", "21", mandatoryAttributes),
                // Pre-1753 values
                new ObservationImpl(seriesKey, "1098-Q1", "22", attributes),
                new ObservationImpl(seriesKey, "1098-Q2", "23", attributes),
                new ObservationImpl(seriesKey, "1222-02-22T22:20:02.202", "24", mandatoryAttributes),
                new ObservationImpl(seriesKey, "Invalid time dimension", "25", mandatoryAttributes)
            };

            foreach (var observation in observations)
            {
                cancellationToken.ThrowIfCancellationRequested();
                yield return new ObservationRow(1, action, observation, false);
            }
        }
    }
}
