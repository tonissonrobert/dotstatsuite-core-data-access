﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Import
{
    [NonParallelizable]
    [TestFixture("sdmx/CsvV2.xml", false)]
    [TestFixture("sdmx/CsvV2.xml", true)]
    [TestFixture("sdmx/TEST_CODELESS_DS.xml", false)]
    [TestFixture("sdmx/TEST_CODELESS_DS.xml", true)]
    public sealed class MetadataImportDsdTests : BaseDbIntegrationTests
    {
        private readonly Dsd _dsd;

        public MetadataImportDsdTests(string structure, bool useCompression) : base(structure)
        {
            _dsd = this.GetDataflow().Dsd;
            base.SetArchive( useCompression);
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_dsd);
        }

        [Test, Order(1)]
        public async Task Import()
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";
            
            var reportedComponents = new ReportedComponents
            {
                Dimensions = _dsd.Dimensions.ToList(),
                MetadataAttributes = _dsd.Msd.MetadataAttributes.ToList(),
                IsPrimaryMeasureReported = false,
                TimeDimension = _dsd.TimeDimension
            };
            
            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction= await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dsd.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);

            // create dataflow DB objects 
            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dsd, MsAccess, CancellationToken);
            Assert.IsTrue(tryNewTransactionResult);

            //Check compression
            var indexCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(name) FROM sys.indexes WHERE object_id = OBJECT_ID('[{DotStatDb.DataSchema}].[{_dsd.SqlMetadataDataStructureTable((char)tableVersion)}]') AND [name] IS NOT NULL", CancellationToken);
            var expectedIndexesAndConstraints = DotStatDb.DataSpace.Archive ? 1 : 4;
            Assert.AreEqual(1, indexCount);

            // generate observations
            var obsCount = 50;
            var observations = ObservationGenerator.Generate(
                    _dsd,
                    null,
                    false,
                    2018,
                    2020,
                    obsCount,
                    action: StagingRowActionEnum.Merge
                );

            Assert.AreEqual(obsCount, await observations.CountAsync());

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.MetadataStoreRepository.RecreateMetadataStagingTables(_dsd, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dsd, CancellationToken);

            var bulkImportResult = await UnitOfWork.MetadataStoreRepository.BulkInsertMetadata(observations, reportedComponents, codeTranslator, _dsd,
                fullValidation, isTimeAtTimeDimensionSupported, CancellationToken);
            Assert.AreEqual(0, bulkImportResult.Errors.Count, "No of errors during bulk insert.");

            if (DotStatDb.DataSpace.Archive)
            {
                Assert.DoesNotThrowAsync(() => UnitOfWork.MetadataStoreRepository.AddUniqueIndexMetadataStagingTable(_dsd, CancellationToken));
            }
            // merge from staging
            var importSummary = await UnitOfWork.MetadataStoreRepository.MergeStagingTable(_dsd, reportedComponents,
                bulkImportResult.BatchActions, bulkImportResult.DataSetLevelAttributeRows, codeTranslator, tableVersion, true, CancellationToken);
            Assert.AreEqual(0, importSummary.Errors.Count, "No of errors during merging to fact data.");
            Assert.AreEqual(obsCount, importSummary.ObservationLevelMergeResult.TotalCount);
            // --------------------------------------------------------------------------
            
            Assert.DoesNotThrowAsync(() => UnitOfWork.MetadataStoreRepository.ReorganizeColumnStoreIndexes(_dsd, (char)tableVersion, CancellationToken));
            
            // check view is created & if observations are returned
            var view = _dsd.SqlMetaDataDsdViewName((char) tableVersion);
            Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));


            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.AreEqual(obsCount, dbObsCount);

            // close transaction
            _dsd.LiveVersion = (char?) tableVersion;
            var success = await DotStatDbService.CloseTransaction(transaction, _dsd,false, obsCount>0, true,
                MsAccess, CancellationToken);
            Assert.IsTrue(success);

        }

        /// <summary>
        ///  runs only after Import Test
        /// </summary>
        [Test, Order(2)]
        public async Task CopyToNewVersion()
        {
            var sourceVersion = DbTableVersion.A;
            var destinationVersion = DbTableVersion.B;
            var targetVersion = TargetVersion.PointInTime;
            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dsd.FullId, null, null, "file.csv", TransactionType.Import, targetVersion, serviceId: null, CancellationToken);

            await DotStatDbService.DeleteMetadata(_dsd, destinationVersion, MsAccess, CancellationToken);
            await DotStatDbService.CopyMetadataToNewVersion(_dsd, sourceVersion, destinationVersion, MsAccess, CancellationToken);

            var view = _dsd.SqlMetaDataDsdViewName((char) destinationVersion);

            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.IsTrue(dbObsCount > 0);
        }
    }
}
