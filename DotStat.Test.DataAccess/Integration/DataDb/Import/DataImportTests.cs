﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;

namespace DotStat.Test.DataAccess.Integration.DataDb.Import
{
    [NonParallelizable]
    [TestFixture("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml", "........./?startPeriod=1900&endPeriod=2079-01", true)]
    [TestFixture("sdmx/KH_NIS,DF_AGRI_NO_TIME,2.0.xml", "........./?startPeriod=1900&endPeriod=2079-01", false)]
    [TestFixture("sdmx/TEST_CODELESS_DS.xml", "all/?startPeriod=1900&endPeriod=2079-01", true)]
    [TestFixture("sdmx/TEST_CODELESS_DS.xml", "all/?startPeriod=1900&endPeriod=2079-01", false)]
    [TestFixture("sdmx/TEST_33_Dimensions_1_NonCoded.xml", "all/?startPeriod=1900&endPeriod=2079-01", false)]
    public sealed class DataImportTests : BaseDbIntegrationTests
    {
        private readonly Dataflow _dataflow;
        private readonly string _dataQuery;

        public DataImportTests(string structure, string dataQuery, bool useCompression) : base(structure)
        {
            _dataflow = this.GetDataflow();
            _dataQuery = dataQuery;
            base.SetArchive(useCompression);
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_dataflow.Dsd);
        }

        [Test, Order(1)]
        public async Task Merge()
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";

            var reportedComponents = GetReportedComponents(_dataflow);

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);
            // create dataflow DB objects 
            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken);
            Assert.IsTrue(tryNewTransactionResult);
            
            //Check compression
            var indexCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(name) FROM sys.indexes WHERE object_id = OBJECT_ID('[{DotStatDb.DataSchema}].[{_dataflow.Dsd.SqlFactTable((char)tableVersion)}]') AND [name] IS NOT NULL", CancellationToken);
            var expectedIndexesAndConstraints = DotStatDb.DataSpace.Archive 
            // Only clustered columnstore index
                    ? 1 
                    : _dataflow.Dsd.TimeDimension == null 
            // Only PrimaryKey
                        ? 1 
            // PrimaryKey and non clustered columnstore index on time dimension
                        : 2;
            Assert.AreEqual(expectedIndexesAndConstraints, indexCount);

            // generate observations
            var obsCount = 1000;
            var observations = ObservationGenerator.Generate(
                _dataflow.Dsd,
                _dataflow,
                true,
                1980,
                2020,
                obsCount,
                action: StagingRowActionEnum.Merge
            );
            Assert.AreEqual(obsCount, await observations.CountAsync(CancellationToken));

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(_dataflow.Dsd, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);

            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observations, reportedComponents, codeTranslator, _dataflow,
                fullValidation, isTimeAtTimeDimensionSupported, false, CancellationToken);

            Assert.AreEqual(0, bulkImportResult.Errors.Count, "No of errors during bulk insert.");

            if(DotStatDb.DataSpace.Archive)
            {
                Assert.DoesNotThrowAsync(()=> UnitOfWork.DataStoreRepository.AddUniqueIndexStagingTable(_dataflow.Dsd, reportedComponents, CancellationToken));
            }

            // merge from staging
            var importSummary = await UnitOfWork.DataStoreRepository.MergeStagingTable(_dataflow, reportedComponents,
                bulkImportResult.BatchActions, bulkImportResult.DataSetLevelAttributeRows, codeTranslator, tableVersion, fullValidation, CancellationToken);
            importSummary.ObservationsCount = bulkImportResult.RowsCopied;

            Assert.AreEqual(0, importSummary.Errors.Count, "No of errors during merging to fact data.");
            Assert.AreEqual(obsCount, importSummary.ObservationsCount);
            // --------------------------------------------------------------------------

            Assert.DoesNotThrowAsync(() => UnitOfWork.DataStoreRepository.ReorganizeColumnStoreIndexes(_dataflow.Dsd, (char)tableVersion, CancellationToken));

            // check view is created & if observations are returned
            var view = _dataflow.Dsd.SqlDataDsdViewName((char) tableVersion);
            Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));


            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.AreEqual(obsCount, dbObsCount);

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?) tableVersion;
             var success = await DotStatDbService.CloseTransaction(transaction, _dataflow, false, obsCount>0, true,
                MsAccess, CancellationToken);

            Assert.IsTrue(success);

            // ---------------------------------
            
            //var dataQuery = new DataQueryImpl(
            //    new RESTDataQueryCore($"data/{_dataflow.AgencyId},{_dataflow.Code},{_dataflow.Version}/{_dataQuery}"),
            //    new InMemoryRetrievalManager(new SdmxObjectsImpl(_dataflow.Dsd.Base, _dataflow.Base)));

            //var dbObservations = await UnitOfWork.ObservationRepository.GetDataObservations(
            //        dataQuery,
            //        _dataflow,
            //        tableVersion,
            //        CancellationToken
            //    )
            //    .ToArrayAsync(CancellationToken);

            //Assert.AreEqual(dbObsCount, dbObservations.Length);
            //Assert.AreEqual(_dataflow.Dimensions.Count(x => !x.Base.TimeDimension),
            //    (await observations.FirstAsync(CancellationToken)).Observation.SeriesKey.Key.Count);
        }

        /// <summary>
        ///  runs only after Import Test
        /// </summary>
        [Test, Order(2)]
        public async Task CopyToNewVersion()
        {
            var sourceTableVersion = DbTableVersion.A;
            var targetTableVersion = DbTableVersion.B;
            var targetVersion = TargetVersion.PointInTime;

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.Dsd.FullId, null, null, "file.csv", TransactionType.Import, targetVersion, serviceId: null, CancellationToken);

            await UnitOfWork.DataStoreRepository.DeleteData(_dataflow.Dsd, targetTableVersion, CancellationToken); 

            await UnitOfWork.DataStoreRepository.CopyDataToNewVersion(_dataflow.Dsd, sourceTableVersion, targetTableVersion, CancellationToken);
            await UnitOfWork.DataStoreRepository.CopyAttributesToNewVersion(_dataflow.Dsd, sourceTableVersion, targetTableVersion, CancellationToken);

            var view = _dataflow.Dsd.SqlDataDsdViewName((char)targetTableVersion);

            var dbObsCount = (int) await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.IsTrue(dbObsCount > 0);
        }

        [Test, Order(3)]
        public async Task Delete()
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";

            var reportedComponents = GetReportedComponents(_dataflow);

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);
            // create dataflow DB objects 
            var tryNewTransactionResult =
                await DotStatDbService.TryNewTransaction(transaction, _dataflow,  MsAccess, CancellationToken);
            Assert.IsTrue(tryNewTransactionResult);

            // generate observations
            var obsCount = 1000;
            var observations = ObservationGenerator.Generate(
                _dataflow.Dsd,
                _dataflow,
                true,
                1980,
                2020,
                obsCount,
                action: StagingRowActionEnum.Delete
            );
            Assert.AreEqual(obsCount, await observations.CountAsync(CancellationToken));

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(_dataflow.Dsd, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);

            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observations, reportedComponents, codeTranslator, _dataflow,
                fullValidation, isTimeAtTimeDimensionSupported, false,CancellationToken);

            Assert.AreEqual(0, bulkImportResult.Errors.Count, "No of errors during bulk insert.");

            // merge from staging
            var importSummary = await UnitOfWork.DataStoreRepository.MergeStagingTable(_dataflow, reportedComponents,
                bulkImportResult.BatchActions, bulkImportResult.DataSetLevelAttributeRows, codeTranslator, tableVersion, fullValidation, CancellationToken);
            importSummary.ObservationsCount = bulkImportResult.RowsCopied;
            Console.WriteLine($@"
inserted:{importSummary.ObservationLevelMergeResult.InsertCount}
updated: {importSummary.ObservationLevelMergeResult.UpdateCount}
deleted:{importSummary.ObservationLevelMergeResult.DeleteCount}");

            Assert.AreEqual(0, importSummary.Errors.Count, "No of errors during merging to fact data.");
            Assert.AreEqual(obsCount, importSummary.ObservationsCount);
            // --------------------------------------------------------------------------

            // check view is created & if observations are returned
            var view = _dataflow.SqlDeletedDataViewName((char)tableVersion);
            Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));


            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.AreEqual(dbObsCount, obsCount);

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
            var success = await DotStatDbService.CloseTransaction(transaction, _dataflow, false, obsCount > 0, true,
                MsAccess, CancellationToken);

            Assert.IsTrue(success);

            // ---------------------------------

            var dataQuery = new DataQueryImpl(
                new RESTDataQueryCore($"data/{_dataflow.AgencyId},{_dataflow.Code},{_dataflow.Version}/{_dataQuery}"),
                MsAccess.GetRetrievalManager());

            var dbObservations = await UnitOfWork.ObservationRepository.GetObservations(
                    dataQuery,
                    _dataflow,
                    tableVersion,
                    isDataQuery: true,
                    CancellationToken
                )
                .ToArrayAsync(CancellationToken);

            Assert.LessOrEqual(dbObservations.Length, obsCount);
        }
        
        [Test, Order(4)]
        public async Task DeleteAll()
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";

            var reportedComponents = GetReportedComponents(_dataflow);

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);
            // create dataflow DB objects 
            var tryNewTransactionResult =
                await DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken);
            Assert.IsTrue(tryNewTransactionResult);

            // generate observations
            var observations = ObservationGenerator.Generate(
                _dataflow.Dsd,
                _dataflow,
                true,
                1980,
                2020,
                1,
                action: StagingRowActionEnum.DeleteAll
            );

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(_dataflow.Dsd, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);

            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observations, reportedComponents, codeTranslator, _dataflow,
                fullValidation, isTimeAtTimeDimensionSupported, false, CancellationToken);

            Assert.AreEqual(0, bulkImportResult.Errors.Count, "No of errors during bulk insert.");

            // merge from staging
            var importSummary = await UnitOfWork.DataStoreRepository.MergeStagingTable(_dataflow, reportedComponents,
                bulkImportResult.BatchActions, bulkImportResult.DataSetLevelAttributeRows, codeTranslator, tableVersion, fullValidation, CancellationToken);
            importSummary.ObservationsCount = bulkImportResult.RowsCopied;
          

            Assert.AreEqual(0, importSummary.Errors.Count, "No of errors during merging to fact data.");
            Assert.AreEqual(1, importSummary.ObservationsCount);
            // --------------------------------------------------------------------------

            // check view is created & if observations are returned
            var view = _dataflow.SqlDataDataFlowViewName((char)tableVersion);
            Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));

            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.AreEqual(0, dbObsCount);

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
            var success = await DotStatDbService.CloseTransaction(transaction, _dataflow, false, false, true,
                MsAccess, CancellationToken);

            Assert.IsTrue(success);

            // ---------------------------------

            var dataQuery = new DataQueryImpl(
                new RESTDataQueryCore($"data/{_dataflow.AgencyId},{_dataflow.Code},{_dataflow.Version}/{_dataQuery}"),
                MsAccess.GetRetrievalManager());

            var dbObservations = await UnitOfWork.ObservationRepository.GetObservations(
                    dataQuery,
                    _dataflow,
                    tableVersion,
                    isDataQuery: true,
                    CancellationToken
                )
                .ToArrayAsync(CancellationToken);

            Assert.AreEqual(0, dbObservations.Length);
        }

        [Test, Order(5)]
        public async Task MixActions()
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";

            var reportedComponents = GetReportedComponents(_dataflow);

            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction = await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);
            // create dataflow DB objects 
            var tryNewTransactionResult =
                await DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken);
            Assert.IsTrue(tryNewTransactionResult);

            // generate observations
            var obsCount = 1000;
            var observations = ObservationGenerator.Generate(
                _dataflow.Dsd,
                _dataflow,
                true,
                1980,
                2020,
                obsCount,
                action:StagingRowActionEnum.Merge
            );
            Assert.AreEqual(obsCount, await observations.CountAsync(CancellationToken));

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.DataStoreRepository.RecreateStagingTables(_dataflow.Dsd, reportedComponents, isTimeAtTimeDimensionSupported, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);

            var bulkImportResult = await UnitOfWork.DataStoreRepository.BulkInsertData(observations, reportedComponents, codeTranslator, _dataflow,
                fullValidation, isTimeAtTimeDimensionSupported, false, CancellationToken);

            Assert.AreEqual(0, bulkImportResult.Errors.Count, "No of errors during bulk insert.");

            // merge from staging
            var importSummary = await UnitOfWork.DataStoreRepository.MergeStagingTable(_dataflow, reportedComponents,
                bulkImportResult.BatchActions, bulkImportResult.DataSetLevelAttributeRows, codeTranslator, tableVersion, true, CancellationToken);
            importSummary.ObservationsCount = bulkImportResult.RowsCopied;

            Assert.AreEqual(0, importSummary.Errors.Count, "No of errors during merging to fact data.");
            Assert.AreEqual(obsCount, importSummary.ObservationsCount);
            // --------------------------------------------------------------------------

            // check view is created & if observations are returned
            var view = _dataflow.Dsd.SqlDataDsdViewName((char)tableVersion);
            Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));


            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.LessOrEqual(dbObsCount, obsCount);

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?)tableVersion;
            var success = await DotStatDbService.CloseTransaction(transaction, _dataflow, false, obsCount > 0, true,
                MsAccess, CancellationToken);

            Assert.IsTrue(success);

            // ---------------------------------

            var dataQuery = new DataQueryImpl(
                new RESTDataQueryCore($"data/{_dataflow.AgencyId},{_dataflow.Code},{_dataflow.Version}/{_dataQuery}"),
                MsAccess.GetRetrievalManager());

            view = _dataflow.SqlDataReplaceDataFlowViewName((char)tableVersion);
            var replaceCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
          
            var dbObservations = await UnitOfWork.ObservationRepository.GetObservations(
                    dataQuery,
                    _dataflow,
                    tableVersion,
                    isDataQuery: true,
                    CancellationToken
                )
                .ToArrayAsync(CancellationToken);

            Assert.LessOrEqual(dbObservations.Length, replaceCount);
        }
    
    }
}
