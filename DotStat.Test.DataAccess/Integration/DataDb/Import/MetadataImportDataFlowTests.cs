﻿using System.Linq;
using System.Threading.Tasks;
using DotStat.Db;
using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Import
{
    [NonParallelizable]
    [TestFixture("sdmx/CsvV2.xml", false)]
    [TestFixture("sdmx/CsvV2.xml", true)]
    [TestFixture("sdmx/TEST_CODELESS_DS.xml", false)]
    [TestFixture("sdmx/TEST_CODELESS_DS.xml", true)]
    public sealed class MetadataImportDataFlowTests : BaseDbIntegrationTests
    {
        private readonly Dataflow _dataflow;

        public MetadataImportDataFlowTests(string structure, bool useCompression) : base(structure)
        {
            _dataflow = this.GetDataflow();
            base.SetArchive( useCompression);
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_dataflow.Dsd);
        }

        [Test, Order(1)]
        public async Task Import()
        {
            var tableVersion = DbTableVersion.A;
            var targetVersion = TargetVersion.Live;
            const bool fullValidation = false;
            const bool isTimeAtTimeDimensionSupported = false;
            const string dataSource = "testFile.csv";
            
            var reportedComponents = new ReportedComponents
            {
                Dimensions = _dataflow.Dimensions.ToList(),
                MetadataAttributes = _dataflow.Dsd.Msd.MetadataAttributes.ToList(),
                IsPrimaryMeasureReported = false,
                TimeDimension = _dataflow.Dsd.TimeDimension
            };
            
            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            var transaction= await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, dataSource, TransactionType.Import, targetVersion, serviceId: null, CancellationToken);

            // create dataflow DB objects 
            var tryNewTransactionResult = await DotStatDbService.TryNewTransaction(transaction, _dataflow, MsAccess, CancellationToken);
            Assert.IsTrue(tryNewTransactionResult);

            //Check compression
            var indexCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(name) FROM sys.indexes WHERE object_id = OBJECT_ID('[{DotStatDb.DataSchema}].[{_dataflow.Dsd.SqlMetadataDataStructureTable((char)tableVersion)}]') AND [name] IS NOT NULL", CancellationToken);
            var expectedIndexesAndConstraints = DotStatDb.DataSpace.Archive ? 1 : 4;
            Assert.AreEqual(1, indexCount);

            // generate observations
            var obsCount = 50;
            var observations = ObservationGenerator.Generate(
                _dataflow.Dsd,
                    _dataflow,
                    false,
                    2018,
                    2020,
                    obsCount,
                    action: StagingRowActionEnum.Merge
                );

            Assert.AreEqual(obsCount, await observations.CountAsync());

            // ------ Logic bellow normally in SqlConsumer of Transfer service ----------

            // Wipe and/or create staging table and target tables
            await UnitOfWork.MetadataStoreRepository.RecreateMetadataStagingTables(_dataflow.Dsd, CancellationToken);

            // import to staging
            var codeTranslator = new CodeTranslator(UnitOfWork.CodeListRepository);
            await codeTranslator.FillDict(_dataflow, CancellationToken);

            var bulkImportResult = await UnitOfWork.MetadataStoreRepository.BulkInsertMetadata(observations, reportedComponents, codeTranslator, _dataflow.Dsd,
                fullValidation, isTimeAtTimeDimensionSupported, CancellationToken);
            Assert.AreEqual(0, bulkImportResult.Errors.Count, "No of errors during bulk insert.");

            if (DotStatDb.DataSpace.Archive)
            {
                Assert.DoesNotThrowAsync(() => UnitOfWork.MetadataStoreRepository.AddUniqueIndexMetadataStagingTable(_dataflow.Dsd, CancellationToken));
            }
            // merge from staging
            var importSummary = await UnitOfWork.MetadataStoreRepository.MergeStagingTable(_dataflow, reportedComponents,
                bulkImportResult.BatchActions, bulkImportResult.DataSetLevelAttributeRows, codeTranslator, tableVersion, true, CancellationToken);
            Assert.AreEqual(0, importSummary.Errors.Count, "No of errors during merging to fact data.");
            Assert.AreEqual(obsCount, importSummary.ObservationLevelMergeResult.TotalCount);
            // --------------------------------------------------------------------------

            Assert.DoesNotThrowAsync(() => UnitOfWork.MetadataStoreRepository.ReorganizeColumnStoreIndexes(_dataflow.Dsd, (char)tableVersion, CancellationToken));
            
            // check view is created & if observations are returned
            var view = _dataflow.SqlMetadataDataFlowViewName((char) tableVersion);
            Assert.IsTrue(await DotStatDb.ViewExists(view, CancellationToken));


            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.AreEqual(obsCount, dbObsCount);

            // close transaction
            _dataflow.Dsd.LiveVersion = (char?) tableVersion;
            var success = await DotStatDbService.CloseTransaction(transaction, _dataflow,false, obsCount>0, true,
                MsAccess, CancellationToken);
            Assert.IsTrue(success);

            // ---------------------------------

            //var sqlRepo = new SqlObservationRepository(new SqlServerDbManager(Configuration, Configuration));

            //var dataQuery = new DataQueryImpl(
            //    new RESTDataQueryCore($"data/{_dataflow.AgencyId},{_dataflow.Code},{_dataflow.Version}/{_dataQuery}"),
            //    new InMemoryRetrievalManager(new SdmxObjectsImpl(_dataflow.Dsd.Base, _dataflow.Base)));

            //var dbObservations = sqlRepo.GetObservations(
            //        dataQuery,
            //        _dataflow,
            //        codeTranslator,
            //        _dataspace.Id,
            //        targetVersion
            //    )
            //    .ToArray();

            //Assert.AreEqual(dbObsCount, dbObservations.Length);
            //Assert.AreEqual(_dataflow.Dimensions.Count(x => !x.Base.TimeDimension),
            //    observations[0].SeriesKey.Key.Count);
        }

        /// <summary>
        ///  runs only after Import Test
        /// </summary>
        [Test, Order(2)]
        public async Task CopyToNewVersion()
        {
            var sourceVersion = DbTableVersion.A;
            var destinationVersion = DbTableVersion.B;
            var targetVersion = TargetVersion.PointInTime;
            var transactionId = await UnitOfWork.TransactionRepository.GetNextTransactionId(CancellationToken);
            await UnitOfWork.TransactionRepository.CreateTransactionItem(transactionId, _dataflow.FullId, null, null, "file.csv", TransactionType.Import, targetVersion, serviceId: null, CancellationToken);

            await DotStatDbService.DeleteMetadata(_dataflow.Dsd, destinationVersion, MsAccess, CancellationToken);
            await DotStatDbService.CopyMetadataToNewVersion(_dataflow.Dsd, sourceVersion, destinationVersion, MsAccess, CancellationToken);

            var view = _dataflow.SqlMetadataDataFlowViewName((char) destinationVersion);

            var dbObsCount = (int)await DotStatDb.ExecuteScalarSqlAsync($"select count(*) from [data].{view}", CancellationToken);
            Assert.IsTrue(dbObsCount > 0);
        }
    }
}
