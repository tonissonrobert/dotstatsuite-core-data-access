﻿using System.Threading.Tasks;
using DotStat.Db.Exception;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Integration.DataDb.Repository.DsdChangeTests
{
    [TestFixture, Parallelizable(ParallelScope.Children)]
    [TestFixture(false)]
    [TestFixture(true)]
    public class DimensionAttributeChangeTests : BaseDsdChangeTests
    {
        private IImportReferenceableStructure _originalStructure;
        private IImportReferenceableStructure _originalStructure4NewCodeTest;
        private readonly bool _useDsdAsReferencedStructure;

        public DimensionAttributeChangeTests(bool useDsdAsReferencedStructure)
        {
            _useDsdAsReferencedStructure = useDsdAsReferencedStructure;
        }

        [OneTimeSetUp]
        public async Task SetUp()
        {
            _originalStructure = await InitOriginalStructure("ATTR_TYPE_DIFF_TEST.xml");
            _originalStructure4NewCodeTest = await InitOriginalStructure("ATTR_TYPE_DIFF_TEST_DIM_2.xml");
        }

        [OneTimeTearDown]
        public async Task TearDown()
        {
            await CleanUpStructure(_originalStructure);
            await CleanUpStructure(_originalStructure4NewCodeTest);
        }

        [Test]
        public void DimensionAttributeAddedTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_ATTR_ADDED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_NEW", ex.Message);
        }

        [Test]
        public void DimensionAttributeRemovedTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_ATTR_REMOVED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1)", ex.Message);
            StringAssert.Contains("ATTR_DIM_STRING", ex.Message);
        }

        [Test]
        public void DimensionAttributeCodedChangedToNonCodedTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_ENUMERATED_TO_STRING.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_ENUMERATED", ex.Message);
        }

        [Test]
        public void DimensionAttributeNonCodedChangedToCodedTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_STRING_TO_ENUMERATED.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_STRING", ex.Message);
        }

        [Test]
        public void DimensionAttributeMandatoryToConditional()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_MANDATORY_TO_CONDITIONAL.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_ENUMERATED", ex.Message);
            StringAssert.Contains("from Mandatory to Conditional", ex.Message);
        }

        [Test]
        public void DimensionAttributeConditionalToMandatory()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_CONDITIONAL_TO_MANDATORY.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_STRING", ex.Message);
            StringAssert.Contains("Conditional to Mandatory", ex.Message);
        }

        [Test]
        public void DimensionAttributeChangedToDatasetLevelTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_TO_DATASET.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_STRING", ex.Message);
            StringAssert.Contains("from DimensionGroup to DataSet", ex.Message);
        }

        [Test]
        public void DimensionAttributeChangedToObservationLevelTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_TO_OBSERVATION.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_STRING", ex.Message);
            StringAssert.Contains("from DimensionGroup to Observation", ex.Message);
        }

        [Test]
        public void DimensionAttributeChangedToGroupLevelTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_TO_GROUP.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_STRING", ex.Message);
            StringAssert.Contains("from DimensionGroup to Group", ex.Message);
        }

        [Test]
        public void DimensionAttributeCodelistChangeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_NEW_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_ENUMERATED", ex.Message);
            StringAssert.Contains("OECD:CL_FREQ(11.0)", ex.Message); //New code list
        }

        [Test]
        public void DimensionAttributeCodelistHasNewCodeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_2_NEW_CODE_IN_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure4NewCodeTest.Dsd.DbId;

            Assert.DoesNotThrowAsync(
                async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));
        }

        [Test]
        public void DimensionAttributeCodelistHasRemovedCodeTest()
        {
            var referencedStructure = GetStructureWithBaseStructures("ATTR_TYPE_DIFF_TEST_DIM_CODE_REMOVED_FROM_CODELIST.xml", useDsdReference: _useDsdAsReferencedStructure);
            referencedStructure.Dsd.DbId = _originalStructure.Dsd.DbId;

            var ex = Assert.ThrowsAsync<ChangeInDsdException>(async () => await DotStatDbService.FillDbIdsAndCreateMissingDbObjects(referencedStructure, MappingStoreDataAccess, CancellationToken));

            StringAssert.Contains("T2, T3", ex.Message); // codes removed
            StringAssert.Contains("OECD:ATTR_TYPE_DSD(11.1).ATTR_DIM_ENUMERATED", ex.Message); //affected component
            StringAssert.Contains("OECD:CL_TEST_DIM(11.0)", ex.Message); //affected codelist
        }
    }
}

