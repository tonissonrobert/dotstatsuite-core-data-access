﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using DbUp;
using DbUp.Engine;

namespace DotStat.Test.DataAccess.Integration
{
    internal class BaseDbSetup : SdmxUnitTestBase
    {
        protected void CreateDb(string connectionString, string scriptFolder)
        {
            var dbVersion = DbUp.DatabaseVersion.DataDbVersion;

            EnsureDatabase.For.SqlDatabase(connectionString);

            var dbUpEngine = DeployChanges.To
                .SqlDatabase(connectionString)
                .WithScriptsEmbeddedInAssembly(typeof(DbUp.MsSql.Resources).Assembly, s => s.Contains(scriptFolder, StringComparison.OrdinalIgnoreCase))
                .WithScript("version.sql", $"UPDATE [dbo].[DB_VERSION] SET VERSION = '{dbVersion}'", new SqlScriptOptions() { RunGroupOrder = 1000 })
                .WithVariables(new Dictionary<string, string>()
                {
                    { "dbName", new SqlConnectionStringBuilder(connectionString).InitialCatalog },
                    { "loginName", "dotStatTestUser" },
                    { "loginPwd", "j48j0Lh_rkd$jjRnGr_ejLj0Jr" }
                })
                .WithExecutionTimeout(TimeSpan.FromSeconds(180))
                .LogToConsole()
                .Build();

            var result = dbUpEngine.PerformUpgrade();

            if (!result.Successful)
            {
                throw result.Error;
            }
        }

        protected void DeleteDb(string connectionString)
        {
            DropDatabase.For.SqlDatabase(connectionString);

            //using (var conn = new SqlConnection(connectionString))
            //    SqlConnection.ClearPool(conn);
        }
    }
}
