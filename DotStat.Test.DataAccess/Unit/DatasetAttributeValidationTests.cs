﻿using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Configuration;
using DotStat.Common.Localization;
using DotStat.Db.Exception;
using DotStat.Db.Validation;
using DotStat.Domain;
using DotStat.Test.Moq;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class DatasetAttributeValidationTests : UnitTestBase
    {
        public enum TestCasePostion
        {
            AtKeyValue,
            AtObservation
        }

        private readonly TestMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly Dataflow _dataflow;
        private readonly BaseConfiguration _configuration;

        public DatasetAttributeValidationTests()
        {
            _mappingStoreDataAccess = new TestMappingStoreDataAccess("sdmx/264D_264_SALDI2_ATTRIBUTE_TEST.xml");
            _dataflow = _mappingStoreDataAccess.GetDataflow();
            _configuration = GetConfiguration().Get<BaseConfiguration>();
            LocalizationRepository.Configure(_configuration);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void MandatoryAttributeMissingTest(bool fullValidation)
        {
            var dataSetAttributesReportedAtDataSetLevel = new List<DataSetAttributeRow>();
            var dataSetAttributesReportedAtObservationLevel = new List<DataSetAttributeRow>();
            var batchActions = new List<BatchAction> { new BatchAction(1, StagingRowActionEnum.Merge, false) };

            var dataSetAttributeValidator =
                new DatasetAttributeValidator(_configuration);
            
            if (fullValidation)
            {
                var mandatoryDsdAttributes = _dataflow.Dsd.Attributes.Where(a => a.Base.Mandatory)
                    .Where(a => a.Base.AttachmentLevel is AttributeAttachmentLevel.DataSet
                        or AttributeAttachmentLevel.Null).ToList();

                if (mandatoryDsdAttributes.Any())
                {
                    var ex = Assert.Throws<KeyValueReadException>(() => dataSetAttributeValidator.Validate(_dataflow.Dsd.Attributes.ToList(), batchActions,
                        dataSetAttributesReportedAtDataSetLevel, dataSetAttributesReportedAtObservationLevel, _dataflow,
                        100,
                        fullValidation));
                    
                    var expectedError = string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MandatoryDatasetAttributeMissing), 
                        null, null, null, null, mandatoryDsdAttributes.FirstOrDefault().Code);

                    Assert.AreEqual(expectedError, ex.Message);
                }
            }
            else
            {
                Assert.IsTrue(dataSetAttributeValidator.Validate(_dataflow.Dsd.Attributes.ToList(), batchActions,
                    dataSetAttributesReportedAtDataSetLevel, dataSetAttributesReportedAtObservationLevel, _dataflow,
                    100,
                    fullValidation));

                var errorsAfterValidation = dataSetAttributeValidator.GetErrors();

                Assert.AreEqual(0, errorsAfterValidation.Count);
            }
        }

        [TestCase(TestCasePostion.AtKeyValue)]
        [TestCase(TestCasePostion.AtObservation)]
        public void AttributeNotInDsd(TestCasePostion testCasePostion)
        {
            var batchActions = new List<BatchAction> { new BatchAction(1, StagingRowActionEnum.Merge, false) };
            var dataSetAttributeRows = new List<DataSetAttributeRow>{new (1, StagingRowActionEnum.Merge)
            {
                Attributes = new List<IKeyValue>
                {
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED"),
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED2"),
                    new KeyValueImpl("Test value for non-existing attribute", "TITLE_TEST"),
                    new KeyValueImpl("Test value for non-existing attribute 2", "TITLE_TEST2")
                }
            }};

            var keyValues = testCasePostion == TestCasePostion.AtKeyValue ? dataSetAttributeRows : new List<DataSetAttributeRow>();
            var keyValuesReportedAtObservation = testCasePostion == TestCasePostion.AtObservation ? dataSetAttributeRows : new List<DataSetAttributeRow>();

            var datasetAttributeValidator =
                new DatasetAttributeValidator(_configuration);

            Assert.IsTrue(datasetAttributeValidator.Validate(_dataflow.Dsd.Attributes.ToList(), batchActions, keyValues, keyValuesReportedAtObservation, _dataflow, 100, true));
        }

        [TestCase(TestCasePostion.AtKeyValue)]
        [TestCase(TestCasePostion.AtObservation)]
        public void DuplicateAttributeTest(TestCasePostion testCasePostion)
        {
            var batchActions = new List<BatchAction> { new BatchAction(1, StagingRowActionEnum.Merge, false) };
            var dataSetAttributeRows = new List<DataSetAttributeRow>{new (1, StagingRowActionEnum.Merge)
            {
                Attributes = new List<IKeyValue>
                {
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED"),
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED2"),
                    new KeyValueImpl("P1Y", "TIME_FORMAT"),
                    new KeyValueImpl("P1Y", "TIME_FORMAT")
                }
            }};

            var keyValues = testCasePostion == TestCasePostion.AtKeyValue ? dataSetAttributeRows : new List<DataSetAttributeRow>();
            var keyValuesReportedAtObservation = testCasePostion == TestCasePostion.AtObservation ? dataSetAttributeRows : new List<DataSetAttributeRow>();

            var datasetAttributeValidator = new DatasetAttributeValidator(_configuration);

            Assert.IsFalse(datasetAttributeValidator.Validate(_dataflow.Dsd.Attributes.ToList(), batchActions, keyValues, keyValuesReportedAtObservation, _dataflow, 100, true));

            var errorsAfterValidation = datasetAttributeValidator.GetErrors();

            Assert.AreEqual(1, errorsAfterValidation.Count);

            Assert.AreEqual(ValidationErrorType.DuplicatedDatasetAttribute, errorsAfterValidation.First(e => e is KeyValueError).Type);

            var errorMessages = datasetAttributeValidator.GetErrorsMessage();

            var attributeId = "TIME_FORMAT";
            Assert.True(errorMessages.Contains(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DuplicatedDatasetAttribute), null, null, null, null, attributeId)));
        }

        [Test]
        public void AttributeReportedAtMultipleAttachmentLevelsTest()
        {
            var keyValues = new List<DataSetAttributeRow>{new (1,StagingRowActionEnum.Merge)
            {
                Attributes = new List<IKeyValue>
                {
                    new KeyValueImpl("P1Y", "TIME_FORMAT")
                }
            }};

            var batchActions = new List<BatchAction> { new BatchAction(1, StagingRowActionEnum.Merge, false) };
            var keyValuesReportedAtObservation = new List<DataSetAttributeRow>{new (1, StagingRowActionEnum.Merge)
            {
                Attributes = new List<IKeyValue>
                {
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED"),
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED2"),
                    new KeyValueImpl("P1Y", "TIME_FORMAT")
                }
            }};

            var datasetAttributeValidator = new DatasetAttributeValidator(_configuration);

            Assert.IsFalse(datasetAttributeValidator.Validate(_dataflow.Dsd.Attributes.ToList(), batchActions, keyValues, keyValuesReportedAtObservation, _dataflow, 100, true));

            var errorsAfterValidation = datasetAttributeValidator.GetErrors();

            Assert.AreEqual(1, errorsAfterValidation.Count);

            Assert.AreEqual(ValidationErrorType.AttributeReportedAtMultipleLevels,
                errorsAfterValidation.First(e => e is KeyValueError).Type);

            var errorMessages = datasetAttributeValidator.GetErrorsMessage();

            var attributeId = "TIME_FORMAT";
            Assert.True(errorMessages.Contains(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeReportedAtMultipleLevels), null, null, null, null, attributeId)));
        }

        [TestCase(TestCasePostion.AtKeyValue)]
        [TestCase(TestCasePostion.AtObservation)]
        public void MandatoryAttributeWithNullValueTest(TestCasePostion testCasePostion)
        {
            var batchActions = new List<BatchAction> { new BatchAction(1, StagingRowActionEnum.Merge, false) };
            var dataSetAttributeRows = new List<DataSetAttributeRow>{new (1, StagingRowActionEnum.Merge)
            {
                Attributes = new List<IKeyValue>
                {
                    new KeyValueImpl("", "TEST_ATTR_DATASET_CODED"),
                    new KeyValueImpl(null, "TEST_ATTR_DATASET_CODED2")
                }
            }};

            var keyValues = testCasePostion == TestCasePostion.AtKeyValue ? dataSetAttributeRows : new List<DataSetAttributeRow>();
            var keyValuesReportedAtObservation = testCasePostion == TestCasePostion.AtObservation ? dataSetAttributeRows : new List<DataSetAttributeRow>();

            var datasetAttributeValidator = new DatasetAttributeValidator(_configuration);

            Assert.IsFalse(datasetAttributeValidator.Validate(_dataflow.Dsd.Attributes.ToList(), batchActions, keyValues, keyValuesReportedAtObservation, _dataflow, 100, true));

            var errorsAfterValidation = datasetAttributeValidator.GetErrors().ToArray();

            Assert.AreEqual(2, errorsAfterValidation.Length);

            Assert.AreEqual(ValidationErrorType.MandatoryAttributeWithNullValueInStaging, errorsAfterValidation[0].Type);
            Assert.AreEqual(ValidationErrorType.MandatoryAttributeWithNullValueInStaging, errorsAfterValidation[1].Type);

            var errorMessages = datasetAttributeValidator.GetErrorsMessage();

            var attributeId = "TEST_ATTR_DATASET_CODED";
            Assert.True(errorMessages.Contains(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                    .MandatoryDatasetAttributeWithNullValueInStaging), null, null, null, null,
                attributeId)));

            attributeId = "TEST_ATTR_DATASET_CODED2";
            Assert.True(errorMessages.Contains(string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                    .MandatoryDatasetAttributeWithNullValueInStaging), null, null, null, null,
                attributeId)));
        }

        
        [TestCase(TestCasePostion.AtKeyValue)]
        [TestCase(TestCasePostion.AtObservation)]
        public void AttributeWithUnknownCodeTest(TestCasePostion testCasePostion)
        {
            var batchActions = new List<BatchAction> { new BatchAction(1, StagingRowActionEnum.Merge, false) };
            var dataSetAttributeRows = new List<DataSetAttributeRow>{new (1, StagingRowActionEnum.Merge)
            {
                Attributes = new List<IKeyValue>
                {
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED2"),
                    new KeyValueImpl("P1YX", "TIME_FORMAT"),
                    new KeyValueImpl("P1YYY", "TEST_ATTR_DATASET_CODED")
                }
            }};

            var keyValues = testCasePostion == TestCasePostion.AtKeyValue ? dataSetAttributeRows : new List<DataSetAttributeRow>();
            var keyValuesReportedAtObservation = testCasePostion == TestCasePostion.AtObservation ? dataSetAttributeRows : new List<DataSetAttributeRow>();

            var datasetAttributeValidator =
                new DatasetAttributeValidator(_configuration);

            Assert.IsFalse(datasetAttributeValidator.Validate(_dataflow.Dsd.Attributes.ToList(), batchActions, keyValues, keyValuesReportedAtObservation, _dataflow, 100, true));

            var errorsAfterValidation = datasetAttributeValidator.GetErrors().ToArray();

            Assert.AreEqual(2, errorsAfterValidation.Length);

            Assert.AreEqual(ValidationErrorType.UnknownAttributeCodeMember, errorsAfterValidation[0].Type);
            Assert.AreEqual(ValidationErrorType.UnknownAttributeCodeMember, errorsAfterValidation[1].Type);

            var errorMessages = datasetAttributeValidator.GetErrorsMessage();

            var attributeId = "TIME_FORMAT";
            var attributeValue = "P1YX";
            Assert.True(errorMessages.Contains(
                string.Format(LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.UnknownAttributeCodeMemberWithoutCoordinate), null, null, null, null, attributeId, attributeValue)));

            attributeId = "TEST_ATTR_DATASET_CODED";
            attributeValue = "P1YYY";
            Assert.True(errorMessages.Contains(
                string.Format(LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.UnknownAttributeCodeMemberWithoutCoordinate), null, null, null, null, attributeId, attributeValue)));
        }

        [Test]
        public void ObservationAttributeReportedAtDatasetAttributesTest()
        {
            var batchActions = new List<BatchAction> { new BatchAction(1, StagingRowActionEnum.Merge, false) };
            var keyValues = new List<DataSetAttributeRow>{new (1, StagingRowActionEnum.Merge)
            {
                Attributes = new List<IKeyValue>
                {
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED"),
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED2"),
                    new KeyValueImpl("This is a sample BREAK - obs level at dataset", "BREAK")
                }
            }};

            var keyValuesReportedAtObservation = new List<DataSetAttributeRow>();

            var datasetAttributeValidator =
                new DatasetAttributeValidator(_configuration);

            Assert.IsFalse(datasetAttributeValidator.Validate(_dataflow.Dsd.Attributes.ToList(), batchActions, keyValues, keyValuesReportedAtObservation, _dataflow, 100, true));

            var errorsAfterValidation = datasetAttributeValidator.GetErrors().ToArray();

            Assert.AreEqual(1, errorsAfterValidation.Length);

            Assert.AreEqual(ValidationErrorType.ObservationAttributeAtNonObservationLevel, errorsAfterValidation[0].Type);

            var errorMessages = datasetAttributeValidator.GetErrorsMessage();

            var attributeId = "BREAK";
            Assert.True(errorMessages.Contains(
                string.Format(LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.ObservationAttributeReportedAtDataset), null, null, null, null, attributeId)));
        }

        [Test]
        public void DimGroupAttributeReportedAtDatasetAttributesTest()
        {
            var batchActions = new List<BatchAction> { new BatchAction(1, StagingRowActionEnum.Merge, false) };
            var keyValues = new List<DataSetAttributeRow>{new (1, StagingRowActionEnum.Merge)
            {
                Attributes = new List<IKeyValue>
                {
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED"),
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED2"),
                    new KeyValueImpl("This is a sample TITLE - obs level at dataset", "TITLE")
                }
            }};

            var keyValuesReportedAtObservation = new List<DataSetAttributeRow>();

            var datasetAttributeValidator = new DatasetAttributeValidator(_configuration);

            Assert.IsFalse(datasetAttributeValidator.Validate(_dataflow.Dsd.Attributes.ToList(), batchActions, keyValues, keyValuesReportedAtObservation, _dataflow, 100, true));

            var errorsAfterValidation = datasetAttributeValidator.GetErrors().ToArray();

            Assert.AreEqual(1, errorsAfterValidation.Length);

            Assert.AreEqual(ValidationErrorType.DimGroupAttributeAtDatasetLevel, errorsAfterValidation[0].Type);

            var errorMessages = datasetAttributeValidator.GetErrorsMessage();

            var attributeId = "TITLE";
            Assert.True(errorMessages.Contains(
                string.Format(LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.DimGroupAttributeReportedAtDataset), null, null, null, null, attributeId)));
        }

        [TestCase(TestCasePostion.AtKeyValue, 2)]
        [TestCase(TestCasePostion.AtObservation, 2)]
        public void MaxErrorCountTest(TestCasePostion testCasePostion, int maxErrorCount)
        {
            var batchActions = new List<BatchAction> { new BatchAction(1, StagingRowActionEnum.Merge, false) };
            var dataSetAttributeRows = new List<DataSetAttributeRow>{new (1, StagingRowActionEnum.Merge)
            {
                Attributes = new List<IKeyValue>
                {
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED2"),
                    new KeyValueImpl("P1YX", "TIME_FORMAT"),
                    new KeyValueImpl("P1YYY", "TEST_ATTR_DATASET_CODED")
                }
            }};

            var keyValues = testCasePostion == TestCasePostion.AtKeyValue ? dataSetAttributeRows : new List<DataSetAttributeRow>();
            var keyValuesReportedAtObservation = testCasePostion == TestCasePostion.AtObservation ? dataSetAttributeRows : new List<DataSetAttributeRow>();

            var datasetAttributeValidator = new DatasetAttributeValidator(_configuration);

            Assert.IsFalse(datasetAttributeValidator.Validate(_dataflow.Dsd.Attributes.ToList(), batchActions, keyValues, keyValuesReportedAtObservation, _dataflow, maxErrorCount, true));

            var errorsAfterValidation = datasetAttributeValidator.GetErrors();

            Assert.AreEqual(maxErrorCount, errorsAfterValidation.Count);
        }

        [TestCase(TestCasePostion.AtKeyValue)]
        [TestCase(TestCasePostion.AtObservation)]
        public void AttributeFormatValidationTest(TestCasePostion testCasePostion)
        {
            var attrsWithErrors = new List<IKeyValue>
            {
                new KeyValueImpl("This is a sample TEST_ATTR_DATASET_TEXT", "TEST_ATTR_DATASET_TEXT"),
            };

            var attsWithNoErrors = new List<IKeyValue>
            {
                new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED"),
                new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED2")
            };
            attsWithNoErrors.AddRange(attrsWithErrors);

            var batchActions = new List<BatchAction> { new BatchAction(1, StagingRowActionEnum.Merge, false) };
            var dataSetAttributeRows = new List<DataSetAttributeRow>{new (1, StagingRowActionEnum.Merge)
            {
                Attributes = attsWithNoErrors
            }};

            var keyValues = testCasePostion == TestCasePostion.AtKeyValue ? dataSetAttributeRows : new List<DataSetAttributeRow>();
            var keyValuesReportedAtObservation = testCasePostion == TestCasePostion.AtObservation ? dataSetAttributeRows : new List<DataSetAttributeRow>();


            var myConfiguration = GetConfiguration().Get<BaseConfiguration>();
            myConfiguration.MaxTextAttributeLength = 10;

            var datasetAttributeValidator = new DatasetAttributeValidator(myConfiguration);

            Assert.IsFalse(datasetAttributeValidator.Validate(_dataflow.Dsd.Attributes.ToList(), batchActions, keyValues, keyValuesReportedAtObservation, _dataflow, _configuration.MaxTransferErrorAmount, true));

            var errorsAfterValidation = datasetAttributeValidator.GetErrors().ToArray();

            Assert.AreEqual(dataSetAttributeRows.Count, errorsAfterValidation.Length);

            foreach (var error in errorsAfterValidation)
            {
                Assert.AreEqual(ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit, error.Type);
            }

            var errorMessages = datasetAttributeValidator.GetErrorsMessage();

            foreach (var keyable in attrsWithErrors)
            {
                StringAssert.Contains(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TextAttributeValueLengthExceedsMaxLimitWithoutCoordinate), null,
                    null, null, null, keyable.Concept, keyable.Code, keyable.Code.Length,
                    myConfiguration.MaxTextAttributeLength), errorMessages);
            }
        }

        [Test]
        [TestCase(null, null, false, null)]

        [TestCase(null, 10, false, null)]
        [TestCase(null, 200, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]
        [TestCase(null, 4000, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]
        [TestCase(null, 4001, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]
        [TestCase(null, 5000, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]

        [TestCase(500, 200, false, null)]
        [TestCase(500, 4000, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]
        [TestCase(500, 4001, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]
        [TestCase(500, 5000, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]

        [TestCase(4000, 200, false, null)]
        [TestCase(4000, 4000, false, null)]
        [TestCase(4000, 4001, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]
        [TestCase(4000, 5000, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]

        [TestCase(4001, 200, false, null)]
        [TestCase(4001, 4000, false, null)]
        [TestCase(4001, 4001, false, null)]
        [TestCase(4001, 5000, false, null)]

        [TestCase(0, 200, false, null)]
        [TestCase(0, 4000, false, null)]
        [TestCase(0, 4001, false, null)]
        [TestCase(0, 5000, false, null)]
        public void NonCodedAttributeTypeCheck(int? maxAttributeLengthAnnotation, int lengthOfAttributeValue, bool isErrorExpected, ValidationErrorType? expectedErrorType)
        {
            var batchActions = new List<BatchAction> { new BatchAction(1, StagingRowActionEnum.Merge, false) };
            var dataSetAttributeRows = new List<DataSetAttributeRow>{new (1, StagingRowActionEnum.Merge)
            {
                Attributes = new List<IKeyValue>
                {
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED"),
                    new KeyValueImpl("P1Y", "TEST_ATTR_DATASET_CODED2"),
                    new KeyValueImpl(string.Empty.PadRight(lengthOfAttributeValue, 'a'), "TEST_ATTR_DATASET_TEXT")
                }
            }};

            var myConfiguration = GetConfiguration().Get<BaseConfiguration>();
            myConfiguration.MaxTextAttributeLength = 10;

            var datasetAttributeValidator = new DatasetAttributeValidator(myConfiguration);

            var dataflow = _mappingStoreDataAccess.GetDataflow(useCache: false);
            dataflow.Dsd.MaxTextAttributeLength = maxAttributeLengthAnnotation;


            foreach (var fullValidation in new[] { true, false })
            {
                var isValid = datasetAttributeValidator.Validate(_dataflow.Dsd.Attributes.ToList(), batchActions,
                    dataSetAttributeRows, new List<DataSetAttributeRow>(), dataflow,
                    _configuration.MaxTransferErrorAmount, fullValidation);

                Assert.AreEqual(isErrorExpected, !isValid);

                var errors = datasetAttributeValidator.GetErrors();

                Assert.AreEqual(isErrorExpected ? 1 : 0, errors.Count);

                if (isErrorExpected)
                    Assert.AreEqual(expectedErrorType, errors.FirstOrDefault()?.Type);
            }
        }
    }
}