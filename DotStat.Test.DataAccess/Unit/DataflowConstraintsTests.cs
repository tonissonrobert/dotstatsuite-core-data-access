﻿using System.Linq;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Constants;


namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class DataflowConstraintsTests: SdmxUnitTestBase
    {
        private TestMappingStoreDataAccess _mappingStoreDataAccess;
        private Dataflow _dataflow;
        private readonly ICodeTranslator _codeTranslator;

        public DataflowConstraintsTests()
        {
            _mappingStoreDataAccess = new TestMappingStoreDataAccess("sdmx/ECB_AME_(with constraints).xml");
            _dataflow = _mappingStoreDataAccess.GetDataflow();
            var codeListRepository = new TestCodeListRepository(_dataflow);

            _codeTranslator = new CodeTranslator(codeListRepository);
        }

        [TestCase("AME_REF_AREA", "IRL", false, true)]
        [TestCase("AME_REF_AREA", "Denmark", false, false)]
        [TestCase("AME_REF_AREA", "Denmark", true, true)]
        [TestCase("AME_REF_AREA", "some dummy text", true, false)]
        public void FindByLabel(string dimCode, string key, bool withLabel, bool expectedResult)
        {
            var dimension = _dataflow.Dimensions.First(dim => dim.Code == dimCode);

            Assert.IsNotNull(dimension);

            var code = dimension.FindMember(key, withLabel);

            Assert.AreEqual(expectedResult, code != null);
        }


        [Test]
        public void TestDataflowAttributesWithConstraints()
        {
            var dsdDim = _dataflow.Dsd.Dimensions.First(dim => dim.Code == "AME_ITEM");

            Assert.IsNotNull(dsdDim.Constraint);

            Assert.AreEqual(882, dsdDim.Codelist.Codes.Count);
            Assert.AreEqual(4, dsdDim.Constraint.Codes.Count);

            // Dimension member not present in constraint, but present in DSD (in full codelist)
            var code = dsdDim.FindMember("CVGD3", false);
            Assert.IsNotNull(code);
            Assert.IsFalse(dsdDim.IsAllowed(code.Code));

            // Dimension member present in constraint, and in DSD (in full codelist)
            code = dsdDim.FindMember("UDGGL", false);
            Assert.IsNotNull(code);
            Assert.IsTrue(dsdDim.IsAllowed(code.Code));

            // Check of constrainted attribute
            Assert.AreEqual(_dataflow.Dsd.Attributes.Count(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation), _dataflow.ObsAttributes.Count);

            var dsdAttribute = _dataflow.Dsd.Attributes.First(a => a.Code.Equals("OBS_STATUS"));

            Assert.IsNotNull(dsdAttribute);
            Assert.IsNotNull(dsdAttribute.Constraint);
            Assert.AreEqual(17, dsdAttribute.Codelist.Codes.Count);
            Assert.AreEqual(6, dsdAttribute.Constraint.Codes.Count);

            // Attribute code member not present in constraint, but present in DSD (in full codelist)
            code = dsdAttribute.FindMember("V", false);
            Assert.IsNotNull(code);
            Assert.IsFalse(dsdAttribute.IsAllowed(code.Code));

            // Attribute code member present in constraint, and in DSD (in full codelist)
            code = dsdAttribute.FindMember("A", false);
            Assert.IsNotNull(code);
            Assert.IsTrue(dsdAttribute.IsAllowed(code.Code));

            // ----------------------------------------

            dsdAttribute = _dataflow.Dsd.Attributes.First(a => a.Code.Equals("OBS_CONF"));

            Assert.IsNotNull(dsdAttribute);
            Assert.IsNotNull(dsdAttribute.Constraint);
            Assert.AreEqual(9, dsdAttribute.Codelist.Codes.Count);
            Assert.AreEqual(5, dsdAttribute.Constraint.Codes.Count);

            // Attribute code member not present in constraint, but present in DSD (in full codelist)
            code = dsdAttribute.FindMember("T", false);
            Assert.IsNotNull(code);
            Assert.IsFalse(dsdAttribute.IsAllowed(code.Code));

            // Attribute code member present in constraint, and in DSD (in full codelist)
            code = dsdAttribute.FindMember("A", false);
            Assert.IsNotNull(code);
            Assert.IsTrue(dsdAttribute.IsAllowed(code.Code));
        }

        [Test]
        [TestCase("([FREQ]='Q') AND ([AME_REF_AREA]='DNK' OR [AME_REF_AREA]='IRL' OR [AME_REF_AREA]='NLD' OR [AME_REF_AREA]='SVK') AND ([AME_TRANSFORMATION]='3') AND ([AME_AGG_METHOD]='4') AND ([AME_UNIT]='0' OR [AME_UNIT]='319') AND ([AME_REFERENCE]='0') AND ([AME_ITEM]='UDGGL' OR [AME_ITEM]='OVGD' OR [AME_ITEM]='UBLGE' OR [AME_ITEM]='ZUTN')")]
        [TestCase("([FREQ]='Q' OR [FREQ] IS NULL) AND ([AME_REF_AREA]='DNK' OR [AME_REF_AREA]='IRL' OR [AME_REF_AREA]='NLD' OR [AME_REF_AREA]='SVK' OR [AME_REF_AREA] IS NULL) AND ([AME_TRANSFORMATION]='3' OR [AME_TRANSFORMATION] IS NULL) AND ([AME_AGG_METHOD]='4' OR [AME_AGG_METHOD] IS NULL) AND ([AME_UNIT]='0' OR [AME_UNIT]='319' OR [AME_UNIT] IS NULL) AND ([AME_REFERENCE]='0' OR [AME_REFERENCE] IS NULL) AND ([AME_ITEM]='UDGGL' OR [AME_ITEM]='OVGD' OR [AME_ITEM]='UBLGE' OR [AME_ITEM]='ZUTN' OR [AME_ITEM] IS NULL)", true)]
        [TestCase("([FREQ]='Q' OR [FREQ] IS NULL OR [FREQ] = '~') AND ([AME_REF_AREA]='DNK' OR [AME_REF_AREA]='IRL' OR [AME_REF_AREA]='NLD' OR [AME_REF_AREA]='SVK' OR [AME_REF_AREA] IS NULL OR [AME_REF_AREA] = '~') AND ([AME_TRANSFORMATION]='3' OR [AME_TRANSFORMATION] IS NULL OR [AME_TRANSFORMATION] = '~') AND ([AME_AGG_METHOD]='4' OR [AME_AGG_METHOD] IS NULL OR [AME_AGG_METHOD] = '~') AND ([AME_UNIT]='0' OR [AME_UNIT]='319' OR [AME_UNIT] IS NULL OR [AME_UNIT] = '~') AND ([AME_REFERENCE]='0' OR [AME_REFERENCE] IS NULL OR [AME_REFERENCE] = '~') AND ([AME_ITEM]='UDGGL' OR [AME_ITEM]='OVGD' OR [AME_ITEM]='UBLGE' OR [AME_ITEM]='ZUTN' OR [AME_ITEM] IS NULL OR [AME_ITEM] = '~')", true, true)]
        public void ConstraintAtObservationAndDimGroupAttributeSqlTest(string expectedStr, bool includeNulls = false, bool includeNullsAndSwitchOff = false)
        {
            var nullTreatment = includeNullsAndSwitchOff ? NullTreatment.IncludeNullsAndSwitchOff : includeNulls ? NullTreatment.IncludeNulls : NullTreatment.ExcludeNulls;
            var where = Predicate.BuildWhereForObservationLevel(null, _dataflow, codeTranslator: _codeTranslator, nullTreatment: nullTreatment);
            
            Assert.AreEqual(expectedStr, where);
        }
    }
}