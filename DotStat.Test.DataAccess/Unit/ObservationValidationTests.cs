﻿using System.Collections.Generic;
using System.Linq;
using DotStat.Common.Configuration;
using DotStat.Common.Localization;
using DotStat.Db;
using DotStat.Db.Validation;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;
using Microsoft.Extensions.Configuration;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using DotStat.DB.Util;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class ObservationValidationTests : UnitTestBase
    {
        private readonly TestMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly Dataflow _dataflow;
        private readonly BaseConfiguration _configuration;
        private readonly ReportedComponents _reportedComponents;
        private readonly DsdMetadataBuilder _metadata;

        public ObservationValidationTests()
        {
            _mappingStoreDataAccess = new TestMappingStoreDataAccess("sdmx/ECB_AME_(with constraints).xml");
            _dataflow = _mappingStoreDataAccess.GetDataflow();
            _configuration = GetConfiguration().Get<BaseConfiguration>();
            _configuration.MaxTextAttributeLength = 10;
            _configuration.MaxTransferErrorAmount = 100;

            _reportedComponents = GetReportedComponents(_dataflow);
            _metadata = new DsdMetadataBuilder(_dataflow.Dsd, _reportedComponents);
        }

        [Test]
        public void ValidConstraintValidation()
        {
            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("Q", "FREQ"),
                new KeyValueImpl("DNK", "AME_REF_AREA"),
                new KeyValueImpl("3", "AME_TRANSFORMATION"),
                new KeyValueImpl("4", "AME_AGG_METHOD"),
                new KeyValueImpl("319", "AME_UNIT"),
                new KeyValueImpl("0", "AME_REFERENCE"),
                new KeyValueImpl("UDGGL", "AME_ITEM"),
            };

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);

            var observations = new List<IObservation>
            {
                new ObservationImpl(key, "2010", "1", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("A", "OBS_STATUS")})),
                new ObservationImpl(key, "2011", "2", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("B", "OBS_STATUS")})),
            };

            var isValid = true;
            var validator = new ObservationValidator(_dataflow, _reportedComponents, _configuration, true, false, _metadata);

            for (var i = 1; i <= observations.Count; i++)
                isValid &= validator.ValidateObservation(observations[i - 1], i);

            Assert.IsTrue(isValid);

            var errors = validator.GetErrors();

            Assert.AreEqual(0, errors.Count);
        }

        [Test]
        public void InValidConstraintValidation()
        {
            var invalidSeriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("A", "FREQ"),
                new KeyValueImpl("AUS", "AME_REF_AREA"),
                new KeyValueImpl("1", "AME_TRANSFORMATION"),
                new KeyValueImpl("4", "AME_AGG_METHOD"),
                new KeyValueImpl("310", "AME_UNIT"),
                new KeyValueImpl("422", "AME_REFERENCE"),
                new KeyValueImpl("UDGGL", "AME_ITEM"),
            };

            var invalidKey = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, invalidSeriesKey, null);

            var validSeriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("Q", "FREQ"),
                new KeyValueImpl("IRL", "AME_REF_AREA"),
                new KeyValueImpl("3", "AME_TRANSFORMATION"),
                new KeyValueImpl("4", "AME_AGG_METHOD"),
                new KeyValueImpl("319", "AME_UNIT"),
                new KeyValueImpl("0", "AME_REFERENCE"),
                new KeyValueImpl("UDGGL", "AME_ITEM"),
            };

            var validKey = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, validSeriesKey, null);

            var observations = new List<IObservation>
            {
                new ObservationImpl(invalidKey, "2010", "1", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("A", "OBS_STATUS")})),
                new ObservationImpl(invalidKey, "2011", "2", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("A", "OBS_STATUS")})),
                new ObservationImpl(validKey, "2012-Q1", "3", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("C", "OBS_STATUS")})),
                new ObservationImpl(validKey, "2012-Q2", "4", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("M", "OBS_STATUS")})),
            };

            var isValid = true;
            var validator = new ObservationValidator(_dataflow, _reportedComponents, _configuration, true, false, _metadata);

            for (var i = 1; i <= observations.Count; i++)
                isValid &= validator.ValidateObservation(observations[i - 1], 1, i);

            Assert.IsFalse(isValid);

            var errors = validator.GetErrors();

            Assert.AreEqual(4, errors.Count);

            Assert.AreEqual(ValidationErrorType.DimensionConstraintViolation, errors[0].Type);
            Assert.AreEqual(1, errors[0].Row);

            Assert.AreEqual(ValidationErrorType.DimensionConstraintViolation, errors[1].Type);
            Assert.AreEqual(2, errors[1].Row);

            Assert.AreEqual(ValidationErrorType.UnknownAttributeCodeMember, errors[2].Type);
            Assert.AreEqual(3, errors[2].Row);

            Assert.AreEqual(ValidationErrorType.AttributeConstraintViolation, errors[3].Type);
            Assert.AreEqual(4, errors[3].Row);
        }

        [Test]
        public void DuplicationTest()
        {
            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("Q", "FREQ"),
                new KeyValueImpl("DNK", "AME_REF_AREA"),
                new KeyValueImpl("3", "AME_TRANSFORMATION"),
                new KeyValueImpl("4", "AME_AGG_METHOD"),
                new KeyValueImpl("319", "AME_UNIT"),
                new KeyValueImpl("0", "AME_REFERENCE"),
                new KeyValueImpl("UDGGL", "AME_ITEM"),
            };

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);

            var observations = new List<IObservation>
            {
                new ObservationImpl(key, "2010", "1", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("A", "OBS_STATUS")})),
                new ObservationImpl(key, "2010", "2", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("B", "OBS_STATUS")})),
                new ObservationImpl(key, "2011", "4", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("D", "OBS_STATUS")})),
                new ObservationImpl(key, "2010", "3", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("E", "OBS_STATUS")})),
            };

            var isValid = true;
            var validator = new ObservationValidator(_dataflow, _reportedComponents, _configuration, true, false, _metadata);

            for (var i = 1; i <= observations.Count; i++)
                isValid &= validator.ValidateObservation(observations[i - 1], 1, i);

            Assert.IsFalse(isValid);

            var errors = validator.GetErrors();

            Assert.AreEqual(2, errors.Count);

            Assert.AreEqual(ValidationErrorType.DuplicatedCoordinate, errors[0].Type);
            Assert.AreEqual(2, errors[0].Row);
            var expectedError = string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DuplicatedCoordinate),
                2, observations[1].GetFullKey(true, observations[1]?.SeriesKey.DataStructure.TimeDimension?.Id),
                null, 3);
            Assert.True(errors[0].ToString().Contains(expectedError));

            Assert.AreEqual(ValidationErrorType.DuplicatedCoordinate, errors[1].Type);
            Assert.AreEqual(4, errors[1].Row);
        }

        [Test]
        public void AttributeTypeCheck()
        {
            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("Q", "FREQ"),
                new KeyValueImpl("DNK", "AME_REF_AREA"),
                new KeyValueImpl("3", "AME_TRANSFORMATION"),
                new KeyValueImpl("4", "AME_AGG_METHOD"),
                new KeyValueImpl("319", "AME_UNIT"),
                new KeyValueImpl("0", "AME_REFERENCE"),
                new KeyValueImpl("UDGGL", "AME_ITEM"),
            };

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);

            var observations = new List<IObservation>
            {
                new ObservationImpl(key, "2010", "1", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("A", "OBS_STATUS")})),
                new ObservationImpl(key, "2011", "2", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("A", "OBS_STATUS"), new KeyValueImpl("B", "UNKNOWN_ATTRIBUTE")})),
                new ObservationImpl(key, "2012", "3", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("UNKNOWN_CODE", "OBS_STATUS") })),
                new ObservationImpl(key, "2013", "4", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("A", "OBS_STATUS"), new KeyValueImpl("Attribute value passing the text limit", "OBS_PRE_BREAK") })),
            };

            var isValid = true;
            var validator = new ObservationValidator(_dataflow, _reportedComponents, _configuration, true, false, _metadata);

            for (var i = 1; i <= observations.Count; i++)
                isValid &= validator.ValidateObservation(observations[i - 1], 1, i);

            Assert.IsFalse(isValid);

            var errors = validator.GetErrors();

            Assert.AreEqual(3, errors.Count);

            Assert.AreEqual(ValidationErrorType.AttributeNotInDsd, errors[0].Type);
            Assert.AreEqual(2, errors[0].Row);

            Assert.AreEqual(ValidationErrorType.UnknownAttributeCodeMember, errors[1].Type);
            Assert.AreEqual(3, errors[1].Row);

            Assert.AreEqual(ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit, errors[2].Type);
            Assert.AreEqual(4, errors[2].Row);

            //Basic validation
            isValid = true;
            validator = new ObservationValidator(_dataflow, _reportedComponents, _configuration, false, false, _metadata);

            for (var i = 1; i <= observations.Count; i++)
                isValid &= validator.ValidateObservation(observations[i - 1], i);

            Assert.IsFalse(isValid);

            errors = validator.GetErrors();

            Assert.AreEqual(3, errors.Count);

        }

        [Test]
        [TestCase(null, null, false, null)]

        [TestCase(null, 10, false, null)]
        [TestCase(null, 200, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]
        [TestCase(null, 4000, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]
        [TestCase(null, 4001, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]
        [TestCase(null, 5000, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]

        [TestCase(500, 200, false, null)]
        [TestCase(500, 4000, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]
        [TestCase(500, 4001, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]
        [TestCase(500, 5000, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]

        [TestCase(4000, 200, false, null)]
        [TestCase(4000, 4000, false, null)]
        [TestCase(4000, 4001, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]
        [TestCase(4000, 5000, true, ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit)]

        [TestCase(4001, 200, false, null)]
        [TestCase(4001, 4000, false, null)]
        [TestCase(4001, 4001, false, null)]
        [TestCase(4001, 5000, false, null)]

        [TestCase(0, 200, false, null)]
        [TestCase(0, 4000, false, null)]
        [TestCase(0, 4001, false, null)]
        [TestCase(0, 5000, false, null)]
        public void NonCodedAttributeTypeCheck(int? maxAttributeLengthAnnotation, int lengthOfAttributeValue, bool isErrorExpected, ValidationErrorType? expectedErrorType)
        {
            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("Q", "FREQ"),
                new KeyValueImpl("DNK", "AME_REF_AREA"),
                new KeyValueImpl("3", "AME_TRANSFORMATION"),
                new KeyValueImpl("4", "AME_AGG_METHOD"),
                new KeyValueImpl("319", "AME_UNIT"),
                new KeyValueImpl("0", "AME_REFERENCE"),
                new KeyValueImpl("UDGGL", "AME_ITEM"),
            };

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);

            var observation =
                new ObservationImpl(key, "2010", "1",
                    new List<IKeyValue>(new[]
                    { 
                        new KeyValueImpl("FORMAT", "TIME_FORMAT"),
                        new KeyValueImpl(string.Empty.PadRight(lengthOfAttributeValue, 'a'), "OBS_PRE_BREAK"),
                        new KeyValueImpl("A", "OBS_STATUS")
                    }));

            var dataflow = _mappingStoreDataAccess.GetDataflow(useCache: false);
            dataflow.Dsd.MaxTextAttributeLength = maxAttributeLengthAnnotation;

            var validators = new[]
            {
                new ObservationValidator(dataflow, _reportedComponents, _configuration, true, false, _metadata),
                new ObservationValidator(dataflow, _reportedComponents, _configuration, false, false, _metadata)
            };

            foreach (var validator in validators)
            {
                var isValid = validator.ValidateObservation(observation, 1);

                Assert.AreEqual(isErrorExpected, !isValid);

                var errors = validator.GetErrors();

                Assert.AreEqual(isErrorExpected ? 1 : 0, errors.Count);

                if (isErrorExpected)
                    Assert.AreEqual(expectedErrorType, errors.FirstOrDefault()?.Type);
            }
        }

        [Test]
        public void MandatoryAttributesCheck()
        {
            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("Q", "FREQ"),
                new KeyValueImpl("DNK", "AME_REF_AREA"),
                new KeyValueImpl("3", "AME_TRANSFORMATION"),
                new KeyValueImpl("4", "AME_AGG_METHOD"),
                new KeyValueImpl("319", "AME_UNIT"),
                new KeyValueImpl("0", "AME_REFERENCE"),
                new KeyValueImpl("UDGGL", "AME_ITEM"),
            };

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);

            var observations = new List<IObservation>
            {
                new ObservationImpl(key, "2010", "1", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("A", "OBS_STATUS")})),
                new ObservationImpl(key, "2011", "2", new List<IKeyValue>(new List<IKeyValue>())),
                new ObservationImpl(key, "2012", "3", new List<IKeyValue>(new []{ new KeyValueImpl("", "TIME_FORMAT"), new KeyValueImpl("", "OBS_STATUS")})),
            };

            //full validation
            var isValid = true;
            var validator = new ObservationValidator(_dataflow, _reportedComponents, _configuration, true, false, _metadata);

            for (var i = 1; i <= observations.Count; i++)
                isValid &= validator.ValidateObservation(observations[i - 1], 1, i);

            Assert.IsFalse(isValid);

            var errors = validator.GetErrors();

            Assert.AreEqual(4, errors.Count);

            Assert.AreEqual(ValidationErrorType.MandatoryAttributeMissing, errors[0].Type);
            Assert.AreEqual(2, errors[0].Row);
            Assert.AreEqual(ValidationErrorType.MandatoryAttributeMissing, errors[1].Type);
            Assert.AreEqual(2, errors[1].Row);

            Assert.AreEqual(ValidationErrorType.MandatoryAttributeWithNullValueInStaging, errors[2].Type);
            Assert.AreEqual(3, errors[2].Row);
            Assert.AreEqual(ValidationErrorType.MandatoryAttributeWithNullValueInStaging, errors[3].Type);
            Assert.AreEqual(3, errors[3].Row);

            //Basic validation
            isValid = true;
            validator = new ObservationValidator(_dataflow, _reportedComponents, _configuration, false, false, _metadata);

            for (var i = 1; i <= observations.Count; i++)
                isValid &= validator.ValidateObservation(observations[i - 1], i);

            Assert.IsTrue(isValid);

            errors = validator.GetErrors();

            Assert.AreEqual(0, errors.Count);
        }

        [Test]
        [TestCase(new[] { "", "", "", "", "", "", "", "" }, "", StagingRowActionEnum.Merge, 0, null)]

        [TestCase(new[] { "", "", "", "", "", "", "", "DF" }, "", StagingRowActionEnum.Merge, 0, null)]
        [TestCase(new[] { null, null, null, null, null, null, null, "DF" }, "", StagingRowActionEnum.Merge, 0, null)]
        [TestCase(new[] { "A", "", "", "", "", "", "", "DF" }, "", StagingRowActionEnum.Merge, 0, null)]

        [TestCase(new[] { null, null, null, null, null, null, "A", "DF" }, "DIM_2", StagingRowActionEnum.Merge, 1, ValidationErrorType.AttributeReferencedDimensionMissing)]
        [TestCase(new[] { "", "", "", "", "", "", "A", "DF" }, "", StagingRowActionEnum.Merge, 1, ValidationErrorType.MissingCodeMember)]
        [TestCase(new[] { "", "B", "", "", "", "", "B", "DF" }, "", StagingRowActionEnum.Merge, 0, null)]

        [TestCase(new[] { null, "B", null, null, null, "AA", "A", "DF" }, "DIM_1", StagingRowActionEnum.Merge, 1, ValidationErrorType.AttributeReferencedDimensionMissing)]
        [TestCase(new[] { "", "B", "", "", "", "AA", "A", "DF" }, "", StagingRowActionEnum.Merge, 1, ValidationErrorType.MissingCodeMember)]
        [TestCase(new[] { "A", "B", "", "", "", "AB", "B", "DF" }, "", StagingRowActionEnum.Merge, 0, null)]

        [TestCase(new[] { "A", "B", null, "", "B", "AA", "A", "DF" }, "TIME_PERIOD", StagingRowActionEnum.Merge, 1, ValidationErrorType.AttributeReferencedDimensionMissing)]
        [TestCase(new[] { "A", "B", "", "", "B", "AA", "A", "DF" }, "", StagingRowActionEnum.Merge, 1, ValidationErrorType.MissingCodeMember)]
        [TestCase(new[] { "A", "B", "2020", "", "A", "AB", "A", "DF" }, "", StagingRowActionEnum.Merge, 0, null)]

        [TestCase(new[] { "A", "B", null, "1", null, "AA", "A", "DF" }, "TIME_PERIOD", StagingRowActionEnum.Merge, 1, ValidationErrorType.ObservationReferencedDimensionMissing)]
        [TestCase(new[] { null, null, null, "1", null, "AA", "A", "DF" }, "DIM_1,DIM_2,TIME_PERIOD", StagingRowActionEnum.Merge, 1, ValidationErrorType.ObservationReferencedDimensionMissing)]
        [TestCase(new[] { "A", "B", "", "1", "", "AA", "A", "DF" }, "", StagingRowActionEnum.Merge, 1, ValidationErrorType.MissingCodeMember)]
        [TestCase(new[] { "A", "B", "2020", "1", "", "AB", "A", "DF" }, "", StagingRowActionEnum.Merge, 0, null)]
        public void CheckReferenceDimensionsPresent(string[] components, string missingDimensions, StagingRowActionEnum action, int expectedErrorsCount, ValidationErrorType? expectedErrorType)
        {
            var mappingStoreDataAccess = new TestMappingStoreDataAccess("sdmx/OECD-DF_TEST_DELETE-1.0-all_structures.xml");
            var dataFlow = mappingStoreDataAccess.GetDataflow();
            var reportedComponents = GetReportedComponents(dataFlow);
            var i = 0;
            //Dimensions
            var seriesKey =
                (from dimId in dataFlow.Dimensions.Where(d => !d.Base.TimeDimension).Select(d => d.Base.Id)
                 let code = components[i++]
                 where code is not null
                 select new KeyValueImpl(code, dimId)).Cast<IKeyValue>().ToList();
            var key = new KeyableImpl(dataFlow.Base, dataFlow.Dsd.Base, seriesKey, null);
            var hasWildCardedDimensions = seriesKey.Any(s => string.IsNullOrEmpty(s.Code));

            var obsTime = components[i++];
            if (string.IsNullOrEmpty(obsTime))
                hasWildCardedDimensions = true;

            //Measure
            var obsVal = components[i++];
            //Attributes
            var attributeValues =
                (from dimId in dataFlow.Dsd.Attributes.Select(d => d.Base.Id)
                 let code = components[i++]
                 where code is not null
                 select new KeyValueImpl(code, dimId)).Cast<IKeyValue>().ToList();

            var observation = new ObservationImpl(key, obsTime, obsVal, attributeValues, crossSectionValue: null);
            var observationRows = new List<ObservationRow>
            {
                new (1, action, observation, hasWildCardedDimensions)
            };

            //full validation
            var isValid = true;
            var validator = new ObservationValidator(dataFlow, reportedComponents, _configuration, true, false, new DsdMetadataBuilder(dataFlow.Dsd, reportedComponents));

            for (var j = 1; j <= observationRows.Count; j++)
            {
                var observationRow = observationRows[j - 1];
                isValid &= validator.ValidateObservation(observationRow.Observation, 1, j, observationRow.Action);
            }

            var expectedIsValid = expectedErrorsCount == 0;
            Assert.AreEqual(expectedIsValid, isValid);

            var errors = validator.GetErrors();
            Assert.AreEqual(expectedErrorsCount, errors.Count);
            if (expectedIsValid) return;

            Assert.AreEqual(expectedErrorType, errors[0].Type);

            if (expectedErrorType is not (ValidationErrorType.AttributeReferencedDimensionMissing or
                ValidationErrorType.ObservationReferencedDimensionMissing)) return;

            var expectedString = $"The following dimension(s) [{missingDimensions}],";
            Assert.IsTrue(errors[0].ToString().StartsWith(expectedString));
        }

        [Test]
        public void FormatValidationTest()
        {
            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("Q", "FREQ"),
                new KeyValueImpl("DNK", "AME_REF_AREA"),
                new KeyValueImpl("3", "AME_TRANSFORMATION"),
                new KeyValueImpl("4", "AME_AGG_METHOD"),
                new KeyValueImpl("319", "AME_UNIT"),
                new KeyValueImpl("0", "AME_REFERENCE"),
                new KeyValueImpl("UDGGL", "AME_ITEM"),
            };

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);

            var observations = new List<IObservation>
            {
                new ObservationImpl(key, "2010", "1", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("A", "OBS_STATUS")})),
                new ObservationImpl(key, "2011", "string", new List<IKeyValue>(new []{ new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("B", "OBS_STATUS")})),
            };

            var isValid = true;
            var validator = new ObservationValidator(_dataflow, _reportedComponents, _configuration, true, false, _metadata);

            for (var i = 1; i <= observations.Count; i++)
                isValid &= validator.ValidateObservation(observations[i - 1], 1, i);

            Assert.IsFalse(isValid);

            var errors = validator.GetErrors();

            Assert.AreEqual(1, errors.Count);

            Assert.AreEqual(ValidationErrorType.InvalidValueFormatNotFloat, errors[0].Type);
            Assert.AreEqual(2, errors[0].Row);
        }

        [Test]
        public void ObservationAttributeFormatValidationTest()
        {
            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("Q", "FREQ"),
                new KeyValueImpl("DNK", "AME_REF_AREA"),
                new KeyValueImpl("3", "AME_TRANSFORMATION"),
                new KeyValueImpl("4", "AME_AGG_METHOD"),
                new KeyValueImpl("319", "AME_UNIT"),
                new KeyValueImpl("0", "AME_REFERENCE"),
                new KeyValueImpl("UDGGL", "AME_ITEM"),
            };

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);

            var observations = new List<IObservation>
            {
                new ObservationImpl(key, "2010", "1",
                    new List<IKeyValue>(new[]
                        {new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("A", "OBS_STATUS"), new KeyValueImpl("1234567890", "OBS_PRE_BREAK")})),
                new ObservationImpl(key, "2011", "2",
                    new List<IKeyValue>(new[]
                        {new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("B", "OBS_STATUS"), new KeyValueImpl("12345678901", "OBS_PRE_BREAK")})),
                new ObservationImpl(key, "2012", "3",
                    new List<IKeyValue>(new[]
                        {new KeyValueImpl("FORMAT", "TIME_FORMAT"), new KeyValueImpl("D", "OBS_STATUS"), new KeyValueImpl("1234567890111", "OBS_PRE_BREAK")})),
            };

            var isValid = true;

            //full validation
            var validator = new ObservationValidator(_dataflow, _reportedComponents, _configuration, true, false, _metadata);

            for (var i = 1; i <= observations.Count; i++)
                isValid &= validator.ValidateObservation(observations[i - 1], 1, i);

            Assert.IsFalse(isValid);

            var errors = validator.GetErrors();

            Assert.AreEqual(2, errors.Count);

            Assert.AreEqual(ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit, errors[0].Type);
            Assert.AreEqual(2, errors[0].Row);

            Assert.AreEqual(ValidationErrorType.TextAttributeValueLengthExceedsMaxLimit, errors[1].Type);
            Assert.AreEqual(3, errors[1].Row);
        }

        [Test]
        public void DeleteObservation()
        {
            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("Q", "FREQ"),
                new KeyValueImpl("DNK", "AME_REF_AREA"),
                new KeyValueImpl("3", "AME_TRANSFORMATION"),
                new KeyValueImpl("4", "AME_AGG_METHOD"),
                new KeyValueImpl("319", "AME_UNIT"),
                new KeyValueImpl("0", "AME_REFERENCE"),
                new KeyValueImpl("UDGGL", "AME_ITEM"),
            };

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);

            var obsAttributes = new List<IKeyValue>()
            { 
                new KeyValueImpl("FORMAT", "TIME_FORMAT"),
                new KeyValueImpl("", "OBS_STATUS")
            };

            var observation = new ObservationImpl(key, "2010", null, obsAttributes);
            Assert.AreEqual(true, observation.IsDeleteOperation(_dataflow));

            //full validation
            var validator = new ObservationValidator(_dataflow, _reportedComponents, _configuration, true, false, _metadata);
            Assert.IsFalse(validator.ValidateObservation(observation, 1, 1));

            var errors = validator.GetErrors();

            Assert.AreEqual(ValidationErrorType.MandatoryAttributeWithNullValueInStaging, errors[0].Type);
            Assert.AreEqual(1, errors[0].Row);
            //basic validation
            validator = new ObservationValidator(_dataflow, _reportedComponents, _configuration, false, false, _metadata);
            Assert.IsTrue(validator.ValidateObservation(observation, 1));

            errors = validator.GetErrors();
            Assert.AreEqual(0, errors.Count);
        }

        [Test]
        public void NullCheck()
        {
            var seriesKey = new List<IKeyValue>
            {
                new KeyValueImpl("Q", "FREQ"),
                new KeyValueImpl("DNK", "AME_REF_AREA"),
                new KeyValueImpl("3", "AME_TRANSFORMATION"),
                new KeyValueImpl("4", "AME_AGG_METHOD"),
                new KeyValueImpl("319", "AME_UNIT"),
                new KeyValueImpl("0", "AME_REFERENCE"),
                new KeyValueImpl("UDGGL", "AME_ITEM"),
            };

            var key = new KeyableImpl(_dataflow.Base, _dataflow.Dsd.Base, seriesKey, null);

            var obsAttributes = new List<IKeyValue>()
            { 
                new KeyValueImpl("FORMAT", "TIME_FORMAT"),
                new KeyValueImpl("", "OBS_STATUS")
            };

            var observation = new ObservationImpl(key, "2010", "1", obsAttributes);
            Assert.AreEqual(false, observation.IsDeleteOperation(_dataflow));

            //full validation
            var validator = new ObservationValidator(_dataflow, _reportedComponents, _configuration, true, false, _metadata);
            Assert.IsFalse(validator.ValidateObservation(observation, 1, 1));

            var errors = validator.GetErrors();
            Assert.AreEqual(1, errors.Count);

            Assert.AreEqual(ValidationErrorType.MandatoryAttributeWithNullValueInStaging, errors[0].Type);
            Assert.AreEqual(1, errors[0].Row);

            //basic validation
            validator = new ObservationValidator(_dataflow, _reportedComponents, _configuration, false, false, _metadata);
            Assert.IsTrue(validator.ValidateObservation(observation, 1, 1));

            errors = validator.GetErrors();
            Assert.AreEqual(0, errors.Count);

        }

    }
}
