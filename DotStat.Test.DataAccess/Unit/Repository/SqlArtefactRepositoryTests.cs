﻿using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Db.Repository.SqlServer;
using Moq;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Unit.Repository
{
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public sealed class SqlArtefactRepositoryTests : SdmxUnitTestBase
    {
        private readonly SqlArtefactRepository _repository;
        private readonly Mock<SqlDotStatDb> _dotStatDbMock;
        //TODO complete unitests
        public SqlArtefactRepositoryTests()
        {
            _dotStatDbMock = new Mock<SqlDotStatDb>(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            _dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            _dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");

            _repository = new SqlArtefactRepository(_dotStatDbMock.Object, Configuration);
        }

        //[TestCase(true)]
        //[TestCase(false)]
        //public void CheckManagementTables(bool tableExists)
        //{
        //    //var tableNames = new Dictionary<string, bool>
        //    //{
        //    //    { "ARTEFACT", tableExists },
        //    //    { "COMPONENT", tableExists },
        //    //    { "METADATA_ATTRIBUTE", tableExists },
        //    //    { "ENUMERATIONS", tableExists },
        //    //    { "DSD_TRANSACTION", tableExists },
        //    //    { "LOGS", tableExists },
        //    //    { "DF_Output_Timestamp", tableExists },
        //    //    { "ATTR_DIM_SET", tableExists }
        //    //};

        //    //foreach (var tableName in tableNames)
        //    //{
        //    //    if(tableName.Value)
        //    //        _dotStatDbMock.Setup(m =>
        //    //        m.TableExists(
        //    //            tableName.Key,
        //    //            It.IsAny<CancellationToken>()))
        //    //        .Returns(() => Task.FromResult(true));
        //    //}

        //    //if(tableExists)
        //    //    Assert.DoesNotThrowAsync(async () => await _repository.CheckManagementTables(CancellationToken));
        //    //else
        //    //    Assert.ThrowsAsync<DatabaseTableNotFoundException>(async () => await _repository.CheckManagementTables(CancellationToken));
        //}

        //[TestCase(1, 2627)]//Primary Key duplicate
        //[TestCase(2, 2601)]//Duplicate id for unique index
        //[TestCase(3, 1)]//Other SQL error 
        //[TestCase(4)]//No SQL error 
        //public async Task CreateNewDsdAftefact(int dbId, int? sqlErrorCode)
        //{
        //    var handledSqlExceptionCodes = new List<int> { 2627, 2601 };
        //    if (sqlErrorCode != null) 
        //    { 
        //        _dotStatDbMock.Setup(m =>
        //            m.ExecuteScalarSqlWithParamsAsync(
        //                It.IsAny<string>(),
        //                It.IsAny<CancellationToken>(),
        //                It.IsAny<DbParameter[]>()))
        //            .Throws(SqlExceptionHelper.NewSqlException((int)sqlErrorCode, "sql error"));
        //    }
        //    else
        //    {
        //        _dotStatDbMock.Setup(m =>
        //            m.ExecuteScalarSqlWithParamsAsync(
        //                It.IsAny<string>(),
        //                It.IsAny<CancellationToken>(),
        //                It.IsAny<DbParameter[]>()))
        //            .Returns(Task.FromResult((object)dbId));
        //    }


        //    var dataflow = GetDataflow();
        //    if(sqlErrorCode != null)
        //    {
        //        Assert.IsTrue( await _repository.CreateNewDsdAftefact(dataflow.Dsd, CancellationToken));
        //        Assert.AreEqual(dbId, dataflow.Dsd.DbId);
        //    }
        //    else if(handledSqlExceptionCodes.Contains((int)sqlErrorCode))
        //    {
        //        Assert.IsFalse(await _repository.CreateNewDsdAftefact(dataflow.Dsd, CancellationToken));
        //    }
        //    else
        //    {
        //        Assert.ThrowsAsync<SqlException>(async ()=> await _repository.CreateNewDsdAftefact(dataflow.Dsd, CancellationToken));
        //    }
        //}

        //[TestCase]
        //public async Task CleanUpDsd()
        //{
        //}

        //[TestCase]
        //public async Task CleanUpMsdOfDsd()
        //{
        //}


        [TestCase]
        public async Task GetDsdMaxTextAttributeLength()
        {
            var expectedLenght = 100;
            //TODO: match string
            _dotStatDbMock.Setup(m =>
                m.ExecuteScalarSqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(Task.FromResult((object)expectedLenght));

            var dataflow = GetDataflow();
            var lenght = await _repository.GetDsdMaxTextAttributeLength(dataflow.Dsd, CancellationToken);
            Assert.AreEqual(expectedLenght, lenght);
        }

        [TestCase]
        public async Task UpdatePITInfoOfDsdArtefact()
        {
            var expected = true;
            //TODO: match string
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(Task.FromResult(1));

            var dataflow = GetDataflow();
            var actual = await _repository.UpdatePITInfoOfDsdArtefact(dataflow.Dsd, CancellationToken);
            Assert.AreEqual(expected, actual);
        }

        [TestCase]
        public async Task UpdateMaxAttributeLengthOfDsdArtefact()
        {
            var expected = true;
            //TODO: match string
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(Task.FromResult(1));

            var dataflow = GetDataflow();
            var actual = await _repository.UpdateMaxAttributeLengthOfDsdArtefact(dataflow.Dsd, CancellationToken);
            Assert.AreEqual(expected, actual);
        }

        //TODO: complete unit tests
        //[TestCase]
        //public async Task GetDSDPITInfo()
        //{
        //}

        [TestCase]
        public async Task GetDsdMsdDbId()
        {
            var expected = 1;
            //TODO: match string
            _dotStatDbMock.Setup(m =>
                m.ExecuteScalarSqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(Task.FromResult((object)expected));

            var actual = await _repository.GetDsdMsdDbId(expected, CancellationToken);
            Assert.AreEqual(expected, actual);
        }


    }
}