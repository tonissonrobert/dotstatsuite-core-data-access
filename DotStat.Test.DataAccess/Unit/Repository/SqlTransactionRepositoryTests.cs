﻿using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.DB;
using DotStat.Db.Repository.SqlServer;
using DotStat.Domain;
using DotStat.Test.Moq;
using Moq;
using NUnit.Framework;
using System;
using DotStat.Db.Dto;
using System.Data;
using System.Data.SqlClient;
using DateTime = System.DateTime;

namespace DotStat.Test.DataAccess.Unit.Repository
{
    [TestFixture, Parallelizable(ParallelScope.Self)]
    public sealed class SqlTransactionRepositoryTests : SdmxUnitTestBase
    {
        private readonly SqlTransactionRepository _repository;
        private readonly Mock<SqlDotStatDb> _dotStatDbMock;

        public SqlTransactionRepositoryTests()
        {
            _dotStatDbMock = new Mock<SqlDotStatDb>(Configuration.SpacesInternal.FirstOrDefault(), DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            _dotStatDbMock.Setup(m => m.DataSchema).Returns("data");
            _dotStatDbMock.Setup(m => m.ManagementSchema).Returns("management");

            _repository = new SqlTransactionRepository(_dotStatDbMock.Object, Configuration);
        }

        [TestCase]
        public async Task UpdateTableVersionOfTransaction()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));
            Assert.AreEqual(true, await _repository.UpdateTableVersionOfTransaction(0, DbTableVersion.A, CancellationToken));

            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(0));

            Assert.AreEqual(false, await _repository.UpdateTableVersionOfTransaction(1, DbTableVersion.A, CancellationToken));
        }

        [TestCase]
        public async Task UpdateFinalTargetVersionOfTransaction()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));
            Assert.AreEqual(true, await _repository.UpdateFinalTargetVersionOfTransaction(0, TargetVersion.Live, CancellationToken));

            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(0));

            Assert.AreEqual(false, await _repository.UpdateFinalTargetVersionOfTransaction(1, TargetVersion.Live, CancellationToken));
        }

        [TestCase]
        public async Task UpdateArtefactFullIdOfTransaction()
        {
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));
            Assert.AreEqual(true, await _repository.UpdateArtefactFullIdOfTransaction(0, "ref1", CancellationToken));

            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(0));

            Assert.AreEqual(false, await _repository.UpdateArtefactFullIdOfTransaction(1, "ref2", CancellationToken));
        }

        [TestCase(true, "", false, true)]
        [TestCase(true, "", false, false)]
        [TestCase(false, "", false, true)]
        [TestCase(false, "", false, false)]
        [TestCase(true, "10, 20", false, true)]
        [TestCase(true, "10, 20", false, false)]
        [TestCase(true, "", false, false, 2627)]//Primary Key duplicate
        [TestCase(true, "", false, false, 2601)]//Duplicate id for unique index
        [TestCase(true, "", false, false, 1)]//Other SQL error 
        public async Task TryLockNewTransaction(
            bool markTransactionInProgressSucceeds, 
            string ongoingTransactionIds, 
            bool hasOngoingTransactionBlockingAll,
            bool isTransactionWithDsd,
            int? sqlErrorCode = null)
        {
            _dotStatDbMock.Reset();
            var ongoingTransactions = string.IsNullOrEmpty(ongoingTransactionIds) ?
                Array.Empty<string>() : ongoingTransactionIds.Split(',');
            var handledSqlExceptionCodes = new List<int> { 2627, 2601 };

            var originalTableVersion = DbTableVersion.A;

            if (sqlErrorCode != null)
            {
                _dotStatDbMock.Setup(m =>
                    m.ExecuteNonQuerySqlWithParamsAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<DbParameter[]>()))
                    .Throws(SqlExceptionHelper.NewSqlException((int)sqlErrorCode, "duplicated rows"));

                //Sql PK/UQ violation
                if (handledSqlExceptionCodes.Contains((int)sqlErrorCode))
                {
                    //Setup Duplicate transaction
                    var duplicateTransactions = new List<Transaction>() { 
                        new Transaction
                            {
                                Id = 0,
                                ArtefactDbId = 1,
                                Successful = false,
                                BlockAllTransactions = false
                            }};
                    //TODO: add specific querystring and DbParameters to mock
                    SetDotStatDbMock(duplicateTransactions, reset: false);

                    Assert.AreEqual(false, await _repository.TryLockNewTransaction(new Transaction(), isTransactionWithDsd, originalTableVersion, CancellationToken));
                }
                //Other SQL error
                else
                {
                    Assert.ThrowsAsync<SqlException>(()=> _repository.TryLockNewTransaction(new Transaction(), isTransactionWithDsd, originalTableVersion, CancellationToken));
                }

                return;
            }

            //LockTransaction & MarkTransactionInProgressExecuteNonQuerySqlWithParamsAsync
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(markTransactionInProgressSucceeds ? 1 : 0));

            //GetOngoingTransactions ExecuteReaderSqlAsync
            var mockOngoingTransactions = new List<Transaction>();
            foreach(var id in ongoingTransactions)
            {
                mockOngoingTransactions.Add(
                    new Transaction
                    {
                        Id = int.Parse(id),
                        ArtefactDbId = 1,
                        Successful = false,
                        BlockAllTransactions = false
                    });
            }
            //TODO: add specific querystring and DbParameters to mock
            SetDotStatDbMock(mockOngoingTransactions, reset: false);

            //GetOngoingTransactionBlockingAll ExecuteScalarSqlAsync
            int? transactionBlockingAll = hasOngoingTransactionBlockingAll ? 1 : null;
            _dotStatDbMock.Setup(m =>
                    m.ExecuteScalarSqlAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>()))
                    .Returns(() => Task.FromResult((object)transactionBlockingAll));

            var transacctionLockSucceeds = true;
            if (!markTransactionInProgressSucceeds || ongoingTransactions.Count() > 0 || hasOngoingTransactionBlockingAll)
                transacctionLockSucceeds = false;

            var isTransactionLocked = await _repository.TryLockNewTransaction(new Transaction(), isTransactionWithDsd, originalTableVersion, CancellationToken);
            Assert.AreEqual(transacctionLockSucceeds, isTransactionLocked);
        }

        [TestCase]
        public async Task GetNextTransactionId()
        {
            for(int i = 0; i < 10; i++)
            {
                _dotStatDbMock.Setup(m =>
                    m.ExecuteScalarSqlAsync(
                        $@"SELECT NEXT VALUE FOR [{_dotStatDbMock.Object.ManagementSchema}].[TRANSFER_SEQUENCE]",
                        It.IsAny<CancellationToken>()))
                    .Returns(() => Task.FromResult((object)i));
                Assert.AreEqual(i, await _repository.GetNextTransactionId(CancellationToken)); 
            }
        }

        [TestCase]
        public async Task MarkTransactionReadyForValidation()
        {
            //TODO: add specific querystring and DbParameters to mock
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));

            Assert.AreEqual(true, await _repository.MarkTransactionReadyForValidation(1, CancellationToken));

            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(0));

            Assert.AreEqual(false, await _repository.MarkTransactionReadyForValidation(1, CancellationToken));
        }

        [TestCase(1)]
        [TestCase(3)]
        public async Task GetTransactionLogs(int transactionId)
        {
            var mockTransactionLogs = new List<TransactionLog>();
            for(int i = 0; i < 10; i++)
            {
                mockTransactionLogs.Add(
                    new TransactionLog
                    {
                        Id = i,
                        Level = $"LogLevel_{i}"
                    });
            }
            //TODO: add specific querystring and DbParameters to mock
            var date = DateTime.UtcNow;
            SetDotStatDbMock(mockTransactionLogs, date: date, transactionId);
            
            var transactionLogsInDb = _repository.GetTransactionLogs(transactionId, true, CancellationToken, true);
            var transactions = await transactionLogsInDb.ToListAsync();

            var filteredTransactions = mockTransactionLogs.Where(t => t.Id == transactionId).ToList();
        
            Assert.AreEqual(filteredTransactions.Count, transactions.Count);
            Assert.AreEqual(filteredTransactions[0].Id, transactions[0].Id);
            Assert.AreEqual(date.ToString(Transaction.TransactionDatetimeFormat), transactions[0].Date);
            Assert.AreEqual(filteredTransactions[0].Level, transactions[0].Level);
        }

        [TestCase(1, "a", "b", TransactionType.Import, TargetVersion.Live, 1, false)]
        [TestCase(2, "a", "b", TransactionType.Import, TargetVersion.Live, 0, false)]
        [TestCase(3, "", "", TransactionType.InitAllMappingSets, null, 1, true)]
        [TestCase(4, "", "", TransactionType.CleanupMappingSets, null, 1, true)]
        public async Task CreateTransactionItem(
            int transactionId, string sourceDataSpace, string dataSource, TransactionType type, 
            TargetVersion? requestedTargetVersion, int createTransactionSucceeded, bool blockAllTransactions = false)
        {
            var dbId = 0;
            var artefact = new ArtefactItem
            {
                Id = "Code",
                Agency = "Agency",
                Version = $"1.0.{dbId++}",
                DbId = dbId++
            };
            
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(createTransactionSucceeded));

            var transaction = new Transaction
            {
                Id=transactionId,
                ArtefactFullId=artefact.ToString(),
                SourceDataspace=sourceDataSpace,
                DataSource=dataSource,
                Type = type,
                Status =TransactionStatus.Queued,
                RequestedTargetVersion = requestedTargetVersion,
                FinalTargetVersion = requestedTargetVersion,
                BlockAllTransactions =blockAllTransactions
            };

            var transactionInDb = await _repository.CreateTransactionItem(
                transactionId, artefact, null, sourceDataSpace, dataSource, 
                type, requestedTargetVersion, serviceId: null, CancellationToken, blockAllTransactions);

            if (createTransactionSucceeded > 0)
                CompareTransaction(transaction, transactionInDb);
            else
                Assert.IsNull(transactionInDb);

            transaction = await _repository.CreateTransactionItem(
                transactionId, artefact.ToString(), null, sourceDataSpace, dataSource,
                type, requestedTargetVersion, serviceId: null, CancellationToken, blockAllTransactions);

            if (createTransactionSucceeded > 0)
                CompareTransaction(transaction, transactionInDb);
            else
                Assert.IsNull(transactionInDb);
        }

        [TestCase(1)]
        [TestCase(5)]
        public async Task GetTransactionById(int transactionId)
        {
            var dbId = 1;
            var mockOngoingTransactions = new List<Transaction>();
            for (int i = 0; i < 10; i++)
            {
                mockOngoingTransactions.Add(
                    new Transaction
                    {
                        Id = i,
                        ArtefactDbId = dbId,
                        Successful = false,
                        BlockAllTransactions = false
                    });
            }
            //TODO: add specific querystring and DbParameters to mock
            SetDotStatDbMock(mockOngoingTransactions, transactionId);

            var transactionInDb = await _repository.GetTransactionById(transactionId, CancellationToken);

            var mockedTransaction = mockOngoingTransactions.Where(t => t.Id == transactionId).FirstOrDefault();
            CompareTransaction(mockedTransaction, transactionInDb);
        }

        [TestCase("email1", "artFullId1", TransactionStatus.Canceled)]
        [TestCase("email1", "artFullId1", TransactionStatus.Canceled)]
        public async Task GetTransactions(string userEmail, string artFullId, TransactionStatus? status)
        {
            var dbId = 1;
            var mockOngoingTransactions = new List<Transaction>();
            var matchTransactionsCount = 5;
            var nonMatchingEmail = "NonMatchingEmail";
            var nonMatchingartFullId = "NonMatchingFullId";
            var nonMatchingStart = DateTime.MinValue;
            var nonMatchingEtart = DateTime.MaxValue;

            var matchingTransactionId = 0;
            for (int i = 0; i < 20; i++)
            {
                var transaction = new Transaction
                {
                    Id = i < matchTransactionsCount ? matchingTransactionId : i,
                    ArtefactDbId = dbId,
                    UserEmail = i < matchTransactionsCount? userEmail: nonMatchingEmail,
                    ArtefactFullId = i < matchTransactionsCount ? artFullId : nonMatchingartFullId,
                    ExecutionStart = i < matchTransactionsCount ? DateTime.UtcNow : nonMatchingStart,
                    ExecutionEnd = i < matchTransactionsCount ? DateTime.UtcNow.AddDays(1) : nonMatchingEtart
                };

                mockOngoingTransactions.Add(transaction);
            }
            //TODO: add specific querystring and DbParameters to mock
            SetDotStatDbMock(mockOngoingTransactions, matchingTransactionId);

            var transactionsInDb = _repository.GetTransactions(userEmail, artFullId, DateTime.UtcNow.AddDays(-1), DateTime.UtcNow.AddDays(2), status, CancellationToken);
            var listOfTransactions = await transactionsInDb.ToListAsync();

            Assert.AreEqual(matchTransactionsCount, listOfTransactions.Count);

            var mockedTransactions = mockOngoingTransactions.Where(t => t.Id < matchTransactionsCount).ToList();
            for (int i = 0; i < matchTransactionsCount; i++)
            {
                CompareTransaction(mockedTransactions[i], listOfTransactions[i]);
            }
        }

        [TestCase(1)]
        [TestCase(5)]
        public async Task GetInProgressTransactionByDsdId(int transactionId)
        {
            var dbId = 1;
            var mockOngoingTransactions = new List<Transaction>();
            for (int i = 0; i < 10; i++)
            {
                mockOngoingTransactions.Add(
                    new Transaction
                    {
                        Id = i,
                        ArtefactDbId = dbId,
                        Successful = null,
                        BlockAllTransactions = false
                    });
            }
            //TODO: add specific querystring and DbParameters to mock
            SetDotStatDbMock(mockOngoingTransactions, transactionId);

            var transactionsInDb = _repository.GetInProgressTransactions(transactionId, dbId, CancellationToken);
            var transactionInDb = await transactionsInDb.FirstAsync(CancellationToken);

            var mockedTransaction = mockOngoingTransactions.Where(t => t.Id == transactionId).FirstOrDefault();
            CompareTransaction(mockedTransaction, transactionInDb);
        }

        [TestCase]
        public async Task MarkTransactionAsTimedOut()
        {
            //TODO: add specific querystring and DbParameters to mock
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));
            Assert.AreEqual(true, await _repository.MarkTransactionAsTimedOut(0));

            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(0));

            Assert.AreEqual(false, await _repository.MarkTransactionAsTimedOut(1));
        }

        [TestCase]
        public async Task LockTransaction()
        {
            //TODO: add specific querystring and DbParameters to mock
            var dsdId = 1;
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));
            Assert.AreEqual(true, await _repository.LockTransaction(0, dsdId, null, DbTableVersion.A, TargetVersion.Live, CancellationToken));

            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(0));

            Assert.AreEqual(false, await _repository.LockTransaction(1, dsdId, null, DbTableVersion.A, TargetVersion.Live, CancellationToken));
            
        }

        [TestCase]
        public async Task MarkTransactionAsCanceled()
        {
            //TODO: add specific querystring and DbParameters to mock
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));
            Assert.AreEqual(true, await _repository.MarkTransactionAsCanceled(0));

            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(0));

            Assert.AreEqual(false, await _repository.MarkTransactionAsCanceled(1));
        }

        [TestCase]
        public async Task MarkTransactionAsCompleted()
        {
            //TODO: add specific querystring and DbParameters to mock
            _dotStatDbMock.Reset();
            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(1));
            Assert.AreEqual(true, await _repository.MarkTransactionAsCompleted(0, true));

            _dotStatDbMock.Setup(m =>
                m.ExecuteNonQuerySqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(0));

            Assert.AreEqual(false, await _repository.MarkTransactionAsCompleted(1, true));
        }


        [TestCase("Instance_1", true)]
        [TestCase("Instance_2", true)]
        [TestCase("Instance_3", false)]
        public async Task CloseTransactions(string serviceId, bool existingServiceId)
        {
            //TODO: add specific querystring and DbParameters to mock
            _dotStatDbMock.Reset();

            var transactionsInDb = new List<object[]> { new object[] {1,2,3} };
            DbDataReader dbDataReader = new TestDbDataReader(transactionsInDb);

            _dotStatDbMock.Setup(m =>
                    m.ExecuteReaderSqlWithParamsAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<CommandBehavior>(),
                        It.IsAny<bool>(),
                        It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(dbDataReader));

            _dotStatDbMock.Setup(m =>
                    m.ExecuteNonQuerySqlWithParamsAsync(
                        It.IsAny<string>(),
                        It.IsAny<CancellationToken>(),
                        It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(existingServiceId ? 1 : 0));

            Assert.AreEqual(existingServiceId, await _repository.CancelTransactions(serviceId));
        }

        private void CompareTransaction(Transaction transaction1, Transaction transaction2)
        {
            Assert.AreEqual(transaction1.Id, transaction2.Id);
            Assert.AreEqual(transaction1.ArtefactDbId, transaction2.ArtefactDbId);
            Assert.AreEqual(transaction1.ArtefactFullId, transaction2.ArtefactFullId);
            Assert.AreEqual(transaction1.RequestedTargetVersion, transaction2.RequestedTargetVersion);
            Assert.AreEqual(transaction1.FinalTargetVersion, transaction2.FinalTargetVersion);
            Assert.AreEqual(transaction1.TableVersion, transaction2.TableVersion);
            Assert.AreEqual(transaction1.QueuedDateTime, transaction2.QueuedDateTime);
            Assert.AreEqual(transaction1.ExecutionStart, transaction2.ExecutionStart);
            Assert.AreEqual(transaction1.ExecutionEnd, transaction2.ExecutionEnd);
            Assert.AreEqual(transaction1.Successful, transaction2.Successful);
            Assert.AreEqual(transaction1.UserEmail, transaction2.UserEmail);
            Assert.AreEqual(transaction1.SourceDataspace, transaction2.SourceDataspace);
            Assert.AreEqual(transaction1.DataSource, transaction2.DataSource);
            Assert.AreEqual(transaction1.Status, transaction2.Status);
            Assert.AreEqual(transaction1.Type, transaction2.Type);
            Assert.AreEqual(transaction1.BlockAllTransactions, transaction2.BlockAllTransactions);
            Assert.AreEqual(transaction1.LastUpdated, transaction2.LastUpdated);
        }

        private void SetDotStatDbMock(IList<Transaction> transactions, int? transactionId = null, bool reset = true)
        {
            if(reset) _dotStatDbMock.Reset();
            var filteredObjects = transactionId != null ?
                transactions.Where(t => t.Id == transactionId).ToList()
                : transactions;

            var transactionsInDb = new List<object[]>();
            foreach(var obj in filteredObjects)
            {
                transactionsInDb.Add(new object[] {
                    obj.Id,
                    obj.ArtefactDbId,
                    obj.ArtefactFullId,
                    obj.TableVersion,
                    obj.QueuedDateTime,
                    obj.ExecutionStart,
                    obj.ExecutionEnd,
                    obj.Successful,
                    obj.UserEmail,
                    obj.SourceDataspace,
                    obj.DataSource,
                    obj.Type,
                    obj.Status,
                    obj.RequestedTargetVersion,
                    obj.FinalTargetVersion,
                    obj.BlockAllTransactions,
                    obj.LastUpdated,
                    obj.ArtefactChildDbId,
                    obj.ServiceId
                });
            }

            DbDataReader dbDataReader = new TestDbDataReader(transactionsInDb);

            _dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(dbDataReader));
        }

        private void SetDotStatDbMock(IList<TransactionLog> transactionLogs, DateTime date, int? transactionId = null, bool reset = true)
        {
            if (reset) _dotStatDbMock.Reset();
            var transactionLogsInDb = new List<object[]>();

            var filteredObjects = transactionId != null ?
                transactionLogs.Where(t => t.Id == transactionId).ToList()
                : transactionLogs;

            foreach (var obj in filteredObjects)
            {
                transactionLogsInDb.Add(new object[] {
                    date,
                    obj.Level,
                    obj.Server,
                    obj.Logger,
                    obj.Message,
                    obj.Exception?.ToString(),
                    obj.UserEmail
                });
            }

            DbDataReader dbDataReader = new TestDbDataReader(transactionLogsInDb);

            _dotStatDbMock.Setup(m =>
                m.ExecuteReaderSqlWithParamsAsync(
                    It.IsAny<string>(),
                    It.IsAny<CancellationToken>(),
                    It.IsAny<CommandBehavior>(),
                    It.IsAny<bool>(),
                    It.IsAny<DbParameter[]>()))
                .Returns(() => Task.FromResult(dbDataReader));
        }

    }
}