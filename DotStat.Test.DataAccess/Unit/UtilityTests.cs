﻿using DotStat.Db.Util;
using DotStat.Domain;
using NUnit.Framework;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class UtilityTests
    {
        [TestCase("1.0.1", 1, 0, 1)]
        [TestCase("1.0", 1, 0, null)]
        [TestCase("1.2.3", 1, 2, 3)]
        [TestCase("1", 1, 0, null)]
        [TestCase("1.2.3.4", 1, 2, 3)]
        public void SdmxVersionParser(string version, int major, int minor, int? patch=0)
        {
            var ver = new SdmxVersion(version);

            Assert.AreEqual(major, ver.Major);
            Assert.AreEqual(minor, ver.Minor);
            Assert.AreEqual(patch, ver.Patch);
        }

        [Test]
        public void SdmxVersionOperators()
        {
            SdmxVersion vNull = null;
            SdmxVersion v1 = new SdmxVersion(1);
            SdmxVersion v10 = new SdmxVersion(1);
            SdmxVersion v12 = new SdmxVersion(1,2);
            SdmxVersion v103 = new SdmxVersion(1, 0, 3);
            SdmxVersion v203 = new SdmxVersion(2, 0, 3);

            // Operator ==
            Assert.IsTrue(vNull == null);
            Assert.IsTrue(null == vNull);
            Assert.IsTrue(v1 == v10);
            Assert.IsTrue(v10 == v1);
            Assert.IsTrue((SdmxVersion)null == null);
            Assert.IsFalse(v1 == vNull);
            Assert.IsFalse(vNull == v1);
            Assert.IsFalse(v1 == null);
            Assert.IsFalse(null == v1);
            Assert.IsFalse(v1 == v12);
            Assert.IsFalse(v12 == v1);
            Assert.IsFalse(v10 == v103);
            Assert.IsFalse(v103 == v10);
            Assert.IsFalse(v203 == v103);

            // Operator !=
            Assert.IsTrue(v1 != vNull);
            Assert.IsTrue(vNull != v1);
            Assert.IsTrue(v1 != null);
            Assert.IsTrue(null != v1);
            Assert.IsTrue(v1 != v12);
            Assert.IsTrue(v12 != v1);
            Assert.IsTrue(v10 != v103);
            Assert.IsTrue(v103 != v10);
            Assert.IsTrue(v203 != v103);
            Assert.IsFalse(vNull != null);
            Assert.IsFalse(null != vNull);
            Assert.IsFalse(v1 != v10);
            Assert.IsFalse(v10 != v1);
            Assert.IsFalse((SdmxVersion)null != null);

            // Operator <=
            Assert.IsTrue(v1 <= v10);
            Assert.IsTrue(v10 <= v1);
            Assert.IsTrue(v1 <= v103);
            Assert.IsTrue(v1 <= v12);
            Assert.IsTrue(v103 <= v12);
            Assert.IsTrue(v103 <= v203);
            Assert.IsTrue(vNull <= v1);
            Assert.IsTrue(null <= v1);
            Assert.IsTrue(vNull <= null);
            Assert.IsTrue(null <= vNull);
            Assert.IsTrue((SdmxVersion)null <= null);
            Assert.IsFalse(v103 <= v1);
            Assert.IsFalse(v12 <= v1);
            Assert.IsFalse(v12 <= v103);
            Assert.IsFalse(v203 <= v103);
            Assert.IsFalse(v203 <= v12);
            Assert.IsFalse(v203 <= v1);
            Assert.IsFalse(v1 <= vNull);
            Assert.IsFalse(v1 <= null);

            // Operator >=
            Assert.IsTrue(v10 >= v1);
            Assert.IsTrue(v1 >= v10);
            Assert.IsTrue(v103 >= v1);
            Assert.IsTrue(v12 >= v1);
            Assert.IsTrue(v12 >= v103);
            Assert.IsTrue(v203 >= v103);
            Assert.IsTrue(v203 >= v12);
            Assert.IsTrue(v203 >= v1);
            Assert.IsTrue(v1 >= vNull);
            Assert.IsTrue(v1 >= null);
            Assert.IsTrue(vNull >= null);
            Assert.IsTrue(null >= vNull);
            Assert.IsTrue((SdmxVersion)null >= null);
            Assert.IsFalse(v1 >= v103);
            Assert.IsFalse(v1 >= v12);
            Assert.IsFalse(v103 >= v12);
            Assert.IsFalse(v103 >= v203);
            Assert.IsFalse(v12 >= v203);
            Assert.IsFalse(v1 >= v203);
            Assert.IsFalse(vNull >= v1);
            Assert.IsFalse(null >= v1);

            // Operator <
            Assert.IsTrue(v1 < v103);
            Assert.IsTrue(v1 < v12);
            Assert.IsTrue(v103 < v12);
            Assert.IsTrue(v103 < v203);
            Assert.IsTrue(v12 < v203);
            Assert.IsTrue(v1 < v203);
            Assert.IsTrue(vNull < v1);
            Assert.IsTrue(null < v1);
            Assert.IsFalse(v10 < v1);
            Assert.IsFalse(v1 < v10);
            Assert.IsFalse(v103 < v1);
            Assert.IsFalse(v12 < v1);
            Assert.IsFalse(v12 < v103);
            Assert.IsFalse(v203 < v103);
            Assert.IsFalse(v203 < v12);
            Assert.IsFalse(v203 < v1);
            Assert.IsFalse(v1 < vNull);
            Assert.IsFalse(v1 < null);
            Assert.IsFalse(vNull < null);
            Assert.IsFalse(null < vNull);
            Assert.IsFalse((SdmxVersion)null < null);
            
            // Operator >
            Assert.IsTrue(v103 > v1);
            Assert.IsTrue(v12 > v1);
            Assert.IsTrue(v12 > v103);
            Assert.IsTrue(v203 > v103);
            Assert.IsTrue(v203 > v12);
            Assert.IsTrue(v203 > v1);
            Assert.IsTrue(v1 > vNull);
            Assert.IsTrue(v1 > null);
            Assert.IsFalse(v1 > v10);
            Assert.IsFalse(v10 > v1);
            Assert.IsFalse(v1 > v103);
            Assert.IsFalse(v1 > v12);
            Assert.IsFalse(v103 > v12);
            Assert.IsFalse(v103 > v203);
            Assert.IsFalse(vNull > v1);
            Assert.IsFalse(null > v1);
            Assert.IsFalse(vNull > null);
            Assert.IsFalse(null > vNull);
            Assert.IsFalse((SdmxVersion)null > null);
        }

        [Test]
        public void Default_Table_Version()
        {
            Assert.AreEqual('A', DbTableVersions.GetDefaultLiveVersion());
        }

        [TestCase('A', true)]
        [TestCase('B', true)]
        [TestCase('a', false)]
        [TestCase('b', false)]
        [TestCase('C', false)]
        [TestCase('x', false)]
        [TestCase('X', true)]
        public void Is_Valid_Table_Version(char version, bool expected)
        {
            Assert.AreEqual(expected, DbTableVersions.IsValidVersion(version));
        }

        [TestCase('A', DbTableVersion.A)]
        [TestCase('B', DbTableVersion.B)]
        [TestCase('a', null)]
        [TestCase('b', null)]
        [TestCase('C', null)]
        [TestCase('x', null)]
        [TestCase('X', DbTableVersion.X)]
        public void Get_Table_Version(char version, DbTableVersion? expected)
        {
            Assert.AreEqual(expected, DbTableVersions.GetDbTableVersion(version));
        }

        [TestCase("A", DbTableVersion.A)]
        [TestCase("B", DbTableVersion.B)]
        [TestCase("a", null)]
        [TestCase("b", null)]
        [TestCase("C", null)]
        [TestCase(null, null)]
        [TestCase("", null)]
        [TestCase("x", null)]
        [TestCase("X", DbTableVersion.X)]
        public void Get_Table_Version(string version, DbTableVersion? expected)
        {
            Assert.AreEqual(expected, DbTableVersions.GetDbTableVersion(version));
        }

        [TestCase('A', 'B')]
        [TestCase('B', 'A')]
        public void Get_New_Version(DbTableVersion current, DbTableVersion @new)
        {
            Assert.AreEqual(@new, DbTableVersions.GetNewTableVersion(current));
        }

        [Test]
        public void CodelistProjetionTest()
        {
            var codes1 = new[]
            {
                "CODE1",
                "CODE2",
                "CODE3"
            };

            var codes2 = new[]
            {
                "CODE1",
                null,
                "CODE3"
            };

            var codelistPorjection = new CodelistProjection(codes1);

            Assert.IsTrue(codelistPorjection.Map.ContainsKey("CODE3"));

            codelistPorjection = new CodelistProjection(codes2);

            Assert.IsTrue(codelistPorjection.Map.ContainsKey("CODE3"));
        }
    }
}
