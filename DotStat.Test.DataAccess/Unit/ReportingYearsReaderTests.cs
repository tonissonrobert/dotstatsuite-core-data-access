﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DotStat.DB;
using DotStat.Db.Reader;
using DotStat.Db.Util;
using DotStat.Domain;
using DotStat.Test.Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Test.DataAccess.Unit
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class ReportingYearsReaderTests : SdmxUnitTestBase
    {
        private readonly TestMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly CodeTranslator _codeTranslator;
        private readonly Dataflow _dataflow;

        public ReportingYearsReaderTests()
        {
            _mappingStoreDataAccess = new TestMappingStoreDataAccess("sdmx/ABS,DF_WPI_OS_2,0.0.2.xml");
            _dataflow = _mappingStoreDataAccess.GetDataflow();
            var codeListRepository = new TestCodeListRepository(_dataflow);
            _codeTranslator = new CodeTranslator(codeListRepository);
        }

        [OneTimeSetUp]
        public async Task Init()
        {
            await _codeTranslator.FillDict(_dataflow, CancellationToken);
        }

        [TestCase("1997-A1", "--07-01", "1997-07-01", "1998-06-30 23:59:59")]
        [TestCase("2011-W36", "--07-01", "2012-03-05", "2012-03-11 23:59:59")]
        [TestCase("1997-Q1", "--07-01", "1997-07-01", "1997-09-30 23:59:59")]
        [TestCase("1997-Q2", "--07-01", "1997-10-01", "1997-12-31 23:59:59")]
        [TestCase("1997-Q3", "--07-01", "1998-01-01", "1998-03-31 23:59:59")]
        [TestCase("1997-Q4", "--07-01", "1998-04-01", "1998-06-30 23:59:59")]
        public async Task ReaderTest(string sdmxTime, string sdmxAdjustment, string stringDtStart, string stringDtEnd)
        {
            var seriesKey = new KeyableImpl(
                _dataflow.Base,
                _dataflow.Dsd.Base,
                new List<IKeyValue>
                {
                                new KeyValueImpl("1", "MEASURE"),
                                new KeyValueImpl("THRPEB", "INDEX"),
                                new KeyValueImpl("1", "SECTOR"),
                                new KeyValueImpl("B", "INDUSTRY"),
                                new KeyValueImpl("AUS", "REGION"),
                                new KeyValueImpl("10", "TSEST"),
                                new KeyValueImpl("Q", "FREQUENCY"),
                },
                null
            );

            var attributes = new IKeyValue[]
            {
                new KeyValueImpl("IN", "UNIT_MEASURE"),
                new KeyValueImpl(sdmxAdjustment, "REPORTING_YEAR_START_DAY"),
            };

            var observations = new List<ObservationRow>
            {
                new ObservationRow(1, StagingRowActionEnum.Merge, new ObservationImpl(seriesKey, sdmxTime, "777", attributes), false)
            }.ToAsyncEnumerable();

            var reportedComponents = GetReportedComponents(_dataflow);

            var dtStart = DateTime.Parse(stringDtStart, CultureInfo.InvariantCulture);
            var dtEnd = DateTime.Parse(stringDtEnd, CultureInfo.InvariantCulture);

            // -------------------------------------------------

            var reader = new SdmxObservationReader(observations, reportedComponents, _dataflow, _codeTranslator, Configuration, true, false, false);
            var count = 0;

            if (reader.Read())
            {
                Assert.AreEqual(dtStart, reader.GetValue(0));
                Assert.AreEqual(dtEnd, reader.GetValue(1));
                Assert.AreEqual(sdmxTime, reader.GetValue(2));
                count++;
            }

            Assert.AreEqual(await observations.CountAsync(), count);
            Assert.IsTrue(!reader.GetErrors().Any());
        }

        [TestCase("1997-A1", "--07-01", "1997-07-01", "1998-06-30 23:59:59")]
        [TestCase("2011-W36", "--07-01", "2012-03-05", "2012-03-11 23:59:59")]
        [TestCase("1997-Q1", "--07-01", "1997-07-01", "1997-09-30 23:59:59")]
        [TestCase("1997-Q2", "--07-01", "1997-10-01", "1997-12-31 23:59:59")]
        [TestCase("1997-Q3", "--07-01", "1998-01-01", "1998-03-31 23:59:59")]
        [TestCase("1997-Q4", "--07-01", "1998-04-01", "1998-06-30 23:59:59")]
        public void ReportingPeriodAdjustmentsTest(string sdmxTime, string sdmxAdjustment, string stringDtStart, string stringDtEnd)
        {
            var expectedStart = DateTime.Parse(stringDtStart, CultureInfo.InvariantCulture);
            var expectedEnd = DateTime.Parse(stringDtEnd, CultureInfo.InvariantCulture);

            var (start, end) = DateUtils.GetPeriod(sdmxTime, sdmxAdjustment);

            Assert.AreEqual(expectedStart, start);
            Assert.AreEqual(expectedEnd, end);
        }


        [TestCase("1997", "1997-01-01 00:00:00", "1997-12-31 23:59:59")]                    // Annual
        [TestCase("1997-A1", "1997-01-01 00:00:00", "1997-12-31 23:59:59")]                 // Reporting year 
        [TestCase("2000-B1", "2000-01-01 00:00:00", "2000-06-30 23:59:59")]                 // Half-year
        [TestCase("2000-B2", "2000-07-01 00:00:00", "2000-12-31 23:59:59")]                 // Half-year
        [TestCase("2000-S1", "2000-01-01 00:00:00", "2000-06-30 23:59:59")]                 // Half-year
        [TestCase("2000-S2", "2000-07-01 00:00:00", "2000-12-31 23:59:59")]                 // Half-year
        [TestCase("2000-T1", "2000-01-01 00:00:00", "2000-04-30 23:59:59")]                 // Trimester
        [TestCase("2000-T2", "2000-05-01 00:00:00", "2000-08-31 23:59:59")]                 // Trimester
        [TestCase("2000-T3", "2000-09-01 00:00:00", "2000-12-31 23:59:59")]                 // Trimester
        [TestCase("1997-Q1", "1997-01-01 00:00:00", "1997-03-31 23:59:59")]                 // Quarter
        [TestCase("1997-Q2", "1997-04-01 00:00:00", "1997-06-30 23:59:59")]                 // Quarter
        [TestCase("1997-Q3", "1997-07-01 00:00:00", "1997-09-30 23:59:59")]                 // Quarter
        [TestCase("1997-Q4", "1997-10-01 00:00:00", "1997-12-31 23:59:59")]                 // Quarter
        [TestCase("2021-01", "2021-01-01 00:00:00", "2021-01-31 23:59:59")]                 // Month
        [TestCase("2021-12", "2021-12-01 00:00:00", "2021-12-31 23:59:59")]                 // Month
        [TestCase("2021-M01", "2021-01-01 00:00:00", "2021-01-31 23:59:59")]                // Reporting Month
        [TestCase("2021-M12", "2021-12-01 00:00:00", "2021-12-31 23:59:59")]                // Reporting Month
        [TestCase("2011-W01", "2011-01-03 00:00:00", "2011-01-09 23:59:59")]                // Week
        [TestCase("2011-W36", "2011-09-05 00:00:00", "2011-09-11 23:59:59")]                // Week
        [TestCase("2011-W52", "2011-12-26 00:00:00", "2012-01-01 23:59:59")]                // Week
        [TestCase("2021-06-01", "2021-06-01 00:00:00", "2021-06-01 23:59:59")]              // Date
        [TestCase("2021-06-01T12:00:00", "2021-06-01 12:00:00", "2021-06-01 12:59:59")]     // Hour
        [TestCase("2021-06-01T13:30:00", "2021-06-01 13:30:00", "2021-06-01 14:29:59")]     // Hour
        [TestCase("2021-D001", "2021-01-01 00:00:00", "2021-01-01 23:59:59")]               // Reporting date
        [TestCase("2000-01-01T00:00:00/P5Y", "2000-01-01 00:00:00", "2004-12-31 23:59:59")] // Timeframe
        public void SdmxPeriodTest(string sdmxTime, string stringDtStart, string stringDtEnd)
        {
            var expectedStart = DateTime.Parse(stringDtStart, CultureInfo.InvariantCulture);
            var expectedEnd = DateTime.Parse(stringDtEnd, CultureInfo.InvariantCulture);

            var (start, end) = DateUtils.GetPeriod(sdmxTime);

            Assert.AreEqual(expectedStart, start);
            Assert.AreEqual(expectedEnd, end);
        }

    }
}
