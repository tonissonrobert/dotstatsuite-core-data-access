﻿using DotStat.Common.Configuration.Dto;
using DotStat.Db.DB;
using Moq;
using NUnit.Framework;
using System.Data.Common;
using System.Linq;

namespace DotStat.Test.DataAccess.Unit.DB
{
    [TestFixture, Parallelizable(ParallelScope.All)]
    public sealed class DotStatDbServiceTests : UnitTestBase
    {
        private readonly DataspaceInternal _dataSpace;
        private Mock<DotStatDbBase<DbConnection>> _dotStatDbMock;
        private const string dataSchema = "data";
        private const string managementSchema ="management";

        //TODO add missing unit tests
        public DotStatDbServiceTests()
        {
            _dataSpace = Configuration.SpacesInternal.First();

            _dotStatDbMock = new Mock<DotStatDbBase<DbConnection>>(
                dataSchema, managementSchema, _dataSpace, DbUp.DatabaseVersion.DataDbVersion) { CallBase = true };
            _dotStatDbMock.Setup(m => m.DataSchema).Returns(dataSchema);
            _dotStatDbMock.Setup(m => m.ManagementSchema).Returns(managementSchema);
        }

        [Test]
        public void DotStatDbProperties()
        {
            var sqlDotStatDb = new SqlDotStatDb(_dataSpace, DbUp.DatabaseVersion.DataDbVersion);

            Assert.AreEqual(_dataSpace.Id.ToLower(), sqlDotStatDb.Id.ToLower());
            Assert.AreEqual(managementSchema.ToLower(), sqlDotStatDb.ManagementSchema.ToLower());
            Assert.AreEqual(dataSchema.ToLower(), sqlDotStatDb.DataSchema.ToLower());
            Assert.AreEqual(_dataSpace.DatabaseCommandTimeoutInSec, sqlDotStatDb.DatabaseCommandTimeout);
        }

        [Test]
        public void GetDbConnection()
        {
        //    //var sqlCommand = new Mock<SqlCommand>();
        //    //sqlCommand.Setup(m => m.ExecuteNonQuery()).Returns(1);
        //    var sqlConnectionStringBuilder = new SqlConnectionStringBuilder
        //    {
        //        ApplicationIntent = ApplicationIntent.ReadWrite
        //    };
        //    SetUpDbConnection(false);

        //    //var conn = _dotStatDbMock.Object.GetConnection(true);
        //    using var conn = _dotStatDbMock.Object.GetConnection();
        //    Assert.AreEqual(sqlConnectionStringBuilder.ConnectionString, conn.ConnectionString);


        //    var sqlConnectionStringBuilderReadOnly = new SqlConnectionStringBuilder
        //    {
        //        ApplicationIntent = ApplicationIntent.ReadOnly
        //    };

        //    //conn = _dotStatDbMock.Object.GetDbConnection(false);
        //    using var conn2 = _dotStatDbMock.Object.GetConnection();
        //    Assert.AreEqual(sqlConnectionStringBuilderReadOnly.ConnectionString, conn2.ConnectionString);
        }

    }
}
