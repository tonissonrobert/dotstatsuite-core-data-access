# Content of this repository
This repository contains multiple libraries part of the [dotstatsuite-core](https://gitlab.com/sis-cc/.stat-suite). The main purpose of these libraries is to provide multiple implementations of the **data access layer**. These libraries contain functions to **manage**, **store**, and **read data and referential metadata** from data databases/storage places. Currently, it only supports MSSQL. 

This repository also contains a database deployment utility called **Db up tool**. This utiliy allows the automatic creation, initialization and upgrade for the .stat-suite data database, and the .stat-suite common database (Authorization management). [This documentation](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/blob/develop/docs/installation/CodeBaseApproach.md) contains the full description of the parameters and usage of this utility.  

>  **This library is publicly published as a nuget package named [DotStat.DataAccess.NuGet](https://www.nuget.org/packages/DotStat.DataAccess.NuGet/)**

>  The Dbup utility is published as a docker image named [dotstatsuite-dbup](https://hub.docker.com/r/siscc/dotstatsuite-dbup)

The following solutions have **direct dependencies** to this library:
*  [dotstatsuite-core-auth-management](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management) uses this library to manage and extract information from the .stat-suite common database (Authorization management)
*  [dotstatsuite-core-transfer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer) uses this library to manage, and import data into the .stat-suite data database.
*  [dotstatsuite-core-sdmxri-nsi-plugin](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin) uses this library extract data from the .stat-suite data database.


# .STAT DATABASE MODEL
## Version 8.1.0
## Installation 
[Installation document](docs/installation/CodeBaseApproach.md)
## .Stat Suite databases
Databases used in .Stat Suite are the following:
### General databases
> * `DotStatServicesCommon:` This database has been removed. Previously it stored system-wide common information, such as authorization rules, generic logs and configuration settings.
> * `DotStatServicesShare:` This database has been removed. Previously it stored generic chart sharing data.

### Dataspace specific databases:
* **DotStatSuiteCore_Struct_\<dataspace\>:** Previously named `DotStatServicesStructure\<dataspace\>`, stores structural information of the artefacts. The structure of the database is maintained by ESTAT.
* **DotStatSuiteCore_Data_\<dataspace\>:** Previously named `DotStatServices\<dataspace\>`, stores meta information related to data storage, e.g. dataflow, dsd and code list related data, attribute and observation data.


> **Merge Management and Data databases**
> * In this new version of the .Stat database model, there has been a major change of merging the `DotStatServicesManagement<dataspace>` and `DotStatServicesData<dataspace>[<index>]` databases, into a single database called **DotStatSuiteCore_Data_\<dataspace\>**. 
>   * Two database schemas, **[Management]** and **[Data]**,  will group the different database objects per domain. The purpose of this change is to ease the database management and supportive tasks. 
> * In the previous version, the `DotStatServicesData<dataspace>[<index>]` could have multiple database instances, identified by *[\<index\>]*.
>   * This feature has been removed, and instead the database administrator can use [SQL Sharding pattern](https://docs.microsoft.com/en-us/azure/architecture/patterns/sharding).

## DotStatSuiteCore_Data_\<dataspace\> Database
There is one instance of this database per data space in the system, e.g. *DotStatSuiteCore_Data_Dissemination*.

![Database diagram](./docs/DbModel.png)

### Database schemas
* [Management]
* [Data]

### [Management].[ARTEFACT]
- Contains the inventory of the artefacts defined in the datastore database. Possible types are DSD, dataflow and code list.

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
|PK|**ART_ID**<br /> Unique ID of the artefact in the datatable.|Int|False
||**SQL_ART_ID**<br />ID of the artefact that references the actual database object related to the current artefact. This also means that if this column is not NULL, then there is no database object (table or view) created in the database with the ART_ID of this artefact, but rather the table/view belonging to SQL_ART_ID is used.<br />E.g.: If a DSD contains just a slight change (i.e. only patch version changes) there is no need to create a new DSD_{dsd id}_{data version} table, but the data table of the previous DSD version could be used. In such a case, this column will store the ART_ID of the previous DSD version.<br />(Currently not used)|int|false
||**TYPE**<br /> Code of the artefact type. Possible values are:<br /> - CL – code list <br />- DF – dataflow <br /> - DSD – data structure definition |Varchar(50)|False
||**ID**<br /> SDMX id of the artefact.|Varchar(50)|False
||**AGENCY**<br /> Maintaining agency of the artefact.|Varchar(50)|False
||**VERSION_1**<br /> Major version of the artefact.|Int|False
||**VERSION_2**<br /> Minor version of the artefact.|Int|False
||**VERSION_3**<br /> Patch version of the artefact.|Int|True
||**DSD_DATA_VERSION** `Removed in version 2.0.0` ||
||**DSD_DATA_STORE_DB_ID** `Removed in version 2.0.0`||
||**DSD_LIVE_VERSION**<br /> Indicates which table (A or B) stores the current LIVE version. In case of null value there is no live version only PIT release version. |char(1)|True
||**DSD_PIT_VERSION**<br /> Indicates if there is a PIT release defined on DSD and stores the id (A or B) of the table storing the PIT release data.|char(1)|True
||**DSD_PIT_RELEASE_DATE**<br /> Optional (automated) release date for PIT.|datetime|True
||**DSD_PIT_RESTORATION_DATE**<br /> The date the *RESTORE* version was created. If the value is not null then indicates if there is a version to be restored in case current *LIVE* version needs to be withdrown. The restoration version (A or B) could be determined by using *DSD_LIVE_VERSION* column.|datetime|True
||**DSD_DATE_CREATED**<br /> `[To Be Reviewed]` Used only at DSD artefact. The time when the DSD entry was created. (Not used at the moment)|Datetime|True
||**DSD_DATE_DELETED**< `Removed in version 2.0.0` ||
||**DSD_CUD_MODE** `Removed in version 2.0.0` ||
||**DSD_TRANSFER_MODE** `Removed in version 2.0.0` ||
||**DSD_VALIDATION** `Removed in version 2.0.0` ||
||**DSD_OBS_VALUE_DATA_TYPE** `Removed in version 5.0.0` ||
||**DF_DSD_ID**<br /> Used only at Dataflow artefact. The id of the DSD referenced by the dataflow.|Int|True
||**MSD_ID**<br /> Used only at DSD artefact. The id of the MSD referenced by the DSD.|Int|True
||**DF_WHERE_CLAUSE**<br /> Used only at Dataflow artefact. The SQL where clause created from the content constraints defined on the dataflow.|Int|True
||**LAST_UPDATED**<br /> -	Dataflow artefacts: the creation time of the row, or the last time the data availability of the dataflow has changed (due to data loaded for this dataflow or some other related to the same DSD)<br /> - DSD artefact: the creation time of the row or the last time the data version (or the datastore database id) changed.<br /> - Other artefacts: the creation time of the row.|Datetime|True
||**DSD_MAX_TEXT_ATTR_LENGTH**<br /> - Sets the max length of text attributes in DSD|Int|True
||**DATA_COMPRESSION**<br /> - Indicate what type of compression (NONE, COLUMNSTORE_ARCHIVE ) this DSD's data and referential metadata should use|varchar(30)|True
||**KEEP_HISTORY**<br /> - Indicate if the database should keep track of the historical changes on the data and referential metadata. (Default False)|bit|False


### [Management].[ATTR_DIM_SET]
- This table used to track the dimensions that are part of keys of dimension and group level attributes.

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
|PK|**ATTR_ID**<br /> ID of the attribute in COMPONENT table.|Int|False
|PK|**DIM_ID**<br /> ID of the dimension in COMPONENT table.|Int|False


> ### `[Management].[CL_TIME]`
> - This table has been removed in version 4.0.0. 
> - It was previously used to store the codelist for TIME and referenced in DSD_FACT_* tables as *DIM_TIME* column for DSD with Time dimension.


### [Management].[CL_{artefact id}]
- Table containing the codes of the code list members defined by the *{artefact_id}* in *ARTEFACT* table.

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
|PK|**ITEM_ID**<br /> Unique ID of the code list member item in the table|Int|False
||**ID**<br /> The code of the code list member.|Varchar(150)|False


### [Management].[COMPONENT]
- Stores information about the component defined in the DSD, such as attributes and dimensions (except time dimension). 

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
|PK|**COMP_ID** <br />Unique ID of component|Int|False
||**ID**<br /> Code of the component (dimension or attribute)|Varchar(50)|False
||**TYPE** <br />The type of the component. Possible values are:<br />- Dimension<br />- TimeDimension<br />- Attribute<br />- PrimaryMeasure|Varchar(50)|False
||**DSD_ID** Unique artefact ID of the DSD (in Artefact table).|Int|False
||**CL_ID** <br />Unique artefact ID of the code list (in Artefact table) for coded attributes and measures. NULL for non-coded attributes and measures.|Int|True
||**ATT_ASS_LEVEL** <br />Assignment level of the attribute. Possible values are:<br /> - DataSet<br /> - DimensionGroup<br /> - Group<br /> - Observation|Varchar(11)|True
||**ATT_STATUS** <br />Status flag indicating whether the attribute is optional or not. Possible values are: - Conditional - Mandatory|Varchar(11)|True
||**ATT_GROUP_ID** <br />Attribute Group ID|Varchar(50)|True
||**ENUM_ID** <br />Reference to the Enumerations table. Indicates the data type of non-coded attributes and measures, for supported types please see [Appendix 3](#appendix-3-supported-sdmx-representations-possible-types-for-observation-values). NULL for coded attributes and measures.|Bigint|True
||**MIN_LENGTH** <br />Specifies the minimum length of the value in characters for measures. NULL for non-measure components.|Int|True
||**MAX_LENGTH** <br />Specifies the maximum length of the value in characters for measures. NULL for non-measure components.|Int|True
||**PATTERN** <br />Holds any regular expression defining the pattern of permitted values for measures. NULL for non-measure components.|Nvarchar(4000)|True
||**MIN_VALUE** <br />Indicates the lower bound of the range of values allowed for the measures. NULL for non-measure components.|Int|True
||**MAX_VALUE** <br />Indicates the upper bound of the range of values allowed for the measures. NULL for non-measure components.|Int|True


### [Management].[METADATA_ATTRIBUTE]
- Stores information about the metadata attribute components defined in the MSD. 

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
|PK|**MTD_ATTR_ID** <br />Unique ID of the metadatata attribute|Int|False
||**ID**<br /> Code of the the metadatata attribute component|Varchar(4000)|False
||**MSD_ID** Unique artefact ID of the MSD (in Artefact table).|Int|False
||**CL_ID** <br />Unique artefact ID of the code list (in Artefact table) for coded metadata attributes. NULL for non-coded metadata attributes.|Int|True
||**PARENT** <br />Parent metadata attribute ID (MTD_ATTR_ID in this same table)|Int|True
||**MIN_OCCURS** <br />The minimum amount of occurences of the metadata attribute|Int|True
||**MAX_OCCURS** <br />The maximum amount of occurences of the metadata attribute|Int|True
||**IS_REPRESENTATIONAL** <br />Indicates if the metadata attribute is used for representation. Metadata attributes with children are marked as representational|smallint|True
||**ENUM_ID** <br />Reference to the Enumerations table. Indicates the data type of non-coded metadata attributes, for supported types please see [Appendix 3](#appendix-3-supported-sdmx-representations-possible-types-for-observation-values). NULL for coded metadata attributes.|Bigint|True


### [Management].[DF_OUTPUT_TIMESTAMP]
- Stores information about generated files per dataflow. As of now used only by SqlToSdmxFile console application.

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
|PK|**ART_ID**<br /> The ID of the dataflow the generated file belongs to.|Int|False
|PK|**FILETYPE**<br /> The format of the file (XML, JSON, CSV)|Varchar(20)|False
|PK|**LANG**<br /> The language of the content of the generated file for CSV type, ‘**’ otherwise.|Char(2)|False
||**FILENAME**<br /> The name of the generated file.|Varchar(100)|False
||**TIMESTAMP**<br /> The time the file was created.|Datetime|False

### [Management].[DSD_TRANSACTION]
- Stores information about the active, ongoing transactions as well as the history of the transactions.
- This table is used to control concurrent transactions for the same DSD.

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
|PK|**TRANSACTION_ID** <br />Unique ID of the transaction in the datatable.|Int|False
|UQ|**ART_ID** <br />The ID of the DSD affected by the transaction. When the transaction is initially queued, this field is NULL.|Int|True
||**TABLE_VERSION** <br />Indicates which table version the transaction targets (A or B). When the transaction is initially queued, this field is NULL.|char(1)|True
||**REQUESTED_TARGET_VERSION** <br />Indicates which version of the data that the user would like to target by this transaction. Possible values: Live, PIT|nvarchar(250)|True
||**FINAL_TARGET_VERSION** <br />Indicates which version of the data actually targeted by this transaction. For example, if the user specifies Live, but there is an existing PIT, the value would be PIT. Possible values: Live, PIT|nvarchar(250)|True
||**QUEUED_DATETIME** <br />The date time when the transaction request was submited by the user.|Datetime|False
||**EXECUTION_START** <br />The date time when the transaction started to be processed.|Datetime|True
||**EXECUTION_END** <br />The date time when the transaction finished being processed.|Datetime|True
|UQ|**SUCCESSFUL** <br />A flag indicating that the transaction was completed or not. When the transaction is initially queued, this field is NULL.|Bit-|True
||**USER_EMAIL** <br />The email of the user, who made the request|nvarchar(350)|True
||**ARTEFACT_FULL_ID** <br />The email of the user, who made the request|nvarchar(255)|False
||**SOURCE_DATASPACE** <br />The dataspace where the data/metadata was transfered|nvarchar(200)|True
||**DATA_SOURCE** <br />The file/filepath or url from where the data/metadata was imported|nvarchar(max)|True
||**STATUS** <br />The status of the transaction. Possible values: Queued, InProgress, Completed, TimedOut, Canceled, Unknown|nvarchar(250)|True
||**TYPE** <br />The type of transaction. Possible values:CleanupOrphans, CleanupDsd, CleanupMappingSets, InitDataFlow, InitAllMappingSets, Import, ValidateImport, Rollback, Restore, Transfer, ValidateTransfer, Unknown|nvarchar(250)|True
||**BLOCK_ALL_TRANSACTIONS** <br />Indicates if this transaction should block all the system. It is used for funcions such as InitAllMappingSets and CleanupOrphans. Default false|BIT|False
||**LAST_UPDATED** <br />The last time this transaction was modified|Datetime|False
||**SERVICE_ID** <br />Unique ID of the transfer-service instance, which initiated this transaction|nvarchar(20)|True

##### Indexes
Index|Columns|Type|Purpose
---|---|---|---
PK_DSD_TRANSACTION|TRANSACTION_ID|Unique - Clustered Row Stored|Ensure that each transaction has a unique ID
IX_DSD_TRANSACTION|ART_ID ONLY WHEN [SUCCESSFUL] IS NULL AND [ART_ID] IS NOT NULL|Unique - Non-Clustered Row Stored|Ensure that at any given time there is only one transaction active (Status=InProgress) for the same DSD. 

### `[Management].[DSD_TRANSACTION_LOG]`
- This table has been merged with the table [Management].[DSD_TRANSACTION] in version 3.2.0.

### [Management].[ENUMERATIONS]
- Contains the list of allowed data and facet types. Copy of the *ENUMERATION* table in MappingStore database.

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
|PK|**ENUM_ID** <br />Unique ID of the row in the table.|Bigint|False
||**ENUM_NAME** <br />The name of the enumeration group.|Varchar(30)|True
||**ENUM_VALUE** <br />Value of the enumeration item.|Varchar(30)|True
||**ENUM_VALUE_DATATYPE** <br />The data type of the enumeration item.|Varchar(20)|True

### [Management].[LOGS]
- A table storing the log entries.

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
||**TRANSACTION_ID** <br />The ID of the transaction the log entry belongs to.|Int|True
||**DATE** <br />The date and time the log entry was created.|Datetime|True
||**THREAD** <br />The logic thread|NVarchar(255)|True
||**SERVER** <br />The name of the server that generated the log entry.|NVarchar(50)|True
||**APPLICATION** <br />The name of the application logging (currently only nsi-ws and dotstatsuite-core-transfer)|NVarchar(255)|True
||**LEVEL** <br />Severity level of the actual log message. Possible values are:<br />- DEBUG<br />- INFO<br />- WARNING<br />- ERROR|NVarchar(50)|True
||**LOGGER** <br />The full reference of the method that created the log message.|NVarchar(255)|True
||**MESSAGE** <br />The text of the log message.|NVarchar(max)|True
||**EXCEPTION** <br />The exception message in case the actual row logs an exception occurred during the transaction.|NVarchar(max)|True
||**URLREFERRER** <br />The url value in the HttpContext.Current.Request.Headers["Referer"][see more](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.http.httprequest.headers?view=aspnetcore-2.2#Microsoft_AspNetCore_Http_HttpRequest_Headers)|NVarchar(max)|True
||**HTTPMETHOD** <br />The HTTP method [see more](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.http.httprequest.method?view=aspnetcore-2.2#Microsoft_AspNetCore_Http_HttpRequest_Method)|NVarchar(max)|True
||**REQUESTPATH** <br />The Request path.[see more](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.http.httprequest.path?view=aspnetcore-2.2#Microsoft_AspNetCore_Http_HttpRequest_Path)|NVarchar(max)|True
||**QUERYSTRING** <br />The query string.[see more](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.http.httprequest.querystring?view=aspnetcore-2.2#Microsoft_AspNetCore_Http_HttpRequest_QueryString)|NVarchar(max)|True


#### [Data].[FACT_{dsd id}_{table version}] 
- The table contains the observation value and observation level atributes belonging to a particular DSD. 
- When the [ARTEFACT] table's column 'KEEP_HISTORY' is set to '1'  for a given DSD, then the corresponding [Data].[FACT_{dsd id}_{table version}] uses the MSSQL feature 'Temporal Tables' [See more](https://learn.microsoft.com/en-us/sql/relational-databases/tables/temporal-tables?view=sql-server-ver16)
  -  In this case a secondary table [Data].[FACT_{dsd id}_{table version}_History] will hold the historical changes for the observation level components. 
- The *{dsd_id}* and *{table version}* (A or B) attributes in the table name are references to the *DSD_ID* values stored in the *ARTEFACT* table. 
- The table may not contain time dimension columns (PERIOD_SDMX,PERIOD_START,PERIOD_END) in case there is no such dimension defined in the DSD. 
- The *[SID]* column is a surrogate key generated on the *FILT* table, which improves query joins on these two tables. 

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
PK|**SID**<br /> Unique, surrogate key generated on the *FILT* table|int|False 
||**PERIOD_SDMX**<br /> SDMX time as reported.|Varchar(10)|False
PK|**PERIOD_START**<br /> The start of reported period as Date|Date|False
PK|**PERIOD_END**<br /> The end of reported period as a Date.|Date|False
||**COMP_{component id#1}**<br /> The actual value of the dataset level attribute defined by {component id#1} in COMPONENT table.|Coded attributes: Int Non-coded attributes: as defined by related COMPONENT.ENUM_ID column.|Depends on related COMPONENT.ATT_STATUS column:<br />True for conditional, False for Mandatory attributes.
||**...**<br />||
||**COMP_{component id#n}**<br /> The actual value of the dataset level attribute defined by {component id#n} in COMPONENT table.| Coded attributes: Int Non-coded attributes: as defined by realted COMPONENT.ENUM_ID column.|Depends on related COMPONENT.ATT_STATUS column:<br />True for conditional,False for Mandatory attributes.
| |**VALUE**<br /> The actual value of the data point. The data type of this field is defined in the COMPONENT table. Value can be NULL when there is only observation level attribute at a particular coordinate.| As per the primary measure definition in the DSD. For supported types please see [Appendix 3](#appendix-3-supported-sdmx-representations-possible-types-for-observation-values).|True
||**LAST_UPDATED**<br /> The time the data row was crated or updated the last time. This column is used to filter data when the SDMX data query contains the updatedAfter parameter.|Datetime|False


##### Indexes
Index|Columns|Type|Purpose
---|---|---|---
PK_FACT_{dsd id}_{table version}|SID,PERIOD_START,PERIOD_END|Unique - Clustered Row Stored|Uniquely identify an observation, improve query performance for joins against FILTER table and query of First|Last # of observations.
NCCI_TIME_DIM_{dsd id}_{table version}|PERIOD_START,PERIOD_END|Non-Clustered Column Stored|Improve calculation of actual content constraint for time ranges and counts. 

#### [Data].[FILT_{dsd id}] 
- The table contains the dimensions, excluding time dimension, belonging to a particular DSD. 
- The *{dsd_id}* attribute in the table name references the *DSD_ID* values stored in the *ARTEFACT* table. 
- If DSD has more than 32 non-time dimension table also includes unique ROW_ID column, the length of the *ROW_ID* depends on the number of dimension defined on the DSD (excluding time dimension). 
- The *PRIMARY KEY*, SID is an identity surrogate key that serves the purpose to make the connection with the *FACT* table. It was added to be able to make light joins agains the *FACT* table and improve pervormance for CRUD operations. 

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
PK|**SID**<br /> Unique Identity surrogate key |int|False
||**DIM_{component id#1}**<br /> References the code list member of the code list (*CL_ID*) defined by *{component id#1}* in *COMPONENT* table for coded dimensions.<br />Non-coded dimensions use nvarchar type with a length defined using its Representation.TextFormat.MaxLength property with default 4000|int/nvarchar(Representation.TextFormat.MaxLength (default 4000))|False
...|...|...|...		
||**DIM_{component id#n}**<br /> References the code list member of the code list (*CL_ID*) defined by *{component id#n}* in *COMPONENT* table for coded dimensions.<br />Non-coded dimensions use nvarchar type with a length defined using its Representation.TextFormat.MaxLength property with default 4000|int/nvarchar(Representation.TextFormat.MaxLength (default 4000))|False

> - If DSD contains less or exactly 32 non-time dimension observation uniqueness enforced by having a unique index on all DIM_{component id} columns
> - If DSD has more than 32 non-time dimensions Unique ROW_ID column is created. **See Appendix 1 – Binary unique row ID generation**[^1] for further details.|Binary(n) where n = 3 * (no. of dimensions excluding time dimension)|False

##### Indexes
Index|Columns|Type|Purpose
---|---|---|---
PK_FILT_{dsd id}|SID|Unique - Clustered Row Stored|Uniquely identify a series (excluding time dimension), improve query performance for joins against *FACT* table. 
NCCI_FILT_{dsd id}|DIM_{component id#1}, ..., DIM_{component id#n}|Non unique - Non-Clustered Column Stored | Improve Lookups and aggregations on Dimensions e.g. calculate actual content constraint.
U_FILT_{dsd id}|DIM_{component id#1}, ..., DIM_{component id#n}|Unique - Non-Clustered|Guarantees uniqueness of observation dimensions combination (coordinate)

#### [Data].[DELETED_{dsd id}_{table version}]
- The table contains delete instructions from data import DELETE operations. 
- The *{dsd_id}* and *{table version}* (A or B) attributes in the table name are references to the *DSD_ID* values stored in the *ARTEFACT* table. 
- The table may not contain time dimension columns (PERIOD_SDMX,PERIOD_START,PERIOD_END) in case there is no such dimension defined in the DSD. 

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
||**DF_ID**<br/>ID of Dataflow from `[Management].[ARTEFACT]` table |int|False
||**PERIOD_SDMX**<br/>SDMX time as reported.|varchar(30)|True
||**PERIOD_START**<br/>The start of reported period as Date|datetime(2)|True
||**PERIOD_END**<br/>The end of reported period as a Date.|datetime(2)|True
||**DIM_{component id#1}**<br /> References the code list member of the code list (*CL_ID*) defined by *{component id#1}* in *COMPONENT* table for coded dimensions.<br />Non-coded dimensions use nvarchar type with a length defined using its Representation.TextFormat.MaxLength property with default 4000|int/nvarchar(Representation.TextFormat.MaxLength (default 4000))|True
...|...|...|...		
||**DIM_{component id#n}**<br /> References the code list member of the code list (*CL_ID*) defined by *{component id#n}* in *COMPONENT* table for coded dimensions.<br />Non-coded dimensions use nvarchar type with a length defined using its Representation.TextFormat.MaxLength property with default 4000|int/nvarchar(Representation.TextFormat.MaxLength (default 4000))|True
||**COMP_{component id#1}**<br /> '1' if Component present in Delete instruction|char(1)|True
...|...|...|...	
||**COMP_{component id#n}**<br /> '1' if Component present in Delete instruction|char(1)|True
||**VALUE**<br/> '1' if Value is present in DELETE instruction |char(1)|True
||**LAST_UPDATED**<br/>The time the data row was crated|datetime|False

> #### [Data].[Fact_{dsd id}_{table version}_DELETED] 
> - This table has been removed in version 7.2.0. 
> - It was previously used to store deleted observations

#### [Data].[ATTR_{dsd id}_{table version}_DF] 
- Table containing the values of dataset level attributes of a DSD. 
- The value of a dataset attribute can be different for each of the dataflows defined on the same DSD. 
- When the [ARTEFACT] table's column 'KEEP_HISTORY' is set to '1'  for a given DSD, then the corresponding [Data].[ATTR_{dsd id}_{table version}_DF] uses the MSSQL feature 'Temporal Tables' [See more](https://learn.microsoft.com/en-us/sql/relational-databases/tables/temporal-tables?view=sql-server-ver16)
  -  In this case a secondary table [Data].[ATTR_{dsd id}_{table version}_DF_History] will hold the historical changes for the dataset level attributes. 

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
| |**DF_ID**<br /> ID of the referenced dataflow item in ARTEFACT table.|Int|False
| |**COMP_{component id#1}**<br /> The actual value of the dataset level attribute defined by {component id#1} in COMPONENT table.|Coded attributes: Int Non-coded attributes: as defined by related COMPONENT.ENUM_ID column.|Depends on related COMPONENT.ATT_STATUS column:<br />True for conditional, False for Mandatory attributes.
| |**...**<br />||
| |**COMP_{component id#n}**<br /> The actual value of the dataset level attribute defined by {component id#n} in COMPONENT table.| Coded attributes: Int Non-coded attributes: as defined by realted COMPONENT.ENUM_ID column.|Depends on related COMPONENT.ATT_STATUS column:<br />True for conditional,False for Mandatory attributes.
| |**LAST_UPDATED**<br /> The time the data row was crated or updated the last time. This column is used to filter data when the SDMX data query contains the updatedAfter parameter.|Datetime|False

#### [Data].[ATTR_{dsd id}_{table version}]
- Contains the dimension and group level attribute values belonging to a DSD defined by {dsd id}. 
- Table has a row for each coordinate in a Fact table, where there is at least 1 series/group level attribute. 
- When the [ARTEFACT] table's column 'KEEP_HISTORY' is set to '1'  for a given DSD, then the corresponding [Data].[ATTR_{dsd id}_{table version}] uses the MSSQL feature 'Temporal Tables' [See more](https://learn.microsoft.com/en-us/sql/relational-databases/tables/temporal-tables?view=sql-server-ver16)
  -  In this case a secondary table [Data].[ATTR_{dsd id}_{table version}_History] will hold the historical changes for the dimension and group level attributes. 


KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
PK|**SID**<br /> Unique, surrogate key generated on the *FILT* table|False
| |**COMP_{component id#1}**<br /> The actual value of the series level attribute defined by {component id#1} in COMPONENT table.|Coded attributes: Int Non-coded attributes: as defined by COMPONENT.ENUM_ID column.|True
| |…||
| |**COMP_{component id#n}**<br /> The actual value of the series level attribute defined by {component id#n} in COMPONENT table.|	Coded attributes: Int Non-coded attributes: as defined by COMPONENT.ENUM_ID column.|True
| |**LAST_UPDATED**<br /> The time the data row was crated or updated the last time. This column is used to filter data when the SDMX data query contains the updatedAfter parameter.|Datetime|False

> ### `[Data].[ATTR_{dsd id}_{table version}_DIMGROUP] `
> This table has been removed in version 4.0.0.


#### [Data].[META_DSD_{dsd id}_{table version}] 
- The table contains the metadata attributes belonging to a particular DSD. The *{dsd_id}* and *{table version}* (A or B) attributes in the table name are references to the *DSD_ID* values stored in the *ARTEFACT* table. 
- The table may not contain time dimension columns (PERIOD_SDMX,PERIOD_START,PERIOD_END) in case there is no such dimension defined in the DSD. 
- The *PRIMARY KEY* is composed by all dimension columns or *ROW_ID*. 

> - If the DSD contains less or exactly 30 dimensions (excluding time dimension), then the uniqueness is enforced by having a unique index on all DIM_{component id} columns and the PERIOD_START, PERIOD_END columns.
> - If the DSD has more than 30 non-time dimensions, a unique ROW_ID column is created. **[See Appendix 1 – Binary unique row ID generation](#appendix-1-binary-unique-row-id-generation)** for further details.

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
PK|**DIM_{component id#1}**<br /> References the code list member of the code list (*CL_ID*) defined by *{component id#1}* in *COMPONENT* table for coded dimensions.<br />Non-coded dimensions use nvarchar type with a length defined using its Representation.TextFormat.MaxLength property with default 4000|int/nvarchar(Representation.TextFormat.MaxLength (default 4000))|False
PK|...|...|...        
PK|**DIM_{component id#n}**<br /> References the code list member of the code list (*CL_ID*) defined by *{component id#n}* in *COMPONENT* table for coded dimensions.<br />Non-coded dimensions use nvarchar type with a length defined using its Representation.TextFormat.MaxLength property with default 4000|int/nvarchar(Representation.TextFormat.MaxLength (default 4000))|False
PK|**PERIOD_START**<br /> The start of reported period as Date. When the time dimension is not referenced, the max sql date/datetime allowed is used instead.|Date/datetime|False
PK|**PERIOD_END**<br /> The end of reported period as a Date. When the time dimension is not referenced, the min sql date/datetime allowed is used instead.|Date/datetime|False
||**PERIOD_SDMX**<br /> SDMX time as reported. When the time dimension is not referenced, a NULL value is used instead.|Varchar(10)|True
||**COMP_{component id#1}**<br /> The actual value of the metadata attribute defined by {component id#1} in COMPONENT table.|Coded metadata attributes: Int Non-coded attributes: as defined by related COMPONENT.ENUM_ID column.|True.
||**...**<br />||
||**COMP_{component id#n}**<br /> The actual value of the metadata attribute defined by {component id#n} in COMPONENT table.| Coded metadata attributes: Int Non-coded attributes: as defined by realted COMPONENT.ENUM_ID column.|True.
||**LAST_UPDATED**<br /> The time the data row was crated or updated the last time. This column is used to filter data when the SDMX data query contains the updatedAfter parameter.|Datetime|False

##### Indexes
Index|Columns|Type|Purpose
---|---|---|---
PK_Meta_{dsd id}_{table version}|DIM_{component id#1}, ..., DIM_{component id#n},PERIOD_START,PERIOD_END|Unique -Clustered|Guarantees uniqueness of dimensions combination (coordinate)

#### [Data].[META_DF_{DataFlow id}_{table version}] 
- The table contains the metadata attributes belonging to a particular DataFlow. The *{dataFlow_id}* and *{table version}* (A or B) attributes in the table name are references to the *DataFlow_ID* values stored in the *ARTEFACT* table. 
- The table may not contain time dimension columns (PERIOD_SDMX,PERIOD_START,PERIOD_END) in case there is no such dimension defined in the DSD. 
- The *PRIMARY KEY* is composed by all dimension columns or *ROW_ID*. 

> - If the DSD contains less or exactly 30 dimensions (excluding time dimension), then the uniqueness is enforced by having a unique index on all DIM_{component id} columns and the PERIOD_START, PERIOD_END columns.
> - If the DSD has more than 30 non-time dimensions, a unique ROW_ID column is created. **[See Appendix 1 – Binary unique row ID generation](#appendix-1-binary-unique-row-id-generation)** for further details.

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
PK|**DIM_{component id#1}**<br /> References the code list member of the code list (*CL_ID*) defined by *{component id#1}* in *COMPONENT* table for coded dimensions.<br />Non-coded dimensions use nvarchar type with a length defined using its Representation.TextFormat.MaxLength property with default 4000|int/nvarchar(Representation.TextFormat.MaxLength (default 4000))|False
PK|...|...|...        
PK|**DIM_{component id#n}**<br /> References the code list member of the code list (*CL_ID*) defined by *{component id#n}* in *COMPONENT* table for coded dimensions.<br />Non-coded dimensions use nvarchar type with a length defined using its Representation.TextFormat.MaxLength property with default 4000|int/nvarchar(Representation.TextFormat.MaxLength (default 4000))|False
PK|**PERIOD_START**<br /> The start of reported period as Date. When the time dimension is not referenced, the max sql date/datetime allowed is used instead.|Date/datetime|False
PK|**PERIOD_END**<br /> The end of reported period as a Date. When the time dimension is not referenced, the min sql date/datetime allowed is used instead.|Date/datetime|False
||**PERIOD_SDMX**<br /> SDMX time as reported. When the time dimension is not referenced, a NULL value is used instead.|Varchar(10)|True
||**COMP_{component id#1}**<br /> The actual value of the metadata attribute defined by {component id#1} in COMPONENT table.|Coded metadata attributes: Int Non-coded attributes: as defined by related COMPONENT.ENUM_ID column.|True.
||**...**<br />||
||**COMP_{component id#n}**<br /> The actual value of the metadata attribute defined by {component id#n} in COMPONENT table.| Coded metadata attributes: Int Non-coded attributes: as defined by realted COMPONENT.ENUM_ID column.|True.
||**LAST_UPDATED**<br /> The time the data row was crated or updated the last time. This column is used to filter data when the SDMX data query contains the updatedAfter parameter.|Datetime|False

##### Indexes
Index|Columns|Type|Purpose
---|---|---|---
PK_Meta_DF_{dataflow id}_{table version}|DIM_{component id#1}, ..., DIM_{component id#n},PERIOD_START,PERIOD_END|Unique -Clustered|Guarantees uniqueness of dimensions combination (coordinate)


#### [Data].[META_DS_{dsd id}_{table version}] 
- Table containing the values of dataset level metadata attributes of imported/transfered with DSD or DataFlow reference. 
- The value of a dataset metadata attribute can be different for each of the dataflows defined on the same DSD. 

KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
|PK|**DF_ID**<br /> ID of the referenced dataflow item in ARTEFACT table. -1 when it belongs to a DSD|Int|False
| |**COMP_{metadata attribute component id#1}**<br /> The actual value of the dataset level metadata attribute defined by {metadata attribute component id#1} in METADATA_ATTRIBUTE table.|Coded metadata attributes: Int Non-coded metadata attributes: as defined by related METADATA_ATTRIBUTE.ENUM_ID column.|True.
| |**...**<br />||
| |**COMP_{metadata attribute component id#n}**<br /> The actual value of the dataset level metadata attribute defined by {metadata attribute component id#n} in METADATA_ATTRIBUTE table.|Coded metadata attributes: Int Non-coded metadata attributes: as defined by realted METADATA_ATTRIBUTE.ENUM_ID column.|True.
||**LAST_UPDATED**<br /> The time the data row was crated or updated the last time. This column is used to filter data when the SDMX data query contains the updatedAfter parameter.|Datetime|False

#### [Data].[META_{dsd id}_{table version}_DF_DELETED]
- This table has been removed in version 7.3.0. 
-  It was previously used to store the reference to the deleted values of dataset level metadata attributes of a DSD. 

#### [Data].[META_{dsd id}_{table version}_DELETED]
- This table has been removed in version 7.3.0.
- It was previously used to store deleted referential metadata.

#### [Data].[DELETED_META_{dsd id}_{table version}]
- The table contains the delete instructions for referential metadata import DELETE operations, used during by the UpdatedAfter feature.
- It stores the deleted instructions for both cases when the import references a dataflow or a data structure. 
- The *{dsd_id}* and *{table version}* (A or B) atributes in the table name are references to the *DSD_ID* values stored in the *ARTEFACT* table. 
> - The table may not contain time dimension columns (PERIOD_SDMX,PERIOD_START,PERIOD_END) in case there is no such dimension defined in the DSD. 


KEY|NAME|DATA TYPE|ALLOW NULLS
---|---|---|---
||**DF_ID**<br/>ID of Dataflow from `[Management].[ARTEFACT]` table, -1 when it belongs to a DSD|int|False
|PK|**DIM_{component id#1}**<br /> References the code list member of the code list (*CL_ID*) defined by *{component id#1}* in *COMPONENT* table for coded dimensions.<br />Non-coded dimensions use nvarchar type with a length defined using its Representation.TextFormat.MaxLength property with default 4000|int/nvarchar(Representation.TextFormat.MaxLength (default 4000))|False
...|...|...|...        
|PK|**DIM_{component id#n}**<br /> References the code list member of the code list (*CL_ID*) defined by *{component id#n}* in *COMPONENT* table for coded dimensions.<br />Non-coded dimensions use nvarchar type with a length defined using its Representation.TextFormat.MaxLength property with default 4000|int/nvarchar(Representation.TextFormat.MaxLength (default 4000))|False
|PK|**PERIOD_SDMX**<br /> SDMX time as reported.|Varchar(20)|True
|PK|**PERIOD_START**<br /> The start of reported period as Date|Date|False
|PK|**PERIOD_END**<br /> The end of reported period as a Date.|Date|False
||**LAST_UPDATED**<br /> The time the metadata row was deleted. This column is used to filter metadata when the SDMX data query contains the updatedAfter parameter.|Datetime|False
||**COMP_{component id#1}**<br /> '1' if Component present in Delete instruction|char(1)|True
...|...|...|...    
||**COMP_{component id#n}**<br /> '1' if Component present in Delete instruction|char(1)|True
||**LAST_UPDATED**<br/>The time the data row was crated|datetime|False

##### Indexes
Index|Columns|Type|Purpose
---|---|---|---
I_DELETED_{dsd id}_{table version}|[DF_ID],[LAST_UPDATED]||Non-Unique - Non-Clustered Row Stored|Performance for UpdatedAfter queries. 


#### [Data].[VI_CurrentDataDsd_{dsd id}_{table version}]
- This view represents the SDMX flat representation of the DSD components along with the primary measure. 
- The values for the dimensions and attributes are represented in SDMX codes/values and not in internal codes/values, which are used in the majority of the tables.

- This view is a join of the following tables:
  - [Data].[FACT_{dsd id}_{table version}]
  - [Data].[FILT_{dsd id}]
  - [Data].[ATTR_{dsd id}_{table version}]
  - [Management].[CL_{internal codelist ID}]

> - The columns of this view are the concept name of each component in the DSD except the Dataset level attributes. 
> - Dataset level atributes are included (extension) in the dataflows views [Data].[VI_CurrentDataDataFlow_{dataflow id}_{table version}]


#### [Data].[VI_CurrentDataDataFlow_{dataflow id}_{table version}]
- This view represents the SDMX flat representation of the base DSD components along with the primary measure. The values for the dimensions and attributes are represented in SDMX codes/values and not in internal codes/values, which are used in the majority of the tables.
- This view is an extension of the view [Data].[VI_CurrentDataDsd_{dsd id}_{table version}], by including the dataset level attributes, concerned to this particular dataflow.

- This view is a join of the following tables/views:
  - [Data].[VI_CurrentDataDsd_{dsd id}_{table version}]
  - [Data].[ATTR_{dataflow id}_{table version}_DF]
  - [Management].[CL_{internal codelist ID}]
 
#### [Data].[VI_CurrentDataReplaceDataFlow_{dataflow id}_{table version}]
- This view is used to determine which components have changed since a given date (updatedAfter).
- This view contains the union of all the components, stored in different levels. 
- The values for the dimensions and attributes are represented in SDMX codes/values and not in internal codes/values, which are used in the majority of the tables.

- This view is a join of the following tables:
  - [Data].[FACT_{dsd id}_{table version}]
  - [Data].[FILT_{dsd id}]
  - [Data].[ATTR_{dsd id}_{table version}]
  - [Data].[ATTR_{dsd id}_{table version}_DF]
  - [Management].[CL_{internal codelist ID}]
  
  
#### [Data].[VI_DataDataFlow_{dataflow id}_{table version}_IncludeHistory]
- This view is used to determine all the historical changes of the measures and attributes.
- This view contains the union of all the components, stored in different levels. 
- The values for the dimensions and attributes are represented in SDMX codes/values and not in internal codes/values, which are used in the majority of the tables.
- This view is only present only when the dataspace has been configured to 'KeepHistory', indicated in the column [KEEP_HISTORY] of the [ARTEFACT] table for the given DSD. 

- This view is a join of the following tables:
  - [Data].[FACT_{dsd id}_{table version}]
  - [Data].[FACT_{dsd id}_{table version}_History]
  - [Data].[FILT_{dsd id}]
  - [Data].[ATTR_{dsd id}_{table version}]
  - [Data].[ATTR_{dsd id}_{table version}_History]
  - [Data].[ATTR_{dsd id}_{table version}_DF]
  - [Data].[ATTR_{dsd id}_{table version}_DF_History]
  - [Management].[CL_{internal codelist ID}]
  
> - It includes two columns [LAST_UPDATED] and [ValidTo] Which represent the period of time when these values were valid.

#### [Data].[VI_DeletedDataDataFlow_{dataflow id}_{table version}]
- This view contains all the historical delete instructions for all components, along with the DataFlow ID, which was used when the import was processed.
- This view contains the dataflow ID, all dimensions, measure, all attributes and the date when it was imported.
- For measure and attributes, the values are 1/'1' to indicate that the component was meant to be deleted, or null to indicate that it should be omited during the delition. 
- Dimensions are represented in SDMX values.

- This view is a join of the following tables:
  - [Data].[DELETED_{Dsd_ID}_{table version}]
  - [Management].[CL_{internal codelist ID}]

#### [Data].[VI_MetadataDsd_{dsd id}_{table version}]
- This view represents the SDMX flat representation of the MSD metadata attributes along with the DSD dimensions, imported using a data structure reference. The values for the dimensions and metadata attributes are represented in SDMX codes/values and not in internal codes/values, which are used in the majority of the tables.

- This view is a join of the following tables:
  - [Data].[META_DSD_{dsd id}_{table version}]
  - [Management].[CL_{internal codelist ID}]

> - The columns of this view are the concept name of each component in the DSD, including the Dataset level metadata attributes. 

#### [Data].[VI_MetadataDataFlow_{dataflow id}_{table version}]
- This view represents the SDMX flat representation of the MSD metadata attributes along with the DSD dimensions, imported using a dataFlow reference. The values for the dimensions and metadata attributes are represented in SDMX codes/values and not in internal codes/values, which are used in the majority of the tables.
- This view is a join of the following tables:
  - [Data].[META_DF_{dataFlow id}_{table version}]
  - [Management].[CL_{internal codelist ID}]

> - The columns of this view are the concept name of each component in the DSD, including the Dataset level metadata attributes. 

#### [Data].[VI_DeletedMetadataDsd_{dsd id}_{table version}]
- This view contains all the historical delete instructions for all the referential metadata components, imported using a data structure reference.
- The values for the dimensions and metadata attributes are represented in SDMX codes/values and not in internal codes/values, which are used in the majority of the tables.

- This view is a join of the following tables:
  - [Data].[DELETED_META_{dsd id}_{table version}]
  - [Management].[CL_{internal codelist ID}]

> - The columns of this view are the concept name of all the component in the DSD used to import the deletions.


#### [Data].[VI_DeletedMetadataDataFlow_{dataflow id}_{table version}]
- This view contains all the historical delete instructions for all the referential metadata components, along with the DataFlow ID, which was used when the import was processed.
- The values for the dimensions and metadata attributes are represented in SDMX codes/values and not in internal codes/values, which are used in the majority of the tables.

- This view is a join of the following tables:
  - [Data].[DELETED_META_{dsd id}_{table version}]
  - [Management].[CL_{internal codelist ID}]

> - The columns of this view are the concept name of all the component in the DSD and the dataflow used to import the deletions.

## Appendix 1 – Binary unique row ID generation ##

### High level overview ###

The proposal is to use BINARY data type for creation of unique row ID at each of the rows using the dimension IDs. The idea is simple. To generate a unique id for a data coordinate: 
1.	Convert each of the dimension IDs to a fixed length binary data
2.	Concatenate these binary results into one single binary data. Do this always in the same order.
As long dimension IDs are integer typed, they can easily transformed into fixed-length binary data. Doing so the length of the key would be
key length = ( number of dimensions ) * (size of the fixed-length binary data)
As of now, it seems that a 3-byte long binary data would be sufficient, allowing to use 16 777 216 distinct dimension IDs. 
### Example ###
Dimension <text>&num;</text>1 ID: **1 600 000**<br />
Dimension <text>&num;</text>2 ID: **32100**<br />
Dimension <text>&num;</text>3 ID: **16 000 000**<br />
Binary version of Dimension <text>&num;</text>1 ID: **0x186A00**<br />
Binary version of Dimension <text>&num;</text>2 ID: **0x007D64**<br />
Binary version of Dimension <text>&num;</text>3 ID: **0xF42400**<br />
Unique row ID: **0x186A00007D64F42400**<br />

### Advantages of Binary row id generation ###
* Easy to calculate
* Irreversible: easy to get the original coordinate information from the row id
* Extendible model. Using 3 bytes for one dimension key it allows more than 16 million dimension IDs, but if needed can be easily extended to 4 bytes (4 294 967 296 distinct values)
* The key size depends on the number of dimensions. Datasets with low number of dimension will have short keys, saving disk space.
* No chance of collision. In this case collision means that two different inputs (i.e. two different coordinates) produce the same output (row ID).
* With the usage of fixed length binary data, there is no need to use any separator “character”. First dimension id starts at the 1st byte, the 2nd id starts at the 4th, the 3rd id at the 7th, etc.
* The same dimension member ID can be used at a different dimension.
* Binary data can be used as a primary key (i.e. unique index) on a database table
* When dimension ID-s start from 1, 0 can represent the NULL value. By applying this approach the row ID could be used as unique ID in tables where components have partial keys, e.g. in dimension/group attribute table.

### Comparison to the solution using SHA2 512 hash algorithm ###
* Result of SHA2 512 has a fixed size of 64 bytes (512 bits). This means that at all datasets the table would containing a 64-byte long ID, no matter how many dimensions are defined in a dataset. The size of the unique key depends on the number of the dimensions used in the dataset at the Binary solution. The size of the ID is 3 * (number of dimensions). This also means that in most of the cases the size of the ID would be significantly smaller compared to the hash based version, up to 21 dimensions.
* However the risk of collision is extremely low at the hash function, it is still not 0 (by construction). With the Binary key construction, the there is no chance for collision. 

### Comparison to the solution using prime numbers ###
* The prime based solution suggests usage of prime numbers as dimension IDs. The unique row ID would be in this case the product of the dimension IDs. The 10 000th prime number is 104729, the 1 000 000th prime number is 15 485 863.  
* Complication with storing the prime based ID: The first prime number that needs 3 bytes 65 537, which is the 6543rd prime number. This means that with relatively large number of dimensions (and dimension members at the dimensions) it is quite easy to get a product of the prime dimension IDs (as row ID) that we cannot store in integer or decimal data types. The boundaries of the largest integer type, bigint  -2E63 to 2E63-1 (8 bytes) while for the decimal it is (-10E38 + 1 to 10E39 – 1). Using the Binary solution there are no barriers at all.
* With the prime based solution all dimension IDs must be unique. The same ID cannot be used at another dimension. This is not the case with the Binary key version.
* Primes are relatively complex to find after a while. There is no such issue with the Binary version.
* Almost one way function: it can be extremly complex to get the original coordinates from a row ID.

## Appendix 2 – ENUMERATIONS table content ##


ENUM_ID|ENUM_NAME|ENUM_VALUE|ENUM_VALUE_DATATYPE
---|---|---|---
1|DataType|String|String
2|DataType|BigInteger|String
3|DataType|Integer|String
4|DataType|Long|String
5|DataType|Short|String
6|DataType|Decimal|String
7|DataType|Float|String
8|DataType|Double|String
9|DataType|Boolean|String
10|DataType|DateTime|String
11|DataType|Time|String
12|DataType|Date|String
13|DataType|Year|String
14|DataType|Month|String
15|DataType|Day|String
16|DataType|MonthDay|String
17|DataType|YearMonth|String
18|DataType|Duration|String
19|DataType|Timespan|String
20|DataType|URI|String
21|DataType|Count|String
22|DataType|InclusiveValueRange|String
23|DataType|ExclusiveValueRange|String
24|DataType|Incremental|String
25|DataType|ObservationalTimePeriod|String
26|DataType|Base64Binary|String
27|DataType|Alpha|String
28|DataType|AlphaNumeric|String
29|DataType|Numeric|String
30|DataType|StandardTimePeriod|String
31|DataType|BasicTimePeriod|String
32|DataType|GregorianTimePeriod|String
33|DataType|GregorianYear|String
34|DataType|GregorianYearMonth|String
35|DataType|GregorianDay|String
36|DataType|ReportingTimePeriod|String
37|DataType|ReportingYear|String
38|DataType|ReportingSemester|String
39|DataType|ReportingTrimester|String
40|DataType|ReportingQuarter|String
41|DataType|ReportingMonth|String
42|DataType|ReportingWeek|String
43|DataType|ReportingDay|String
44|DataType|TimeRange|String
45|DataType|XHTML|String
46|DataType|KeyValues|String
47|DataType|IdentifiableReference|String
48|DataType|DataSetReference|String
49|DataType|AttachmentConstraintReference|String
50|FacetType|isSequence|Boolean
51|FacetType|isInclusive|Boolean
52|FacetType|minLength|Integer
53|FacetType|maxLength|Integer
54|FacetType|minValue|String
55|FacetType|maxValue|String
56|FacetType|startValue|String
57|FacetType|endValue|String
58|FacetType|decimals|Integer
59|FacetType|interval|Double
60|FacetType|timeInterval|Duration
61|FacetType|pattern|String
62|FacetType|enumeration|ItemScheme
63|FacetType|startTime|String
64|FacetType|endTime|String
65|FacetType|isMultiLingual|Boolean

## Appendix 3 – Supported SDMX representations (possible types) for observation values

### TextFormat representation

### *Supported* textType attributes
| SDMX - SimpleDataType   | Description                                                                                                                                                                                                                                                                                                                                                                                              | SQL data type | Remarks                                                                                                                                                                                                            |
|-------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| String                  | A string datatype corresponding to W3C XML Schema's [xs:string](https://www.w3.org/TR/xmlschema11-2/#string) datatype (default)                                                                                                                                                                                                                                                                          | NVARCHAR(n)   | Size of NVARCHAR column id defined by maxLength facet attribute. In case maxLength is not provided or exceeds 4000 then SQL type is NVARCHAR(MAX).                                                                 |
| Alpha                   | A string datatype which only allows for the simple alphabetic character set of A-Z, a-z.<br />Based on W3C XML Schema's [xs:string](https://www.w3.org/TR/xmlschema11-2/#string) datatype.                                                                                                                                                                                                               | NVARCHAR(n)   | Size of NVARCHAR column id defined by maxLength facet attribute. In case maxLength is not provided or exceeds 4000 then SQL type is NVARCHAR(MAX).                                                                 |
| AlphaNumeric            | A string datatype which only allows for the simple alphabetic character set of A-Z, a-z plus the simple numeric character set of 0-9.<br />Based on W3C XML Schema's [xs:string](https://www.w3.org/TR/xmlschema11-2/#string) datatype.                                                                                                                                                                  | NVARCHAR(n)   | Size of NVARCHAR column id defined by maxLength facet attribute. In case maxLength is not provided or exceeds 4000 then SQL type is NVARCHAR(MAX).                                                                 |
| Numeric                 | A string datatype which only allows for the simple numeric character set of 0-9. <br />This format is not treated as an integer, and therefore can having leading zeros.<br>Based on W3C XML Schema's [xs:string](https://www.w3.org/TR/xmlschema11-2/#string) datatype.                                                                                                                                 | VARCHAR(n)    | Size of VARCHAR column id defined by maxLength facet attribute. In case maxLength is not provided or exceeds 8000 then SQL type is VARCHAR(MAX).                                                                   |
| BigInteger              | An integer datatype corresponding to W3C XML Schema's [xs:integer](https://www.w3.org/TR/2012/REC-xmlschema11-2-20120405/datatypes.html#integer) datatype.                                                                                                                                                                                                                                               | VARCHAR(n)    | Size of VARCHAR column id defined by maxLength facet attribute. In case maxLength is not provided or exceeds 8000 then SQL type is VARCHAR(MAX).                                                                   |
| Integer                 | An integer datatype corresponding to W3C XML Schema's [xs:int](https://www.w3.org/TR/2012/REC-xmlschema11-2-20120405/datatypes.html#int) datatype.                                                                                                                                                                                                                                                       | INT           | SQL limit: -2^31 (-2,147,483,648) to 2^31-1 (2,147,483,647) which is identical to limits of  xs:int datatype                                                                                                       |
| Long                    | A numeric datatype corresponding to W3C XML Schema's [xs:long](https://www.w3.org/TR/2012/REC-xmlschema11-2-20120405/datatypes.html#long) datatype.                                                                                                                                                                                                                                                      | BIGINT        | SQL limit: -2^63 (-9,223,372,036,854,775,808) to 2^63-1 (9,223,372,036,854,775,807) which is identical to limits of  xs:long datatype                                                                              |
| Short                   | A numeric datatype corresponding to W3C XML Schema's [xs:short](https://www.w3.org/TR/2012/REC-xmlschema11-2-20120405/datatypes.html#short) datatype.                                                                                                                                                                                                                                                    | SMALLINT      | SQL limit: -2^15 (-32,768) to 2^15-1 (32,767) which is identical to limits of xs:short datatype                                                                                                                    |
| Decimal                 | A numeric datatype corresponding to W3C XML Schema's [xs:decimal](https://www.w3.org/TR/xmlschema11-2/#decimal) datatype.                                                                                                                                                                                                                                                                                | VARCHAR(n)    | Size of VARCHAR column id defined by maxLength facet attribute. In case maxLength is not provided or exceeds 8000 then SQL type is VARCHAR(MAX).                                                                   |
| Float                   | A numeric datatype corresponding to W3C XML Schema's [xs:float](https://www.w3.org/TR/xmlschema11-2/#float) datatype.                                                                                                                                                                                                                                                                                    | FLOAT(24)     | SQL limit: -3.40E + 38 to -1.18E-38, 0 and 1.18E-38 to 3.40E+38,<br />no support for special values: +0 (positiveZero), -0 (negativeZero), INF (positiveInfinity), -INF (negativeInfinity), and NaN (notANumber)   |
| Double                  | A numeric datatype corresponding to W3C XML Schema's [xs:double](https://www.w3.org/TR/xmlschema11-2/#double) datatype.                                                                                                                                                                                                                                                                                  | FLOAT(53)     | SQL limit: -1.79E+308 to -2.23E-308, 0 and 2.23E-308 to 1.79E+308,<br />no support for special values: +0 (positiveZero), -0 (negativeZero), INF (positiveInfinity), -INF (negativeInfinity), and NaN (notANumber) |
| Boolean                 | A datatype corresponding to W3C XML Schema's [xs:boolean](https://www.w3.org/TR/xmlschema11-2/#boolean) datatype.                                                                                                                                                                                                                                                                                        | SHORT         | In data messages boolean observation values are displayed as 0 or 1.                                                                                                                                               |
| URI                     | A datatype corresponding to W3C XML Schema's [xs:anyURI](https://www.w3.org/TR/xmlschema11-2/#anyURI) datatype.                                                                                                                                                                                                                                                                                          | NVARCHAR(n)   | Size of NVARCHAR column id defined by maxLength facet attribute. In case maxLength is not provided or exceeds 4000 then SQL type is NVARCHAR(MAX).                                                                 |
| Count                   | A simple incrementing Integer type. The isSequence and interval facets are ignored for this type.                                                                                                                                                                                                                                                                                                        | INT           | SQL limit: -2^31 (-2,147,483,648) to 2^31-1 (2,147,483,647) which is identical to limits of  xs:int datatype                                                                                                       |
| ObservationalTimePeriod | [Observational time period](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L232) is a superset of all time periods in SDMX.<br />It is the union of the standard time periods (i.e. Gregorian time periods, the reporting time periods, and date time) and time range.                                                                                                    | VARCHAR(100)  |                                                                                                                                                                                                                    |
| StandardTimePeriod      | [Standard time period](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L239) is a superset of distinct time periods in SDMX.<br />It is the union of the basic time periods (i.e. the Gregorian time periods and date time) and the reporting time periods.                                                                                                                | VARCHAR(100)  |                                                                                                                                                                                                                    |
| BasicTimePeriod         | [BasicTimePeriod time period](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L246) is a superset of the Gregorian time periods and a date time.                                                                                                                                                                                                                           | VARCHAR(100)  |                                                                                                                                                                                                                    |
| GregorianTimePeriod     | [Gregorian time period](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L253) corresponds to calendar periods and are represented in ISO-8601 formats.<br />This is the union of the year, year month, and date formats.                                                                                                                                                   | VARCHAR(100)  |                                                                                                                                                                                                                    |
| GregorianYear           | A Gregorian time period corresponding to W3C XML Schema's [xs:gYear](https://www.w3.org/TR/xmlschema11-2/#gYear) datatype, which is based on ISO-8601.                                                                                                                                                                                                                                                   | VARCHAR(100)  | Sample: 2014Z                                                                                                                                                                                                      |
| GregorianYearMonth      | A time datatype corresponding to W3C XML Schema's [xs:gYearMonth](https://www.w3.org/TR/xmlschema11-2/#gYearMonth) datatype, which is based on ISO-8601.                                                                                                                                                                                                                                                 | VARCHAR(100)  | Sample: 2014-09Z                                                                                                                                                                                                   |
| GregorianDay            | A time datatype corresponding to W3C XML Schema's [xs:date](https://www.w3.org/TR/xmlschema11-2/#date) datatype, which is based on ISO-8601.                                                                                                                                                                                                                                                             | VARCHAR(100)  | Sample: 2014-09-12Z                                                                                                                                                                                                |
| ReportingTimePeriod     | [Reporting time period](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L260) represents periods of a standard length within a reporting year,<br />where the start of the year (defined as a month and day) must be defined elsewhere or it is assumed to be January 1.<br />This is the union of the reporting year, semester, trimester, quarter, month, week, and day. | VARCHAR(100)  |                                                                                                                                                                                                                    |
| ReportingYear           | A reporting year represents a period of 1 year (P1Y) from the start date of the reporting year. <br />This is expressed as using the SDMX specific [ReportingYearType](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L287).                                                                                                                                              | VARCHAR(100)  | Sample: 2014-A1                                                                                                                                                                                                    |
| ReportingSemester       | A reporting semester represents a period of 6 months (P6M) from the start date of the reporting year. <br />This is expressed as using the SDMX specific [ReportingSemesterType](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L296).                                                                                                                                    | VARCHAR(100)  | Sample: 2014-S1                                                                                                                                                                                                    |
| ReportingTrimester      | A reporting trimester represents a period of 4 months (P4M) from the start date of the reporting year. <br />This is expressed as using the SDMX specific [ReportingTrimesterType](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L305).                                                                                                                                  | VARCHAR(100)  | Sample: 2014-T2                                                                                                                                                                                                    |
| ReportingQuarter        | A reporting quarter represents a period of 3 months (P3M) from the start date of the reporting year. <br />This is expressed as using the SDMX specific [ReportingQuarterType](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L314).                                                                                                                                      | VARCHAR(100)  | Sample: 2014-Q4                                                                                                                                                                                                    |
| ReportingMonth          | A reporting month represents a period of 1 month (P1M) from the start date of the reporting year. <br />This is expressed as using the SDMX specific [ReportingMonthType](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L323).                                                                                                                                           | VARCHAR(100)  | Sample: 2014-M09                                                                                                                                                                                                   |
| ReportingWeek           | A reporting week represents a period of 7 days (P7D) from the start date of the reporting year. <br />This is expressed as using the SDMX specific [ReportingWeekType](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L332).                                                                                                                                              | VARCHAR(100)  | Sample: 2014-W23                                                                                                                                                                                                   |
| ReportingDay            | A reporting day represents a period of 1 day (P1D) from the start date of the reporting year. <br />This is expressed as using the SDMX specific [ReportingDayType](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L341).                                                                                                                                                 | VARCHAR(100)  | Sample: 2014-D227                                                                                                                                                                                                  |
| DateTime                | A time datatype corresponding to W3C XML Schema's [xs:dateTime](https://www.w3.org/TR/xmlschema11-2/#dateTime) datatype.                                                                                                                                                                                                                                                                                 | VARCHAR(100)  | Sample: 2014-09-12T14:26:37Z                                                                                                                                                                                       |
| TimeRange               | [TimeRange](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L411) defines a time period by providing a distinct start (date or date time) and a duration.                                                                                                                                                                                                                  | VARCHAR(100)  | Sample: 2014-09-12T14:26:37Z/P1Y2M3DT4H5M6.7S                                                                                                                                                                      |
| Month                   | A time datatype corresponding to W3C XML Schema's [xs:gMonth](https://www.w3.org/TR/xmlschema11-2/#gMonth) datatype.                                                                                                                                                                                                                                                                                     | VARCHAR(100)  | Sample: --09Z                                                                                                                                                                                                      |
| MonthDay                | A time datatype corresponding to W3C XML Schema's [xs:gMonthDay](https://www.w3.org/TR/xmlschema11-2/#gMonthDay) datatype.                                                                                                                                                                                                                                                                               | VARCHAR(100)  | Sample: --09-12Z                                                                                                                                                                                                   |
| Day                     | A time datatype corresponding to W3C XML Schema's [xs:gDay](https://www.w3.org/TR/xmlschema11-2/#gDay) datatype.                                                                                                                                                                                                                                                                                         | VARCHAR(100)  | Sample: ---12Z                                                                                                                                                                                                     |
| Time                    | A time datatype corresponding to W3C XML Schema's [xs:time](https://www.w3.org/TR/xmlschema11-2/#time) datatype.                                                                                                                                                                                                                                                                                         | VARCHAR(100)  | Sample: 14:26.37Z                                                                                                                                                                                                  |
| Duration                | A time datatype corresponding to W3C XML Schema's [xs:duration](https://www.w3.org/TR/xmlschema11-2/#duration) datatype.                                                                                                                                                                                                                                                                                 | VARCHAR(100)  | Sample: -P1Y2M3DT4H5M6.7S.                                                                                                                                                                                         |

### *Not supported* textType attributes
|SDMX - SimpleDataType|Description|
|---------------------|-----------|
|InclusiveValueRange|This value indicates that the startValue and endValue attributes provide the inclusive boundaries of a numeric range of type [xs:decimal](https://www.w3.org/TR/xmlschema11-2/#decimal).|
|ExclusiveValueRange|This value indicates that the startValue and endValue attributes provide the exclusive boundaries of a numeric range, of type [xs:decimal](https://www.w3.org/TR/xmlschema11-2/#decimal).|
|Incremental|This value indicates that the value increments according to the value provided in the interval facet, and has a true value for the isSequence facet.|

#### Example
```xml
<!-- At least 1 character and max. 50,  must containt at least 1 '0' and must start with a capital letter (A to Z) -->
<structure:TextFormat textType="String" minLength="1" maxLength="50" pattern="^[A-Z].*[0].*$"/>
```

#### Optional attributes *supported*:
The optional TextFormat attributes, which .Stat Suite supports, are:
- **minLength** type=*[xs:positiveInteger](https://www.w3.org/TR/2012/REC-xmlschema11-2-20120405/datatypes.html#positiveInteger)*: The minLength attribute specifies the minimum and length of the value in characters.
- **maxLength** type=*[xs:positiveInteger](https://www.w3.org/TR/2012/REC-xmlschema11-2-20120405/datatypes.html#positiveInteger)*: The maxLength attribute specifies the maximum length of the value in characters.
- **minValue** type=*[xs:integer](https://www.w3.org/TR/2012/REC-xmlschema11-2-20120405/datatypes.html#int)*: The minValue attribute is used for inclusive and exclusive ranges, indicating what the lower bound of the range is. If this is used with an inclusive range, a valid value will be greater than or equal to the value specified here. If the inclusive and exclusive data type is not specified (e.g. this facet is used with an integer data type), the value is assumed to be inclusive.
- **maxValue** type=*[xs:integer](https://www.w3.org/TR/2012/REC-xmlschema11-2-20120405/datatypes.html#int)*: The maxValue attribute is used for inclusive and exclusive ranges, indicating what the upper bound of the range is. If this is used with an inclusive range, a valid value will be less than or equal to the value specified here. If the inclusive and exclusive data type is not specified (e.g. this facet is used with an integer data type), the value is assumed to be inclusive.
- **pattern** type=*[xs:string]((https://www.w3.org/TR/xmlschema11-2/#string))*: The pattern attribute holds any regular expression permitted in the similar facet in W3C XML Schema.

#### Optional attributes *not supported*:
The optional TextFormat attributes, which .Stat Suite does *NOT* support, are:
- isSequence type=*[xs:boolean](https://www.w3.org/TR/xmlschema11-2/#boolean)*
- interval type=*[xs:integer](https://www.w3.org/TR/2012/REC-xmlschema11-2-20120405/datatypes.html#int)*
- startValue type=*[xs:integer](https://www.w3.org/TR/2012/REC-xmlschema11-2-20120405/datatypes.html#int)*
- endValue type=*[xs:integer](https://www.w3.org/TR/2012/REC-xmlschema11-2-20120405/datatypes.html#int)*
- timeInterval type=*[xs:duration](https://www.w3.org/TR/xmlschema11-2/#duration)*
- startTime type=*[common:StandardTimePeriodType](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L239)*
- endTime type=*[common:StandardTimePeriodType](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXCommon.xsd#L239)*

### Enumerated representation

|SDMX representation|SQL data type|Remarks|Description|
|---------------------|-------------|-------|-----------|
|Enumeration|VARCHAR(150)|For observation values the SDMX code is stored in database on the contrary to dimensions and coded attributes, where the id referencing the code in a CL_{N} table.|Enumeration references a codelist that enumerates the allowable values for this representation.| 

#### Example 
```xml
<structure:Enumeration><URN>urn:sdmx:org.sdmx.infomodel.codelist.Codelist=MYAGENCY:CL_OBS_VALUES(1.0)</URN></structure:Enumeration>
```

#### Optional attributes *not supported*:
- EnumerationFormat type=*[CodededTextFormatType](https://github.com/sdmx-twg/sdmx-ml/blob/import21files/schemas/SDMXStructureBase.xsd#L446)*
## Index maintenance and Optimization
Each dataspace specific database, contains a daily schedule job named `Daily DotStatSuiteCore_Data_\<dataspace\> Db Index Optimization`. 

This job uses a third party stored procedure named `dbo.IndexOptimize` for Index and Statistics Maintenance. [See more](https://ola.hallengren.com/) 

The job is scheduled by default to run daily at 1:00 am (host server time), with the following parameters. 

```sql
EXEC DotStatSuiteCore_Data_<dataspace>.dbo.IndexOptimize 
	@Databases = 'DotStatSuiteCore_Data_<dataspace>', 
	@FragmentationLow = NULL, 
	@FragmentationMedium = 'INDEX_REORGANIZE,INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE',
	@FragmentationHigh = 'INDEX_REBUILD_ONLINE,INDEX_REBUILD_OFFLINE',
	@FragmentationLevel1 = 5,
	@FragmentationLevel2 = 30,
	@UpdateStatistics = 'ALL',
	@OnlyModifiedStatistics = 'N'
```
For more details, please refeer to the [SQL Server Index and Statistics Maintenance documentation](https://ola.hallengren.com/sql-server-index-and-statistics-maintenance.html)
